/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable import/no-extraneous-dependencies, import/no-internal-modules */
import * as HtmlWebpackHarddiskPlugin from 'html-webpack-harddisk-plugin'
import * as HtmlWebpackPlugin from 'html-webpack-plugin'
import { LanguageMap } from 'intl-ts'
import { resolve } from 'path'
import { merge } from 'webpack-merge'

import { messages } from './src/locale'
import commonConfig, { paths } from './webpack.common'

const devServer: import('webpack-dev-server').Configuration = {
  publicPath: paths.public,
  contentBase: resolve(__dirname, 'public'),
  port: 9080,
  overlay: true,
  proxy: [
    {
      context: ['/api', '/ws', '/helpsheet'],
      target: 'https://localhost:8443',
      secure: false,
    },
  ],
}
const simplePreloaded: import('./src/context').DryContextType = {
  sdata: {
    serverInfo: {
      plugins: [],
    },
    captchaKey: '',
  },
  lang: {
    preferences: [],
  },
}
const preloaded =
  'window.__PRELOADED_SENT__=' +
  JSON.stringify(simplePreloaded).replace(/</g, '\\u003c') +
  '; window.__PRELOADED_SENT__.lang.languageMap=' +
  new LanguageMap(messages).toString()
const config = merge(commonConfig, {
  mode: 'development',
  output: {
    filename: paths.bundle + '.dev.js',
    chunkFilename: '[name].dev.js',
  },
  devtool: 'inline-source-map',
  devServer,
  plugins: [
    new HtmlWebpackPlugin({
      template: resolve(__dirname, '__tests__', 'index.ejs'),
      alwaysWriteToDisk: true,
      title: '@gecogvidanto/client-web client test',
      favicon: resolve(__dirname, 'src', 'images', 'g1-favicon.png'),
      meta: {
        viewport: 'width=device-width, initial-scale=1.0',
      },
      appMountId: 'root',
      preloaded,
    }),
    new HtmlWebpackHarddiskPlugin({
      outputPath: resolve(__dirname, 'public'),
    }),
  ],
})

module.exports = config
