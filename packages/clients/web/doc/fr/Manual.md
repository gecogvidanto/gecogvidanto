# Mode d'emploi

Ce mode d'emploi explique comment utiliser le client web de ĞecoĞvidanto. Le client web est une aide à l'animation de Ğeconomicus. Si vous l'utilisez, vous êtes censé déjà connaitre les [règles du jeu du Ğeconomicus](http://geconomicus.glibre.org/), qui ne seront pas abordées ici.

# Présentation de l'écran

L'écran de ĞecoĞvidanto se compose d'une zone principale, d'une barre de menu et d'une barre d'entête.

## Zone principale

La zone principale affiche des informations en fonction des actions en cours. Son contenu est détaillé avec la description de ces actions ci-dessous.

## Barre de menu

La barre de menu permet d'accéder aux différentes pages de l'application. Par défaut, elle contient le menu « Accueil » qui affiche la page principale du site et le menu « À propos » qui affiche des informations sur le serveur ĞecoĞvidanto. En fonction de la situation, cette barre peut s'enrichir d'autres options.

## Barre d'entête

La barre d'entête contient, à son extrémité gauche, un bouton permettant de basculer l'affichage du menu du mode réduit au mode complet. À son extrémité droite, elle contient, de gauche à droite :

- le nom de l'utilisateur connecté, le cas échéant ;
- le bouton du menu utilisateur (détaillé ci-dessous) ;
- le bouton de sélection de langue, qui peut être grisé si aucune extension linguistique n'est installée sur le serveur ;
- le bouton de fenêtre de notification, qui peut être grisé si aucune notification n'a été reçue — ce bouton permet d'afficher et éventuellement supprimer les dernières notifications reçues.

# Menu utilisateur

Le menu utilisateur, accessible depuis le bouton de la barre d'entête, permet de gérer les informations sur la personne utilisant actuellement le client web. Il se présente sous la forme d'une silhouette si personne n'est connecté, ou sous la forme de l'avatar de l'utilisateur connecté. Le menu contient quatre options, mais deux seulement sont affichés, différentes si l'on est connecté ou non.

## Se déconnecter

L'option « Se déconnecter » n'est disponible que pour un utilisateur connecté. Elle permet à celui-ci de terminer sa session.

## Profil

L'option « Profil » n'est disponible que pour un utilisateur connecté. Elle permet à cet utilisateur de modifier les informations le concernant.

## Se connecter

L'option « Se connecter » n'est disponible que lorsque personne n'est connecté. Elle permet à un maitre du jeu de se connecter à l'application en utilisant son adresse électronique et son mot de passe.

## S'inscrire

L'option « S'inscrire » n'est disponible que lorsque personne n'est connecté. Elle permet à un nouvel utilisateur de s'inscrite en tant que maitre du jeu.

L'inscription en tant que maitre du jeu nécessite une adresse électronique valide. Selon la configuration du serveur, celui-ci enverra un courriel à l'adresse spécifiée afin de la valider. Il en est de même à chaque modification de cette adresse électronique.

L'utilisateur devra donc attendre de recevoir ce courriel, puis suivre les instruction qu'il contient, avant de pouvoir se connecter à l'application.

L'adresse électronique ainsi que le nom d'utilisateur doivent être uniques. L'inscription sera refusée si ce n'est pas le cas.

Selon le paramétrage du serveur, l'inscription est protégée contre les robots par Google reCaptcha v3.

## Avatar

Les avatars des utilisateurs ne sont pas enregistrés dans l'application, mais récupérés sur le site [Gravatar](https://fr.gravatar.com) en fonction de l'adresse électronique utilisée. Si votre adresse est reconnue par le site _Gravatar_, l'avatar que vous y avez enregistré sera affiché. Si vous souhaitez vous créer un avatar, rendez-vous sur ce site et suivez les instructions qui y sont indiqués.

# Accueil

La page d'accueil, accessible depuis l'option de barre de menu « Accueil », présente une liste de toutes les parties enregistrées. Des couleurs différentes permettent de visualiser rapidement l'état des parties. Un bouton présent dans le coin en bas à droite permet de rafraichir la liste depuis le serveur. En cliquant sur l'une des parties de cette liste, cela permet d'en visualiser les détails.

# Visualiser une partie

Lorsque vous cliquez sur une partie depuis la page d'accueil, l'application affiche les détails de celle-ci. Au dessous d'informations générales (maitre du jeu, adresse, etc.) se trouvent des « tuiles » pouvant être dépilées.

La première tuile contient la liste de tous les joueurs de la partie. Si vous en êtes le maitre de jeu et êtes connecté, vous pouvez cliquer sur le nom des joueurs pour le changer.

Les tuiles suivantes contiennent chacune les statistiques de l'une des différentes manches. Un bouton permet d'afficher ou masquer les personnages non joueur (comme la banque dans une manche en monnaie-dette).

Sous les statistiques des manches se trouve un résumé. Celui-ci est un texte libre permettant d'enregistrer les impressions des joueurs ou tout autre information concernant la partie. Tant que la partie n'est pas fermée, ce champ est modifiable par le maitre du jeu de la partie ou par un administrateur.

Enfin, suite au résumé, se trouvent les différentes actions possible pour la partie. Ces actions dépendent de la personne connectée et de l'état de la partie. Dans une partie en cours, on trouvera en particulier des liens pour afficher les pages d'aide pour les valeurs et pour la monnaie.

## Pages d'aide

Les pages d'aide sont accessibles depuis l'affichage détaillé des parties. Contrairement aux autres pages, les pages d'aide (valeurs ou monnaie) sont des pages indépendantes établissants une connexion avec le serveur afin de se mettre à jour automatiquement. Ces pages sont prévues pour être affichées en plein écran durant la partie afin d'aider les joueurs. Elles peuvent éventuellement être affichées depuis un autre terminal que celui utilisé par le maitre du jeu.

# Créer une partie

Le bouton « Nouvelle partie » n'apparait que si la personne connectée a des droits suffisants. Il permet d'ouvrir une page pour créer une nouvelle partie.

Cette page permet d'entrer les différents paramètres de la partie. Il est ainsi possible d'indiquer le nom du lieu où la partie se déroule, ainsi que son adresse précise. Si la connexion est correcte, l'adresse est controlée et auto-complétée. Il est également possible de préciser le nombre de tours par manche et la durée d'un tour en minutes.

# Jouer

Après avoir créé une partie, on se retrouve automatiquement en mode d'animation. Il est également possible de rejoindre se mode en passant par la visualisation d'une partie, en utilisant le bouton d'action « Jouer la partie ».

Lorsque l'application est en mode « jeu » et si la taille de l'écran le permet, une barre d'aide s'ajoute à la droite de la zone principale. L'affichage de cette barre dépend de la phase de jeu actuelle, mais elle contient en général la liste des joueurs actifs avec un bouton permettant de les faire quitter le jeu. Notez que si un joueur quitte la partie, son départ sera définitif. S'il souhaite revenir dans la partie plus tard, cela ne pourra se faire que sous un autre nom. Le départ ainsi que l'arrivée de nouveaux joueurs peut également se contrôler tout au long de la partie depuis la barre de menu.

La partie, comme chaque manche, commence par un tour d'initialisation. Celui-ci consiste en un simple formulaire permettant d'exercer les actions spécifiques au système économique choisi. Il est parfaitement possible que ce formulaire soit totalement vide et ne consiste donc qu'en un simple bouton pour passer au début du premier tour.

## Préparation des feuilles d'aide

La première phase affichée au début de chaque tour est un récapitulatif permettant de mettre à jour les feuilles d'aide. Cette mise à jour est nécessaire si vous n'utilisez pas les pages d'aide que l'on peut obtenir depuis la visualisation de la partie.

Lors de cette phase, la barre d'aide affiche le chronomètre avec le temps total d'un tour.

## Tour

La phase suivante est le tour proprement dit. C'est à ce moment là que les joueur vont faire des échange et tenter de créer des valeurs. Lors de cette phase, le chronomètre s'affiche en grand sur l'écran, tandis que la barre d'aide contient un rappel des feuilles d'aide.

Le tour peut s'écouler normalement (on assiste alors au décompte du chronomètre), ou être mis en pause (le chronomètre ne change plus et l'affichage du temps restant clignote). La mise en pause peut s'avérer intéressante lorsque, par exemple, il est nécessaire d'interrompre le jeu pour préciser un point de règle important, ou pour exécuter une action liée au système économique qui prendrait du temps. Notez que lorsque cette phase débute, le chronomètre est automatiquement en pause. Il bascule également automatiquement en pause en cas de rupture technologique, mais pas lors de l'affichage d'un formulaire d'action du système économique. C'est le bouton « Démarrer » / « Pause » qui permet de passer d'un état à l'autre.

Durant cette phase, il est possible d'exécuter les actions prévues par le système économique ou d'indiquer une rupture technologique. Cela se fait par l'entrée « Action » de la barre de menu.

Lorsque le décompte est terminé, une sonnerie se fait entendre pour signaler la fin du tour aux joueurs.

## Formulaires

Les formulaires s'affichent dans deux cas :

- soit lorsque l'on sélectionne une action spécifique au système économique ;
- soit en début et fin de tour.

Les formulaires contiennent des champs classés par joueurs, rangés dans des « tuiles ». Il peut également éventuellement y avoir un bloc supplémentaire au dessus de la première tuile qui contient des champs ou informations générales (non liées à un joueur).

Lorsqu'une action est requise pour un joueur, sa tuile est initialement déroulée et son nom est affiché en rouge, à la fois sur le titre de la tuile et dans la barre d'aide.

### Action spécifique

Lorsqu'une action spécifique au système économique est demandée, le formulaire correspondant s'affiche dans la zone principale, tandis que la barre d'aide affiche le chronomètre (avec un bouton pour mettre ou sortir de pause), la liste des joueurs ainsi qu'un rappel des feuilles d'aide.

### Bilan inter-tour

Après avoir validé la fin du tour, on se retrouve sur un formulaire permettant de faire le bilan inter-tour. Ce formulaire permet d'indiquer le bilan pour les personnes mortes à ce tour (ou celles qui quittent la partie, ou encore tous les joueurs actifs en fin de manche). Il peut également y avoir d'autres informations à donner en fonction du système économique. De même que pour le tour d'initialisation, il est possible que le formulaire soit totalement vide.

# Fin de manche

Lorsqu'une manche est terminée, l'application bascule en mode de visualisation de la partie. On peut ainsi regarder les statistiques de la manche qui vient de s'écouler ou démarrer une nouvelle manche. Il est également possible, si l'on a déjà joué au moins deux manches, de déclarer la partie close et ainsi en empêcher toute modification ultérieure. Il est toutefois conseillé de bien remplir le résumé avant de fermer la partie.
