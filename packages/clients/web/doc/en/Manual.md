# Manual

This manual describes the usage of the ĞecoĞvidanto web client. The web client is a help for mastering Ğeconomicus games. If you use it, you are supposed to already know the [rules of the Ğeconomicus game](http://geconomicus.glibre.org/), which will not be explained here.

# Aspect of the screen

The ĞecoĞvidanto screen consists of a main area, a menu bar and a header bar.

## Main area

The main area displays information based on the actions in progress. Its content is detailed with the description of these actions below.

## Menu bar

The menu bar provides access to the different pages of the application. By default, it contains the “Home” menu that displays the main page of the site and the “About” menu that displays information about the ĞecoĞvidanto server. Depending on the situation, this bar can be enriched with other options.

## Header bar

The header bar contains a button at the left end to toggle the menu bar from reduced display mode to full display mode. At its right end, it contains, from left to right:

- the name of the logged in user, if any;
- the user menu button (detailed below);
- the language selection button, which may be grayed out if no language plugin is installed on the server;
- the notification window button, which may be grayed if no notification was received — this button can be used to display and possibly delete the lastest received notifications.

# User menu

The user menu, accessible from the button of the header bar, allows to manage the information on the person currently using the web client. It is in the form of a silhouette if no one is logged in, or in the form of the avatar of the logged in user. The menu contains four options, but only two are displayed, different if you are logged in or not.

## Sign out

The “Sign out” option is only available for a logged in user. It allows him to finish his session.

## Profile

The “Profile” option is only available for a logged in user. It allows this user to modify information about him.

## Sign in

The “Sign in” option is only available when no one is logged in. It allows a game master to log in the application using his email address and password.

## Sign up

The “Sign up” option is only available when no one is logged in. It allows a new user to register as a game master.

Registration as a game master requires a valid email address. Depending on the server configuration, the server will send an email to the specified address to validate it. The same applies to each modification of this email address.

The user will have to wait to receive this email, then follow the instructions it contains, before being able to log in to the application.

The email address and username must be unique. Registration will be refused if this is not the case.

Depending on the server settings, the registration is protected against robots by Google reCaptcha v3.

## Avatar

Users' avatars are not saved in the application, but retrieved from the [Gravatar](https://fr.gravatar.com) website depending on the email address used. If your address is recognized by the site _Gravatar_, the avatar you have saved will be displayed. If you want to create an avatar, go to this site and follow the instructions therein.

# Home

The home page, accessed from the “Home” menu bar option, displays a list of all saved games. Different colors make it possible to quickly visualize the state of the games. A button in the bottom right corner refreshes the list from the server. Clicking on one of the games of this list, shows the details.

# View a game

When you click on a game from the home page, the application displays details of it. Below general information (game master, address, etc.) are “tiles” that can be expanded.

The first tile contains the list of all players in the game. If you are the game master and are logged in, you can click on the name of the players to change it.

Each following tile contains statistics from one of the different sets. A button allows you to show or hide non-player characters (like the bank in a debt-money set).

Below the set statistics is a summary. This is a free text to record player impressions or other information about the game. As long as the game is not closed, this field can be modified by the game master or by an administrator.

Finally, following the summary, are the different actions possible for the game. These actions depend on the logged in person and the state of the game. For a game in progress, you will find links to display the help pages for the values ​​and the currency.

## Help pages

The help pages are accessible from the detailed view of the games. Unlike other pages, help pages (values ​​or currency) are independent pages establishing a connection with the server to update automatically. These pages are intended to be displayed in full screen during the game to help players. They may possibly be displayed from another terminal than the one used by the game master.

# Create a game

The “New game” button only appears if the logged in person has sufficient rights. It opens a page to create a new game.

This page allows you to enter the different parameters of the game. It is thus possible to indicate the name of the place where the game takes place, as well as its precise address. If the connection is correct, the address is checked and auto-completed. It is also possible to specify the number of rounds per set and the duration of a round in minutes.

# Play

After creating a game, the application automatically switches to animation mode. It is also possible to join this mode by game view, using the "Play the game" action button.

When the application is in “playing” mode and the screen is large enough, a help bar is added to the right of the main area. The display of this bar depends on the current game phase, but it usually contains the list of active players with a button to make them leave the game. Note that if a player leaves the game, his departure will be final. If he wishes to return to the game later, this can only be done with another name. The departure as well as the arrival of new players can also be controlled throughout the game from the menu bar.

The game, like each set, begins with an initialization round. This consists of a simple form allowing to do the specific actions of the chosen economic system. It is totally possible that this form is completely empty and is therefore only a simple button to go to the beginning of the first round.

## Preparing Help Sheets

The first phase displayed at the beginning of each round is a help for updating the help sheets. This update is necessary if you do not use the help pages that can be obtained from the game view.

During this phase, the help bar displays the stopwatch with the total time of one round.

## Round

The next phase is the round itself. It is at this point that the players will make exchanges and try to create values. During this phase, the stopwatch is displayed in large on the screen, while the help bar contains a reminder of the help sheets.

The round may go on normally (the stopwatch is counting down) or paused (the timer no longer changes and the remaining time display flashes). Pausing can be interesting when, for example, it is necessary to interrupt the game to specify an important rule point, or to perform a time-consuming action related to the economic system. Note that when this phase starts, the stopwatch is automatically paused. It also automatically switches to pause in case of technological break, but not when displaying an action form of the economic system. It's the “Start” / “Pause” button that allows you to toggle from one state to another.

During this phase, it is possible to perform the actions planned by the economic system or to indicate a technological break. This is done through the “Action” entry of the menu bar.

When the countdown is complete, a ringing tone will sound to signal the end of the round to the players.

## Forms

The forms are displayed in two cases:

- when selecting an economic system specific action;
- at the beginning and end of each round.

The forms contain fields sorted by players, arranged in “tiles”. There may also be an extra block above the first tile that contains general (not related to a player) fields or information.

When an action is required for a player, its tile is initially expanded and its name is displayed in red, both on the title of the tile and in the help bar.

### Specific action

When a specific action of the economic system is requested, the corresponding form is displayed in the main area, while the help bar displays the stopwatch (with a button to enter or leave pause), the list of players and a reminder of the help sheets.

### Inter-round summary

After validating the end of the round is displayed a form allowing to make the inter-turn summary. This form allows to indicate the summary for the people which died at this turn (or those who left the game, or all the active players at the end of the round). There may also be other information to give depending on the economic system. As for the initialization round, it is possible that the form is completely empty.

# End of set

When a round is over, the application switches to view mode of the game. We can look at the statistics of the set that has just finished or start a new set. It is also possible, if one has already played at least two sets, to declare the game closed and thus prevent any subsequent modification. It is however advisable to fill the summary before closing the game.
