/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable import/no-extraneous-dependencies */
import { Theme } from '@material-ui/core'
import { StylesProvider, ThemeProvider, createGenerateClassName } from '@material-ui/styles'
import { Queries, RenderOptions, RenderResult, queries, render } from '@testing-library/react'
import Intl, { LanguageMap } from 'intl-ts'
// eslint-disable-next-line import/no-internal-modules
import 'mobx-react-lite/batchingForReactDom'
import * as React from 'react'
import { FunctionComponent, useMemo } from 'react'
import { StaticRouter } from 'react-router-dom'

import { messages } from './locale'
import { StoreSet, StoreSetProvider, buildStoreSet } from './stores'
import { createTheme } from './themes'

// Re-export useful testing library items
export * from '@testing-library/react'
export { default as userEvent } from '@testing-library/user-event'

/**
 * The language map.
 */
const languageMap = new LanguageMap(messages, 'en')

/**
 * The component to be used as context for tests.
 */
/* (JSX element) */
const ContextProviders: FunctionComponent = ({ children }) => {
  const theme = useMemo<Theme>(createTheme, [])
  const storeSet = useMemo<StoreSet>(() => buildStoreSet({ plugins: [] }, '', languageMap, []), [])
  return (
    <StaticRouter>
      <StylesProvider generateClassName={createGenerateClassName()}>
        <ThemeProvider theme={theme}>
          <StoreSetProvider value={storeSet}>{children}</StoreSetProvider>
        </ThemeProvider>
      </StylesProvider>
    </StaticRouter>
  )
}

// Override the render method
interface CustomRenderOptions<Q extends Queries = typeof queries> extends RenderOptions<Q> {
  mute?: true
}
function customRender(ui: React.ReactElement, options?: Omit<CustomRenderOptions, 'queries'>): RenderResult
function customRender<Q extends Queries>(
  ui: React.ReactElement,
  options: CustomRenderOptions<Q>
): RenderResult<Q>
function customRender(
  ui: React.ReactElement,
  options?: CustomRenderOptions<any>
): ReturnType<typeof render> {
  const initialErrorChanel = console.error // eslint-disable-line no-console
  try {
    if (options && options.mute) {
      console.error = () => undefined // eslint-disable-line no-console
    }
    return render(ui, { wrapper: ContextProviders, ...options })
  } finally {
    console.error = initialErrorChanel // eslint-disable-line no-console
  }
}
export { customRender as render }
export const lang = new Intl(languageMap)
