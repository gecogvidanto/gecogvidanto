/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Theme, createMuiTheme } from '@material-ui/core'
import {
  amber,
  blue,
  blueGrey,
  cyan,
  green,
  grey,
  indigo,
  lime,
  pink,
  purple,
  yellow,
  // eslint-disable-next-line import/no-internal-modules
} from '@material-ui/core/colors'
// eslint-disable-next-line import/no-internal-modules
import { ThemeOptions } from '@material-ui/core/styles/createMuiTheme'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import ErrorIcon from '@material-ui/icons/Error'
import InfoIcon from '@material-ui/icons/Info'
import WarningIcon from '@material-ui/icons/Warning'
import isPlainObject from 'is-plain-object'
import type * as DeepMergeType from 'deepmerge'

import './GecoTheme'

// Workaround for Webpack which cannot import functions from CommonJS
// eslint-disable-next-line @typescript-eslint/no-var-requires
const deepMerge: typeof DeepMergeType = require('deepmerge')

export function createGecoTheme(options: ThemeOptions): Theme {
  const augmentedOptions: Theme = createMuiTheme(
    deepMerge<ThemeOptions>(
      {
        palette: {
          success: green,
          info: indigo,
          warning: amber,
          game: {
            empty: {
              main: blueGrey[200],
            },
            inProgress: {
              main: yellow[700],
            },
            finished: {
              main: cyan[500],
            },
            closeable: {
              main: indigo[600],
            },
            closed: {
              main: purple[700],
            },
          },
          chart: {
            grid: { main: grey[100] },
            score: blue,
            mean: pink,
            deviation: lime,
          },
        },
        gecogvidanto: {
          applicationName: 'ĞecoĞvidanto',
          menuWidth: 240,
          icons: {
            success: CheckCircleIcon,
            info: InfoIcon,
            warning: WarningIcon,
            error: ErrorIcon,
          },
          form: {
            style: {
              container: {
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                padding: '16px 24px 24px',
              },
              form: {
                width: '100%',
                marginTop: '8px',
              },
              submit: {
                marginTop: '24px',
              },
            },
            dialogWidth: 400,
            pageWidth: 800,
          },
        },
      },
      options,
      { isMergeableObject: isPlainObject }
    )
  )
  augmentedOptions.palette.success = augmentedOptions.palette.augmentColor(augmentedOptions.palette.success)
  augmentedOptions.palette.info = augmentedOptions.palette.augmentColor(augmentedOptions.palette.info)
  augmentedOptions.palette.warning = augmentedOptions.palette.augmentColor(augmentedOptions.palette.warning)
  augmentedOptions.palette.game.empty = augmentedOptions.palette.augmentColor(
    augmentedOptions.palette.game.empty
  )
  augmentedOptions.palette.game.inProgress = augmentedOptions.palette.augmentColor(
    augmentedOptions.palette.game.inProgress
  )
  augmentedOptions.palette.game.finished = augmentedOptions.palette.augmentColor(
    augmentedOptions.palette.game.finished
  )
  augmentedOptions.palette.game.closeable = augmentedOptions.palette.augmentColor(
    augmentedOptions.palette.game.closeable
  )
  augmentedOptions.palette.game.closed = augmentedOptions.palette.augmentColor(
    augmentedOptions.palette.game.closed
  )
  augmentedOptions.palette.chart.grid = augmentedOptions.palette.augmentColor(
    augmentedOptions.palette.chart.grid
  )
  augmentedOptions.palette.chart.score = augmentedOptions.palette.augmentColor(
    augmentedOptions.palette.chart.score
  )
  augmentedOptions.palette.chart.mean = augmentedOptions.palette.augmentColor(
    augmentedOptions.palette.chart.mean
  )
  augmentedOptions.palette.chart.deviation = augmentedOptions.palette.augmentColor(
    augmentedOptions.palette.chart.deviation
  )
  return augmentedOptions
}
