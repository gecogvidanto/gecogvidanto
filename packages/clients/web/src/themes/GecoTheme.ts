/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
// eslint-disable-next-line import/no-internal-modules
import { SvgIconProps } from '@material-ui/core/SvgIcon'
import { CSSProperties } from '@material-ui/styles'
import { ComponentType } from 'react'

declare module '@material-ui/core/styles/createPalette' {
  interface Palette {
    success: PaletteColor
    info: PaletteColor
    warning: PaletteColor
    game: {
      empty: PaletteColor
      inProgress: PaletteColor
      finished: PaletteColor
      closeable: PaletteColor
      closed: PaletteColor
    }
    chart: {
      grid: PaletteColor
      score: PaletteColor
      mean: PaletteColor
      deviation: PaletteColor
    }
  }

  interface PaletteOptions {
    success?: PaletteColorOptions
    info?: PaletteColorOptions
    warning?: PaletteColorOptions
    game?: {
      empty?: PaletteColorOptions
      inProgress?: PaletteColorOptions
      finished?: PaletteColorOptions
      closeable?: PaletteColorOptions
      closed?: PaletteColorOptions
    }
    chart?: {
      grid?: PaletteColorOptions
      score?: PaletteColorOptions
      mean?: PaletteColorOptions
      deviation?: PaletteColorOptions
    }
  }
}

declare module '@material-ui/core/styles/createMuiTheme' {
  interface Theme {
    gecogvidanto: {
      applicationName: string
      menuWidth: number
      icons: {
        success: ComponentType<SvgIconProps>
        info: ComponentType<SvgIconProps>
        warning: ComponentType<SvgIconProps>
        error: ComponentType<SvgIconProps>
      }
      form: {
        style: {
          container: CSSProperties
          form: CSSProperties
          submit: CSSProperties
        }
        dialogWidth: CSSProperties['width']
        pageWidth: CSSProperties['maxWidth']
      }
    }
  }

  // Allow configuration using “createMuiTheme”
  interface ThemeOptions {
    gecogvidanto?: {
      applicationName?: string
      menuWidth?: number
      icons?: {
        success?: ComponentType<SvgIconProps>
        info?: ComponentType<SvgIconProps>
        warning?: ComponentType<SvgIconProps>
        error?: ComponentType<SvgIconProps>
      }
      form?: {
        style?: {
          container?: CSSProperties
          form?: CSSProperties
          submit?: CSSProperties
        }
        dialogWidth?: CSSProperties['width']
        pageWidth?: CSSProperties['maxWidth']
      }
    }
  }
}
