/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { StylesProvider, createGenerateClassName } from '@material-ui/styles'
import { LanguageMap } from 'intl-ts'
import * as React from 'react'
import { FunctionComponent, useEffect } from 'react'
import { hydrate } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'

import App from './App'
import { StoreSet, buildStoreSet } from './stores'
import { createTheme } from './themes'
import { UiUrlAnalyzer } from './tools'

// Hot reload (dev only)
if (process.env.NODE_ENV !== 'production') {
  const isHotModule = (tested: any): tested is { hot: { accept: () => any } } => !!tested && !!tested.hot
  if (isHotModule(module)) {
    module.hot.accept()
  }
}

/**
 * Application theme.
 */
const theme = createTheme()

/**
 * Convert the value to a method returning it.
 *
 * @param value - The value to convert.
 * @returns The method.
 */
function valueToMethod<T>(value: T): () => T {
  return () => value
}

/**
 * The store of the application.
 */
const storeSet = (function (): StoreSet {
  window.__PRELOADED_STATE__ = window.__PRELOADED_SENT__ as any
  if (!window.__PRELOADED_STATE__) {
    throw new Error('Application was not properly initialized')
  }

  // Convert values into methods
  if (window.__PRELOADED_STATE__.user) {
    window.__PRELOADED_STATE__.user.read = valueToMethod(window.__PRELOADED_SENT__!.user!.read!)
    window.__PRELOADED_STATE__.user.checkEmail = valueToMethod(window.__PRELOADED_SENT__!.user!.checkEmail!)
  }
  if (window.__PRELOADED_STATE__.game) {
    window.__PRELOADED_STATE__.game.search = valueToMethod(window.__PRELOADED_SENT__!.game!.search!)
  }
  if (window.__PRELOADED_STATE__.economicSystems) {
    window.__PRELOADED_STATE__.economicSystems.search = valueToMethod(
      window.__PRELOADED_SENT__!.economicSystems!.search!
    )
  }

  // Delete sent values
  delete window.__PRELOADED_SENT__

  // Get preloaded values
  const { sdata, lang, user, game } = window.__PRELOADED_STATE__

  // Create context
  const { preferences, languageMap } = lang
  const loggedUser = user && user.logged
  const currentGame = game && game.current
  return buildStoreSet(
    sdata.serverInfo,
    sdata.captchaKey,
    new LanguageMap(languageMap),
    preferences,
    loggedUser,
    currentGame
  )
})()

/**
 * Base name for the application.
 */
const basename = new UiUrlAnalyzer(
  window.location.pathname,
  storeSet.langStore.lang.$languageMap.availables
).langUrl

/**
 * The root component for the client side rendering.
 */
/* (JSX element) */
const Root: FunctionComponent = () => {
  // Terminate initialisation when first called
  useEffect(() => {
    if ('__PRELOADED_STATE__' in window) {
      delete window.__PRELOADED_STATE__
    }
    const preloadContainer = document.getElementById('preload')
    preloadContainer && preloadContainer.remove()
    const jssContainer = document.getElementById('jss-server-side')
    jssContainer && jssContainer.remove()
  }, [])

  return (
    <BrowserRouter basename={basename}>
      <StylesProvider generateClassName={createGenerateClassName()}>
        <App theme={theme} storeSet={storeSet} />
      </StylesProvider>
    </BrowserRouter>
  )
}

// Hydrate
hydrate(<Root />, document.querySelector('#root'))
