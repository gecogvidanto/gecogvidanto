/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { useEffect, useMemo } from 'react'

import { EconomicSystem, UnassembledMessages } from '@gecogvidanto/shared'

import { useServerContext } from '../../context'
import { langType } from '../../locale'
import { api, preload } from '../../tools'
import useFetcher from '../Fetcher'
import useHookCache from '../HookCache'
import { context } from './context'

/**
 * Hook used to add economic system name in component. In a page where multiple component may want to query
 * economic system name, it is advised to wrap all in a `EconomicSystemNameCache`.
 *
 * @param id - The economic system identifier.
 * @returns The hook.
 */
export default function useEconomicSystemName(
  id: string
): UnassembledMessages<langType>[keyof langType] | undefined {
  // Look for preloaded
  const serverContext = useServerContext()
  const preloaded = useMemo(() => {
    const searchEconomicSystems = preload(serverContext, 'economicSystems', 'search')
    return searchEconomicSystems && searchEconomicSystems()
  }, [serverContext])

  // Manage cache
  const [element, addElements] = useHookCache(context, id, true)
  const [loaded] = useFetcher<EconomicSystem<langType>[]>(
    addElements && !preloaded ? api.economicSystem.search : undefined
  )
  const economicSystems = preloaded || loaded
  useEffect(() => {
    if (addElements && economicSystems) {
      addElements(
        economicSystems.map(economicSystem => ({
          identifier: economicSystem.id,
          element: economicSystem.name,
        }))
      )
    }
  }, [addElements, economicSystems])

  // Return result from cache
  return element
}
