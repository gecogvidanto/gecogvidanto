/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Reducer, useReducer } from 'react'

import ReducerSystem, { Action, ActionType, State, Worker } from './ReducerSystem'

/**
 * Interface for an object managing the workers depending on `globalLoad`.
 */
interface WorkerManager<W extends Worker> {
  hasWorker(stateWorker: W, identifier: string): boolean
  setWorker(stateWorker: W, worker: Record<string, unknown>, identifier: string): W
  isWorking(stateWorker: W, worker: Record<string, unknown>, identifier: string): boolean
  removeWorker(stateWorker: W, identifier: string): W
}

/**
 * Object managing the worker when `globalLoad` is true.
 */
const GLOBAL_WORKER: WorkerManager<Record<string, unknown> | undefined> = {
  hasWorker: (stateWorker: Record<string, unknown> | undefined) => !!stateWorker,
  setWorker: (_: Record<string, unknown> | undefined, worker: Record<string, unknown>) => worker,
  isWorking: (stateWorker: Record<string, unknown> | undefined, worker: Record<string, unknown>) =>
    stateWorker === worker,
  removeWorker: () => undefined,
}

/**
 * Object managing the worker when `globalLoad` is false.
 */
const SINGLE_WORKER: WorkerManager<{ [identifier: string]: Record<string, unknown> }> = {
  hasWorker: (stateWorker: { [identifier: string]: Record<string, unknown> }, identifier: string) =>
    !!stateWorker[identifier],
  setWorker: (
    stateWorker: { [identifier: string]: Record<string, unknown> },
    worker: Record<string, unknown>,
    identifier: string
  ) => ({
    ...stateWorker,
    [identifier]: worker,
  }),
  isWorking: (
    stateWorker: { [identifier: string]: Record<string, unknown> },
    worker: Record<string, unknown>,
    identifier: string
  ) => stateWorker[identifier] === worker,
  removeWorker: (stateWorker: { [identifier: string]: Record<string, unknown> }, identifier: string) =>
    Object.entries(stateWorker)
      .filter(([key]) => key !== identifier)
      .reduce((obj, [key, val]) => {
        obj[key] = val
        return obj
      }, {} as any),
}

/**
 * The cache reducer.
 *
 * @param state - The previous state.
 * @param action - The action to execute.
 * @param workerManager - The appropriate worker manager.
 * @returns The new state.
 */
function reducer<V, W extends Worker>(
  state: State<V, W>,
  action: Action<V>,
  workerManager: WorkerManager<W>
): State<V, W> {
  switch (action.type) {
    case ActionType.AddElement:
      return {
        cache: { ...state.cache, [action.payload.identifier]: action.payload.element },
        worker: workerManager.removeWorker(state.worker, action.payload.identifier),
      }
    case ActionType.StartWorking:
      if (!(action.payload.identifier in state.cache)) {
        if (!workerManager.hasWorker(state.worker, action.payload.identifier)) {
          return {
            cache: state.cache,
            worker: workerManager.setWorker(state.worker, action.payload.worker, action.payload.identifier),
          }
        }
      }
      break
    case ActionType.StopWorking:
      if (workerManager.isWorking(state.worker, action.payload.worker, action.payload.identifier)) {
        return {
          cache: state.cache,
          worker: workerManager.removeWorker(state.worker, action.payload.identifier),
        }
      }
      break
  }
  return state
}

function globalReducer<V>(
  state: State<V, Record<string, unknown> | undefined>,
  action: Action<V>
): State<V, Record<string, unknown> | undefined> {
  return reducer(state, action, GLOBAL_WORKER)
}

function singleReducer<V>(
  state: State<V, { [identifier: string]: Record<string, unknown> }>,
  action: Action<V>
): State<V, { [identifier: string]: Record<string, unknown> }> {
  return reducer(state, action, SINGLE_WORKER)
}

/**
 * Shortcut to create the appropriate reducer system.
 *
 * @param globalLoad - Indicate that the objects are loaded globally.
 * @returns The created reducer system.
 */
export default function useCacheReducer<V>(
  globalLoad: true
): ReducerSystem<V, Record<string, unknown> | undefined>

/**
 * Shortcut to create the appropriate reducer system.
 *
 * @param globalLoad - Indicate that the objects are loaded one at a time.
 * @returns The created reducer system.
 */
export default function useCacheReducer<V>(
  globalLoad: false
): ReducerSystem<V, { [identifier: string]: Record<string, unknown> }>

/*
 * Implementation.
 */
export default function useCacheReducer<V>(globalLoad: boolean): ReducerSystem<V, any> {
  return useReducer<Reducer<State<V, any>, Action<V>>>(globalLoad ? globalReducer : singleReducer, {
    cache: {},
    worker: globalLoad ? undefined : {},
  })
}
