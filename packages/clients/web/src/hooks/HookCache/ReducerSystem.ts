/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Dispatch } from 'react'

/**
 * The type used to store workers in the state.
 */
export type Worker =
  | { [identifier: string]: Record<string, unknown> }
  | (Record<string, unknown> | undefined)

/**
 * The state is composed of:
 * - a cache, which is a simple object with strings as keys and typed values;
 * - the workers currently loading elements.
 */
export interface State<V, W extends Worker> {
  cache: { [identifier: string]: V | undefined }
  worker: W
}

/**
 * Type of actions which can be applied to the state.
 */
export const enum ActionType {
  AddElement,
  StartWorking,
  StopWorking,
}

/**
 * The action used to add an element.
 */
interface AddElementAction<V> {
  type: ActionType.AddElement
  payload: {
    identifier: string
    element: V | undefined
  }
}

/**
 * Create an action used to add an element.
 *
 * @param (unnamed) - The element to add.
 * @param (unnamed).identifier - The element identifier.
 * @param (unnamed).element - The element to add.
 * @returns The appropriate action.
 */
export function addElement<V>({
  identifier,
  element,
}: {
  identifier: string
  element: V | undefined
}): AddElementAction<V> {
  return { type: ActionType.AddElement, payload: { identifier, element } }
}

/**
 * The action used to start working.
 */
interface StartWorkingAction {
  type: ActionType.StartWorking
  payload: {
    identifier: string
    worker: Record<string, unknown>
  }
}

/**
 * Create an action used to start working.
 *
 * @param identifier - The element identifier.
 * @param worker - Any object, used to uniquely identify the worker.
 * @returns The appropriate action.
 */
export function startWorking(identifier: string, worker: Record<string, unknown>): StartWorkingAction {
  return { type: ActionType.StartWorking, payload: { identifier, worker } }
}

/**
 * The action used to stop working.
 */
interface StopWorkingAction {
  type: ActionType.StopWorking
  payload: {
    identifier: string
    worker: Record<string, unknown>
  }
}

/**
 * Create an action used to stop working.
 *
 * @param identifier - The element identifier.
 * @param worker - The object used to uniquely identify the worker.
 * @returns The appropriate action.
 */
export function stopWorking(identifier: string, worker: Record<string, unknown>): StopWorkingAction {
  return { type: ActionType.StopWorking, payload: { identifier, worker } }
}

/**
 * The action expected by the reducer.
 */
export type Action<V> = AddElementAction<V> | StartWorkingAction | StopWorkingAction

/**
 * Alias for the useReducer return type.
 */
type ReducerSystem<V, W extends Worker> = [State<V, W>, Dispatch<Action<V>>]
export default ReducerSystem
