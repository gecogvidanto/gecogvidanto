/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Context, useCallback, useContext, useEffect, useMemo } from 'react'

import useCacheReducer from './CacheReducer'
import ReducerSystem, { addElement, startWorking, stopWorking } from './ReducerSystem'

/**
 * A hook which manages a cache.
 *
 * @param context - The context created with `createHookCache` (**must be consistent** across calls).
 * @param identifier - The identifier of the element to be extracted from the cache.
 * @param globalLoad - If false or absent, elements will be retrieved and loaded one by one in the cache
 * (**must be consistent** across calls).
 * @returns An array containing:
 *   - the searched value or undefined if not in cache yet,
 *   - the method to add the element in the cache, if the element can be loaded by this component, or
 *     undefined otherwise.
 */
function useHookCache<V>(
  context: Context<ReducerSystem<V, { [identifier: string]: Record<string, unknown> }> | undefined>,
  identifier: string,
  globalLoad?: false
): [V | undefined, ((element: V) => void) | undefined]

/**
 * A hook which manages a cache.
 *
 * @param context - The context created with `createHookCache` (**must be consistent** across calls).
 * @param identifier - The identifier of the element to be extracted from the cache.
 * @param globalLoad - If true, the loading process will load all elements at once (**must be consistent**
 * across calls).
 * @returns An array containing:
 *   - the searched value or undefined if not in cache yet,
 *   - the method to add all elements in the cache, if the elements can be loaded by this component, or
 *     undefined otherwise.
 */
function useHookCache<V>(
  context: Context<ReducerSystem<V, Record<string, unknown> | undefined> | undefined>,
  identifier: string,
  globleLoad: true
): [V | undefined, ((elements: Array<{ identifier: string; element: V }>) => void) | undefined]

/*
 * Implementation
 */
function useHookCache<V>(
  context: Context<ReducerSystem<V, any> | undefined>,
  identifier: string,
  globalLoad = false
): [V | undefined, ((elements: any) => void) | undefined] {
  // Search for cache to use
  const contextCache = useContext(context)
  const localCache = useCacheReducer<V>(globalLoad as any)
  const [state, dispatch] = contextCache || localCache

  // Search if master for the given identifier (or all if global)
  const workerId = useMemo(() => ({ identifier }), [identifier])
  useEffect(() => {
    dispatch(startWorking(identifier, workerId))
    return () => dispatch(stopWorking(identifier, workerId))
  })

  // Add elements to cache
  const addAllElements = useCallback(
    (elements: Array<{ identifier: string; element: V }>): void => {
      dispatch(addElement({ identifier, element: undefined }))
      elements.forEach(element => dispatch(addElement(element)))
    },
    [dispatch, identifier]
  )
  const addOneElement = useCallback((element: V): void => dispatch(addElement({ identifier, element })), [
    dispatch,
    identifier,
  ])

  // Return result
  const isMaster = workerId === (globalLoad ? state.worker : state.worker[identifier])
  const resultFunction = isMaster ? (globalLoad ? addAllElements : addOneElement) : undefined
  return [state.cache[identifier], resultFunction]
}

export default useHookCache
