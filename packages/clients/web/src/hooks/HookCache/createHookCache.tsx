/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as React from 'react'
import { Context, FunctionComponent, createContext } from 'react'

import useCacheReducer from './CacheReducer'
import ReducerSystem from './ReducerSystem'

/**
 * Create the reducer cache. This will create both hook context and hook cache provider.
 *
 * @param globalLoad - Indicate that the objects are loaded globally.
 * @returns The provider and context for the hook cache.
 */
export function createHookCache<V>(
  globalLoad: true
): [FunctionComponent, Context<ReducerSystem<V, Record<string, unknown> | undefined> | undefined>]

/**
 * Create the reducer cache. This will create both hook context and hook cache provider.
 *
 * @param globalLoad - Indicate that the objects are loaded one at a time.
 * @returns The provider and context for the hook cache.
 */
export function createHookCache<V>(
  globalLoad?: false
): [
  FunctionComponent,
  Context<ReducerSystem<V, { [identifier: string]: Record<string, unknown> }> | undefined>
]

/*
 * Implementation.
 */
export function createHookCache<V>(
  globalLoad = false
): [FunctionComponent, Context<ReducerSystem<V, any> | undefined>] {
  const context = createContext<ReducerSystem<V, any> | undefined>(undefined)
  const HookCache: FunctionComponent = ({ children }) => {
    return <context.Provider value={useCacheReducer<V>(globalLoad as any)}>{children}</context.Provider>
  }
  return [HookCache, context]
}
