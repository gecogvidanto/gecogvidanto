/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { MutableRefObject, useCallback, useEffect, useRef, useState } from 'react'

import { AsyncValidator, Validator, validate as valueValidate } from '@gecogvidanto/shared'

import { useLangStore } from '../../stores'
import useEventCallback from '../EventCallback'
import AsyncFieldValidator from './AsyncFieldValidator'

export interface ValidationEvent<T = Element> {
  valid: boolean
  target: T
}

function sameErrors(a: string[], b: string[]): boolean {
  return a.length === b.length && a.every((value, index) => value === b[index])
}

const enum Touched {
  Never,
  Modified,
  Checkable,
}

interface FieldElement extends Element {
  value: string
}

interface InputProps<T extends FieldElement> {
  validators?: Validator[]
  asyncValidators?: AsyncValidator[]
  onValidate?: (event: ValidationEvent<T>) => void
}

interface OutputProps<T extends FieldElement> {
  fieldRef: MutableRefObject<T | undefined>
  onChange: () => void
  onBlur: () => void
  errors: string[] | undefined
}

/**
 * Default values for the hook (must be the same object across calls).
 */
const noValidators: any[] = []

/**
 * The field validator can be used to validate the content of a field against given validators. The
 * validation event is sent at mount and update if the field is valid, so that it can be considered invalid
 * until event received. **Note** that the component using this hook must be an observer.
 *
 * @param (unnamed) - Validation parameters.
 * @param (unnamed).validators - The synchronous validators.
 * @param (unnamed).asyncValidators - The asynchronous validators.
 * @param (unnamed).onValidate - Function to call when valid state changes.
 * @returns The output properties.
 */
export default function useFieldValidator<T extends FieldElement>({
  validators = noValidators,
  asyncValidators = noValidators,
  onValidate,
}: InputProps<T>): OutputProps<T> {
  const { lang } = useLangStore()

  // State
  const [touched, setTouched] = useState<Touched>(Touched.Never)
  const [errors, setErrors] = useState<string[]>([])

  // Data
  const asyncFieldValidator = useRef<AsyncFieldValidator>()
  const field = useRef<T>()
  const lastSentState = useRef<string>()

  // Validation
  const sendValidation = useCallback(
    (target: T, newErrors: string[]): void => {
      const valid = newErrors.length === 0
      const sentState = valid ? target.value : undefined
      if (lastSentState.current !== sentState) {
        lastSentState.current = sentState
        onValidate && onValidate({ valid, target })
      }
      if (valid && touched === Touched.Modified) {
        setTouched(Touched.Checkable)
      }
      if (!sameErrors(errors, newErrors)) {
        setErrors(newErrors)
      }
    },
    [onValidate, touched, errors]
  )
  const validate = useCallback(async (): Promise<void> => {
    if (asyncFieldValidator.current && field.current) {
      let newErrors: string[] = []
      for (const validator of validators!) {
        const validError = valueValidate(field.current.value, validator, lang)
        validError && newErrors.push(validError)
      }
      try {
        if (newErrors.length === 0) {
          newErrors = await asyncFieldValidator.current.validate(field.current.value)
        }
        sendValidation(field.current, newErrors)
      } catch {
        // Skip
      }
    }
  }, [validators, sendValidation, lang])

  // Manage asynchronous validators
  useEffect(() => {
    asyncFieldValidator.current = new AsyncFieldValidator(lang, asyncValidators)
    return () => asyncFieldValidator.current && asyncFieldValidator.current.cancel()
  }, [asyncValidators, lang])

  // Manage validation
  useEffect(() => {
    validate()
  }, [validate])

  // Events
  const onChange = useEventCallback((): void => {
    setTouched(previous => (previous === Touched.Never ? Touched.Modified : previous))
    validate()
  })
  const onBlur = useEventCallback((): void => {
    setTouched(previous => (previous !== Touched.Checkable ? Touched.Checkable : previous))
  })

  // Calculate errors
  return {
    fieldRef: field,
    onChange,
    onBlur,
    errors: touched === Touched.Checkable && errors.length > 0 ? errors : undefined,
  }
}
