/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import Intl from 'intl-ts'
import { Deferred } from 'ts-deferred'

import { AsyncValidator, asyncValidate } from '@gecogvidanto/shared'

import { langType } from '../../locale'

/**
 * A validation request.
 */
interface ValidationRequest {
  value: string
  response: Deferred<string[]>
}

/**
 * Class making the asynchronous validations, with a system to prevent network or server overload.
 */
export default class AsyncFieldValidator {
  private readonly queue: ValidationRequest[] = []
  private busy = false
  private cancelled = false

  public constructor(
    private readonly lang: Intl<langType>,
    private readonly validators: AsyncValidator[]
  ) {}

  public validate(value: string): Promise<string[]> {
    if (this.cancelled) {
      return Promise.reject()
    }
    const response = new Deferred<string[]>()
    this.queue.push({ value, response })
    this.wakeUp()
    return response.promise
  }

  public cancel(): void {
    this.cancelled = true
    while (this.queue.length > 0) {
      this.queue.pop()
    }
  }

  private wakeUp(): void {
    // Start by cleaning the queue
    while (this.queue.length > 1) {
      const { response } = this.queue.shift()!
      response.reject()
    }

    // Wake up validation process if needed
    if (this.queue.length > 0 && !this.busy) {
      this.busy = true
      const request: ValidationRequest = this.queue.shift()!
      setTimeout(() => this.requestValidation(request))
    }
  }

  private async requestValidation(request: ValidationRequest): Promise<void> {
    try {
      const errors: Array<string | undefined> = await Promise.all(
        this.validators.map(validator => asyncValidate(request.value, validator, this.lang))
      )
      if (!this.cancelled && this.queue.length === 0) {
        request.response.resolve(errors.filter(error => !!error) as string[])
      } else {
        request.response.reject()
      }
    } catch {
      request.response.reject()
    } finally {
      this.busy = false
      this.wakeUp()
    }
  }
}
