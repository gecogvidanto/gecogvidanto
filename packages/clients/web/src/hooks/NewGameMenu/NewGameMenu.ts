/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import GameIcon from '@material-ui/icons/Category'
import { useEffect } from 'react'

import { CertLevel } from '@gecogvidanto/shared'

import { MenuEntryDescription, addMenuEntry, removeMenuEntry, useMenuAction } from '../../menu'
import { routes } from '../../routes'
import { useUserStore } from '../../stores'

const MENU_ENTRY: MenuEntryDescription = {
  icon: GameIcon,
  title: 'pageNewGame',
  action: routes.NEWGAME.build(),
}
/**
 * A hook adding the “new game” menu if possible. This hook must be used in an observable area.
 */
function useNewGameMenu(): void {
  const { logged } = useUserStore()
  const menuDispatch = useMenuAction()
  const userCanCreateGame = logged && [CertLevel.Administrator, CertLevel.GameMaster].includes(logged.level)
  useEffect(() => {
    const activeMenu = userCanCreateGame
    activeMenu && menuDispatch(addMenuEntry(MENU_ENTRY))
    return () => {
      activeMenu && menuDispatch(removeMenuEntry(MENU_ENTRY))
    }
  }, [menuDispatch, userCanCreateGame])
}
export default useNewGameMenu
