/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { useCallback, useEffect, useState } from 'react'

import { UnassembledMessages } from '@gecogvidanto/shared'

import { langType } from '../../locale'
import { useGameStore, useLangStore } from '../../stores'
import { api } from '../../tools'
import useFetcher from '../Fetcher'

/**
 * Hook searching for the name of a current game character. This hook must be used in observer component.
 *
 * @param roundSet - The round set where character must be searched.
 * @param playerId - Identification of the player behind the character (may be negative for NPC).
 * @returns The name of the character. A default value is provided until name is found.
 */
function useCharacterNames(roundSet: number, playerId: number): string

/**
 * Hook searching for the name of all characters of current game. This hook must be used in observer
 * component.
 *
 * @returns A function used to retrieve the name for each character. This method provides default values
 * until the true names are found.
 */
function useCharacterNames(): (roundSet: number, playerId: number) => string

/*
 * Implementation
 */
function useCharacterNames(
  roundSet?: number,
  playerId?: number
): string | ((roundSet: number, playerId: number) => string) {
  const { assemble } = useLangStore()
  const { neededGame: game } = useGameStore()

  // Non player character management
  const [searchedNames, setSearchedNames] = useState<
    Array<{ searchedRoundSet: number; searchedPlayerId: number }>
  >([])
  useEffect(
    () =>
      setSearchedNames(
        roundSet !== undefined && playerId !== undefined
          ? playerId < 0
            ? [{ searchedRoundSet: roundSet, searchedPlayerId: playerId }]
            : []
          : game.characters
              .filter(c => c.player < 0)
              .map(npc => ({ searchedRoundSet: npc.roundSet, searchedPlayerId: npc.player }))
      ),
    [game.characters, playerId, roundSet]
  )
  const loadNpc: () => Promise<
    Map<number, Map<number, UnassembledMessages<langType>[keyof langType]>>
  > = useCallback(async () => {
    const foundNames = await Promise.all(
      searchedNames.map(async ({ searchedRoundSet, searchedPlayerId }) => {
        let name: UnassembledMessages<langType>[keyof langType] | undefined
        try {
          name = await api.game.readNpcName<langType>(game, searchedRoundSet, searchedPlayerId)
        } catch {
          // Ignore errors
        }
        return { searchedRoundSet, searchedPlayerId, name }
      })
    )
    const result = new Map<number, Map<number, UnassembledMessages<langType>[keyof langType]>>()
    foundNames.forEach(({ searchedRoundSet, searchedPlayerId, name }) => {
      if (name) {
        let setNames: Map<number, UnassembledMessages<langType>[keyof langType]> | undefined = result.get(
          searchedRoundSet
        )
        if (!setNames) {
          setNames = new Map()
          result.set(searchedRoundSet, setNames)
        }
        setNames.set(searchedPlayerId, name)
      }
    })
    return result
  }, [game, searchedNames])
  const [npcNames] = useFetcher(searchedNames.length ? loadNpc : undefined)

  // Player name function
  const getPlayerName = useCallback(
    (searchedRoundSet: number, searchedPlayerId: number): string => {
      if (searchedPlayerId >= 0) {
        return game.players[searchedPlayerId].name
      } else {
        let npcName: UnassembledMessages<langType>[keyof langType] | undefined
        if (npcNames) {
          const setNames:
            | Map<number, UnassembledMessages<langType>[keyof langType]>
            | undefined = npcNames.get(searchedRoundSet)
          if (setNames) {
            npcName = setNames.get(searchedPlayerId)
          }
        }
        return npcName ? assemble(npcName) : `(${-searchedPlayerId})`
      }
    },
    [assemble, game.players, npcNames]
  )

  return roundSet !== undefined && playerId !== undefined
    ? getPlayerName(roundSet, playerId)
    : getPlayerName
}

export default useCharacterNames
