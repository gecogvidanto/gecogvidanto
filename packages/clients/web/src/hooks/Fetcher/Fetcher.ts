/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { useEffect, useState } from 'react'

import Status from './Status'

/**
 * Creates a hook used to fetch data.
 *
 * @param fetch - The fetching function. It will be a noop if the value may be undefined. This will prevent
 * from the need to put the hook in a conditional loop.
 * @returns The created hook.
 */
function useFetcher<T>(fetch: (() => Promise<T>) | undefined): [T | undefined, Status]

/**
 * Creates a hook used to fetch data.
 *
 * @param fetch - The fetching function. It will be a noop if the value may be undefined. This will prevent
 * from the need to put the hook in a conditional loop.
 * @param defaultValue - The value to return while not resolved.
 * @returns The created hook.
 */
function useFetcher<T>(fetch: (() => Promise<T>) | undefined, defaultValue: T): [T, Status]

/*
 * Implementation.
 */
function useFetcher<T>(fetch: (() => Promise<T>) | undefined, defaultValue?: T): [T | undefined, Status] {
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState<any>(undefined)
  const [result, setResult] = useState(defaultValue)

  useEffect(() => {
    let cancelled = false
    fetch &&
      (async () => {
        setLoading(true)
        try {
          const loaded: T | undefined = cancelled ? undefined : await fetch()
          cancelled || setResult(loaded)
        } catch (e) {
          cancelled || setError(e)
        } finally {
          cancelled || setLoading(false)
        }
      })()
    return () => {
      cancelled = true
      setLoading(false)
      setError(undefined)
      setResult(defaultValue)
    }
  }, [defaultValue, fetch])

  return [result, { loading, error }]
}

export default useFetcher
