/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { useCallback, useEffect } from 'react'

import { User } from '@gecogvidanto/shared'

import { api } from '../../tools'
import useFetcher from '../Fetcher'
import useHookCache from '../HookCache'
import { context } from './context'

/**
 * This hooks can be used to manage user data. In a page where multiple component may want to query user
 * data, it is advised to wrap all in a `UserDataCache`.
 *
 * @param id - The user identifier for which we need the data.
 * @returns The user data, or undefined if no data loaded yet.
 */
export default function useUserData(id: string): User | undefined {
  const [element, addElement] = useHookCache(context, id)
  const load = useCallback(async (): Promise<User | undefined> => {
    try {
      return api.user.read(id)
    } catch {
      return undefined
    }
  }, [id])
  const [loaded] = useFetcher<User | undefined>(addElement ? load : undefined)
  useEffect(() => {
    if (addElement && loaded) {
      addElement(loaded)
    }
  }, [addElement, loaded])
  return element
}
