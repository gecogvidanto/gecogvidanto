/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { useCallback, useEffect, useState } from 'react'

import { CaptchaAction } from '@gecogvidanto/shared'

import { loadScript } from '../../tools'

interface GoogleReCaptchaExecuteOptions {
  action: string
}

interface GoogleReCaptchaRenderOptions {
  sitekey: string
  size: 'invisible'
}

interface GoogleReCaptcha {
  ready: (cb: () => void) => void
  execute: (options: GoogleReCaptchaExecuteOptions) => Promise<string>
  render: (id: string, options: GoogleReCaptchaRenderOptions) => void
}

declare global {
  interface Window {
    grecaptcha: GoogleReCaptcha
  }
}

/**
 * Hook managing Google Re-Captcha v3.
 *
 * @param siteKey - The key of the site, or undefined to *not* use the captcha.
 * @param widgetId - The identifier of the widget element.
 * @param action - The captcha action.
 * @returns The getToken function or undefined while not ready.
 */
function useReCaptcha(
  siteKey: string | undefined,
  widgetId: string,
  action: CaptchaAction
): (() => Promise<string>) | undefined {
  // State
  const [ready, setReady] = useState<boolean>(false)

  // Token retrieval
  const getToken = useCallback((): Promise<string> => {
    if (!ready) {
      throw new Error('Captcha component is not ready')
    }
    return window.grecaptcha.execute({ action })
  }, [action, ready])

  // Manage library loading
  useEffect(() => {
    if (siteKey) {
      const disposePromise: Promise<() => void> = loadScript('https://www.google.com/recaptcha/api.js')
      disposePromise
        .then(() => {
          window.grecaptcha.ready(() => {
            setReady(true)
          })
        })
        .catch(() => setReady(false))
      return () => {
        disposePromise && disposePromise.then(dispose => dispose())
      }
    } else {
      return
    }
  }, [siteKey])

  // Manage widget
  useEffect(() => {
    if (siteKey && ready) {
      const widgetElement = document.createElement('div')
      widgetElement.id = widgetId
      const widget: HTMLDivElement = document.body.appendChild(widgetElement)
      window.grecaptcha.render(widgetId, {
        sitekey: siteKey,
        size: 'invisible',
      })
      return () => {
        document.body.removeChild(widget)
      }
    } else {
      return
    }
  }, [ready, siteKey, widgetId])

  // Return result
  return ready ? getToken : undefined
}
export default useReCaptcha
