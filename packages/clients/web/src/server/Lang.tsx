/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import * as React from 'react'
import { FunctionComponent } from 'react'

import { Alternate } from '../tools'

export interface LangProps {
  /**
   * URL for the default (no language) access.
   */
  defaultUrl: string

  /**
   * All the alternate URL for other provided languages.
   */
  alternates: Alternate[]
}

/**
 * The alternate link for all the languages.
 */
/* (JSX element) */
const Lang: FunctionComponent<LangProps> = ({ defaultUrl, alternates }) => (
  <>
    <link key="x-default" rel="alternate" href={defaultUrl} hrefLang="x-default" />
    {alternates.map(alternate => (
      <link key={alternate.lang} rel="alternate" href={alternate.url} hrefLang={alternate.lang} />
    ))}
  </>
)

export default Lang
