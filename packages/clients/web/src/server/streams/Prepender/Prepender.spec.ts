/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import { v2 as streamTest } from 'streamtest'

import Prepender from './Prepender'

describe('Prepender', function () {
  it('should prepend the given text to the stream', function () {
    const prepender = new Prepender('In the Age of Chaos')
    streamTest
      .fromChunks([
        ', ',
        'two factions battled for dominance. The Kingdom of Azeroth was a prosperous one. ',
        'The Humans who dwelled there turned the land into a paradise.',
      ])
      .pipe(prepender)
      .pipe(
        streamTest.toText((err, text) => {
          expect(err).to.not.exist
          expect(text).to.equal(
            'In the Age of Chaos, two factions battled for dominance. ' +
              'The Kingdom of Azeroth was a prosperous one. ' +
              'The Humans who dwelled there turned the land into a paradise.'
          )
        })
      )
  })
})
