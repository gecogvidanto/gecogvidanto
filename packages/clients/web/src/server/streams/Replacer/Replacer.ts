/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Readable, Transform, TransformCallback } from 'stream'

/**
 * A transform stream replacing one single place holder with either a string or a steam.
 */
export default class Replacer extends Transform {
  /**
   * The remaining to be output.
   */
  private remain = ''

  /**
   * Create the stream transform.
   *
   * @param placeholder - The place holder to replace with content.
   * @param replacement - The replacement to use.
   */
  public constructor(
    private placeholder: string,
    private readonly replacement: { toString(): string } | NodeJS.ReadableStream
  ) {
    super()
  }

  public _transform(chunk: unknown, encoding: BufferEncoding, done: TransformCallback): void {
    if (this.placeholder.length > 0) {
      const text = this.remain + String(chunk)
      this.remain = ''
      const foundIndex = text.indexOf(this.placeholder)
      if (foundIndex >= 0) {
        this.push(Buffer.from(text.slice(0, foundIndex)))
        this.remain = text.slice(foundIndex + this.placeholder.length)
        this.placeholder = ''
        return this.replace(done)
      } else {
        if (text.length >= this.placeholder.length) {
          this.push(Buffer.from(text.slice(0, text.length + 1 - this.placeholder.length)))
          this.remain = text.slice(text.length + 1 - this.placeholder.length)
        } else {
          this.remain = text
        }
      }
    } else {
      this.flushRemaining()
      this.push(chunk, encoding)
    }
    done()
  }

  public _flush(done: TransformCallback): void {
    this.flushRemaining()
    done()
  }

  /**
   * Replace the placeholder with the given content.
   *
   * @param done - The callback to execute when replacement is done.
   */
  private replace(done: TransformCallback): void {
    if (this.replacement instanceof Readable) {
      this.replacement.on('data', chunk => this.push(chunk))
      this.replacement.once('error', error => this.emit('error', error))
      this.replacement.once('end', done)
    } else {
      this.push(Buffer.from(this.replacement.toString()))
      done()
    }
  }

  /**
   * Flush the remaining data.
   */
  private flushRemaining(): void {
    if (this.remain.length > 0) {
      this.push(Buffer.from(this.remain))
      this.remain = ''
    }
  }
}
