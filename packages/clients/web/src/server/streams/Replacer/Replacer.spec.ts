/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import { v2 as streamTest } from 'streamtest'

import Replacer from './Replacer'

const beginningResult = 'Age of Chaos,Kingdom of Azeroth'
const endResult = 'Kingdom of AzerothAge of Chaos,'

describe('Replacer', function () {
  Object.entries({
    'in a single chunk': [
      'I',
      'n the',
      ' ###REPLACE-ME### ',
      'two factions battled for dominance. The Kingdom of Azeroth was a prosperous one. ',
      'The Humans who dwelled there turned the land into a paradise.',
    ],
    'split in multiple chunks': [
      'In the ###REPLACE',
      '-ME### ',
      'two',
      ' factions battled for ',
      'dominance. The Kingdom of Azeroth was a prosperous one. The',
      ' Humans who dwelled there turned the land into a paradise.',
    ],
    'split in small chunks': [
      'I',
      'n the ##',
      '#REP',
      'LACE-ME### ',
      'two factions battled f',
      'or dominance. The Kingdom of Azeroth was a pros',
      'perous one. The Humans who dwelled there turned ',
      'the land into a paradise.',
    ],
    'at the beginning': ['###REPLACE-ME###Kingdom of Azeroth'],
    'at the end': ['Kingdom of Azeroth###REPLACE-ME###'],
  }).forEach(([testSuiteName, chunks]) => {
    describe(`# with placeholder ${testSuiteName}`, function () {
      Object.entries({
        'not replace string with no placeholder': [
          new Replacer('not-exist', 'unused'),
          'In the ###REPLACE-ME### two factions battled for dominance. ' +
            'The Kingdom of Azeroth was a prosperous one. ' +
            'The Humans who dwelled there turned the land into a paradise.',
        ],
        'replace placeholder with string': [
          new Replacer('###REPLACE-ME###', 'Age of Chaos,'),
          'In the Age of Chaos, two factions battled for dominance. ' +
            'The Kingdom of Azeroth was a prosperous one. ' +
            'The Humans who dwelled there turned the land into a paradise.',
        ],
        'replace placeholder with stream': [
          new Replacer('###REPLACE-ME###', streamTest.fromChunks(['Age of', ' Chaos,'])),
          'In the Age of Chaos, two factions battled for dominance. ' +
            'The Kingdom of Azeroth was a prosperous one. ' +
            'The Humans who dwelled there turned the land into a paradise.',
        ],
      } as { [name: string]: [Replacer, string] }).forEach(([testName, [replacer, expectedResult]]) => {
        const begTest = testSuiteName.endsWith('beginning')
        const endTest = testSuiteName.endsWith('end')
        const realResult = begTest ? beginningResult : endTest ? endResult : expectedResult
        if (!((begTest || endTest) && testName.startsWith('not'))) {
          it(`should ${testName}`, async function () {
            const result = new Promise<string>((res, rej) =>
              streamTest
                .fromChunks(chunks)
                .pipe(replacer)
                .pipe(
                  streamTest.toText((err, text) => {
                    if (err) {
                      rej(err)
                    } else {
                      res(text)
                    }
                  })
                )
            )
            await expect(result).to.eventually.equal(realResult)
          })
        }
      })
    })
  })

  it('should fail if secondary stream fails', async function () {
    const replacer = new Replacer(
      '###REPLACE-ME###',
      streamTest.fromErroredChunks(new Error('expected'), ['Age of', ' Chaos,'])
    )
    const result = new Promise<string>((res, rej) => {
      replacer.on('error', rej)
      streamTest
        .fromChunks(['In the ###REPLACE-ME### ', 'two factions battled'])
        .pipe(replacer)
        .pipe(
          streamTest.toText((err, text) => {
            if (err) {
              rej(err)
            } else {
              res(text)
            }
          })
        )
    })
    await expect(result).to.eventually.be.rejectedWith(/expected/)
  })
})
