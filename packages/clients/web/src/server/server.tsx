/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { SheetsRegistry } from 'jss'
import * as React from 'react'
import { renderToNodeStream, renderToStaticNodeStream } from 'react-dom/server'

import { ServerContext } from '../context'
import { StoreSet } from '../stores'
import { UiUrlAnalyzer } from '../tools'
import { placeHolder, preloadedVarName } from './constants'
import HtmlPage from './HtmlPage'
import Lang from './Lang'
import Root from './Root'
import { Prepender as PrependerTransform, Replacer as ReplacerTransform } from './streams'

/**
 * Asynchronously build the HTML page to send to client.
 *
 * @param url - The requested URL.
 * @param csrfToken - The CSRF token calculated by the server.
 * @param nonce - The nonce to be used for the inline scripts and styles.
 * @param storeSet - The prepared store set.
 * @param serverContext - The server context.
 * @param asStream - True if the result should directly be a stream.
 * @returns The HTML page to send to client.
 */
export function buildPage(
  url: string,
  csrfToken: string,
  nonce: string,
  storeSet: StoreSet,
  serverContext: ServerContext,
  asStream?: false
): Promise<string>

/**
 * Build the HTML page to send to client, and output it as a stream. The stream will directly output a
 * string, not a buffer.
 *
 * @param url - The requested URL.
 * @param csrfToken - The CSRF token calculated by the server.
 * @param nonce - The nonce to be used for the inline scripts and styles.
 * @param storeSet - The prepared store set.
 * @param serverContext - The server context.
 * @param asStream - True if the result should directly be a stream.
 * @returns The HTML page to send to client.
 */
export function buildPage(
  url: string,
  csrfToken: string,
  nonce: string,
  storeSet: StoreSet,
  serverContext: ServerContext,
  asStream: true
): NodeJS.ReadableStream

/*
 * Implementation
 */
export function buildPage(
  url: string,
  csrfToken: string,
  nonce: string,
  storeSet: StoreSet,
  serverContext: ServerContext,
  asStream?: boolean
): Promise<string> | NodeJS.ReadableStream {
  // Read the full stream and return a promise
  if (asStream !== true) {
    return new Promise<string>((res, rej) => {
      const stream = buildPage(url, csrfToken, nonce, storeSet, serverContext, true)
      let data = ''
      stream.on('data', chunk => (data += chunk))
      stream.on('end', () => res(data))
      stream.on('error', error => rej(error))
    })
  }

  const analyzer = new UiUrlAnalyzer(url, storeSet.langStore.lang.$languageMap.availables)
  const sheetsRegistry = new SheetsRegistry()
  return renderToStaticNodeStream(
    <HtmlPage csrfToken={csrfToken} nonce={nonce} preloaded={serverContext.stringify(preloadedVarName)}>
      <Lang defaultUrl={analyzer.cleanUrl} alternates={analyzer.alternates} />
    </HtmlPage>
  )
    .pipe(
      new ReplacerTransform(
        placeHolder.content,
        renderToNodeStream(
          <Root
            url={url}
            basename={analyzer.langUrl}
            serverContext={serverContext}
            sheetsRegistry={sheetsRegistry}
            storeSet={storeSet}
          />
        )
      )
    )
    .pipe(new ReplacerTransform(placeHolder.jss, sheetsRegistry))
    .pipe(new PrependerTransform('<!DOCTYPE html>'))
    .setEncoding('utf-8')
}
