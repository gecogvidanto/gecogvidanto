/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { StylesProvider, createGenerateClassName } from '@material-ui/styles'
import { SheetsRegistry } from 'jss'
import * as React from 'react'
import { FunctionComponent } from 'react'
import { StaticRouter } from 'react-router-dom'

import App from '../App'
import { ServerContext, ServerContextProvider } from '../context'
import { StoreSet } from '../stores'
import { createTheme } from '../themes'

const theme = createTheme()

export interface RootProps {
  url: string
  basename: string
  serverContext: ServerContext
  sheetsRegistry: SheetsRegistry
  storeSet: StoreSet
}

/**
 * The root component of the server.
 */
/* (JSX element) */
const Root: FunctionComponent<RootProps> = ({ url, basename, serverContext, sheetsRegistry, storeSet }) => (
  <StaticRouter basename={basename} location={url}>
    <ServerContextProvider value={serverContext}>
      <StylesProvider
        sheetsRegistry={sheetsRegistry}
        sheetsManager={new Map()}
        generateClassName={createGenerateClassName()}
      >
        <App theme={theme} storeSet={storeSet} />
      </StylesProvider>
    </ServerContextProvider>
  </StaticRouter>
)
export default Root
