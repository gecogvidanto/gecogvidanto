/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { resolve } from 'path'

import { UiUrlAnalyzer } from '../tools'

/**
 * The place holders.
 */
export const placeHolder = {
  content: '###CONTENT###',
  jss: '###JSS-STYLE###',
}

/**
 * The variable for the pre-loaded data.
 */
export const preloadedVarName = 'window.__PRELOADED_SENT__'

/**
 * Routes for the languages.
 */
export const langRoutes = {
  /**
   * The URL path indicating the language.
   */
  url: UiUrlAnalyzer.LANG_URL,

  /**
   * The parameter in the path containing the language code.
   */
  param: UiUrlAnalyzer.LANG_PARAM,
}

/**
 * Routes for the static items.
 */
export const staticRoutes = {
  /**
   * The public URL where all static files should be found.
   */
  url: '/assets',

  /**
   * The path where static items are stored.
   */
  path: resolve(__dirname, '..', 'public'),
}

/**
 * The allowed external sources.
 */
export const externalSources = {
  images: ['https://*.gravatar.com'],
  styles: [
    // For Algolia places, should be replaced by nonce
    "'sha256-47DEQpj8HBSa+/TImW+5JCeuQeRkm5NMpJWZG3hSuFU='",
    "'sha256-J2Bd+eQsoFjrYKhvGlownfy2lVG3q7337wj7g9gJtT8='",
  ],
  data: ['https://*.algolia.net', 'https://*.algolianet.com'],
  scripts: ['https://*.algolia.net', 'https://*.algolianet.com'],
}

/**
 * The content type of the HTML pages.
 */
export const contentType = 'text/html; charset=utf-8'
