/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as React from 'react'
import { FunctionComponent } from 'react'

import { G1FaviconImage } from '../images'
import { placeHolder, staticRoutes } from './constants'

/**
 * Environment suffix to load appropriate script.
 */
const scriptEnvironment = process.env.NODE_ENV === 'production' ? 'min' : 'dev'

export interface HtmlPageProps {
  /**
   * The token sent by the server to prevent CSRF.
   */
  csrfToken: string

  /**
   * The nonce to be used for the inline scripts and styles.
   */
  nonce: string

  /**
   * The script for pre-loaded data.
   */
  preloaded: string
}

/**
 * The empty HTML page. This page should have the language URLs as a children element.
 */
/* (JSX element) */
const HtmlPage: FunctionComponent<HtmlPageProps> = ({ csrfToken, nonce, preloaded, children }) => {
  return (
    <html>
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="csrf-token" content={csrfToken} />
        <meta property="csp-nonce" content={nonce} />
        <link rel="shortcut icon" type="image/png" href={G1FaviconImage} />
        {children}
        <title>ĞecoĞvidanto</title>
      </head>
      <body>
        <div id="root">{placeHolder.content}</div>
        <style id="jss-server-side" nonce={nonce}>
          {placeHolder.jss}
        </style>
        <script id="preload" nonce={nonce} dangerouslySetInnerHTML={{ __html: preloaded }} />
        <script src={`${staticRoutes.url}/gecogvidanto.${scriptEnvironment}.js`}></script>
      </body>
    </html>
  )
}
export default HtmlPage
