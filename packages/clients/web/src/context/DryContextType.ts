/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { EconomicSystem, EmailCheck, Game, User } from '@gecogvidanto/shared'

import { langType } from '../locale'
import { ServerInfo } from '../tools'

export default interface DryContextType {
  sdata: {
    serverInfo: ServerInfo
    captchaKey: string
  }
  lang: {
    preferences: ReadonlyArray<string>
  }
  user?: {
    logged?: User
    read(id: string): User
    checkEmail(emailCheck: EmailCheck): boolean
  }
  game?: {
    current?: Game
    search(): Game[]
  }
  economicSystems?: {
    search(): EconomicSystem<langType>[]
  }
}
