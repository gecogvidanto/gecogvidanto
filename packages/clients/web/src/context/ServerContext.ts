/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { LanguageMap } from 'intl-ts'
import { createContext, useContext } from 'react'

import { CertLevel, EconomicSystem, EmailCheck, Game, User } from '@gecogvidanto/shared'

import { langType } from '../locale'
import { ServerInfo } from '../tools'
import Context from './Context'
import DryContextType from './DryContextType'

type UserReader = (id: string) => Promise<User>
type EmailChecker = (emailCheck: EmailCheck) => Promise<boolean>
type GamesSearcher = () => Promise<Game[]>
type EconomicSystemSearcher = () => Promise<EconomicSystem<langType>[]>

class SdataContext extends Context<DryContextType['sdata']> {
  public set serverInfo(serverInfo: ServerInfo) {
    this.store.serverInfo = serverInfo
  }

  public get serverInfo(): ServerInfo {
    return this.store.serverInfo!
  }

  public set captchaKey(captchaKey: string) {
    this.store.captchaKey = captchaKey
  }

  public get captchaKey(): string {
    return this.store.captchaKey!
  }
}

class LangContext extends Context<DryContextType['lang']> {
  public set preferences(preferences: ReadonlyArray<string>) {
    this.store.preferences = preferences
  }

  public get preferences(): ReadonlyArray<string> {
    return this.store.preferences!
  }

  public set languageMap(languageMap: LanguageMap<any>) {
    this.addSpecialData('languageMap', languageMap.toString())
  }
}

class UserContext extends Context<Required<DryContextType>['user']> {
  private static _userReader?: UserReader
  private static _emailChecker?: EmailChecker

  public static set userReader(userReader: UserReader) {
    UserContext._userReader = userReader
  }

  public static set emailChecker(emailChecker: EmailChecker) {
    UserContext._emailChecker = emailChecker
  }

  public set logged(logged: User) {
    this.store.logged = logged
  }

  public get logged(): User {
    return this.store.logged!
  }

  public read = (id: string): User => {
    return this.readLoadedData(
      'read',
      UserContext._userReader,
      {
        email: '',
        name: '',
        level: CertLevel.GameMaster,
      },
      id
    )
  }

  public checkEmail = (emailCheck: EmailCheck): boolean => {
    return this.readLoadedData('checkEmail', UserContext._emailChecker, false, emailCheck)
  }
}

class GameContext extends Context<Required<DryContextType>['game']> {
  private static _gamesSearcher?: GamesSearcher

  public static set gamesSearcher(gamesSearcher: GamesSearcher) {
    GameContext._gamesSearcher = gamesSearcher
  }

  public set current(current: Game) {
    this.store.current = current
  }

  public get current(): Game {
    return this.store.current!
  }

  public search = (): Game[] => {
    return this.readLoadedData('search', GameContext._gamesSearcher, [])
  }
}

class EconomicSystemContext extends Context<Required<DryContextType>['economicSystems']> {
  private static _economicSystemsSearcher?: EconomicSystemSearcher

  public static set economicSystemsSearcher(economicSystemsSearcher: EconomicSystemSearcher) {
    EconomicSystemContext._economicSystemsSearcher = economicSystemsSearcher
  }

  public search = (): EconomicSystem<langType>[] => {
    return this.readLoadedData('search', EconomicSystemContext._economicSystemsSearcher, [])
  }
}

/**
 * The context used by server renderer.
 */
export default class ServerContext extends Context<DryContextType> implements DryContextType {
  public statusCode?: number
  public url?: string

  public constructor() {
    super()
  }

  public static set dataProviders({
    userReader,
    emailChecker,
    gamesSearcher,
    economicSystemsSearcher,
  }: {
    userReader: UserReader
    emailChecker: EmailChecker
    gamesSearcher: GamesSearcher
    economicSystemsSearcher: EconomicSystemSearcher
  }) {
    UserContext.userReader = userReader
    UserContext.emailChecker = emailChecker
    GameContext.gamesSearcher = gamesSearcher
    EconomicSystemContext.economicSystemsSearcher = economicSystemsSearcher
  }

  public get sdata(): SdataContext {
    return this.getSubElement('sdata', SdataContext)
  }

  public get lang(): LangContext {
    return this.getSubElement('lang', LangContext)
  }

  public get user(): UserContext {
    return this.getSubElement('user', UserContext)
  }

  public get game(): GameContext {
    return this.getSubElement('game', GameContext)
  }

  public get economicSystems(): EconomicSystemContext {
    return this.getSubElement('economicSystems', EconomicSystemContext)
  }
}

const context = createContext<ServerContext | undefined>(undefined)
export const ServerContextProvider = context.Provider
export function useServerContext(): ServerContext | undefined {
  return useContext(context)
}
