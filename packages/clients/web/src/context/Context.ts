/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
export type UndefinedDeepReturnTypes<T> = {
  [P in keyof T]: T[P] extends (...args: any[]) => any
    ? ReturnType<T[P]> | undefined
    : UndefinedDeepReturnTypes<T[P]>
}

interface SpecialData {
  key: string
  value: string
}

type Store<T> = Partial<UndefinedDeepReturnTypes<Required<T>>>
interface DataLoaders {
  [key: string]: Promise<void>
}

type ContextContructor<T> = new (
  subStore: Store<T>,
  dataLoaders: DataLoaders,
  specialData: SpecialData[],
  specialKeyPrefix: string
) => Context<T>

/**
 * This class can be used to build and use a context containing all type-safe required data.
 */
export default class Context<T> {
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public constructor()
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public constructor(
    store: Store<T>,
    dataLoaders: DataLoaders,
    specialData: SpecialData[],
    specialKeyPrefix: string
  )
  public constructor(
    protected readonly store: Store<T> = {},
    private dataLoaders: DataLoaders = {},
    private readonly specialData: SpecialData[] = [],
    private readonly specialKeyPrefix: string = '.'
  ) {}

  /**
   * Create a `variable=value` with value containing all the context.
   *
   * @param variable - The variable in which to render context.
   * @returns The created string.
   */
  public stringify(variable: string): string {
    const allValues: SpecialData[] = this.specialData.slice(0)
    allValues.unshift({
      key: '',
      value: JSON.stringify(this.store).replace(/</g, '\\u003c'),
    })
    return allValues.map(data => variable + data.key + '=' + data.value).join('; ')
  }

  /**
   * Read the loading data promise. The promise array will be cleaned once resolved or rejected.
   *
   * @returns A promise which, if resolved, indicate if a new rendering is needed. If rejected, will probably indicate
   * the HTTP error code.
   */
  public get loadingData(): Promise<boolean> {
    const loaders: string[] = Object.keys(this.dataLoaders)
    return loaders.length === 0
      ? Promise.resolve(false)
      : new Promise((resolve, reject) => {
          Promise.all(loaders.map(loader => this.dataLoaders[loader]))
            .then(() => {
              this.dataLoaders = {}
              resolve(true)
            })
            .catch(error => {
              this.dataLoaders = {}
              reject(error)
            })
        })
  }

  protected readLoadedData<D extends keyof Store<T>, U extends any[]>(
    data: D,
    dataLoader: ((...params: U) => Promise<Required<Store<T>>[D]>) | undefined,
    defaultValue: Required<Store<T>>[D],
    ...params: U
  ): Required<Store<T>>[D] {
    const inStore: Store<T>[D] | undefined = this.store[data]
    if (inStore !== undefined) {
      return inStore
    } else {
      const index: string = this.specialKeyPrefix + data
      if (dataLoader && !(index in this.dataLoaders)) {
        this.dataLoaders[index] = new Promise((resolve, reject) => {
          dataLoader(...params)
            .then(value => {
              this.store[data] = value
              resolve()
            })
            .catch(error => reject(error))
        })
        // Prevent asynchronous failures
        this.dataLoaders[index].catch(error => error)
      }
      return defaultValue
    }
  }

  protected addSpecialData(key: string, value: string): void {
    this.specialData.push({ key: this.specialKeyPrefix + key, value })
  }

  protected getSubElement<K extends keyof T, S extends Context<Required<T>[K]>>(
    key: K,
    constructor: ContextContructor<Required<T>[K]>
  ): S {
    if (!this.store[key]) {
      this.store[key] = {} as any
    }
    return new constructor(
      this.store[key]! as Store<Required<T>[K]>,
      this.dataLoaders,
      this.specialData,
      this.specialKeyPrefix + key + '.'
    ) as S
  }
}
