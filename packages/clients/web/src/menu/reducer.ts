/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Dispatch, createContext } from 'react'

import MenuEntryDescription from './MenuEntryDescription'

/**
 * The state managed by the menu.
 */
export interface State {
  menu: MenuEntryDescription[][]
  pendingAction?: string | (() => void)
}

/**
 * Type of actions which can be applied to the state.
 */
const enum ActionType {
  SelectEntry,
  AddEntry,
  RemoveEntry,
  CleanAction,
}

/**
 * Select menu entry action.
 */
interface SelectEntryAction {
  type: ActionType.SelectEntry
  target: EventTarget & HTMLElement
  payload: MenuEntryDescription['action']
}

/**
 * Build an action to select a menu entry.
 *
 * @param target - The target of the menu event.
 * @param action - The action to be executed by the menu entry.
 * @returns The corresponding action.
 */
export function selectEntry(
  target: EventTarget & HTMLElement,
  action: MenuEntryDescription['action']
): SelectEntryAction {
  return {
    type: ActionType.SelectEntry,
    target,
    payload: action,
  }
}

/**
 * Add menu entry action.
 */
interface AddEntryAction {
  type: ActionType.AddEntry
  payload: MenuEntryDescription
}

/**
 * Build an action to add a menu entry.
 *
 * @param description - The menu entry description.
 * @returns The corresponding action.
 */
export function addEntry(description: MenuEntryDescription): AddEntryAction {
  return {
    type: ActionType.AddEntry,
    payload: description,
  }
}

/**
 * Remove menu entry action.
 */
interface RemoveEntryAction {
  type: ActionType.RemoveEntry
  payload: MenuEntryDescription
}

/**
 * Build an action to remove a menu entry.
 *
 * @param reference - The reference on the entry to remove (must be the same object as used to add the menu
 * entry).
 * @returns The corresponding action.
 */
export function removeEntry(reference: MenuEntryDescription): RemoveEntryAction {
  return {
    type: ActionType.RemoveEntry,
    payload: reference,
  }
}

/**
 * Clean the pending action action.
 */
interface CleanActionAction {
  type: ActionType.CleanAction
}

/**
 * Build an action to clean the pending action.
 *
 * @returns The corresponding action.
 */
export function cleanAction(): CleanActionAction {
  return {
    type: ActionType.CleanAction,
  }
}

/**
 * The reducer actions.
 */
export type Action = SelectEntryAction | AddEntryAction | RemoveEntryAction | CleanActionAction

/**
 * The index of the customizable menu.
 */
const CUSTOMIZABLE_MENU = 1

/**
 * The reducer function. Treat all actions to update the state.
 *
 * @param state - The initial state.
 * @param action - The action to be executed.
 * @returns The new state.
 */
export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case ActionType.SelectEntry:
      if (typeof action.payload === 'string') {
        return { ...state, pendingAction: action.payload }
      } else {
        const actionfn = action.payload
        const paramfn = action.target
        return { ...state, pendingAction: () => actionfn(paramfn) }
      }
    case ActionType.AddEntry: {
      const menu = state.menu.slice()
      menu[CUSTOMIZABLE_MENU].push(action.payload)
      return { ...state, menu }
    }
    case ActionType.RemoveEntry:
      return {
        ...state,
        menu: state.menu.map((menu, index) =>
          index !== CUSTOMIZABLE_MENU ? menu : menu.filter(entry => entry !== action.payload)
        ),
      }
    case ActionType.CleanAction:
      return { menu: state.menu }
    default:
      // eslint-disable-next-line no-case-declarations
      const error: never = action
      throw new Error(`Unexpected action ${error}`)
  }
}

export const context = createContext<[State, Dispatch<Action>] | undefined>(undefined)
