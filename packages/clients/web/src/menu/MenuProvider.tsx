/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import HomeIcon from '@material-ui/icons/Home'
import AboutIcon from '@material-ui/icons/Place'
import * as React from 'react'
import { FunctionComponent, useReducer } from 'react'

import { routes } from '../routes'
import { State, context, reducer } from './reducer'

/**
 * The initial state, when no customized menu is added.
 */
const initialState: State = {
  menu: [
    [
      {
        icon: HomeIcon,
        title: 'pageHome',
        action: routes.HOME.build(),
      },
    ],
    [], // The customizable menu
    [
      {
        icon: AboutIcon,
        title: 'pageAbout',
        action: routes.ABOUT.build(),
      },
    ],
  ],
}

const MenuProvider: FunctionComponent = ({ children }) => {
  return <context.Provider value={useReducer(reducer, initialState)}>{children}</context.Provider>
}
export default MenuProvider
