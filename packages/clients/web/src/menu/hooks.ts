/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Dispatch, useContext, useEffect } from 'react'
import { useHistory } from 'react-router'

import { Action, State, cleanAction, context } from './reducer'

/**
 * @returns The menu reducer in the provider.
 */
function useMenu(): [State, Dispatch<Action>] {
  const value = useContext(context)
  if (!value) {
    throw Error('Menu provider not found')
  }
  return value
}

/**
 * @returns The menu dispatcher, in order to execute some actions on it.
 */
export function useMenuAction(): Dispatch<Action> {
  const [, dispatch] = useMenu()
  return dispatch
}

/**
 * This hook treats the pending actions and returns the menu content. It should be only used in one single
 * component (the component displaying the menu).
 *
 * @returns The menu data.
 */
export function useMenuContent(): State['menu'] {
  const [state, dispatch] = useMenu()
  const history = useHistory()

  useEffect(() => {
    const { pendingAction } = state
    if (pendingAction) {
      dispatch(cleanAction())
      if (typeof pendingAction === 'string') {
        history.push(pendingAction)
      } else {
        pendingAction()
      }
    }
  }, [dispatch, history, state])

  return state.menu
}
