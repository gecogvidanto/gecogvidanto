/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {
  Avatar,
  Button,
  Dialog,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Theme,
  Typography,
} from '@material-ui/core'
// eslint-disable-next-line import/no-internal-modules
import { DialogProps } from '@material-ui/core/Dialog'
import StyleIcon from '@material-ui/icons/StyleOutlined'
import { makeStyles } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { ChangeEvent, FunctionComponent, SyntheticEvent, useEffect, useState } from 'react'

import { EconomicSystem } from '@gecogvidanto/shared'

import { useEventCallback, useFetcher } from '../../hooks'
import { langType } from '../../locale'
import { useGameStore, useLangStore } from '../../stores'
import { api } from '../../tools'

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    ...theme.gecogvidanto.form.style.container,
    width: theme.gecogvidanto.form.dialogWidth,
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    ...theme.gecogvidanto.form.style.form,
  },
  submit: {
    ...theme.gecogvidanto.form.style.submit,
  },
}))

export interface RoundSetSelectProps extends Omit<DialogProps, 'classes' | 'onClose'> {
  onClose?: () => void
}

/**
 * Component used to select economic system for set.
 */
const RoundSetSelect: FunctionComponent<RoundSetSelectProps> = observer(({ onClose, ...dialogProps }) => {
  const classes = useStyles()
  const { addSet } = useGameStore()
  const { lang, assemble } = useLangStore()

  // State
  const [selected, setSelected] = useState<string>('')

  // Data
  const { open } = dialogProps
  const [economicSystems] = useFetcher<EconomicSystem<langType>[]>(api.economicSystem.search)

  // Events
  const onSelectChange = useEventCallback((e: ChangeEvent<{ value: unknown }>): void => {
    setSelected(e.target.value as string)
  })
  const onCreateSet = useEventCallback((e: SyntheticEvent<any>): void => {
    e.preventDefault()
    if (selected) {
      addSet(selected!)
      onClose && onClose()
    }
  })

  // Manage selection state
  useEffect(() => {
    open && setSelected('')
  }, [open])

  // Render
  const ecoSysItems: JSX.Element[] = economicSystems
    ? economicSystems.map(economicSystem => (
        <MenuItem button key={economicSystem.id} value={economicSystem.id}>
          {assemble(economicSystem.name)}
        </MenuItem>
      ))
    : []
  return (
    <Dialog {...dialogProps}>
      <div className={classes.container}>
        <Avatar className={classes.avatar}>
          <StyleIcon />
        </Avatar>
        <Typography variant="h5">{lang.setNew()}</Typography>
        <form className={classes.form}>
          <FormControl margin="normal" fullWidth>
            <InputLabel htmlFor="set">{lang.economicSystem()}</InputLabel>
            <Select id="set" name="set" value={selected} onChange={onSelectChange}>
              {ecoSysItems}
            </Select>
          </FormControl>
          <Button
            type="submit"
            disabled={!selected}
            onClick={onCreateSet}
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            {lang.setCreate()}
          </Button>
        </form>
      </div>
    </Dialog>
  )
})

export default RoundSetSelect
