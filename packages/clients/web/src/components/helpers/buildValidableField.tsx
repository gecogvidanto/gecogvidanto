/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { FormControl, FormHelperText, InputLabel } from '@material-ui/core'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import {
  ChangeEvent,
  ChangeEventHandler,
  ComponentType,
  FocusEvent,
  FocusEventHandler,
  FunctionComponent,
  Ref,
} from 'react'

import { AsyncValidator, Validator } from '@gecogvidanto/shared'

import { ValidationEvent, useFieldValidator } from '../../hooks'
import { mergeFunctions } from '../../tools'
import NewLineJoined from '../NewLineJoined'

interface InnerElement extends Element {
  value: string
}
interface VisibleFieldProps<I extends InnerElement> {
  id: string
  onChange?: ChangeEventHandler<I>
  onBlur?: FocusEventHandler<I>
}
interface FieldProps<I extends InnerElement> extends VisibleFieldProps<I> {
  inputRef: Ref<any>
}
interface Props<I extends InnerElement> extends VisibleFieldProps<I> {
  label: string
  validators?: Validator[]
  asyncValidators?: AsyncValidator[]
  onValidate?: (event: ValidationEvent<I>) => void
  disabled?: boolean
  required?: boolean
  fullWidth?: boolean
}

export type ValidableFieldProps<C extends Partial<FieldProps<I>>, I extends InnerElement> = Props<I> &
  Omit<C, keyof Props<I>>

/**
 * Convert an input field into a validable field.
 *
 * @param Field - The fiels to convert.
 * @returns The validable field.
 */
export function buildValidableField<C extends Partial<FieldProps<I>>, I extends InnerElement>(
  Field: ComponentType<C>
): FunctionComponent<ValidableFieldProps<C, I>> {
  return observer(
    ({
      id,
      label,
      validators,
      asyncValidators,
      onValidate,
      disabled = false,
      required = false,
      fullWidth = false,
      onChange,
      onBlur,
      ...fieldProps
    }) => {
      const { fieldRef, onChange: onChangeValid, onBlur: onBlurValid, errors } = useFieldValidator({
        validators,
        asyncValidators,
        onValidate,
      })
      const mergedChanged = mergeFunctions<[ChangeEvent<I>]>(onChangeValid, onChange)
      const mergedBlur = mergeFunctions<[FocusEvent<I>]>(onBlurValid, onBlur)
      return (
        <FormControl
          margin="normal"
          error={!!errors}
          disabled={disabled}
          required={required}
          fullWidth={fullWidth}
        >
          <InputLabel htmlFor={id}>{label}</InputLabel>
          <Field
            {...((fieldProps as unknown) as C)}
            id={id}
            inputRef={fieldRef}
            onChange={mergedChanged}
            onBlur={mergedBlur}
          />
          {errors && (
            <FormHelperText>
              <NewLineJoined>{errors}</NewLineJoined>
            </FormHelperText>
          )}
        </FormControl>
      )
    }
  )
}
