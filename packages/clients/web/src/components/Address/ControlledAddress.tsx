/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Input, InputLabel } from '@material-ui/core'
// eslint-disable-next-line import/no-internal-modules
import { useFormControl } from '@material-ui/core/FormControl'
import * as React from 'react'
import { ChangeEvent, FunctionComponent, useEffect, useRef } from 'react'

import { IdentifiedAddress } from '@gecogvidanto/shared'

import { useEventCallback } from '../../hooks'
import { Places, PlacesChangeEvent, PlacesErrorEvent, PlacesLimitEvent } from '../../tools'

export interface ControlledAddressProps {
  appId?: string
  apiKey?: string
  id: string
  name?: string
  label: string
  fullWidth?: boolean
  autoFocus?: boolean
  defaultValue?: string | IdentifiedAddress
  onChange?: (value: string | IdentifiedAddress) => void
  onLimit?: (value: string) => void
  onError?: (value: string) => void
}

/**
 * A controlled address with auto-completion.
 */
/* (JSX element) */
const ControlledAddress: FunctionComponent<ControlledAddressProps> = ({
  appId,
  apiKey,
  id,
  name,
  label,
  fullWidth,
  autoFocus,
  defaultValue,
  onChange,
  onLimit,
  onError,
}) => {
  const formControl = useFormControl()
  const places = useRef<Places>()
  const innerInput = useRef<HTMLInputElement>()
  const usedDefaultValue = useRef(defaultValue)

  // Places events
  const onSelect = useEventCallback((e: PlacesChangeEvent): void => {
    const { value, city, countryCode, latlng } = e.suggestion
    const address: IdentifiedAddress = {
      value,
      city,
      countryCode,
      latitude: latlng.lat,
      longitude: latlng.lng,
    }
    onChange && onChange(address)
  })
  const onClear = useEventCallback((): void => {
    formControl && formControl.onEmpty()
    onChange && onChange('')
  })
  const onInnerLimit = useEventCallback((e: PlacesLimitEvent): void => {
    onLimit && onLimit(e.message)
  })
  const onInnerError = useEventCallback((e: PlacesErrorEvent): void => {
    onError && onError(e.message)
  })

  // Input attach/detach
  useEffect(() => {
    ;(async (): Promise<void> => {
      if (innerInput.current) {
        places.current = await Places.create(innerInput.current, appId, apiKey, 'address')
        places.current.placesInstance.on('change', onSelect)
        places.current.placesInstance.on('clear', onClear)
        places.current.placesInstance.on('limit', onInnerLimit)
        places.current.placesInstance.on('error', onInnerError)
        usedDefaultValue.current &&
          places.current.placesInstance.setVal(
            typeof usedDefaultValue.current === 'string'
              ? usedDefaultValue.current
              : usedDefaultValue.current.value
          )
      }
    })()
    return () => {
      ;(async (): Promise<void> => {
        if (places.current) {
          places.current.placesInstance.removeAllListeners('error')
          places.current.placesInstance.removeAllListeners('limit')
          places.current.placesInstance.removeAllListeners('clear')
          places.current.placesInstance.removeAllListeners('change')
          places.current.close()
          places.current = undefined
        }
      })()
    }
  }, [apiKey, appId, onClear, onInnerError, onInnerLimit, onSelect])

  // Events
  const onInnerChange = useEventCallback((e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>): void => {
    if (e.target.value) {
      onChange && onChange(e.target.value)
    }
  })

  // Render
  return (
    <>
      <InputLabel htmlFor={id}>{label}</InputLabel>
      <Input
        autoFocus={autoFocus}
        fullWidth={fullWidth}
        name={name}
        id={id}
        onChange={onInnerChange}
        inputRef={innerInput}
      />
    </>
  )
}

export default ControlledAddress
