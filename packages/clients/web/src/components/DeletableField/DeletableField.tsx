/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { IconButton, Input, InputAdornment } from '@material-ui/core'
// eslint-disable-next-line import/no-internal-modules
import { InputProps } from '@material-ui/core/Input'
import DeleteIcon from '@material-ui/icons/Delete'
import * as React from 'react'
import { ForwardRefExoticComponent, RefForwardingComponent, forwardRef } from 'react'

export interface DeletableFieldProps extends Omit<InputProps, 'endAdornment'> {
  ariaLabel: string
  deleteDisabled: boolean
  onDelete: () => void
}

const RefDeletableField: RefForwardingComponent<any, DeletableFieldProps> = (
  { ariaLabel, deleteDisabled, onDelete, ...inputProps },
  ref
) => {
  const adornment = (
    <InputAdornment position="end">
      <IconButton aria-label={ariaLabel} disabled={deleteDisabled} onClick={onDelete}>
        <DeleteIcon />
      </IconButton>
    </InputAdornment>
  )
  return <Input innerRef={ref} {...inputProps} endAdornment={adornment} />
}

/**
 * A field with a delete button.
 */
const DeletableField: ForwardRefExoticComponent<DeletableFieldProps> = forwardRef<any, DeletableFieldProps>(
  RefDeletableField
)

export default DeletableField
