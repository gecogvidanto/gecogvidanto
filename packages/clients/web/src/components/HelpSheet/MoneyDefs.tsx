/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as React from 'react'
import { FunctionComponent } from 'react'

/**
 * Render definitions for money.
 */
/* (JSX element) */
const MoneyDefs: FunctionComponent = () => {
  return (
    <defs>
      <rect id="moneybackground" width="301" height="195" />
      <path id="moneyhalf" d="M 0,15 a 15,15 0 0 1 0,-30 z" stroke="#000000" />
      <circle id="moneyhalfdouble" r="15" stroke="#000000" />
      <g id="moneydouble">
        <use x="18" y="0" href="#moneyhalfdouble" />
        <use x="0" y="0" href="#moneyhalfdouble" />
      </g>
      <text id="moneymark" fontSize="96px" fontFamily="serif">
        Ğ
      </text>
    </defs>
  )
}

export default MoneyDefs
