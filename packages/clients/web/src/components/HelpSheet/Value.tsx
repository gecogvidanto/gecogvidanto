/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as React from 'react'
import { FunctionComponent } from 'react'

import { BlueMarkImage, GreenMarkImage, RedMarkImage, YellowMarkImage } from '../../images'
import { gaussRandom } from '../../tools'
import { CategoryContainer, ItemProps, Position, PositionContainer } from './description'

interface Category {
  background: string
  center: string
  mark: string
  position: Position
}

interface ItemDescription {
  position: PositionContainer
  size: Position
  category: CategoryContainer<Category>
}

const DESCRIPTION: ItemDescription = {
  position: {
    low: { x: 5.5, y: 2.5 },
    medium: { x: 345, y: 2.5 },
    high: { x: 684.5, y: 2.5 },
    waiting: { x: 345, y: 303 },
  },
  size: { x: 308, y: 224 },
  category: {
    red: {
      background: '#ce1522',
      center: '#f4a4a4',
      mark: RedMarkImage,
      position: { x: 57, y: 50 },
    },
    yellow: {
      background: '#c4bb13',
      center: '#f4efa4',
      mark: YellowMarkImage,
      position: { x: 76.5, y: 14 },
    },
    green: {
      background: '#13a549',
      center: '#96cc9c',
      mark: GreenMarkImage,
      position: { x: 63.5, y: 34.5 },
    },
    blue: {
      background: '#3c8fa3',
      center: '#b3dbe4',
      mark: BlueMarkImage,
      position: { x: 62, y: 29.5 },
    },
  },
}

/**
 * Display a value card.
 */
/* (JSX element) */
const Value: FunctionComponent<ItemProps> = ({ positionKey, categoryKey }) => {
  const category: Category = DESCRIPTION.category[categoryKey]
  const position: Position = DESCRIPTION.position[positionKey]
  const xcenter: number = position.x + DESCRIPTION.size.x / 2
  const ycenter: number = position.y + DESCRIPTION.size.y / 2
  const rotate: number = gaussRandom(-10, 10) / 10
  const xlimit: number = Math.floor(Math.abs(rotate) / 0.8)
  const ylimit: number = Math.floor((Math.abs(rotate) + 0.25) / 0.4)
  const xshift: number = gaussRandom(-4 + xlimit, 4 - xlimit)
  const yshift: number = gaussRandom(-2 + ylimit, 2)
  const deform = `rotate(${rotate},${xcenter},${ycenter}) translate(${xshift},${yshift})`
  return (
    <g key={positionKey} transform={deform}>
      <use
        x={position.x}
        y={position.y}
        fill={category.background}
        stroke={category.background}
        href="#valuebackground"
      />
      <use x={position.x} y={position.y} fill={category.center} href="#valuecenter" />
      <image
        x={position.x + category.position.x}
        y={position.y + category.position.y}
        href={category.mark}
      />
    </g>
  )
}

export default Value
