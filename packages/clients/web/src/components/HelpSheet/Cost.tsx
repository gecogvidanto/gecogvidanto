/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as React from 'react'
import { FunctionComponent } from 'react'

interface Position {
  x: number
  y: number
}

const CELL_DESCRIPTION = {
  position: {
    low: { x: 0, y: 0 },
    medium: { x: 339.5, y: 0 },
    high: { x: 679, y: 0 },
  },
  size: { x: 319, y: 229 },
}

export interface CostProps {
  costFactor: number
}

/**
 * Display the cost over the help sheet cells.
 */
/* (JSX element) */
const Cost: FunctionComponent<CostProps> = ({ costFactor }) => {
  const content: JSX.Element[] | undefined =
    costFactor === 0 // No cost factor (data probably not loaded yet)
      ? undefined
      : new Array<keyof typeof CELL_DESCRIPTION['position']>('low', 'medium', 'high').map((key, index) => {
          const position: Position = CELL_DESCRIPTION.position[key]
          const x = position.x
          const y = position.y + CELL_DESCRIPTION.size.y
          return (
            <text
              key={key}
              x={x + 18}
              y={y - 9}
              fontSize="256px"
              fill="#000000"
              fillOpacity="0.5"
              fontFamily="serif"
            >
              {costFactor * Math.pow(2, index)}
            </text>
          )
        })
  return <>{content}</>
}

export default Cost
