/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as React from 'react'
import { FunctionComponent } from 'react'

import { gaussRandom } from '../../tools'
import { CategoryContainer, ItemProps, Position, PositionContainer } from './description'

interface Category {
  background: string
  mark: string
  prev: keyof CategoryContainer<Category>
  next: keyof CategoryContainer<Category>
}

interface ItemDescription {
  position: PositionContainer
  size: Position
  category: CategoryContainer<Category>
}

const DESCRIPTION: ItemDescription = {
  position: {
    low: { x: 9, y: 17 },
    medium: { x: 348.5, y: 17 },
    high: { x: 688, y: 17 },
    waiting: { x: 348.5, y: 317.5 },
  },
  size: { x: 301, y: 195 },
  category: {
    red: {
      background: '#e20f23',
      mark: '#f55656',
      prev: 'blue',
      next: 'yellow',
    },
    yellow: {
      background: '#fde300',
      mark: '#f67d15',
      prev: 'red',
      next: 'green',
    },
    green: {
      background: '#00a45c',
      mark: '#008b4c',
      prev: 'yellow',
      next: 'blue',
    },
    blue: {
      background: '#008bbb',
      mark: '#00a3d9',
      prev: 'green',
      next: 'red',
    },
  },
}

/**
 * Render a banknote.
 */
/* (JSX element) */
const Money: FunctionComponent<ItemProps> = ({ positionKey, categoryKey }) => {
  const category: Category = DESCRIPTION.category[categoryKey]
  const position: Position = DESCRIPTION.position[positionKey]
  const halfColor: string = DESCRIPTION.category[category.next].background
  const doubleColor: string = DESCRIPTION.category[category.prev].background
  const xcenter: number = position.x + DESCRIPTION.size.x / 2
  const ycenter: number = position.y + DESCRIPTION.size.y / 2
  const rotate: number = gaussRandom(-20, 20) / 10
  const xshift: number = gaussRandom(-6, 6)
  const yshift: number = gaussRandom(-12, 12)
  const deform = `rotate(${rotate},${xcenter},${ycenter}) translate(${xshift},${yshift})`
  return (
    <g key={positionKey} transform={deform}>
      <use x={position.x} y={position.y} fill={category.background} href="#moneybackground" />
      <use x={position.x + 33} y={position.y + 33} fill={halfColor} href="#moneyhalf" />
      <use x={position.x + 55} y={position.y + 33} fill={doubleColor} href="#moneydouble" />
      <use x={position.x + 33} y={position.y + 162} fill={halfColor} href="#moneyhalf" />
      <use x={position.x + 55} y={position.y + 162} fill={doubleColor} href="#moneydouble" />
      <use
        x={-position.x - 268}
        y={position.y + 33}
        fill={halfColor}
        transform="scale(-1,1)"
        href="#moneyhalf"
      />
      <use
        x={-position.x - 246}
        y={position.y + 33}
        fill={doubleColor}
        transform="scale(-1,1)"
        href="#moneydouble"
      />
      <use
        x={-position.x - 268}
        y={position.y + 162}
        fill={halfColor}
        transform="scale(-1,1)"
        href="#moneyhalf"
      />
      <use
        x={-position.x - 246}
        y={position.y + 162}
        fill={doubleColor}
        transform="scale(-1,1)"
        href="#moneydouble"
      />
      <use x={position.x + 113} y={position.y + 140} fill={category.mark} href="#moneymark" />
    </g>
  )
}

export default Money
