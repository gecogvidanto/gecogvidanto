/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as React from 'react'
import { FunctionComponent } from 'react'

/**
 * Display definitions for values.
 */
/* (JSX element) */
const ValueDefs: FunctionComponent = () => {
  return (
    <defs>
      <rect
        id="valuebackground"
        x="12"
        y="12"
        width="284"
        height="200"
        strokeWidth="24"
        strokeLinejoin="round"
      />
      <circle id="valuecenter" cx="154" cy="112" r="100" />
    </defs>
  )
}

export default ValueDefs
