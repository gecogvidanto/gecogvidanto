/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { makeStyles } from '@material-ui/styles'
import * as React from 'react'
import { FunctionComponent, memo } from 'react'

import { HelpSheet as HelpSheetDefinition, PlaceContent } from '@gecogvidanto/shared'

import { HelpSheetImage } from '../../images'
import Cost from './Cost'
import { CategoryContainer, PositionContainer } from './description'
import Money from './Money'
import MoneyDefs from './MoneyDefs'
import Value from './Value'
import ValueDefs from './ValueDefs'

const useStyles = makeStyles({
  image: {
    width: '100%',
    height: '100%',
  },
})

export interface HelpSheetProps {
  type: 'values' | 'money'
  helpSheet: HelpSheetDefinition | undefined
  costFactor: number
  className?: string
}

/**
 * Display the help sheets.
 */
/* (JSX element) */
const HelpSheet: FunctionComponent<HelpSheetProps> = ({ type, helpSheet, costFactor, className }) => {
  const classes = useStyles()
  const Defs = type === 'values' ? ValueDefs : MoneyDefs
  const Card = type === 'values' ? Value : Money
  const content: Array<JSX.Element | undefined> = !helpSheet
    ? [undefined]
    : new Array<keyof PositionContainer>('low', 'medium', 'high', 'waiting').map(positionKey => {
        let categoryKey: keyof CategoryContainer<any> | undefined
        switch (helpSheet[positionKey]) {
          case PlaceContent.Red:
            categoryKey = 'red'
            break
          case PlaceContent.Yellow:
            categoryKey = 'yellow'
            break
          case PlaceContent.Green:
            categoryKey = 'green'
            break
          case PlaceContent.Blue:
            categoryKey = 'blue'
            break
        }
        return categoryKey && <Card key={positionKey} positionKey={positionKey} categoryKey={categoryKey} />
      })

  // Render
  return (
    <svg
      version="1.1"
      viewBox="0 0 998.5 566.5"
      preserveAspectRatio="xMidYMid meet"
      className={className || classes.image}
    >
      <Defs />
      <image href={HelpSheetImage} />
      {content}
      <Cost costFactor={costFactor} />
    </svg>
  )
}
const HelpSheetMemo: FunctionComponent<HelpSheetProps> = memo(HelpSheet)
export default HelpSheetMemo
