/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { CircularProgress, Modal } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import * as React from 'react'
import { FunctionComponent } from 'react'

const useStyles = makeStyles({
  parent: {
    position: 'relative',
    height: '100%',
    width: '100%',
  },
  backdrop: {
    position: 'absolute',
  },
  middle: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translateX(-50%) translateY(-50%)',
  },
})

export interface WaitingProps {
  active: boolean
}

const Waiting: FunctionComponent<WaitingProps> = ({ active, children }) => {
  const classes = useStyles()
  const backdropProps = {
    className: classes.backdrop,
  }
  return (
    <div className={classes.parent}>
      <Modal
        open={active}
        className={classes.backdrop}
        disableAutoFocus
        disablePortal
        BackdropProps={backdropProps}
      >
        <div className={classes.middle}>
          <CircularProgress />
        </div>
      </Modal>
      {children}
    </div>
  )
}

export default Waiting
