/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { IconButton, Input, InputAdornment } from '@material-ui/core'
// eslint-disable-next-line import/no-internal-modules
import { InputProps } from '@material-ui/core/Input'
import VisibilityIcon from '@material-ui/icons/Visibility'
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff'
import * as React from 'react'
import { FunctionComponent, useState } from 'react'

import { useEventCallback } from '../../hooks'

export interface PasswordProps extends Omit<InputProps, 'type' | 'endAdornment'> {
  visibility?: boolean
  onToggleVisibility?: (visibility: boolean) => void
  buttonAriaLabel?: string
}

/**
 * Component managing hidable password.
 */
/* (JSX element) */
const Password: FunctionComponent<PasswordProps> = ({
  visibility: forcedVisibility,
  onToggleVisibility,
  buttonAriaLabel,
  ...inputProps
}) => {
  // State
  const [visibility, setVisibility] = useState<boolean>(false)

  // Data
  const show = forcedVisibility === undefined ? visibility : forcedVisibility

  // Events
  const onInnerToggleVisibility = useEventCallback((): void => {
    onToggleVisibility && onToggleVisibility(!show)
    setVisibility(!show)
  })

  // Render
  const adornment = (
    <InputAdornment position="end">
      <IconButton aria-label={buttonAriaLabel} onClick={onInnerToggleVisibility}>
        {show ? <VisibilityOffIcon /> : <VisibilityIcon />}
      </IconButton>
    </InputAdornment>
  )
  return <Input {...inputProps} type={show ? 'text' : 'password'} endAdornment={adornment} />
}

export default Password
