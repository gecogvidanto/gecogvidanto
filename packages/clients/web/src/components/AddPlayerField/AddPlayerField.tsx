/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { FormControl, Input, InputLabel } from '@material-ui/core'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { Validator } from '@gecogvidanto/shared'

import { ValidationEvent, useEventCallback, useFieldValidator } from '../../hooks'
import { useGameStore } from '../../stores'

export interface AddPlayerFieldProps {
  id: string
  name: string
  label: string
  autoFocus: boolean
  validators: Validator[]
  onAddPlayer: () => void
}

/**
 * A field used to add a player.
 */
const AddPlayerField: FunctionComponent<AddPlayerFieldProps> = observer(
  ({ id, name, label, autoFocus, validators, onAddPlayer }) => {
    const { addPlayer } = useGameStore()

    // Events
    const onValidate = useEventCallback(
      (e: ValidationEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
        if (e.valid) {
          addPlayer(e.target.value)
          onAddPlayer()
        }
      }
    )

    // Field validator
    const { fieldRef, onChange, onBlur } = useFieldValidator({
      validators,
      onValidate,
    })

    // Render
    return (
      <FormControl margin="normal" fullWidth>
        <InputLabel htmlFor={id}>{label}</InputLabel>
        <Input
          id={id}
          name={name}
          autoFocus={autoFocus}
          onChange={onChange}
          onBlur={onBlur}
          inputRef={fieldRef}
        />
      </FormControl>
    )
  }
)

export default AddPlayerField
