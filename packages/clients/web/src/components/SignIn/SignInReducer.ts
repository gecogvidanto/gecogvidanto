/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Dispatch, useReducer } from 'react'

/**
 * The state used for the component.
 */
interface State {
  email?: string
  password?: string
}

/**
 * The target component names.
 */
export const enum TargetName {
  Email = 'email',
  Password = 'password',
}

/**
 * Type of actions which can be applied to the state.
 */
const enum ActionType {
  UpdateState,
  ResetState,
}

/**
 * Update state action.
 */
interface UpdateStateAction {
  type: ActionType.UpdateState
  target: string
  payload: string | undefined
}

/**
 * Build an action used to update the state.
 *
 * @param target - The target component for which to update state.
 * @param value - The value to set.
 * @returns The corresponding action.
 */
export function updateState(target: string, value: string | undefined): UpdateStateAction {
  return { type: ActionType.UpdateState, target, payload: value }
}

/**
 * Reset state action.
 */
interface ResetStateAction {
  type: ActionType.ResetState
}

/**
 * Build an action used to reset the state.
 *
 * @returns The corresponding action.
 */
export function resetState(): ResetStateAction {
  return { type: ActionType.ResetState }
}

/**
 * The reducer action.
 */
type Action = UpdateStateAction | ResetStateAction

/**
 * The initial state.
 */
const initialState: State = {}

/**
 * The reducer function. Treat all actions to update the state.
 *
 * @param state - The initial state.
 * @param action - The action to be executed.
 * @returns The new state.
 */
function reducer(state: State, action: Action): State {
  switch (action.type) {
    case ActionType.UpdateState:
      switch (action.target) {
        case TargetName.Email:
          return { ...state, email: action.payload }
        case TargetName.Password:
          return { ...state, password: action.payload }
        default:
          return state
      }
    case ActionType.ResetState:
      return initialState
    default:
      // eslint-disable-next-line no-case-declarations
      const error: never = action
      throw new Error(`Unexpected action ${error}`)
  }
}

/**
 * Use the component reducer.
 *
 * @returns The same values as for `React.useReducer`.
 */
export default function useSignInReducer(): [State, Dispatch<Action>] {
  return useReducer(reducer, initialState)
}
