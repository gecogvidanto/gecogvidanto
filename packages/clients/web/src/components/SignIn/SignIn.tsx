/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Avatar, Button, Dialog, Theme, Typography } from '@material-ui/core'
// eslint-disable-next-line import/no-internal-modules
import { DialogProps } from '@material-ui/core/Dialog'
import LockIcon from '@material-ui/icons/LockOutlined'
import { makeStyles } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, SyntheticEvent, useCallback, useEffect } from 'react'

import { validators } from '@gecogvidanto/shared'

import { ValidationEvent, useEventCallback } from '../../hooks'
import { useLangStore, useUserStore } from '../../stores'
import ValidableInput from '../ValidableInput'
import ValidablePassword from '../ValidablePassword'
import useSignInReducer, { TargetName, resetState, updateState } from './SignInReducer'

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    ...theme.gecogvidanto.form.style.container,
    width: theme.gecogvidanto.form.dialogWidth,
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    ...theme.gecogvidanto.form.style.form,
  },
  submit: {
    ...theme.gecogvidanto.form.style.submit,
  },
}))

export interface SignInProps extends Omit<DialogProps, 'classes' | 'onClose'> {
  onClose?: () => void
}

/**
 * Component managing user sign in.
 */
const SignIn: FunctionComponent<SignInProps> = observer(({ onClose, ...dialogProps }) => {
  const classes = useStyles()
  const { lang } = useLangStore()
  const { signIn } = useUserStore()
  const [state, dispatch] = useSignInReducer()
  const { open } = dialogProps

  // Manage state
  useEffect(
    () => () => {
      !open && dispatch(resetState())
    },
    [dispatch, open]
  )
  const onValueValidate = useCallback(
    (e: ValidationEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
      dispatch(updateState(e.target.name, e.valid ? e.target.value : undefined))
    },
    [dispatch]
  )

  // Sign in
  const onSignIn = useEventCallback((e: SyntheticEvent<any>): void => {
    e.preventDefault()
    const { email, password } = state
    if (!!email && !!password) {
      onClose && onClose()
      signIn(email!, password!)
    }
  })

  // Render
  return (
    <Dialog {...dialogProps}>
      <div className={classes.container}>
        <Avatar className={classes.avatar}>
          <LockIcon />
        </Avatar>
        <Typography variant="h5">{lang.signInTitle()}</Typography>
        <form className={classes.form}>
          <ValidableInput
            id={TargetName.Email}
            name={TargetName.Email}
            label={lang.email()}
            type="email"
            autoComplete="email"
            autoFocus
            required
            fullWidth
            validators={validators.email}
            onValidate={onValueValidate}
          />
          <ValidablePassword
            id={TargetName.Password}
            name={TargetName.Password}
            label={lang.password()}
            autoComplete="current-password"
            required
            fullWidth
            validators={validators.password}
            onValidate={onValueValidate}
          />
          <Button
            type="submit"
            disabled={!state.email || !state.password}
            onClick={onSignIn}
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            {lang.signIn()}
          </Button>
        </form>
      </div>
    </Dialog>
  )
})

export default SignIn
