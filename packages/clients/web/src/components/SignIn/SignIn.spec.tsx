/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable import/no-extraneous-dependencies, prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import * as React from 'react'
import * as sinon from 'sinon'

import { fireEvent, lang, render, userEvent, waitFor } from '../../common.spec'
import { api } from '../../tools'
import SignIn from './SignIn'

describe('<SignIn />', () => {
  afterEach(() => {
    sinon.restore()
  })

  it('must render correctly', () => {
    const { getAllByRole, getByRole } = render(<SignIn open />)
    expect(getByRole('heading')).to.have.text(lang.signInTitle())
    expect(getAllByRole('button')).to.have.length(2)
  })

  it('must close window if value is set', async () => {
    const close = sinon.spy()
    const signIn = sinon.stub(api.user, 'signIn')
    const { getByLabelText, getAllByRole } = render(<SignIn open onClose={close} />)
    const email = getByLabelText(new RegExp(`^${lang.email()}`))
    const password = getByLabelText(new RegExp(`^${lang.password()}`))
    const buttons = getAllByRole('button')
    const submit = buttons[buttons.length - 1]
    expect(submit).to.have.attribute('disabled')
    expect(close).to.not.have.been.called
    await userEvent.type(email, 'user@example.com')
    expect(fireEvent.blur(email), 'Error loosing focus for e-mail').to.be.true
    await userEvent.type(password, 'verysecretpassword')
    expect(fireEvent.blur(password), 'Error loosing focus for password').to.be.true
    await waitFor(() => expect(submit).to.not.have.attribute('disabled'))
    userEvent.click(submit)
    await waitFor(() => expect(close).to.have.been.calledOnce)
    await waitFor(() =>
      expect(signIn).to.have.been.calledOnceWith('user@example.com', 'verysecretpassword')
    )
  })
})
