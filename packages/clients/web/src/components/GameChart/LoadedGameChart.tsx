/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as React from 'react'
import { FunctionComponent } from 'react'
import {
  Bar,
  BarChart,
  CartesianGrid,
  Label,
  ReferenceLine,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts'

/**
 * The type for the data.
 */
export interface DataType {
  name: string
  score: number
}

/**
 * The type for the statistics.
 */
export interface StatType {
  mean: number
  meanStandardDeviation: number
}

export interface LoadedGameChartProps {
  data: DataType[]
  stats: StatType
  labels: {
    mean: string
    deviation: string
  }
  colors: {
    grid: string
    score: string
    mean: string
    deviation: string
  }
}

/**
 * This component displays a chart for game set. It is used directly, so the components are supposed being
 * already loaded.
 */
/* (JSX element) */
const LoadedGameChart: FunctionComponent<LoadedGameChartProps> = ({ data, stats, labels, colors }) => (
  <ResponsiveContainer aspect={4} width="100%">
    <BarChart data={data}>
      <XAxis dataKey="name" />
      <YAxis />
      <YAxis yAxisId={1} orientation="right" unit="%" type="number" domain={[0, 100]} />
      <Tooltip />
      <CartesianGrid stroke={colors.grid} />
      <Bar dataKey="score" barSize={20} fill={colors.score} />
      <ReferenceLine y={stats.mean} stroke={colors.mean}>
        <Label value={labels.mean} />
      </ReferenceLine>
      <ReferenceLine
        y={stats.meanStandardDeviation}
        yAxisId={1}
        stroke={colors.deviation}
        strokeDasharray="3 3"
      >
        <Label value={labels.deviation} />
      </ReferenceLine>
    </BarChart>
  </ResponsiveContainer>
)

export default LoadedGameChart
