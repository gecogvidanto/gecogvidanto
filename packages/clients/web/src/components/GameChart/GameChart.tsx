/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { CircularProgress } from '@material-ui/core'
import * as React from 'react'
import { FunctionComponent, Suspense, lazy } from 'react'

export type DataType = import('./LoadedGameChart').DataType
export type StatType = import('./LoadedGameChart').StatType

export type GameChartProps = import('./LoadedGameChart').LoadedGameChartProps

/**
 * This component displays a chart for game set.
 */
/* (JSX element) */
const GameChart: FunctionComponent<GameChartProps> = props => {
  const LoadedGameChart = lazy(() => import(/* webpackChunkName: "chart" */ './LoadedGameChart'))
  return (
    <Suspense fallback={<CircularProgress />}>
      <LoadedGameChart {...props} />
    </Suspense>
  )
}
export default GameChart
