/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { ChangeEvent, FunctionComponent, useState } from 'react'

import { Validator } from '@gecogvidanto/shared'

import { ValidationEvent, useEventCallback } from '../../hooks'
import { useGameStore, useLangStore } from '../../stores'
import DeletableField, { DeletableFieldProps } from '../DeletableField'
import { buildValidableField } from '../helpers'

const PLAYER_PREFIX = 'player-'

const PlayerDeletableField = buildValidableField<
  DeletableFieldProps,
  HTMLInputElement | HTMLTextAreaElement
>(DeletableField)

export interface UpdatePlayerFieldProps {
  order: number
  readOnly: boolean
  autoFocus: boolean
  validators: Validator[]
  onValidityChange: (order: number, valid: boolean) => void
  onBlur: (order: number) => void
  onDelete: (order: number) => void
}

/**
 * Field used to update a player name.
 */
const UpdatePlayerField: FunctionComponent<UpdatePlayerFieldProps> = observer(
  ({
    order,
    readOnly,
    autoFocus,
    validators,
    onValidityChange,
    onBlur: onBlurPlayer,
    onDelete: onDeletePlayer,
  }) => {
    const { neededGame: game, updatePlayer, deletePlayer } = useGameStore()
    const { lang } = useLangStore()

    // State
    const [name, setName] = useState<string>(game.players[order].name)
    const [valid, setValid] = useState<boolean>(false)

    // Events
    const onChange = useEventCallback((e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
      setName(e.target.value)
    })
    const onValidate = useEventCallback(
      (e: ValidationEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
        if (valid !== e.valid) {
          setValid(e.valid)
          onValidityChange(order, e.valid)
        }
      }
    )
    const onBlur = useEventCallback((): void => {
      if (valid && game.players[order].name !== name) {
        updatePlayer(order, name)
        onBlurPlayer(order)
      }
    })
    const onDelete = useEventCallback((): void => {
      deletePlayer(order)
      onDeletePlayer(order)
    })

    // Render
    return (
      <PlayerDeletableField
        id={PLAYER_PREFIX + order}
        name={PLAYER_PREFIX + order}
        label={lang.pageAddPlayersId(order + 1)}
        ariaLabel={lang.delete()}
        value={name}
        fullWidth
        readOnly={readOnly}
        deleteDisabled={readOnly}
        autoFocus={autoFocus}
        validators={validators}
        onChange={onChange}
        onValidate={onValidate}
        onBlur={onBlur}
        onDelete={onDelete}
      />
    )
  }
)

export default UpdatePlayerField
