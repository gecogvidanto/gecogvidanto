/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { messages as sharedMessages } from '@gecogvidanto/client'

/**
 * The messages for the shared library.
 */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
export const messages = {
  ...sharedMessages,
  backToSiteHome: 'Back to the site home page',
  cancel: 'Cancel',
  close: 'Close',
  closeMenu: 'Close menu',
  delete: 'Delete',
  economicSystem: 'Economic system',
  email: 'e-mail',
  formDying: (name: string) => `${name} is dying at this round`,
  gameClosed: 'Game closed',
  gameScoreSet: (name: string) => `Score recorded for player ${name}`,
  gameTechBreakRecorded: 'New technological break recorded',
  loading: 'Loading…',
  logged: (name: string) => `You are now logged in as ${name}`,
  loggedOut: 'You are now logged out',
  menuAction: 'Action',
  menuActionTechBreak: 'Technological break',
  menuActionTechBreakConfirm: 'Please confirm the technological break',
  menuPlayerAdd: 'Add player',
  minutes: (value: number) => `${value} minutes`,
  notConnected: '(not connected)',
  ok: 'OK',
  openMenu: 'Open menu',
  pageAbout: 'About',
  pageAboutPlugins: 'Installed plugins',
  pageAboutServer: (app: string, name: string | undefined, version: string | undefined) =>
    `${app} server: ${name || '(unknown)'}, version ${version || 'unknown'}`,
  pageAboutVersion: (version: string | undefined) => `Version: ${version || 'unknown'}`,
  pageAddPlayers: 'Add players',
  pageAddPlayersId: (id: number) => `Player ${id}`,
  pageAddPlayersSameName: 'Another player already has this name',
  pageEmailCheck: 'Checking e-mail',
  pageEmailCheckFailed: 'Failed to validate e-mail address',
  pageEmailCheckPleaseWait: 'Validating your e-mail address, please wait…',
  pageEmailCheckSucceeded: 'e-mail successfully checked — You can now sign in with the new e-mail',
  pageGameAddPlayers: 'Add players',
  pageGameChartIncludeNpc: 'Include non playing characters',
  pageGameChartMean: 'Mean',
  pageGameChartRelDeviation: 'Deviation/mean',
  pageGameChartStdDeviation: 'Standard deviation',
  pageGameChartTotal: 'Total',
  pageGameClose: 'Close the game',
  pageGameMaster: (name: string) => `Game mastered by ${name}`,
  pageGameMasterThanks: (key: string) => `Thank her/him with Ğ1 gift using her/his public key ${key}`,
  pageGameNoPlayers: 'There is currently no player in the game',
  pageGamePlay: 'Play the game',
  pageGamePlayers: 'Players',
  pageGamePlayersLimit: (start: boolean, roundSet: number, round: number) =>
    `${start ? 'from' : 'until'} round ${round + 1} of set ${roundSet + 1}`,
  pageGameSet: (index: number, ecoSysName: string) => `Set #${index + 1}: ${ecoSysName}`,
  pageGameSetInProgress: 'Set in progress',
  pageGameSummary: 'Summary',
  pageGameViewMoney: 'View money help sheet',
  pageGameViewValues: 'View values help sheet',
  pageHome: 'Home',
  pageIndex: 'Games',
  pageIndexDate: 'Date',
  pageIndexLocation: 'Location',
  pageIndexNoGame: 'No game in database',
  pageIndexPlayers: '# players',
  pageIndexSets: '# sets',
  pageNewGame: 'New game',
  pageNewGameLocationAddress: 'Address',
  pageNewGameLocationName: 'Location name',
  pageNewGameRoundLength: 'Round length',
  pageNewGameRoundsPerSet: 'Rounds per set',
  pageNotFound: 'Page not found',
  pageNotFoundLost: 'It seems you got tied up in the maze of money creation.',
  pagePlayStatsCardGame: 'Card game',
  pagePlayStatsFour: 'Four of kind',
  pagePlayStatsPlayers: 'Players',
  pageProfile: 'Profile',
  pageProfileAccount: 'Account public key',
  pageProfileDataSent: 'Data sent to server',
  pageProfileEmailDuplicate: 'This e-mail is already used by another user',
  pageProfileLevel: 'Level of certification',
  pageProfileName: 'Name or pseudonym',
  pageProfileNameDuplicate: 'This name is already used by another user',
  pageProfilePasswordCheck: 'Type password again',
  pageProfilePasswordReset: 'Reset password',
  pageProfileSignUp: (app: string) => `Sign-up as ${app} game master`,
  pageRound: (round: number, total: number) =>
    round ? `Round ${round} of ${total} (${(round * 80) / total} years)` : 'Initialization round',
  pageRoundPreview: 'Prepare help sheets',
  password: 'Password',
  pause: 'Pause',
  playerExit: 'Leaving player',
  playerExitConfirm: (name: string) => `Please confirm that ${name} is leaving the game.`,
  playerExitSelect: 'Select the leaving player',
  refresh: 'Refresh',
  removeAll: 'Remove all',
  setCreate: 'Create set',
  setNew: 'New set',
  signIn: 'Sign in',
  signInTitle: 'Sign in',
  signOut: 'Sign out',
  signUp: 'Sign up',
  start: 'Start',
  unchanged: '(unchanged)',
  unknownError: 'Unknown error',
  userCheckEmail: 'Please check your mailbox for e-mail confirmation',
  userCreated: (name: string) => `User ${name} successfully created`,
  userUpdateSuccess: (name: string) => `User ${name} successfully updated`,
  _displayCertLevel: (level: keyof typeof import('@gecogvidanto/shared').CertLevel): string => {
    switch (level) {
      case 'Administrator':
        return 'Administrator'
      case 'GameMaster':
        return 'Game master'
      default:
        // eslint-disable-next-line no-case-declarations
        const l: never = level
        throw new Error(`Unknown level ${l}`)
    }
  },
  _displayDate: (date: Date) => {
    const hour = ((date.getHours() + 11) % 12) + 1
    const ap = date.getHours() < 12 ? 'am' : 'pm'
    return `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()} ${hour}:${(
      '0' + date.getMinutes()
    ).slice(-2)} ${ap}`
  },
}
/* eslint-enable @typescript-eslint/explicit-module-boundary-types */

/**
 * The type for the messages.
 */
export type langType = typeof messages
