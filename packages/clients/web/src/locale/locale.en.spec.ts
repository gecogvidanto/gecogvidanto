/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import { messages } from './ui.locale.en'

describe('ui.locale.en', () => {
  it('must be ordered', () => {
    const prefixes: string[] = []
    let previous: string | undefined

    for (const key in messages) {
      const idx = key.lastIndexOf('$')
      const prefix = key.substring(0, idx + 1)
      const msgKey = key.substring(idx + 1)

      if (prefixes.length === 0 || prefix !== prefixes[prefixes.length - 1]) {
        expect(prefixes.includes(prefix), `${key} is not ordered`).to.be.false
        previous = undefined
        prefixes.push(prefix)
      }

      if (previous) {
        if (msgKey.startsWith('_') !== previous.startsWith('_')) {
          expect(msgKey.startsWith('_'), `${key} is not ordered`).to.be.true
        } else {
          expect(msgKey > previous, `${key} is not ordered`).to.be.true
        }
      }
      previous = msgKey
    }
  })
})
