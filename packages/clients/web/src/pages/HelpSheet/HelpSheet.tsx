/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import * as React from 'react'
import { FunctionComponent, useEffect, useRef, useState } from 'react'
import { useParams } from 'react-router'

import { HelpSheet as HelpSheetDefinition } from '@gecogvidanto/shared'

import { HelpSheet as HelpSheetComponent } from '../../components'
import { WebSocket } from '../../tools'

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    width: '100vw',
    height: '100vh',
    padding: theme.spacing(1),
  },
}))

/**
 * @returns Display the help sheets.
 */
const HelpSheet: FunctionComponent = () => {
  const classes = useStyles()
  const params = useParams<{ id: string; 0: 'values' | 'money' }>() as { id: string; 0: 'values' | 'money' }
  const socket = useRef<WebSocket>()

  // State
  const [helpSheet, setHelpSheet] = useState<HelpSheetDefinition | undefined>()
  const [costFactor, setCostFactor] = useState<number>(0)

  // Manage WebSocket connection
  useEffect(() => {
    const event: 'valuesHelpSheet' | 'moneyHelpSheet' =
      params[0] === 'values' ? 'valuesHelpSheet' : 'moneyHelpSheet'

    socket.current = new WebSocket()
    socket.current.onConnect(() => {
      socket.current && socket.current.emit('register', params.id, event)
    })
    socket.current.on('valuesHelpSheet', (newHelpSheet, newCostFactor) => {
      setHelpSheet(newHelpSheet)
      setCostFactor(newCostFactor)
    })
    socket.current.on('moneyHelpSheet', newHelpSheet => {
      setHelpSheet(newHelpSheet)
      setCostFactor(1)
    })
    return () => {
      socket.current && socket.current.close()
      socket.current = undefined
    }
  }, [params])

  // Render
  const type: 'values' | 'money' = params[0]
  return (
    <div className={classes.container}>
      <HelpSheetComponent type={type} helpSheet={helpSheet} costFactor={costFactor} />
    </div>
  )
}

export default HelpSheet
