/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Avatar, Theme, Typography } from '@material-ui/core'
// eslint-disable-next-line import/no-internal-modules
import { indigo, yellow } from '@material-ui/core/colors'
import ImportExportIcon from '@material-ui/icons/ImportExport'
import StorageIcon from '@material-ui/icons/Storage'
import { makeStyles } from '@material-ui/styles'
import classNames from 'classnames'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { Center, NewLineJoined } from '../../../components'
import EconomicSystemName from '../EconomicSystemName'

const useStyles = makeStyles((theme: Theme) => ({
  avatar: {
    margin: theme.spacing(1),
  },
  database: {
    backgroundColor: indigo[500],
    color: 'white',
  },
  ecoSys: {
    backgroundColor: yellow[300],
    color: 'black',
  },
}))

export interface PluginOptionsProps {
  database: boolean
  ecoSysIds: ReadonlyArray<string>
}

/**
 * Displaying options of a plugin.
 */
/* (JSX element) */
const PluginOptions: FunctionComponent<PluginOptionsProps> = ({ database, ecoSysIds }) => {
  const classes = useStyles()

  // Render
  if (database) {
    return (
      <Center>
        <Avatar className={classNames(classes.avatar, classes.database)}>
          <StorageIcon />
        </Avatar>
      </Center>
    )
  } else if (ecoSysIds.length) {
    return (
      <>
        <Center>
          <Avatar className={classNames(classes.avatar, classes.ecoSys)}>
            <ImportExportIcon />
          </Avatar>
        </Center>
        <Typography>
          <NewLineJoined>
            {...ecoSysIds.map(id => <EconomicSystemName key={id} identifier={id} />)}
          </NewLineJoined>
        </Typography>
      </>
    )
  } else {
    return null
  }
}

export default PluginOptions
