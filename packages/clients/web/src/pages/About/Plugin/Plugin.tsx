/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Card, CardContent, CardHeader, Theme, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { useObserver } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { useLangStore } from '../../../stores'
import { ServerInfo } from '../../../tools'
import PluginOptions from '../PluginOptions'

const useStyles = makeStyles((theme: Theme) => ({
  card: {
    display: 'flex',
    width: 440,
    margin: theme.spacing(2),
  },
  mainContent: {
    display: 'flex',
    flexDirection: 'column',
    width: 320,
  },
  optionContent: {
    display: 'flex',
    flexDirection: 'column',
    flex: 'none',
    width: 120,
    paddingTop: theme.spacing(2),
    paddingRight: theme.spacing(3),
    paddingBottom: theme.spacing(3),
  },
}))

export interface PluginProps {
  plugin: ServerInfo['plugins'][number]
}

/**
 * A plugin card.
 */
/* (JSX element) */
const Plugin: FunctionComponent<PluginProps> = ({ plugin }) => {
  const classes = useStyles()
  const langStore = useLangStore()

  // Render
  return (
    <Card className={classes.card}>
      <div className={classes.mainContent}>
        <CardHeader title={plugin.name} />
        <CardContent>
          <Typography color="textSecondary">{plugin.module}</Typography>
          <Typography color="textSecondary">
            {useObserver(() => langStore.lang.pageAboutVersion(plugin.version))}
          </Typography>
          <Typography>{plugin.description}</Typography>
        </CardContent>
      </div>
      <div className={classes.optionContent}>
        <PluginOptions database={plugin.database} ecoSysIds={plugin.ecoSysIds} />
      </div>
    </Card>
  )
}

export default Plugin
