/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Paper, Theme, Typography } from '@material-ui/core'
import { makeStyles, useTheme } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { EconomicSystemNameCache, useNewGameMenu } from '../../../hooks'
import { useLangStore, useSdataStore } from '../../../stores'
import Plugin from '../Plugin'

const useStyles = makeStyles((theme: Theme) => ({
  paper: {
    padding: theme.spacing(1),
  },
  container: {
    display: 'flex',
    flexFlow: 'row wrap',
  },
}))

/**
 * The about page, displaying server information.
 */
const About: FunctionComponent = observer(() => {
  const classes = useStyles()
  const theme = useTheme<Theme>()
  const { lang } = useLangStore()
  const { serverInfo } = useSdataStore()
  const { applicationName: appName } = theme.gecogvidanto
  useNewGameMenu()

  // Render
  const plugins: JSX.Element[] = serverInfo.plugins.map(plugin => (
    <Plugin key={plugin.module} plugin={plugin} />
  ))
  return (
    <Paper className={classes.paper}>
      <Typography variant="h5" gutterBottom>
        {lang.pageAbout()}
      </Typography>
      <Typography gutterBottom>
        {lang.pageAboutServer(appName, serverInfo.name, serverInfo.version)}
      </Typography>
      <Typography variant="h6" gutterBottom>
        {lang.pageAboutPlugins()}
      </Typography>
      <EconomicSystemNameCache>
        <div className={classes.container}>{...plugins}</div>
      </EconomicSystemNameCache>
    </Paper>
  )
})

export default About
