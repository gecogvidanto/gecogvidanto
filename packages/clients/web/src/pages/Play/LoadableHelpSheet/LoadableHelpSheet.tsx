/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { makeStyles } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, useCallback } from 'react'

import { EconomicSystem, HelpSheet as HelpSheetDefinition } from '@gecogvidanto/shared'

import { HelpSheet } from '../../../components'
import { useFetcher } from '../../../hooks'
import { useGameStore } from '../../../stores'
import { api } from '../../../tools'

const useStyles = makeStyles({
  container: {
    position: 'relative',
    height: 0,
    width: '100%',
    padding: 0,
    paddingBottom: '56.74%',
  },
  image: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    left: 0,
    top: 0,
  },
})

export interface LoadableHelpSheetProps {
  type: 'values' | 'money'
}

/**
 * @returns Wrapper for the help sheet components which loads the help sheet.
 */
const LoadableHelpSheet: FunctionComponent<LoadableHelpSheetProps> = observer(({ type }) => {
  const classes = useStyles()
  const { neededGame: game } = useGameStore()

  // Cost factor
  const costFactorFetcher = useCallback(async (): Promise<number> => {
    const { roundSets } = game
    const lastSet = roundSets[roundSets.length - 1]
    const ecoSys: EconomicSystem<any> = await api.economicSystem.read(lastSet.ecoSysId)
    return ecoSys.valueCost * Math.pow(2, lastSet.techBreakRounds.length)
  }, [game])
  const [costFactor] = useFetcher(
    type === 'values' && game.roundSets.length ? costFactorFetcher : undefined,
    0
  )

  // Help sheet
  const helpSheetFetcher = useCallback((): Promise<HelpSheetDefinition> => {
    switch (type) {
      case 'values':
        return api.game.readValuesHelpSheet(game)
      case 'money':
        return api.game.readMoneyHelpSheet(game)
      default:
        // eslint-disable-next-line no-case-declarations
        const sType: never = type
        throw new Error(`Unknown help sheet type ${sType}`)
    }
  }, [game, type])
  const [helpSheet] = useFetcher(helpSheetFetcher)

  // Render
  return (
    <div className={classes.container}>
      <HelpSheet type={type} helpSheet={helpSheet} costFactor={costFactor} className={classes.image} />
    </div>
  )
})

export default LoadableHelpSheet
