/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Button, Theme, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import classNames from 'classnames'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { Center } from '../../../components'
import { useEventCallback } from '../../../hooks'
import { useGameStore, useLangStore } from '../../../stores'
import { Step, goToStep, usePlayReducer } from '../Play'

const useStyles = makeStyles((theme: Theme) => ({
  submit: {
    ...theme.gecogvidanto.form.style.submit,
  },
  timer: {
    marginTop: '-0.3em',
    marginBottom: '-0.3em',
    fontSize: '30vw',
  },
  '@keyframes blink': {
    from: { opacity: 0 },
    to: { opacity: 1 },
  },
  paused: {
    animation: 'blink 1s ease-in-out 0s infinite alternate',
  },
}))

export interface TimeProps {
  blink?: boolean
  responsive?: boolean
  showAction?: boolean
}

/**
 * A component used to display a time in minutes and seconds.
 */
const Time: FunctionComponent<TimeProps> = observer(
  ({ blink = false, responsive = false, showAction = false }) => {
    const classes = useStyles()
    const { lang } = useLangStore()
    const { neededGame: game } = useGameStore()
    const [state, dispatch] = usePlayReducer()

    // Calculate displayed time
    const time = state.remaining === undefined ? game.roundLength * 60 : state.remaining
    const seconds = ('0' + (time % 60)).slice(-2)
    const minutes = Math.floor(time / 60)

    // Action
    const onAction = useEventCallback(() => {
      if (state.step === Step.Pause) {
        dispatch(goToStep(Step.Running, Date.now() + time * 1000))
      } else if (state.step === Step.Running) {
        dispatch(time === 0 ? goToStep(Step.InterRound) : goToStep(Step.Pause))
      }
    })
    const button: JSX.Element | undefined = showAction ? (
      <Center>
        <Button onClick={onAction} variant="contained" color="primary" className={classes.submit}>
          {state.step === Step.Pause ? lang.start() : time === 0 ? lang.ok() : lang.pause()}
        </Button>
      </Center>
    ) : undefined

    // Render
    const className = classNames(responsive && classes.timer, blink && classes.paused)
    return (
      <>
        <Center>
          <Typography className={className} variant={responsive ? undefined : 'h1'}>
            {`${minutes}:${seconds}`}
          </Typography>
        </Center>
        {button}
      </>
    )
  }
)

export default Time
