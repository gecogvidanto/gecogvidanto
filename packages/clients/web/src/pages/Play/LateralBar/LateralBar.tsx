/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Divider, Drawer, Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { CertLevel } from '@gecogvidanto/shared'

import { Loading } from '../../../components'
import { useGameStore, useUserStore } from '../../../stores'
import LoadableHelpSheet from '../LoadableHelpSheet'
import NewGameStats from '../NewGameStats'
import { Step, usePlayReducer } from '../Play'
import PlayerList from '../PlayerList'
import Time from '../Time'

const useStyles = makeStyles((theme: Theme) => ({
  drawer: {
    height: '100vh',
    width: theme.gecogvidanto.menuWidth,
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
  },
  content: {
    overflowX: 'hidden',
    overflowY: 'auto',
  },
  toolbar: theme.mixins.toolbar,
}))

/**
 * Play game bar.
 */
const LateralBar: FunctionComponent = observer(() => {
  const classes = useStyles()
  const { game } = useGameStore()
  const { logged } = useUserStore()
  const [state] = usePlayReducer()

  // Main content
  const content: JSX.Element[] = []
  if (
    game &&
    logged &&
    (logged.level === CertLevel.Administrator ||
      (logged.level === CertLevel.GameMaster && logged._id === game.masterId))
  ) {
    if (state.formOption) {
      content.push(
        <Time
          key="time"
          blink={state.step === Step.Pause || (!!state.remaining && state.remaining === 0)}
          showAction
        />,
        <PlayerList key="players" />,
        <LoadableHelpSheet key="values" type="values" />,
        <LoadableHelpSheet key="money" type="money" />
      )
    } else if (game.roundSets.length === 0) {
      content.push(<NewGameStats key="stats" playerCount={game.players.length} />)
    } else {
      switch (state.step) {
        case Step.InterRound:
          content.push(<PlayerList key="players" canDelete />)
          if (game.roundSets[game.roundSets.length - 1].currentRound > 0) {
            content.push(
              <LoadableHelpSheet key="values" type="values" />,
              <LoadableHelpSheet key="money" type="money" />
            )
          }
          break
        case Step.RoundPreview:
          content.push(<PlayerList key="players" />, <Time key="time" blink />)
          break
        default:
          content.push(
            <LoadableHelpSheet key="values" type="values" />,
            <LoadableHelpSheet key="money" type="money" />,
            <PlayerList key="players" canDelete />
          )
      }
    }
  } else {
    content.push(<Loading key="loading" />)
  }

  // Render
  const allNodes: JSX.Element[] = [<Divider key={0} />]
  content.forEach(node => {
    allNodes.push(node)
    allNodes.push(<Divider key={allNodes.length} />)
  })
  return (
    <Drawer
      classes={{
        paper: classes.drawer,
      }}
      variant="permanent"
      anchor="right"
    >
      <div className={classes.toolbar} />
      <div className={classes.content}>{allNodes}</div>
    </Drawer>
  )
})

export default LateralBar
