/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  List,
} from '@material-ui/core'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, useCallback } from 'react'

import { useGameStore, useLangStore } from '../../../stores'
import { DialogType, openDialog, usePlayReducer } from '../Play'
import PlayerItem from './PlayerItem'

/**
 * A dialog used to select a leaving player.
 */
const PlayerExit: FunctionComponent = observer(() => {
  const { game } = useGameStore()
  const { lang } = useLangStore()
  const [state, dispatch] = usePlayReducer()

  // Events
  const onClose = useCallback(() => dispatch(openDialog()), [dispatch])

  // Render
  const playerNames: Array<{ id: number; name: string }> = game
    ? game.players
        .map((player, index) => ({ ...player, id: index }))
        .filter(player => player.roundLeft === 0)
        .map(player => ({ id: player.id, name: player.name }))
    : []
  const playerMenu: JSX.Element[] = playerNames.map(player => (
    <PlayerItem key={player.id} id={player.id} name={player.name} />
  ))
  return (
    <Dialog
      open={state.dialog.type === DialogType.PlayerExit}
      onClose={onClose}
      aria-labelledby="del-player-title"
      aria-describedby="del-player-description"
    >
      <DialogTitle id="add-player-title">{lang.playerExit()}</DialogTitle>
      <DialogContent>
        <DialogContentText id="del-player-description">{lang.playerExitSelect()}</DialogContentText>
        <List dense>{...playerMenu}</List>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary">
          {lang.cancel()}
        </Button>
      </DialogActions>
    </Dialog>
  )
})

export default PlayerExit
