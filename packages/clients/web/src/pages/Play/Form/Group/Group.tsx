/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import * as React from 'react'
import { Fragment, FunctionComponent, useCallback } from 'react'

import { DynField, FieldGroup, InitFunction, UpdateFunction } from '@gecogvidanto/shared'

import { langType } from '../../../../locale'
import { isLinkedFields } from '../../../../tools'
import Field from '../Field'
import LinkedFields from '../LinkedFields'

const useStyles = makeStyles((theme: Theme) => ({
  separatorSpace: {
    marginTop: theme.spacing(3),
  },
  separator: {
    height: 0,
    padding: 0,
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1),
    marginLeft: theme.spacing(1),
    borderStyle: 'solid',
    borderWidth: 1,
  },
}))

export interface GroupProps {
  dynFields: DynField[]
  player: number
  group: FieldGroup<langType>
  groupIndex: number
  updateForm: (InitFunction & UpdateFunction) | undefined
}

/**
 * Component used to display a group of a game form.
 */
/* (JSX element) */
const Group: FunctionComponent<GroupProps> = ({ dynFields, player, group, groupIndex, updateForm }) => {
  const classes = useStyles()

  // Events
  const onFieldChange = useCallback(
    (field: DynField, previous: unknown): void => {
      updateForm && updateForm(field, previous)
    },
    [updateForm]
  )

  // Render
  const groupNodes: JSX.Element[] = []
  if (groupIndex !== 0) {
    groupNodes.push(<div key="space" className={classes.separatorSpace} />)
  }
  if (group.title) {
    groupNodes.push(
      <Field
        key={'!' + group.title.name}
        player={player}
        field={group.title}
        titled
        dynFields={dynFields}
        fieldChanged={onFieldChange}
      />
    )
  } else if (groupIndex !== 0) {
    groupNodes.push(<div key="separator" className={classes.separator} />)
  }
  group.fields.forEach((field, index) => {
    if (isLinkedFields(field)) {
      groupNodes.push(
        <LinkedFields
          key={index}
          dynFields={dynFields}
          player={player}
          fields={field}
          onFieldChange={onFieldChange}
        />
      )
    } else {
      groupNodes.push(
        <Field
          key={'!' + field.name}
          player={player}
          field={field}
          dynFields={dynFields}
          fieldChanged={onFieldChange}
        />
      )
    }
  })
  return <Fragment key={groupIndex}>{groupNodes}</Fragment>
}

export default Group
