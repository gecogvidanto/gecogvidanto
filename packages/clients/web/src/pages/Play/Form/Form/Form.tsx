/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Button, Paper, Theme, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import classNames from 'classnames'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, SyntheticEvent, useCallback, useEffect, useRef, useState } from 'react'

import {
  DynField,
  Form as FormDescription,
  FormFieldIdentifier,
  InitFunction,
  UpdateFunction,
} from '@gecogvidanto/shared'

import { Waiting } from '../../../../components'
import { useFetcher } from '../../../../hooks'
import { langType } from '../../../../locale'
import { useGameStore, useLangStore } from '../../../../stores'
import { api, isLinkedFields, notifyError } from '../../../../tools'
import {
  Step,
  clearEmphasisedPlayers,
  emphasisePlayers,
  formClose,
  goToStep,
  usePlayReducer,
} from '../../Play'
import AnonymousPart from '../AnonymousPart'
import PlayerPart from '../PlayerPart'

/**
 * A class used to find a field, using a cache for already searched fields.
 */
class FieldFinder {
  /**
   * The cache contains index of the field based on a built key. The number is negative for a non found field.
   */
  private readonly cache: Map<string, number> = new Map()

  /**
   * Create the field finder.
   *
   * @param dynFields - The dynamic fields.
   */
  public constructor(private readonly dynFields: DynField[]) {}

  /**
   * Create a finder function.
   *
   * @returns The function.
   */
  public get finder(): (player: number, name: string) => (callback: (field: DynField) => void) => void {
    return (player, name) => {
      // Create key
      let key = name + '$'
      if (isFinite(player)) {
        key += player
      }
      key += '$'

      // Search matching index
      let index: number
      if (this.cache.has(key)) {
        index = this.cache.get(key)!
      } else {
        index = this.dynFields.findIndex(field => field.player === player && field.name === name)
        this.cache.set(key, index)
      }

      // Create field function
      return callback => {
        if (index >= 0) {
          callback(this.dynFields[index])
        }
      }
    }
  }
}

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    ...theme.gecogvidanto.form.style.container,
    maxWidth: theme.gecogvidanto.form.pageWidth,
  },
  form: {
    ...theme.gecogvidanto.form.style.form,
  },
  partContent: {
    display: 'flex',
    flexDirection: 'column',
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(3),
  },
  anonymousPartContent: {
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    marginBottom: theme.spacing(2),
  },
  submit: {
    ...theme.gecogvidanto.form.style.submit,
  },
}))

/**
 * Component used to display a game form. When this component is used:
 * - a game must be loaded.
 */
const GameForm: FunctionComponent = observer(() => {
  const classes = useStyles()
  const { lang } = useLangStore()
  const { neededGame: game, sendForm } = useGameStore()
  const [state, dispatch] = usePlayReducer()
  const dynFields = useRef<DynField[]>([])
  const updateForm = useRef<InitFunction & UpdateFunction>()

  // Manage first execution
  const [firstExecution, setFirstExecution] = useState(true)
  useEffect(() => {
    setFirstExecution(false)
  }, [])

  // Fetch form description
  const playerLeftCount = game.players.filter(player => player.roundLeft !== 0).length
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const fetchForm = useCallback(() => api.game.readForm(game, state.formOption), [
    game,
    playerLeftCount, // Reload if player left count change
    state.formOption,
  ])
  const [loadedForm, { error }] = useFetcher<FormDescription<langType, any>>(
    firstExecution ? undefined : fetchForm
  )
  useEffect(() => {
    if (error) {
      notifyError(error, lang)
    }
  }, [error, lang])

  // Form validation
  const [sending, setSending] = useState(false)
  const onValid = useCallback(
    (e: SyntheticEvent<any>): void => {
      e.preventDefault()
      ;(async (): Promise<void> => {
        if (!sending) {
          setSending(true)
          try {
            const data: {
              [key: string]: string | number | boolean
            } = dynFields.current
              .filter(field => field.hasData)
              .reduce(
                (previous, field) => ({
                  ...previous,
                  [new FormFieldIdentifier(field.player, field.name).identifier!]: field.content,
                }),
                {}
              )
            await sendForm(data, state.formOption)
            // Either close special form or go to next step (INTER_ROUND -> PREVIEW)
            dispatch(state.formOption ? formClose() : goToStep(Step.RoundPreview))
          } catch {
            setSending(false)
          }
        }
      })()
    },
    [dispatch, sendForm, sending, state.formOption]
  )

  // Manage dynamic form
  useEffect(() => {
    // Update a text field
    function updateTextField(
      player: number,
      {
        type,
        name,
        content,
      }: {
        type: string
        name: string
        content: unknown
      }
    ): void {
      if (type === 'text') {
        const dynField = dynFields.current.find(field => field.player === player && field.name === name)
        if (dynField) {
          dynField.content = content
        }
      }
    }

    if (loadedForm && loadedForm.dynFunction) {
      // Load dynamic function
      updateForm.current = loadedForm.dynFunction(dynFields.current, loadedForm.params, {
        forField: new FieldFinder(dynFields.current).finder,
      })

      // Update all text fields
      loadedForm!.parts.forEach(part =>
        part.groups.forEach(group => {
          if (group.title) {
            updateTextField(part.player, group.title)
          }
          group.fields.forEach(fieldOrLinked => {
            if (isLinkedFields(fieldOrLinked)) {
              fieldOrLinked.forEach(field => updateTextField(part.player, field))
            } else {
              updateTextField(part.player, fieldOrLinked)
            }
          })
        })
      )

      // Update the form
      updateForm.current()
    }
    return () => {
      updateForm.current = undefined
    }
  }, [loadedForm])

  // Manage emphasised players
  useEffect(() => {
    if (loadedForm) {
      dispatch(
        emphasisePlayers(
          loadedForm.parts
            .filter(part => part.player >= 0)
            .filter(part => !part.groups.every(group => !group.mandatory))
            .map(part => part.player!)
        )
      )
    }
    return () => {
      dispatch(clearEmphasisedPlayers())
    }
  }, [dispatch, loadedForm])

  // Render
  const currentRound: number = game.roundSets[game.roundSets.length - 1].currentRound
  const anonymousPartClassName = classNames(classes.partContent, classes.anonymousPartContent)
  const formContent: JSX.Element[] = loadedForm
    ? loadedForm.parts.map(part =>
        isFinite(part.player) ? (
          <PlayerPart
            key={part.player}
            dynFields={dynFields.current}
            part={part}
            optional={part.groups.every(group => !group.mandatory)}
            panelClassName={classes.partContent}
            updateForm={updateForm.current}
          />
        ) : (
          <AnonymousPart
            key="all"
            dynFields={dynFields.current}
            part={part}
            className={anonymousPartClassName}
            updateForm={updateForm.current}
          />
        )
      )
    : []
  return (
    <>
      <Typography variant="h5" gutterBottom>
        {lang.pageRound(currentRound, game.roundsPerSet)}
      </Typography>
      <Paper className={classes.container}>
        <Waiting active={!loadedForm}>
          <form className={classes.form}>
            {formContent}
            <Button
              type="submit"
              disabled={sending}
              onClick={onValid}
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              {lang.ok()}
            </Button>
          </form>
        </Waiting>
      </Paper>
    </>
  )
})

export default GameForm
