/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Paper } from '@material-ui/core'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { DynField, FormPart, InitFunction, UpdateFunction } from '@gecogvidanto/shared'

import { langType } from '../../../../locale'
import Group from '../Group'

interface AnonymousPartProps {
  dynFields: DynField[]
  part: FormPart<langType>
  className: string
  updateForm: (InitFunction & UpdateFunction) | undefined
}

/**
 * Component used to display a game form.
 */
/* (JSX element) */
const AnonymousPart: FunctionComponent<AnonymousPartProps> = ({
  dynFields,
  part,
  className,
  updateForm,
}) => {
  const groups: JSX.Element[] = part.groups.map((group, index) => (
    <Group
      key={index}
      dynFields={dynFields}
      player={-Infinity}
      group={group}
      groupIndex={index}
      updateForm={updateForm}
    />
  ))

  // Render
  return <Paper className={className}>{...groups}</Paper>
}

export default AnonymousPart
