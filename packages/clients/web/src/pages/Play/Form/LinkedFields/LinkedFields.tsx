/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { makeStyles } from '@material-ui/styles'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { DynField, Field as FieldDescription } from '@gecogvidanto/shared'

import { langType } from '../../../../locale'
import Field from '../Field'

const useStyles = makeStyles({
  linkedField: {
    display: 'flex',
  },
})

export interface LinkedFieldsProps {
  dynFields: DynField[]
  player: number
  fields: ReadonlyArray<FieldDescription<langType>>
  onFieldChange: (field: DynField, previous: unknown) => void
}

/**
 * Linked fields in a form.
 */
/* (JSX element) */
const LinkedFields: FunctionComponent<LinkedFieldsProps> = ({
  dynFields,
  player,
  fields,
  onFieldChange,
}) => {
  const classes = useStyles()
  const linked: JSX.Element[] = fields.map(field => (
    <Field
      key={'!' + field.name}
      player={player}
      field={field}
      dynFields={dynFields}
      fieldChanged={onFieldChange}
    />
  ))
  return <div className={classes.linkedField}>{...linked}</div>
}

export default LinkedFields
