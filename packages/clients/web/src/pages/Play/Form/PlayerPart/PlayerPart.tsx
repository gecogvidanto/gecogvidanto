/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, Typography } from '@material-ui/core'
import ExpandIcon from '@material-ui/icons/ExpandMore'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { DynField, FormPart, InitFunction, UpdateFunction } from '@gecogvidanto/shared'

import { useCharacterNames } from '../../../../hooks'
import { langType } from '../../../../locale'
import { useGameStore } from '../../../../stores'
import Group from '../Group'

export interface PlayerPartProps {
  dynFields: DynField[]
  part: FormPart<langType>
  optional: boolean
  panelClassName: string
  updateForm: (InitFunction & UpdateFunction) | undefined
}

/**
 * A part of a game form.
 */
const PlayerPart: FunctionComponent<PlayerPartProps> = observer(
  ({ dynFields, part, optional, panelClassName, updateForm }) => {
    const { neededGame: game } = useGameStore()
    const playerName = useCharacterNames(game.roundSets.length - 1, part.player)
    const groups: JSX.Element[] = part.groups.map((group, index) => (
      <Group
        key={index}
        dynFields={dynFields}
        player={part.player}
        group={group}
        groupIndex={index}
        updateForm={updateForm}
      />
    ))

    // Render
    return (
      <ExpansionPanel defaultExpanded={!optional}>
        <ExpansionPanelSummary expandIcon={<ExpandIcon />}>
          <Typography variant="h6" color={optional ? 'primary' : 'secondary'}>
            {playerName}
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails className={panelClassName}>{groups}</ExpansionPanelDetails>
      </ExpansionPanel>
    )
  }
)

export default PlayerPart
