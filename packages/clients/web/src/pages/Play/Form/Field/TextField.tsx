/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { TextField as InnerTextField } from '@material-ui/core'
import classNames from 'classnames'
import { useObserver } from 'mobx-react-lite'
import * as React from 'react'
import { ChangeEvent, FunctionComponent } from 'react'

import { UnassembledMessages } from '@gecogvidanto/shared'

import { useEventCallback } from '../../../../hooks'
import { langType } from '../../../../locale'
import { useLangStore } from '../../../../stores'

export interface TextFieldProps {
  outerClasses: { [key: string]: boolean }
  innerClasses: { [key: string]: boolean }
  identifier: string
  titled: boolean
  disabled: boolean
  help: UnassembledMessages<langType>[keyof langType]
  minLength?: number
  maxLength?: number
  content: string
  onValueChange: (value: string) => void
}

/**
 * Component used to display a text field of game form.
 */
/* (JSX element) */
const TextField: FunctionComponent<TextFieldProps> = ({
  outerClasses,
  innerClasses,
  identifier,
  titled,
  disabled,
  help,
  minLength,
  maxLength,
  content,
  onValueChange,
}) => {
  const { assemble } = useLangStore()

  // Events
  const onChange = useEventCallback((e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
    onValueChange(e.target.value)
  })

  // Render
  const inputProps = {
    className: classNames(innerClasses),
    minLength,
    maxLength,
  }
  const commonProps = {
    id: identifier,
    name: identifier,
    className: classNames(outerClasses),
    disabled,
    inputProps,
    label: useObserver(() => assemble(help)),
    value: content,
    onChange,
  }
  return titled ? (
    <InnerTextField {...commonProps} variant="outlined" />
  ) : (
    <InnerTextField {...commonProps} />
  )
}

export default TextField
