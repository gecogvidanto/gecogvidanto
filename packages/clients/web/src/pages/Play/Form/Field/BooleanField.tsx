/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Checkbox, FormControlLabel, Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import classNames from 'classnames'
import { useObserver } from 'mobx-react-lite'
import * as React from 'react'
import { ChangeEvent, FunctionComponent } from 'react'

import { UnassembledMessages } from '@gecogvidanto/shared'

import { useEventCallback } from '../../../../hooks'
import { langType } from '../../../../locale'
import { useLangStore } from '../../../../stores'

const useStyles = makeStyles((theme: Theme) => ({
  titleField: {
    borderRadius: theme.shape.borderRadius,
    borderStyle: 'solid',
    borderWidth: 1,
  },
}))

export interface BooleanFieldProps {
  outerClasses: { [key: string]: boolean }
  innerClasses: { [key: string]: boolean }
  identifier: string
  titled: boolean
  disabled: boolean
  help: UnassembledMessages<langType>[keyof langType]
  content: boolean
  onValueChange: (value: boolean) => void
}

/**
 * Component used to display a field of game form.
 */
/* (JSX element) */
const BooleanField: FunctionComponent<BooleanFieldProps> = ({
  outerClasses,
  innerClasses,
  identifier,
  titled,
  disabled,
  help,
  content,
  onValueChange,
}) => {
  const classes = useStyles()
  const { assemble } = useLangStore()

  // Events
  const onChange = useEventCallback((_e: ChangeEvent<HTMLInputElement>, checked: boolean): void => {
    onValueChange(checked)
  })

  // Render
  const booleanClassNames = classNames(outerClasses, innerClasses, titled && classes.titleField)
  const checkBox: JSX.Element = (
    <Checkbox disabled={disabled} checked={content as boolean} onChange={onChange} />
  )
  return (
    <FormControlLabel
      id={identifier}
      name={identifier}
      className={booleanClassNames}
      control={checkBox}
      label={useObserver(() => assemble(help))}
    />
  )
}

export default BooleanField
