/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { useMemo } from 'react'

import { DynField, Field } from '@gecogvidanto/shared'

import { langType } from '../../../../locale'

export default function useFieldIdentifier(
  dynFields: DynField[],
  player: number,
  field: Field<langType>
): [number, string] {
  return useMemo(() => {
    let fieldIndex: number = dynFields.findIndex(
      dynField => dynField.player === player && dynField.name === field.name
    )
    if (fieldIndex < 0) {
      fieldIndex = dynFields.length
      const dynField = new DynField(player, field)
      dynFields.push(dynField)
    }
    return [fieldIndex, `gecoField-${fieldIndex}`] as [number, string]
  }, [dynFields, field, player])
}
