/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { TextField } from '@material-ui/core'
import classNames from 'classnames'
import { useObserver } from 'mobx-react-lite'
import * as React from 'react'
import { ChangeEvent, FunctionComponent } from 'react'

import { LocalizedOption, UnassembledMessages } from '@gecogvidanto/shared'

import { useEventCallback } from '../../../../hooks'
import { langType } from '../../../../locale'
import { useLangStore } from '../../../../stores'
import SelectChoice from './SelectChoice'

export interface SelectFieldProps {
  outerClasses: { [key: string]: boolean }
  innerClasses: { [key: string]: boolean }
  identifier: string
  titled: boolean
  disabled: boolean
  help: UnassembledMessages<langType>[keyof langType]
  choices: LocalizedOption<langType, keyof langType>[]
  content: string | number | boolean
  onValueChange: (value: string) => void
}

/**
 * Component used to display a field of game form.
 */
/* (JSX element) */
const SelectField: FunctionComponent<SelectFieldProps> = ({
  outerClasses,
  innerClasses,
  identifier,
  titled,
  disabled,
  help,
  choices,
  content,
  onValueChange,
}) => {
  const { assemble } = useLangStore()

  // Events
  const onChange = useEventCallback((e: ChangeEvent<{ value: unknown }>): void => {
    onValueChange(e.target.value as string)
  })

  // Render
  const renderedChoices: JSX.Element[] = choices.map(choice => (
    <SelectChoice key={choice.id} choice={choice} />
  ))
  const selectProps = {
    className: classNames(innerClasses),
  }
  const commonProps = {
    id: identifier,
    name: identifier,
    className: classNames(outerClasses),
    select: true,
    disabled,
    SelectProps: selectProps,
    label: useObserver(() => assemble(help)),
    value: content,
    onChange,
  }
  return titled ? (
    <TextField {...commonProps} variant="outlined">
      {...renderedChoices}
    </TextField>
  ) : (
    <TextField {...commonProps}>{...renderedChoices}</TextField>
  )
}

export default SelectField
