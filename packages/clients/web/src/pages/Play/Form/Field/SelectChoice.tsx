/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { MenuItem } from '@material-ui/core'
import { useObserver } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { LocalizedOption } from '@gecogvidanto/shared'

import { langType } from '../../../../locale'
import { useLangStore } from '../../../../stores'

export interface SelectChoiceProps {
  choice: LocalizedOption<langType, keyof langType>
}

/**
 * A choice of the select component.
 */
/* (JSX element) */
const SelectChoice: FunctionComponent<SelectChoiceProps> = ({ choice }) => {
  const { assemble } = useLangStore()
  return useObserver(() => <MenuItem value={choice.id}>{assemble(choice.description)}</MenuItem>)
}

export default SelectChoice
