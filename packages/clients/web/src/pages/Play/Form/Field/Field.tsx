/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import * as React from 'react'
import { FunctionComponent, useEffect, useState } from 'react'

import { Aspect, DynField, Field as FieldDescription, FieldOptions } from '@gecogvidanto/shared'

import { useEventCallback } from '../../../../hooks'
import { langType } from '../../../../locale'
import BooleanField from './BooleanField'
import useFieldIdentifier from './FieldIdentifier'
import NumberField from './NumberField'
import SelectField from './SelectField'
import Text from './Text'
import TextField from './TextField'

const useStyles = makeStyles((theme: Theme) => ({
  field: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(1),
    marginLeft: theme.spacing(1),
    flexGrow: 1,
  },
  optionHidden: {
    display: 'none',
  },
  optionInvisible: {
    visibility: 'hidden',
  },
  optionInfo: {
    color: theme.palette.type === 'dark' ? theme.palette.success.light : theme.palette.success.dark,
  },
  optionWarning: {
    color: theme.palette.type === 'dark' ? theme.palette.warning.light : theme.palette.warning.dark,
  },
  optionError: {
    color: theme.palette.type === 'dark' ? theme.palette.error.light : theme.palette.error.dark,
  },
  optionRed: {
    backgroundColor: '#B71C1C40',
    color: 'white',
  },
  optionYellow: {
    backgroundColor: '#FFEB3B40',
    color: 'black',
  },
  optionGreen: {
    backgroundColor: '#388E3C40',
    color: 'white',
  },
  optionBlue: {
    backgroundColor: '#0D47A140',
    color: 'white',
  },
}))

export interface FieldProps {
  player: number
  field: FieldDescription<langType>
  titled?: boolean
  dynFields: DynField[]
  fieldChanged: (field: DynField, previous: unknown) => void
}

/**
 * Component used to display a field of game form.
 */
/* (JSX element) */
const Field: FunctionComponent<FieldProps> = ({
  player,
  field,
  titled = false,
  dynFields,
  fieldChanged,
}) => {
  const classes = useStyles()
  const [fieldIndex, identifier] = useFieldIdentifier(dynFields, player, field)

  // State
  const [content, setContent] = useState<any>(dynFields[fieldIndex].content)
  const [options, setOptions] = useState<FieldOptions>(dynFields[fieldIndex].options)

  // Connect to field
  useEffect(() => {
    function onDynChange(): void {
      const { content: newContent, options: newOptions } = dynFields[fieldIndex]
      setContent(newContent)
      setOptions(newOptions)
    }
    dynFields[fieldIndex].on(onDynChange)
    return () => {
      dynFields[fieldIndex].off(onDynChange)
    }
  }, [dynFields, fieldIndex])

  // Events
  const onFieldValueChange = useEventCallback((value: any) => {
    setContent(value)
    const previous = dynFields[fieldIndex].content
    dynFields[fieldIndex].content = value
    fieldChanged(dynFields[fieldIndex], previous)
  })

  // Field classes
  const outerClasses: { [key: string]: boolean } = {
    [classes.field]: true,
    [classes.optionHidden]: !!options.hidden,
    [classes.optionInvisible]: !!options.invisible,
  }
  const innerClasses: { [key: string]: boolean } = {
    [classes.optionInfo]: options.aspect === Aspect.Info,
    [classes.optionWarning]: options.aspect === Aspect.Warning,
    [classes.optionError]: options.aspect === Aspect.Error,
    [classes.optionRed]: options.aspect === Aspect.Red,
    [classes.optionYellow]: options.aspect === Aspect.Yellow,
    [classes.optionGreen]: options.aspect === Aspect.Green,
    [classes.optionBlue]: options.aspect === Aspect.Blue,
  }

  // Render
  const commonProps = {
    outerClasses,
    innerClasses,
    identifier,
    titled,
    disabled: !!options.disabled,
    content,
    onValueChange: onFieldValueChange,
  }
  switch (field.type) {
    case 'text':
      return <Text {...commonProps} />
    case 'text-input':
      return <TextField {...field} {...commonProps} />
    case 'number-input':
      return <NumberField {...field} {...commonProps} />
    case 'boolean-input':
      return <BooleanField {...field} {...commonProps} />
    case 'select-input':
      return <SelectField {...field} {...commonProps} />
    default:
      // eslint-disable-next-line no-case-declarations
      const unknown: never = field
      throw new Error(`Unknown type for field ${unknown}`)
  }
}

export default Field
