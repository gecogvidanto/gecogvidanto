/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { TextField } from '@material-ui/core'
import classNames from 'classnames'
import { useObserver } from 'mobx-react-lite'
import * as React from 'react'
import { ChangeEvent, FunctionComponent } from 'react'

import { UnassembledMessages } from '@gecogvidanto/shared'

import { useEventCallback } from '../../../../hooks'
import { langType } from '../../../../locale'
import { useLangStore } from '../../../../stores'

export interface NumberFieldProps {
  outerClasses: { [key: string]: boolean }
  innerClasses: { [key: string]: boolean }
  identifier: string
  titled: boolean
  disabled: boolean
  help: UnassembledMessages<langType>[keyof langType]
  minValue?: number
  maxValue?: number
  incrementStep?: number
  content: number
  onValueChange: (value: number) => void
}

/**
 * Component used to display a number field of game form.
 */
/* (JSX element) */
const NumberField: FunctionComponent<NumberFieldProps> = ({
  outerClasses,
  innerClasses,
  identifier,
  titled,
  disabled,
  help,
  minValue,
  maxValue,
  incrementStep,
  content,
  onValueChange,
}) => {
  const { assemble } = useLangStore()

  // Events
  const onChange = useEventCallback((e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
    onValueChange(Number(e.target.value))
  })

  // Render
  const inputProps = {
    className: classNames(innerClasses),
    min: minValue,
    max: maxValue,
    step: incrementStep,
  }
  const commonProps = {
    id: identifier,
    name: identifier,
    className: classNames(outerClasses),
    type: 'number',
    disabled,
    inputProps,
    label: useObserver(() => assemble(help)),
    value: Number(content),
    onChange,
  }
  return titled ? <TextField {...commonProps} variant="outlined" /> : <TextField {...commonProps} />
}

export default NumberField
