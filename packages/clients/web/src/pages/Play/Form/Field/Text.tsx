/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Theme, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import classNames from 'classnames'
import { useObserver } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { UnassembledMessages } from '@gecogvidanto/shared'

import { langType } from '../../../../locale'
import { useLangStore } from '../../../../stores'

const useStyles = makeStyles((theme: Theme) => ({
  textField: {
    padding: '5px 0px 6px',
  },
  titleTextField: {
    borderRadius: theme.shape.borderRadius,
    borderStyle: 'solid',
    borderWidth: 1,
    padding: '16px 14px',
  },
}))

export interface TextProps {
  outerClasses: { [key: string]: boolean }
  innerClasses: { [key: string]: boolean }
  identifier: string
  titled: boolean
  content: UnassembledMessages<langType>[keyof langType]
}

/**
 * A text message.
 */
/* (JSX element) */
const Text: FunctionComponent<TextProps> = ({
  outerClasses,
  innerClasses,
  identifier,
  titled,
  content,
}) => {
  const { assemble } = useLangStore()
  const classes = useStyles()
  const textClassName = classNames(
    outerClasses,
    innerClasses,
    titled ? classes.titleTextField : classes.textField
  )

  // Render
  return (
    <Typography id={identifier} className={textClassName}>
      {useObserver(() => assemble(content))}
    </Typography>
  )
}

export default Text
