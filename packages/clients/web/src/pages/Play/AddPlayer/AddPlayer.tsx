/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, useCallback, useState } from 'react'

import { Validator, validators } from '@gecogvidanto/shared'

import { ValidableInput } from '../../../components'
import { ValidationEvent, useEventCallback } from '../../../hooks'
import { useGameStore, useLangStore } from '../../../stores'
import { PlayerSameNameValidator } from '../../../tools'
import { DialogType, openDialog, usePlayReducer } from '../Play'

/**
 * A dialog used to add a player. When this component is used:
 * - a provider must make the play reducer available.
 */
const AddPlayer: FunctionComponent = observer(() => {
  const { game, addPlayer } = useGameStore()
  const { lang } = useLangStore()
  const [state, dispatch] = usePlayReducer()

  // Manage player name
  const [name, setName] = useState<string>()
  const onValueValidate = useCallback(
    (e: ValidationEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
      setName(e.valid ? e.target.value : undefined)
    },
    []
  )

  // Events
  const onClose = useCallback((): void => {
    dispatch(openDialog())
  }, [dispatch])
  const onValid = useEventCallback((): void => {
    if (name) {
      addPlayer(name)
      onClose()
    }
  })

  // Render
  const id: number = game ? game.players.length + 1 : 0
  const addValidators: Validator[] = [
    ...validators.playerName,
    new PlayerSameNameValidator(game ? game.players.map(player => player.name) : []),
  ]
  return (
    <Dialog
      open={state.dialog.type === DialogType.AddPlayer}
      onClose={onClose}
      aria-labelledby="add-player-title"
    >
      <DialogTitle id="add-player-title">{lang.menuPlayerAdd()}</DialogTitle>
      <DialogContent>
        <ValidableInput
          id="player-name"
          name="player-name"
          label={lang.pageAddPlayersId(id)}
          autoFocus
          fullWidth
          validators={addValidators}
          onValidate={onValueValidate}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary">
          {lang.cancel()}
        </Button>
        <Button disabled={!name} onClick={onValid} color="primary" autoFocus>
          {lang.ok()}
        </Button>
      </DialogActions>
    </Dialog>
  )
})

export default AddPlayer
