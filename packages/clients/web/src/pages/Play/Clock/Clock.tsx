/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Typography } from '@material-ui/core'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { useGameStore, useLangStore } from '../../../stores'
import { Step, usePlayReducer } from '../Play'
import Time from '../Time'

/**
 * Display the clock on round page. When this component is used:
 * - a game must be loaded.
 */
const Clock: FunctionComponent = observer(() => {
  const { lang } = useLangStore()
  const { neededGame: game } = useGameStore()
  const [state] = usePlayReducer()

  // Render
  const currentRound = game.roundSets[game.roundSets.length - 1].currentRound
  return (
    <>
      <Typography variant="h5" gutterBottom>
        {lang.pageRound(currentRound, game.roundsPerSet)}
      </Typography>
      <Time blink={state.step === Step.Pause} responsive showAction />
    </>
  )
})

export default Clock
