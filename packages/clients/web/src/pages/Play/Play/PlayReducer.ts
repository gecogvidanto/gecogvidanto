/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Dispatch, useReducer } from 'react'

import Dialog, { DialogType } from './Dialog'
import Step from './Step'

/**
 * The state used for the component.
 */
export interface State {
  step: Step
  endDate?: number
  remaining?: number
  emphasised: number[]
  formOption?: string
  dialog: Dialog
}

/**
 * Type of actions which can be applied to the state.
 */
const enum ActionType {
  ResetState,
  ClockTick,
  GoToStep,
  FormOption,
  EmphasisedPlayers,
  OpenDialog,
}

/**
 * Reset state action.
 */
interface ResetStateAction {
  type: ActionType.ResetState
}

/**
 * Build an action used to reset the state.
 *
 * @returns The corresponding action.
 */
export function resetState(): ResetStateAction {
  return { type: ActionType.ResetState }
}

/**
 * Clock tick action.
 */
interface ClockTickAction {
  type: ActionType.ClockTick
  payload: number
}

/**
 * Build an action used to indicate a clock tick.
 *
 * @param remaining - The remaining time to the end of the turn.
 * @returns The corresponding action.
 */
export function clockTick(remaining: number): ClockTickAction {
  return { type: ActionType.ClockTick, payload: remaining }
}

/**
 * Go to step action.
 */
interface GoToStepAction {
  type: ActionType.GoToStep
  payload: Step
  endDateOrRemaining?: number
}

/**
 * Build an action used to indicate that the state must go to given step.
 *
 * @param step - The step to go to.
 *
 * @returns The corresponding action.
 */
export function goToStep(step: Step.InterRound | Step.RoundPreview): GoToStepAction

/**
 * Build an action used to indicate that the state must go to given step.
 *
 * @param step - The step to go to.
 * @param remaining - Fix the remaining time instead of using the current one.
 *
 * @returns The corresponding action.
 */
export function goToStep(step: Step.Pause, remaining?: number): GoToStepAction

/**
 * Build an action used to indicate that the state must go to given step.
 *
 * @param step - The running step to go to.
 * @param endDate - The terminating time for the round, in ms since Epoch.
 *
 * @returns The corresponding action.
 */
export function goToStep(step: Step.Running, endDate: number): GoToStepAction

/*
 * Implementation
 */
export function goToStep(step: Step, endDateOrRemaining?: number): GoToStepAction {
  return { type: ActionType.GoToStep, payload: step, endDateOrRemaining }
}

/**
 * Emphasised players action.
 */
interface EmphasisedPlayersAction {
  type: ActionType.EmphasisedPlayers
  payload: number[]
}

/**
 * Build an action used to set emphasised players.
 *
 * @param players - The players to emphasise.
 * @returns The corresponding action.
 */
export function emphasisePlayers(players: number[]): EmphasisedPlayersAction {
  return { type: ActionType.EmphasisedPlayers, payload: players }
}

/**
 * Build an action used to reset emphasised players.
 *
 * @returns The corresponding action.
 */
export function clearEmphasisedPlayers(): EmphasisedPlayersAction {
  return { type: ActionType.EmphasisedPlayers, payload: [] }
}

/**
 * Form option action.
 */
interface FormOptionAction {
  type: ActionType.FormOption
  payload?: string
}

/**
 * Build an action used to indicate that the user request an option form.
 *
 * @param option - The identifier of the requested option form.
 * @returns The corresponding action.
 */
export function formOpen(option: string): FormOptionAction {
  return { type: ActionType.FormOption, payload: option }
}

/**
 * Build an action used to indicate that the user closed an option form.
 *
 * @returns The corresponding action.
 */
export function formClose(): FormOptionAction {
  return { type: ActionType.FormOption }
}

/**
 * Open dialog action.
 */
interface OpenDialogAction {
  type: ActionType.OpenDialog
  payload: Dialog
}

/**
 * Build an action used to open a dialog (or close open dialog if no type specified).
 *
 * @param type - The type of dialog to open.
 * @returns The corresponding action.
 */
export function openDialog(type?: DialogType.AddPlayer | DialogType.PlayerExit): OpenDialogAction

/**
 * Build an action used to open a confirmation dialog.
 *
 * @param type - The type of dialog to open.
 * @param title - The confirmation dialog title.
 * @param message - The message to display for confirmation.
 * @param action - The action requiring confirmation.
 * @returns The corresponding action.
 */
export function openDialog(
  type: DialogType.Confirm,
  title: string,
  message: string,
  action: () => void
): OpenDialogAction

/**
 * Build an action used to open an anchored dialog.
 *
 * @param type - The type of dialog to open.
 * @param anchorEl - The HTML element where to anchor dialog.
 * @returns The corresponding action.
 */
export function openDialog(type: DialogType.ActionMenu, anchorEl: HTMLElement): OpenDialogAction

/*
 * Implementation
 */
export function openDialog(
  type: DialogType = DialogType.None,
  titleOrAnchor?: string | HTMLElement,
  message?: string,
  action?: () => void
): OpenDialogAction {
  let payload: Dialog
  switch (type) {
    case DialogType.Confirm:
      payload = { type, title: titleOrAnchor as string, message: message!, action: action! }
      break
    case DialogType.ActionMenu:
      payload = { type, anchorEl: titleOrAnchor as HTMLElement }
      break
    default:
      payload = { type }
  }
  return { type: ActionType.OpenDialog, payload }
}

/**
 * The reducer action.
 */
export type Action =
  | ResetStateAction
  | ClockTickAction
  | GoToStepAction
  | FormOptionAction
  | EmphasisedPlayersAction
  | OpenDialogAction

/**
 * The initial state.
 */
const initialState: State = {
  step: Step.InterRound,
  emphasised: [],
  dialog: { type: DialogType.None },
}

/**
 * The reducer function. Treat all actions to update the state.
 *
 * @param state - The initial state.
 * @param action - The action to be executed.
 * @returns The new state.
 */
function reducer(state: State, action: Action): State {
  switch (action.type) {
    case ActionType.ResetState:
      return initialState
    case ActionType.ClockTick:
      return { ...state, remaining: action.payload }
    case ActionType.GoToStep: {
      const endDate: number | undefined =
        action.payload === Step.Running ? action.endDateOrRemaining : undefined
      const remaining: number | undefined =
        action.payload === Step.Pause ? action.endDateOrRemaining || state.remaining : undefined
      return { ...state, step: action.payload, endDate, remaining }
    }
    case ActionType.EmphasisedPlayers:
      return { ...state, emphasised: action.payload }
    case ActionType.FormOption:
      return { ...state, formOption: action.payload, dialog: { type: DialogType.None } }
    case ActionType.OpenDialog:
      return { ...state, dialog: action.payload }
    default:
      // eslint-disable-next-line no-case-declarations
      const error: never = action
      throw new Error(`Unexpected action ${error}`)
  }
}

/**
 * Use the component reducer.
 *
 * @returns The same values as for `React.useReducer`.
 */
export default function usePlayReducer(): [State, Dispatch<Action>] {
  return useReducer(reducer, initialState)
}
