/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Paper, Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, useEffect } from 'react'
import { Redirect, useParams } from 'react-router'

import { CertLevel } from '@gecogvidanto/shared'

import { Loading, SignIn } from '../../../components'
import { routes } from '../../../routes'
import { useGameStore, useUserStore } from '../../../stores'
import ActionMenu from '../ActionMenu'
import AddPlayer from '../AddPlayer'
import AddPlayers from '../AddPlayers'
import Clock from '../Clock'
import Confirm from '../Confirm'
import Form from '../Form'
import LateralBar from '../LateralBar'
import PlayerExit from '../PlayerExit'
import RoundPreview from '../RoundPreview'
import { PlayReducerProvider } from './context'
import usePlayEffect from './PlayEffect'
import usePlayReducer from './PlayReducer'
import Step from './Step'

const useStyles = makeStyles((theme: Theme) => ({
  paper: {
    marginRight: theme.gecogvidanto.menuWidth,
    [theme.breakpoints.down('md')]: {
      marginRight: 0,
    },
    padding: theme.spacing(1),
  },
  drawer: {
    width: theme.gecogvidanto.menuWidth,
    whiteSpace: 'nowrap',
  },
  toolbar: theme.mixins.toolbar,
}))

/**
 * Main play game page.
 */
const Play: FunctionComponent = observer(() => {
  const classes = useStyles()
  const params = useParams<{ id: string }>()
  const { game, loadIfNeeded } = useGameStore()
  const { logged } = useUserStore()
  const [state, dispatch] = usePlayReducer()
  usePlayEffect(game, logged, state, dispatch)

  // Load game if parameter given
  useEffect(() => {
    loadIfNeeded(params.id)
  }, [loadIfNeeded, params.id])

  // Main node
  let node: JSX.Element | undefined
  if (game && logged) {
    if (
      logged.level === CertLevel.Administrator ||
      (logged.level === CertLevel.GameMaster && logged._id === game.masterId)
    ) {
      if (game.roundSets.length === 0) {
        node = <AddPlayers />
      } else if (
        game.roundSets.length > 0 &&
        game.roundSets[game.roundSets.length - 1].currentRound <= game.roundsPerSet
      ) {
        if (state.formOption) {
          node = <Form />
        } else {
          switch (state.step) {
            case Step.InterRound:
              node = <Form />
              break
            case Step.RoundPreview:
              node = <RoundPreview />
              break
            case Step.Running:
            case Step.Pause:
              node = <Clock />
              break
            default:
              // eslint-disable-next-line no-case-declarations
              const error: never = state.step
              throw new Error(`Unexpected step ${error}`)
          }
        }
      }
    }
    if (!node) {
      // Redirect to view mode
      node = <Redirect to={routes.GAME.build({ id: params.id })} />
    }
  }

  // Render
  return (
    <PlayReducerProvider value={[state, dispatch]}>
      <Paper className={classes.paper}>{node || <Loading />}</Paper>
      <LateralBar />
      <SignIn open={!logged} />
      <Confirm />
      <AddPlayer />
      <PlayerExit />
      <ActionMenu />
    </PlayReducerProvider>
  )
})

export default Play
