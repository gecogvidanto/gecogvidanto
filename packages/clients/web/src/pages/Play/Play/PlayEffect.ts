/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import GameIcon from '@material-ui/icons/Category'
import ExitIcon from '@material-ui/icons/MeetingRoom'
import PlayerAddIcon from '@material-ui/icons/PersonAdd'
import ActionIcon from '@material-ui/icons/Whatshot'
import { Dispatch, useCallback, useEffect, useRef } from 'react'

import { CertLevel, Game, User } from '@gecogvidanto/shared'

import { useEventCallback } from '../../../hooks'
import { MenuEntryDescription, addMenuEntry, removeMenuEntry, useMenuAction } from '../../../menu'
import { routes } from '../../../routes'
import { RoundEndSound } from '../../../sounds'
import { DialogType } from './Dialog'
import { Action, State, clockTick, goToStep, openDialog, resetState } from './PlayReducer'
import Step from './Step'

/**
 * Data stored in session storage.
 */
interface Stored {
  gameId: string
  time: number
  paused?: true
}

/**
 * Item used for session storage.
 */
const STORAGE_ITEM = 'playtime'

/**
 * A hook managing the effects of the whole play page and reducer (must be called in observer component).
 *
 * @param game - The game from the game store.
 * @param logged - The currently logged user.
 * @param state - The global play state.
 * @param dispatch - The play dispatch action.
 */
function usePlayEffect(
  game: Game | undefined,
  logged: User | undefined,
  state: State,
  dispatch: Dispatch<Action>
): void {
  const menuDispatch = useMenuAction()
  const timerId = useRef<number | undefined>(undefined)

  // Manage game change
  const previousGameId = useRef<string | undefined>(undefined)
  useEffect(() => {
    const gameId = game ? game._id : undefined
    if (gameId !== previousGameId.current) {
      previousGameId.current = gameId
      dispatch(resetState())
      if (typeof window !== 'undefined') {
        const stored = window.sessionStorage.getItem(STORAGE_ITEM)
        if (stored) {
          try {
            const result = JSON.parse(stored) as Stored
            if (result.gameId === gameId) {
              if (result.paused) {
                dispatch(goToStep(Step.Pause, result.time))
              } else {
                dispatch(goToStep(Step.Running, result.time))
              }
            }
          } catch {
            // Ignored
          }
        }
      }
    }
  })

  // Manage timer
  const onUpdateTime = useEventCallback(() => {
    const remaining = Math.max(Math.ceil(((state.endDate || 0) - Date.now()) / 1000), 0)
    if (state.remaining !== remaining) {
      dispatch(clockTick(remaining))
    }
  })
  useEffect(() => {
    if (typeof window !== 'undefined') {
      if (state.remaining !== undefined && state.remaining > 0 && state.step === Step.Running) {
        if (timerId.current === undefined) {
          timerId.current = window.setInterval(onUpdateTime, 200)
        }
      } else {
        if (timerId.current !== undefined) {
          window.clearInterval(timerId.current)
          timerId.current = undefined
        }
      }
    }
  })
  useEffect(() => {
    if (state.step === Step.Running) {
      onUpdateTime()
    }
  }, [onUpdateTime, state.endDate, state.step])

  // Manage step change
  const previousStep = useRef(state.step)
  useEffect(() => {
    if (state.step !== previousStep.current) {
      // Entering running state
      if (state.step === Step.Running) {
        if (typeof window !== 'undefined') {
          const stored: Stored = { gameId: game!._id!, time: state.endDate! }
          window.sessionStorage.setItem(STORAGE_ITEM, JSON.stringify(stored))
        }
        onUpdateTime()
      }

      // Entering pause state
      if (state.step === Step.Pause) {
        if (typeof window !== 'undefined') {
          const stored: Stored = { gameId: game!._id!, time: state.remaining!, paused: true }
          window.sessionStorage.setItem(STORAGE_ITEM, JSON.stringify(stored))
        }
      }

      // Leaving pause, returning to running state
      if (previousStep.current === Step.Pause && state.step === Step.Running) {
        if (typeof window !== 'undefined') {
          const stored: Stored = { gameId: game!._id!, time: state.endDate! }
          window.sessionStorage.setItem(STORAGE_ITEM, JSON.stringify(stored))
        }
      }

      // Leaving both pause and running state
      if (
        [Step.Running, Step.Pause].includes(previousStep.current) &&
        ![Step.Running, Step.Pause].includes(state.step)
      ) {
        if (typeof window !== 'undefined') {
          window.sessionStorage.removeItem(STORAGE_ITEM)
        }
      }

      // Update previous step
      previousStep.current = state.step
    }
  })

  // Manage menu
  const userCanMaster =
    logged &&
    game &&
    (logged.level === CertLevel.Administrator ||
      (logged.level === CertLevel.GameMaster && logged._id === game.masterId))
  const gameIsRunning =
    userCanMaster &&
    game!.roundSets.length > 0 &&
    game!.roundSets[game!.roundSets.length - 1].currentRound <= game!.roundsPerSet
  const roundIsRunning = userCanMaster && [Step.Running, Step.Pause].includes(state.step)
  const onAddPlayer = useCallback(() => dispatch(openDialog(DialogType.AddPlayer)), [dispatch])
  const onPlayerExit = useCallback(() => dispatch(openDialog(DialogType.PlayerExit)), [dispatch])
  const onActionMenu = useCallback(
    (anchorEl: HTMLElement) => dispatch(openDialog(DialogType.ActionMenu, anchorEl)),
    [dispatch]
  )
  useEffect(() => {
    const menuContent: MenuEntryDescription[] = []
    if (userCanMaster) {
      menuContent.push({ icon: GameIcon, title: 'pageNewGame', action: routes.NEWGAME.build() })
    }
    if (gameIsRunning) {
      menuContent.push({
        icon: PlayerAddIcon,
        title: 'menuPlayerAdd',
        action: onAddPlayer,
      })
      menuContent.push({ icon: ExitIcon, title: 'playerExit', action: onPlayerExit })
    }
    if (roundIsRunning) {
      menuContent.push({ icon: ActionIcon, title: 'menuAction', action: onActionMenu })
    }
    menuContent.forEach(entry => menuDispatch(addMenuEntry(entry)))
    return () => {
      menuContent.forEach(entry => menuDispatch(removeMenuEntry(entry)))
    }
  }, [
    dispatch,
    gameIsRunning,
    menuDispatch,
    onActionMenu,
    onAddPlayer,
    onPlayerExit,
    roundIsRunning,
    userCanMaster,
  ])

  // Manage end-of-turn sound
  const previousRemaining = useRef(state.remaining)
  useEffect(() => {
    if (state.remaining === 0 && previousRemaining.current && previousRemaining.current > 0) {
      const endNotification = new Audio(RoundEndSound)
      endNotification.play()
    }
    previousRemaining.current = state.remaining
  })
}
export default usePlayEffect
