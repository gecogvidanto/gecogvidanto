/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Menu, MenuItem } from '@material-ui/core'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, useCallback, useEffect } from 'react'

import { LocalizedOption } from '@gecogvidanto/shared'

import { useEventCallback, useFetcher } from '../../../hooks'
import { langType } from '../../../locale'
import { useGameStore, useLangStore } from '../../../stores'
import { api, notifyError } from '../../../tools'
import { DialogType, Step, goToStep, openDialog, usePlayReducer } from '../Play'
import OptionMenuItem from './OptionMenuItem'

const NO_OPTION: LocalizedOption<langType, keyof langType>[] = []

/**
 * Action menu contains the technological break and economic system custom actions. When this component is
 * used:
 * - menu must be open only if a game is running.
 */
const ActionMenu: FunctionComponent = observer(() => {
  const { game, recordTechnologicalBreak } = useGameStore()
  const { lang } = useLangStore()
  const [state, dispatch] = usePlayReducer()

  // Manage options
  const fetchOptions = useCallback(() => {
    return api.game.readOptions<langType>(game!)
  }, [game])
  const [options, { error: optionsError }] = useFetcher(
    game && state.step !== Step.InterRound ? fetchOptions : undefined,
    NO_OPTION
  )
  useEffect(() => {
    if (optionsError) {
      notifyError(optionsError, lang)
    }
  }, [lang, optionsError])

  // Technological break
  const onTechBreak = useEventCallback((): void => {
    if (state.remaining !== undefined) {
      dispatch(
        openDialog(
          DialogType.Confirm,
          lang.menuActionTechBreak(),
          lang.menuActionTechBreakConfirm(),
          () => {
            recordTechnologicalBreak()
            if (state.remaining !== undefined && state.remaining > 0) {
              dispatch(goToStep(Step.Pause))
            }
          }
        )
      )
    }
  })

  // Menu items
  const items: JSX.Element[] = []
  if (game) {
    // Technological break
    items.push(
      <MenuItem key="tech-break" onClick={onTechBreak}>
        {lang.menuActionTechBreak()}
      </MenuItem>
    )

    // Add all optional items
    items.push(...options.map(option => <OptionMenuItem key={`option-${option.id}`} option={option} />))
  }

  // Render
  const onClose = useCallback(() => dispatch(openDialog()), [dispatch])
  const anchorEl = state.dialog.type === DialogType.ActionMenu ? state.dialog.anchorEl : undefined
  return (
    <Menu id="action-menu" anchorEl={anchorEl} open={!!anchorEl} onClose={onClose}>
      {items}
    </Menu>
  )
})

export default ActionMenu
