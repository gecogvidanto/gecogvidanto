/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Button, Paper, Theme, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, SyntheticEvent, useCallback, useEffect, useRef, useState } from 'react'

import { Validator, validators } from '@gecogvidanto/shared'

import { AddPlayerField, RoundSetSelect, UpdatePlayerField } from '../../../components'
import { useGameStore, useLangStore } from '../../../stores'
import { PlayerSameNameValidator } from '../../../tools'

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    ...theme.gecogvidanto.form.style.container,
    maxWidth: theme.gecogvidanto.form.pageWidth,
  },
  form: {
    ...theme.gecogvidanto.form.style.form,
  },
  submit: {
    ...theme.gecogvidanto.form.style.submit,
  },
}))

/**
 * Component used to add players to a new game. When this component is used:
 * - a game must be loaded;
 * - logged user must have appropriate rights to create users.
 */
const AddPlayers: FunctionComponent = observer(() => {
  const classes = useStyles()
  const { neededGame: game, loading: gameLoading } = useGameStore()
  const { lang } = useLangStore()
  const nextIdentifiers = useRef<number[]>()
  const nextIdentifier = useRef<number>(0)

  // State
  const [identifiers, setIdentifiers] = useState<number[]>([])
  const [rsSelectOpen, setRsSelectOpen] = useState<boolean>(false)
  const [currentEdit, setCurrentEdit] = useState<number>(-1)
  const [valid, setValid] = useState<boolean>(true)

  // Manage players and identifiers
  const players = game.players
  const playerCount = players.length
  useEffect(() => {
    if (!gameLoading && nextIdentifiers.current) {
      setIdentifiers(nextIdentifiers.current)
      nextIdentifiers.current = undefined
    }
    if (identifiers.length !== playerCount) {
      setIdentifiers(
        Array(playerCount)
          .fill(false)
          .map(_ => nextIdentifier.current++)
      )
    }
  }, [gameLoading, identifiers.length, playerCount])

  // Updatable players
  const onValidPlayer = useCallback((order: number, nameValid: boolean): void => {
    if (!nameValid) {
      setCurrentEdit(order)
    }
    setValid(nameValid)
  }, [])
  const onBlurPlayer = useCallback((): void => {
    setCurrentEdit(-1)
    setValid(true)
  }, [])
  const onDeletePlayer = useCallback(
    (order: number): void => {
      setCurrentEdit(previous =>
        previous === order
          ? (() => {
              setValid(true)
              return -1
            })()
          : previous > order
          ? previous - 1
          : previous
      )
      nextIdentifiers.current = identifiers.filter((_, index) => index !== order)
    },
    [identifiers]
  )
  const currentValidators = (playerNum: number): Validator[] => [
    ...validators.playerName,
    new PlayerSameNameValidator(
      players.filter((_, index) => index !== playerNum).map(player => player.name)
    ),
  ]
  const playerFields: JSX.Element[] = Array(Math.min(playerCount, identifiers.length))
    .fill(false)
    .map((_, index) => (
      <UpdatePlayerField
        key={identifiers[index]}
        order={index}
        readOnly={currentEdit !== index && !valid}
        autoFocus={currentEdit === index}
        validators={currentValidators(index)}
        onValidityChange={onValidPlayer}
        onBlur={onBlurPlayer}
        onDelete={onDeletePlayer}
      />
    ))

  // New player
  const onAddPlayer = useCallback((): void => {
    setCurrentEdit(game.players.length)
    nextIdentifiers.current = [...identifiers, nextIdentifier.current++]
  }, [game.players.length, identifiers])
  if (valid) {
    const addValidators: Validator[] = [
      ...validators.playerName,
      new PlayerSameNameValidator(players.map(player => player.name)),
    ]
    playerFields.push(
      <AddPlayerField
        key={nextIdentifier.current}
        id={'add-player'}
        name={'add-player'}
        label={lang.pageAddPlayersId(playerCount + 1)}
        autoFocus={currentEdit === -1}
        validators={addValidators}
        onAddPlayer={onAddPlayer}
      />
    )
  }

  // Render
  const onSubmit = useCallback(
    (e: SyntheticEvent<any>): void => {
      e.preventDefault()
      if (valid) {
        setRsSelectOpen(true)
      }
    },
    [valid]
  )
  const onSetClose = useCallback((): void => {
    setRsSelectOpen(false)
  }, [])
  return (
    <>
      <Typography variant="h5" gutterBottom>
        {lang.pageAddPlayers()}
      </Typography>
      <Paper className={classes.container}>
        <form className={classes.form}>
          {...playerFields}
          <Button
            type="submit"
            disabled={!valid}
            onClick={onSubmit}
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            {lang.setCreate()}
          </Button>
        </form>
      </Paper>
      <RoundSetSelect open={rsSelectOpen} onClose={onSetClose} />
    </>
  )
})

export default AddPlayers
