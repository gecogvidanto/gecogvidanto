/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { IconButton, ListItem, ListItemSecondaryAction, ListItemText } from '@material-ui/core'
import ExitIcon from '@material-ui/icons/MeetingRoom'
import { useObserver } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, useCallback } from 'react'

import { Player } from '@gecogvidanto/shared'

import { useGameStore, useLangStore } from '../../../stores'
import { DialogType, openDialog, usePlayReducer } from '../Play'

export const enum PlayerCategory {
  Emphasised = 2,
  Playing = 1,
  Leaving = 0,
}

export interface DisplayablePlayer extends Player {
  index: number
  deletable: boolean
  category: PlayerCategory
}

export interface PlayerListItemProps {
  player: DisplayablePlayer
  canDelete?: boolean
}

/**
 * The list of the players.
 */
/* (JSX element) */
const PlayerListItem: FunctionComponent<PlayerListItemProps> = ({ player, canDelete = false }) => {
  const { lang } = useLangStore()
  const gameStore = useGameStore()
  const [, dispatch] = usePlayReducer()

  // Delete button
  const onRemovePlayer = useCallback(() => {
    if (gameStore.game) {
      dispatch(
        openDialog(
          DialogType.Confirm,
          lang.playerExit(),
          lang.playerExitConfirm(gameStore.game.players[player.index].name),
          () => {
            gameStore.deletePlayer(player.index)
          }
        )
      )
    }
  }, [dispatch, gameStore, lang, player.index])
  const deleteButton = useObserver(() =>
    player.deletable && gameStore.game && canDelete ? (
      <ListItemSecondaryAction>
        <IconButton aria-label={lang.playerExit()} onClick={onRemovePlayer}>
          <ExitIcon />
        </IconButton>
      </ListItemSecondaryAction>
    ) : undefined
  )

  // Render
  const color =
    player.category === PlayerCategory.Emphasised
      ? 'secondary'
      : player.category === PlayerCategory.Playing
      ? 'primary'
      : 'inherit'
  return (
    <ListItem disableGutters>
      <ListItemText primary={player.name} primaryTypographyProps={{ color, noWrap: true }} />
      {deleteButton}
    </ListItem>
  )
}

export default PlayerListItem
