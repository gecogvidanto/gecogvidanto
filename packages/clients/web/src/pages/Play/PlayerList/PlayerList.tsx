/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { List, Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { useGameStore } from '../../../stores'
import { usePlayReducer } from '../Play'
import PlayerListItem, { PlayerCategory } from './PlayerListItem'

const useStyles = makeStyles((theme: Theme) => ({
  list: {
    padding: theme.spacing(1),
  },
}))

export interface PlayerListProps {
  canDelete?: boolean
}

/**
 * The list of the players. When this component is used:
 * - a provider must make the play reducer available.
 */
const PlayerList: FunctionComponent<PlayerListProps> = observer(({ canDelete = false }) => {
  const classes = useStyles()
  const [state] = usePlayReducer()
  const { game } = useGameStore()

  // Render
  const gamePlayers = game ? game.players : []
  const players: JSX.Element[] = gamePlayers
    .map((player, index) => ({
      ...player,
      index,
      deletable: player.roundLeft === 0,
      category: state.emphasised.includes(index)
        ? PlayerCategory.Emphasised
        : player.roundLeft === 0
        ? PlayerCategory.Playing
        : PlayerCategory.Leaving,
    }))
    .sort((a, b) => {
      return b.category - a.category || a.index - b.index
    })
    .map(player => <PlayerListItem key={player.name} player={player} canDelete={canDelete} />)
  return (
    <List className={classes.list} dense>
      {...players}
    </List>
  )
})

export default PlayerList
