/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, useCallback } from 'react'

import { useLangStore } from '../../../stores'
import { DialogType, openDialog, usePlayReducer } from '../Play'

/**
 * A confirmation dialog. When this component is used:
 * - a provider must make the play reducer available.
 */
const Confirm: FunctionComponent = observer(() => {
  const { lang } = useLangStore()
  const [state, dispatch] = usePlayReducer()
  const title: string = state.dialog.type === DialogType.Confirm ? state.dialog.title : ''
  const message: string = state.dialog.type === DialogType.Confirm ? state.dialog.message : ''

  // Events
  const onClose = useCallback((): void => {
    dispatch(openDialog())
  }, [dispatch])
  const onValid = useCallback((): void => {
    if (state.dialog.type === DialogType.Confirm) {
      state.dialog.action()
      onClose()
    }
  }, [onClose, state.dialog])

  return (
    <Dialog
      open={state.dialog.type === DialogType.Confirm}
      onClose={onClose}
      aria-labelledby="confirm-dialog-title"
      aria-describedby="confirm-dialog-description"
    >
      <DialogTitle id="confirm-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <DialogContentText id="confirm-dialog-description">{message}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary">
          {lang.cancel()}
        </Button>
        <Button onClick={onValid} color="primary" autoFocus>
          {lang.ok()}
        </Button>
      </DialogActions>
    </Dialog>
  )
})

export default Confirm
