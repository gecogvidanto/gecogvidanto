/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Button, Theme, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, useCallback } from 'react'

import { Center } from '../../../components'
import { useGameStore, useLangStore } from '../../../stores'
import LoadableHelpSheet from '../LoadableHelpSheet'
import { Step, goToStep, usePlayReducer } from '../Play'

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    display: 'flex',
  },
  helpSheets: {
    width: '50%',
    flexGrow: 1,
    padding: theme.spacing(1),
  },
  submit: {
    ...theme.gecogvidanto.form.style.submit,
  },
}))

/**
 * Page displaying a round. When this component is used:
 * - a game must be loaded;
 * - a provider must make the play reducer available.
 */
const RoundPreview: FunctionComponent = observer(() => {
  const classes = useStyles()
  const { lang } = useLangStore()
  const { neededGame: game } = useGameStore()
  const [, dispatch] = usePlayReducer()

  // Events
  const onClose = useCallback(() => {
    const roundEnd: number = Date.now() + game.roundLength * 60 * 1000
    dispatch(goToStep(Step.Running, roundEnd))
  }, [dispatch, game.roundLength])

  // Render
  const currentRound = game.roundSets[game.roundSets.length - 1].currentRound
  return (
    <>
      <Typography variant="h5" gutterBottom>
        {lang.pageRound(currentRound, game.roundsPerSet)}
      </Typography>
      <Typography variant="h6" gutterBottom>
        {lang.pageRoundPreview()}
      </Typography>
      <div className={classes.container}>
        <div className={classes.helpSheets}>
          <LoadableHelpSheet type="values" />
        </div>
        <div className={classes.helpSheets}>
          <LoadableHelpSheet type="money" />
        </div>
      </div>
      <Center>
        <Button onClick={onClose} variant="contained" color="primary" className={classes.submit}>
          {lang.start()}
        </Button>
      </Center>
    </>
  )
})

export default RoundPreview
