/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Divider, Typography } from '@material-ui/core'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { Center } from '../../../components'
import { useLangStore } from '../../../stores'

/**
 * Letters usable in standard game cards.
 */
const LETTERS = 'ABCDEFGHIJKLM'

/**
 * When no letter can be displayed (no players).
 */
const NO_LETTER = '-'

export interface NewGameStatsProps {
  playerCount: number
}

/**
 * @returns Statistics about a new game being created.
 */
const NewGameStats: FunctionComponent<NewGameStatsProps> = observer(({ playerCount }) => {
  const { lang } = useLangStore()
  const fourCount = Math.ceil(playerCount * 1.25)
  const maxLetter: string = fourCount === 0 ? NO_LETTER : LETTERS.charAt((fourCount - 1) % LETTERS.length)
  const gameCount: number = fourCount === 0 ? 0 : Math.floor((fourCount - 1) / LETTERS.length)
  const game: string = gameCount === 0 ? '' : String(gameCount + 1)
  return (
    <>
      <Center>
        <Typography variant="h6">{lang.pagePlayStatsPlayers()}</Typography>
      </Center>
      <Center>
        <Typography variant="h1">{playerCount}</Typography>
      </Center>
      <Divider />
      <Center>
        <Typography variant="h6">{lang.pagePlayStatsFour()}</Typography>
      </Center>
      <Center>
        <Typography variant="h1">{fourCount}</Typography>
      </Center>
      <Divider />
      <Center>
        <Typography variant="h6">{lang.pagePlayStatsCardGame()}</Typography>
      </Center>
      <Center>
        <Typography variant="h1">
          {game}
          {maxLetter}
        </Typography>
      </Center>
    </>
  )
})

export default NewGameStats
