/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Typography } from '@material-ui/core'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { useGameStore } from '../../../stores'

/**
 * Display the location of the game. Should only be called if game loaded with name || address.
 */
/* (JSX element) */
const Location: FunctionComponent = () => {
  const locationElements: JSX.Element[] = []
  const {
    neededGame: { location },
  } = useGameStore()
  if (location.name) {
    locationElements.push(<Typography key="name">{location.name}</Typography>)
  }
  if (location.address) {
    const address: string = typeof location.address === 'object' ? location.address.value : location.address
    locationElements.push(<Typography key="address">{address}</Typography>)
  }
  return <>{locationElements}</>
}
export default Location
