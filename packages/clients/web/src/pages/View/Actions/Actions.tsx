/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Button } from '@material-ui/core'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, useCallback, useState } from 'react'
import { useHistory } from 'react-router'

import { RoundSetSelect, createLinkComponent } from '../../../components'
import { routes } from '../../../routes'
import { useGameStore, useLangStore } from '../../../stores'

export interface ActionsProps {
  canMasterGame: boolean
  onGoingGame: boolean
}

/**
 * Display the actions. Should only be called if onGoingGame || canMasterGame.
 */
const Actions: FunctionComponent<ActionsProps> = observer(({ canMasterGame, onGoingGame }) => {
  const { neededGame: game, close, loadIfNeeded } = useGameStore()
  const { lang } = useLangStore()
  const history = useHistory()
  const closable: boolean = !onGoingGame && game.roundSets.length >= 2 && !game.closed
  const actionButtons: JSX.Element[] = []

  // Help sheets
  if (onGoingGame) {
    const valuesLink: FunctionComponent = createLinkComponent(
      routes.HELPSHEET.build({ id: game._id, 0: 'values' })
    )
    actionButtons.push(
      <Button key="values" component={valuesLink}>
        {lang.pageGameViewValues()}
      </Button>
    )
    const moneyLink: FunctionComponent = createLinkComponent(
      routes.HELPSHEET.build({ id: game._id, 0: 'money' })
    )
    actionButtons.push(
      <Button key="money" component={moneyLink}>
        {lang.pageGameViewMoney()}
      </Button>
    )
  }

  // Play game
  if ((game.roundSets.length === 0 || onGoingGame) && canMasterGame) {
    const playLink: FunctionComponent = createLinkComponent(routes.PLAYGAME.build({ id: game._id }))
    const text: string = game.roundSets.length === 0 ? lang.pageGameAddPlayers() : lang.pageGamePlay()
    actionButtons.push(
      <Button key="play" component={playLink}>
        {text}
      </Button>
    )
  }

  // Add a new set
  const [dialogOpen, setDialogOpen] = useState<boolean>(false)
  const onDialogOpen = useCallback((): void => {
    setDialogOpen(true)
  }, [])
  const onDialogClose = useCallback((): void => {
    loadIfNeeded()
    history.push(routes.PLAYGAME.build({ id: game._id }))
  }, [game._id, history, loadIfNeeded])
  if (!onGoingGame && canMasterGame) {
    actionButtons.push(
      <Button key="set" onClick={onDialogOpen}>
        {lang.setCreate()}
      </Button>
    )
  }

  // Close game
  const onGameClose = useCallback((): void => {
    close()
  }, [close])
  if (closable && canMasterGame) {
    actionButtons.push(
      <Button key="close" onClick={onGameClose}>
        {lang.pageGameClose()}
      </Button>
    )
  }

  // Build result
  return (
    <>
      {actionButtons}
      <RoundSetSelect open={dialogOpen} onClose={onDialogClose} />
    </>
  )
})
export default Actions
