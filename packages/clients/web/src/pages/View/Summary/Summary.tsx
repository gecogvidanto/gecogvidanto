/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { TextField } from '@material-ui/core'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { ChangeEvent, FunctionComponent, useCallback, useState } from 'react'

import { useGameStore, useLangStore } from '../../../stores'

export interface SummaryProps {
  readOnly: boolean
}

/**
 * The summary of the game. Should be called if game.summary || !readOnly.
 */
const Summary: FunctionComponent<SummaryProps> = observer(({ readOnly }) => {
  const { neededGame: game, updateSummary } = useGameStore()
  const { lang } = useLangStore()

  // State
  const [editedSummary, setEditedSummary] = useState<string>()

  // Events
  const onSummaryFocus = useCallback((): void => {
    if (game && !readOnly) {
      setEditedSummary(game.summary || '')
    }
  }, [game, readOnly])
  const onSummaryChanged = useCallback(
    (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
      if (editedSummary !== undefined) {
        setEditedSummary(e.target.value)
      }
    },
    [editedSummary]
  )
  const onSummaryBlur = useCallback((): void => {
    if (editedSummary !== undefined) {
      updateSummary(editedSummary)
    }
    setEditedSummary(undefined)
  }, [editedSummary, updateSummary])

  // Render
  const value = editedSummary === undefined ? game.summary : editedSummary
  return (
    <>
      <TextField
        label={lang.pageGameSummary()}
        value={value}
        fullWidth
        multiline
        InputProps={{ readOnly }}
        onFocus={onSummaryFocus}
        onChange={onSummaryChanged}
        onBlur={onSummaryBlur}
      />
    </>
  )
})

export default Summary
