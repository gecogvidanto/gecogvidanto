/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Avatar, Theme, Typography } from '@material-ui/core'
import LockIcon from '@material-ui/icons/LockOutlined'
import { makeStyles } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { useUserData } from '../../../hooks'
import { useGameStore, useLangStore } from '../../../stores'
import { Gravatar } from '../../../tools'

const PICTURE_SIZE = 40

const useStyles = makeStyles((theme: Theme) => ({
  header: {
    display: 'flex',
  },
  avatar: {
    margin: theme.spacing(1),
  },
  closed: {
    display: 'flex',
    flexGrow: 1,
    justifyContent: 'flex-end',
  },
  locked: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
  },
}))

/**
 * Display header (main game information). When this component is used:
 * - a game must be loaded.
 */
const Header: FunctionComponent = observer(() => {
  const classes = useStyles()
  const { lang } = useLangStore()
  const { neededGame: game } = useGameStore()
  const gameMaster = useUserData(game.masterId)
  const name: string = gameMaster ? gameMaster.name : ''
  const gravatar: Gravatar | undefined = gameMaster ? new Gravatar(gameMaster.email) : undefined

  // Render
  const avatar: JSX.Element = gravatar ? (
    <Avatar src={gravatar.getImageUrl(PICTURE_SIZE)} className={classes.avatar} />
  ) : (
    <div />
  )
  const thanks: JSX.Element | undefined =
    gameMaster && gameMaster.account ? (
      <Typography>{lang.pageGameMasterThanks(gameMaster.account)}</Typography>
    ) : undefined
  const closed: JSX.Element = game.closed ? (
    <div className={classes.closed}>
      <Avatar className={classes.locked}>
        <LockIcon />
      </Avatar>
    </div>
  ) : (
    <div />
  )
  return (
    <div className={classes.header}>
      {avatar}
      <div>
        <Typography variant="h6">{lang.pageGameMaster(name)}</Typography>
        {thanks}
      </div>
      {closed}
    </div>
  )
})
export default Header
