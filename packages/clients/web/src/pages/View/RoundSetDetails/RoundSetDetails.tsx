/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, Typography } from '@material-ui/core'
import ExpandIcon from '@material-ui/icons/ExpandMore'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { RoundSet } from '@gecogvidanto/shared'

import { useEconomicSystemName } from '../../../hooks'
import { useGameStore, useLangStore } from '../../../stores'
import FinishedRoundSet from '../FinishedRoundSet'

export interface RoundSetDetailsProps {
  roundSet: { roundSet: RoundSet; index: number }
}

/**
 * Display the round set details. When this component is used:
 * - a game must be loaded.
 */
const RoundSetDetails: FunctionComponent<RoundSetDetailsProps> = observer(({ roundSet }) => {
  const { lang, assemble } = useLangStore()
  const { neededGame: game } = useGameStore()
  const ecoSysId = roundSet.roundSet.ecoSysId
  const economicSystemName = useEconomicSystemName(ecoSysId)

  // Render
  const finishedSet: boolean = roundSet.roundSet.currentRound > game.roundsPerSet
  const roundSetContent: JSX.Element = finishedSet ? (
    <FinishedRoundSet roundSet={roundSet} />
  ) : (
    <Typography>{lang.pageGameSetInProgress()}</Typography>
  )
  return (
    <ExpansionPanel defaultExpanded={false}>
      <ExpansionPanelSummary expandIcon={<ExpandIcon />}>
        <Typography variant="h6" color={finishedSet ? 'inherit' : 'primary'}>
          {lang.pageGameSet(roundSet.index, economicSystemName ? assemble(economicSystemName) : ecoSysId)}
        </Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>{roundSetContent}</ExpansionPanelDetails>
    </ExpansionPanel>
  )
})
export default RoundSetDetails
