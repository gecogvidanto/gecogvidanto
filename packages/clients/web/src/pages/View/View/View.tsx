/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Paper, Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, useEffect } from 'react'
import { useParams } from 'react-router'

import { CertLevel } from '@gecogvidanto/shared'

import { Loading } from '../../../components'
import { EconomicSystemNameCache } from '../../../hooks'
import { useGameStore, useUserStore } from '../../../stores'
import Action from '../Actions'
import Header from '../Header'
import Location from '../Location'
import Players from '../Players'
import RoundSetDetails from '../RoundSetDetails'
import Separator from '../Separator'
import Summary from '../Summary'

const useStyles = makeStyles((theme: Theme) => ({
  paper: {
    padding: theme.spacing(1),
  },
}))

/**
 * View a game page.
 */
const View: FunctionComponent = observer(() => {
  const classes = useStyles()
  const { game, loadIfNeeded } = useGameStore()
  const { logged } = useUserStore()
  const { id: paramsId } = useParams<{ id: string }>()
  const canMasterGame: boolean =
    !!game &&
    !!logged &&
    !game.closed &&
    (logged.level === CertLevel.Administrator ||
      (logged.level === CertLevel.GameMaster && logged._id === game.masterId))

  // Load game
  useEffect(() => {
    loadIfNeeded(paramsId)
  }, [loadIfNeeded, paramsId])

  // Render
  if (game && game._id === paramsId) {
    const onGoingGame: boolean =
      game.roundSets.length > 0 &&
      game.roundSets[game.roundSets.length - 1].currentRound <= game.roundsPerSet
    const gameElements: JSX.Element[] = []
    gameElements.push(<Header key="header" />)
    if (game.location.name || game.location.address) {
      gameElements.push(<Location key="location" />)
    }
    gameElements.push(<Players key="players" canMasterGame={canMasterGame} />)
    gameElements.push(
      ...game.roundSets.map((set, id) => (
        <RoundSetDetails key={`set-${id}`} roundSet={{ roundSet: set, index: id }} />
      ))
    )
    if (game.summary || canMasterGame) {
      gameElements.push(<Summary key="summary" readOnly={!canMasterGame} />)
    }
    if (onGoingGame || canMasterGame) {
      gameElements.push(<Action key="action" onGoingGame={onGoingGame} canMasterGame={canMasterGame} />)
    }
    const allElements: JSX.Element[] = []
    gameElements.forEach((elem, index) => {
      index !== 0 && allElements.push(<Separator key={`sep-${index}`} />)
      allElements.push(elem)
    })
    return (
      <EconomicSystemNameCache>
        <Paper className={classes.paper}>{allElements}</Paper>
      </EconomicSystemNameCache>
    )
  } else {
    return <Loading />
  }
})
export default View
