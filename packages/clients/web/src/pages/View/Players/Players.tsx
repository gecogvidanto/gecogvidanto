/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary,
  List,
  Typography,
} from '@material-ui/core'
import ExpandIcon from '@material-ui/icons/ExpandMore'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { useGameStore, useLangStore } from '../../../stores'
import PlayerDetails from '../PlayerDetails'

export interface PlayersProps {
  canMasterGame: boolean
}

/**
 * Diplay players panel. When using this component:
 * - a game must be loaded.
 */
const Players: FunctionComponent<PlayersProps> = observer(({ canMasterGame }) => {
  const { lang } = useLangStore()
  const { neededGame: game } = useGameStore()

  const playerDetails: JSX.Element =
    game.players.length === 0 ? (
      <Typography>{lang.pageGameNoPlayers()}</Typography>
    ) : (
      ((): JSX.Element => {
        const players: JSX.Element[] = game.players.map((p, id) => (
          <PlayerDetails key={id} canMasterGame={canMasterGame} player={{ player: p, index: id }} />
        ))
        return <List>{...players}</List>
      })()
    )
  return (
    <ExpansionPanel key="players" defaultExpanded={false}>
      <ExpansionPanelSummary expandIcon={<ExpandIcon />}>
        <Typography variant="h6">{lang.pageGamePlayers()}</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>{playerDetails}</ExpansionPanelDetails>
    </ExpansionPanel>
  )
})
export default Players
