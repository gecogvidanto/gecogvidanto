/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { FormControlLabel, FormGroup, Switch, TextField, Theme } from '@material-ui/core'
import { useTheme } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, useCallback, useState } from 'react'

import { Character, RoundSet } from '@gecogvidanto/shared'

import { DataType, GameChart } from '../../../components'
import { useCharacterNames } from '../../../hooks'
import { useGameStore, useLangStore, useUserStore } from '../../../stores'
import { calculateStats } from './calculateStats'
import { round2Digits } from './round2Digits'

export interface FinishedRoundSetProps {
  roundSet: { roundSet: RoundSet; index: number }
}

/**
 * Display the details of a finished round set.
 */
const FinishedRoundSet: FunctionComponent<FinishedRoundSetProps> = observer(({ roundSet }) => {
  const theme = useTheme<Theme>()
  const { lang } = useLangStore()
  const { neededGame: game } = useGameStore()
  const { logged } = useUserStore()
  const getCharacterName = useCharacterNames()
  const hasNpc: boolean =
    game.characters.filter(character => character.roundSet === roundSet.index && character.player < 0)
      .length > 0
  const chartLabels = {
    mean: lang.pageGameChartMean(),
    deviation: lang.pageGameChartRelDeviation(),
  }
  const chartColors = {
    grid: theme.palette.chart.grid.main,
    score: theme.palette.chart.score.main,
    mean: theme.palette.chart.mean.main,
    deviation: theme.palette.chart.deviation.main,
  }

  // Calculate data
  const [includeNpc, setIncludeNpc] = useState<boolean>(!logged || logged._id !== game.masterId)
  const onNpcChange = useCallback((_: any, checked: boolean): void => {
    setIncludeNpc(checked)
  }, [])
  const characters: Character[] = game.characters
    .filter(character => character.roundSet === roundSet.index)
    .sort((a, b) =>
      a.player < 0 === b.player < 0 ? Math.abs(a.player) - Math.abs(b.player) : b.player - a.player
    )
    .filter(character => includeNpc || character.player >= 0)
  const data: DataType[] = characters.map(character => ({
    name: getCharacterName(roundSet.index, character.player),
    score: character.totalScore,
  }))
  const stats = calculateStats(characters.map(character => character.totalScore))
  const npcSwitch: JSX.Element = <Switch checked={includeNpc} disabled={!hasNpc} onChange={onNpcChange} />

  // Render
  return (
    <>
      <GameChart data={data} stats={stats} labels={chartLabels} colors={chartColors} />
      <FormGroup>
        <FormControlLabel control={npcSwitch} label={lang.pageGameChartIncludeNpc()} />
        <TextField
          label={lang.pageGameChartTotal()}
          value={round2Digits(stats.total)}
          InputProps={{ readOnly: true }}
        />
        <TextField
          label={lang.pageGameChartMean()}
          value={round2Digits(stats.mean)}
          InputProps={{ readOnly: true }}
        />
        <TextField
          label={lang.pageGameChartStdDeviation()}
          value={round2Digits(stats.standardDeviation)}
          InputProps={{ readOnly: true }}
        />
        <TextField
          label={lang.pageGameChartRelDeviation()}
          value={round2Digits(stats.meanStandardDeviation) + '%'}
          InputProps={{ readOnly: true }}
        />
      </FormGroup>
    </>
  )
})
export default FinishedRoundSet
