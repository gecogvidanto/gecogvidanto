/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import EditIcon from '@material-ui/icons/Edit'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { ChangeEvent, FunctionComponent, KeyboardEvent, useCallback, useState } from 'react'

import { Player, Validator, validators } from '@gecogvidanto/shared'

import { ValidableInput } from '../../../components'
import { ValidationEvent } from '../../../hooks'
import { useGameStore, useLangStore } from '../../../stores'
import { PlayerSameNameValidator } from '../../../tools'
import { calculateRound } from './calculateRound'

export interface PlayerDetailsProps {
  canMasterGame: boolean
  player: { player: Player; index: number }
}

/**
 * Display details of the player. A game must be loaded for this component.
 */
const PlayerDetails: FunctionComponent<PlayerDetailsProps> = observer(({ canMasterGame, player }) => {
  const { lang } = useLangStore()
  const { neededGame: game, updatePlayer } = useGameStore()

  // State
  const [editedPlayerName, setEditedPlayerName] = useState<string>()
  const [validPlayerName, setValidPlayerName] = useState<boolean>(true)

  // Events
  const validatePlayer = useCallback(
    (cancel?: true): void => {
      if (!cancel && editedPlayerName && validPlayerName) {
        updatePlayer(player.index, editedPlayerName)
      }
      setEditedPlayerName(undefined)
    },
    [editedPlayerName, player.index, updatePlayer, validPlayerName]
  )
  const onPlayerEdited = useCallback((): void => {
    setEditedPlayerName(game.players[player.index].name)
    setValidPlayerName(true)
  }, [game.players, player.index])
  const onPlayerKeyDown = useCallback(
    (e: KeyboardEvent): void => {
      if (e.key === 'Enter') {
        validatePlayer()
      } else if (e.key === 'Escape') {
        validatePlayer(true)
      }
    },
    [validatePlayer]
  )
  const onPlayerChange = useCallback((e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
    setEditedPlayerName(e.target.value)
  }, [])
  const onPlayerValidate = useCallback(
    (e: ValidationEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
      setEditedPlayerName(e.target.value)
      setValidPlayerName(e.valid)
    },
    []
  )
  const onPlayerBlur = useCallback((): void => {
    validatePlayer()
  }, [validatePlayer])

  // Render
  const edited = !!editedPlayerName
  const editable: boolean = canMasterGame && !edited
  const listContent: JSX.Element[] = []
  if (editable) {
    listContent.push(
      <ListItemIcon key="editIcon">
        <EditIcon />
      </ListItemIcon>
    )
  }
  if (edited) {
    const validator: Validator[] = [
      ...validators.playerName,
      new PlayerSameNameValidator(
        game.players.filter((_, index) => index !== player.index).map(p => p.name)
      ),
    ]
    listContent.push(
      <ValidableInput
        key="editField"
        id={`update-player-${player.index}`}
        label={lang.pageAddPlayersId(player.index + 1)}
        value={editedPlayerName}
        autoFocus
        fullWidth
        onKeyDown={onPlayerKeyDown}
        onChange={onPlayerChange}
        onValidate={onPlayerValidate}
        onBlur={onPlayerBlur}
        validators={validator}
      />
    )
  } else {
    let playerData: string = player.player.name
    const playerDetails: string[] = []
    if (player.player.roundArrived !== 0) {
      const { roundSet, round } = calculateRound(player.player.roundArrived, game.roundsPerSet)
      playerDetails.push(lang.pageGamePlayersLimit(true, roundSet, round))
    }
    if (player.player.roundLeft !== 0) {
      const { roundSet, round } = calculateRound(player.player.roundLeft, game.roundsPerSet)
      playerDetails.push(lang.pageGamePlayersLimit(false, roundSet, round))
    }
    if (playerDetails.length > 0) {
      playerData += ' (' + playerDetails.join(', ') + ')'
    }
    listContent.push(<ListItemText key="playerData" primary={playerData} />)
  }
  return editable && !edited ? (
    <ListItem button onClick={onPlayerEdited}>
      {listContent}
    </ListItem>
  ) : (
    <ListItem>{listContent}</ListItem>
  )
})
export default PlayerDetails
