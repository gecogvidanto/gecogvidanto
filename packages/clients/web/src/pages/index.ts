/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
export { default as About } from './About'
export { default as EmailCheck } from './EmailCheck'
export { default as HelpSheet } from './HelpSheet'
export { default as Home } from './Home'
export { default as New } from './New'
export { default as NotFound } from './NotFound'
export { default as Play } from './Play'
export { default as Profile } from './Profile'
export { default as View } from './View'
