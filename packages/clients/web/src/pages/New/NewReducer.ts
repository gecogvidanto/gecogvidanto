/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Dispatch, useReducer } from 'react'

import { IdentifiedAddress } from '@gecogvidanto/shared'

/**
 * The state used for the component.
 */
interface State {
  locationName?: string
  address?: IdentifiedAddress | string
  roundsPerSet: number
  roundLength: number
  locationNameValid: boolean
  sending: boolean
}

/**
 * The target component names.
 */
export const enum TargetName {
  LocationName = 'location-name',
  Address = 'address',
  RoundsPerSet = 'rounds-per-set',
  RoundLength = 'round-length',
}

/**
 * Type of actions which can be applied to the state.
 */
const enum ActionType {
  UpdateValue,
  UpdateValidation,
  SetSending,
}

/**
 * Update string field value.
 */
interface UpdateStringValueAction {
  type: ActionType.UpdateValue
  target: TargetName.LocationName
  payload: string
}

/**
 * Update address field value.
 */
interface UpdateAddressValueAction {
  type: ActionType.UpdateValue
  target: TargetName.Address
  payload: string | IdentifiedAddress
}

/**
 * Update unknown field value.
 */
interface UpdateUnknownValueAction {
  type: ActionType.UpdateValue
  target: TargetName.RoundsPerSet | TargetName.RoundLength
  payload: unknown
}

/**
 * All update actions.
 */
type UpdateValueAction = UpdateStringValueAction | UpdateAddressValueAction | UpdateUnknownValueAction

/**
 * Build an action used to update the (location name) field value.
 *
 * @param target - The target component for which to update value.
 * @param value - The value to set.
 * @returns The corresponding action.
 */
export function updateValue(target: TargetName.LocationName, value: string): UpdateStringValueAction

/**
 * Build an action used to update the (address) field value.
 *
 * @param target - The target component for which to update value.
 * @param value - The value to set.
 * @returns The corresponding action.
 */
export function updateValue(
  target: TargetName.Address,
  value: string | IdentifiedAddress
): UpdateAddressValueAction

/**
 * Build an action used to update the (rounds) field value.
 *
 * @param target - The target component for which to update value.
 * @param value - The value to set.
 * @returns The corresponding action.
 */
export function updateValue(
  target: TargetName.RoundsPerSet | TargetName.RoundLength,
  value: unknown
): UpdateUnknownValueAction

/*
 * Implementation.
 */
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function updateValue(target: TargetName, value: any): UpdateValueAction {
  return { type: ActionType.UpdateValue, target, payload: value }
}

/**
 * Update validation action.
 */
interface UpdateValidationAction {
  type: ActionType.UpdateValidation
  target: string
  payload: boolean
}

/**
 * Build an action used to update the validation status.
 *
 * @param target - The target component for which to update validation state.
 * @param value - The value to set.
 * @returns The corresponding action.
 */
export function updateValidation(target: TargetName, value: boolean): UpdateValidationAction {
  return { type: ActionType.UpdateValidation, target, payload: value }
}

/**
 * Send action.
 */
interface SetSendingAction {
  type: ActionType.SetSending
  payload: boolean
}

/**
 * Build an action used to set the sending status.
 *
 * @param sending - The sending status.
 * @returns The corresponding action.
 */
export function setSending(sending: boolean): SetSendingAction {
  return { type: ActionType.SetSending, payload: sending }
}

/**
 * The reducer action.
 */
type Action = UpdateValueAction | UpdateValidationAction | SetSendingAction

/**
 * The initial state.
 */
const initialState: State = {
  roundsPerSet: 10,
  roundLength: 5,
  locationNameValid: true,
  sending: false,
}

/**
 * The reducer function. Treat all actions to update the state.
 *
 * @param state - The initial state.
 * @param action - The action to be executed.
 * @returns The new state.
 */
function reducer(state: State, action: Action): State {
  switch (action.type) {
    case ActionType.UpdateValue:
      switch (action.target) {
        case TargetName.LocationName:
          return { ...state, locationName: action.payload }
        case TargetName.Address:
          return { ...state, address: action.payload }
        case TargetName.RoundsPerSet:
          return { ...state, roundsPerSet: Number(action.payload) }
        case TargetName.RoundLength:
          return { ...state, roundLength: Number(action.payload) }
        default:
          // eslint-disable-next-line no-case-declarations
          const updateError: never = action
          throw new Error(`Unexpected target in action ${updateError}`)
      }
    case ActionType.UpdateValidation:
      if (action.target === TargetName.LocationName) {
        return { ...state, locationNameValid: action.payload }
      }
      break
    case ActionType.SetSending:
      return { ...state, sending: action.payload }
    default:
      // eslint-disable-next-line no-case-declarations
      const error: never = action
      throw new Error(`Unexpected type in action ${error}`)
  }
  return state
}

/**
 * Use the component reducer.
 *
 * @returns The same values as for `React.useReducer`.
 */
export default function useNewReducer(): [State, Dispatch<Action>] {
  return useReducer(reducer, initialState)
}
