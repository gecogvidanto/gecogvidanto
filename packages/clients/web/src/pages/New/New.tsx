/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  Theme,
  Typography,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { ChangeEvent, FunctionComponent, SyntheticEvent, useCallback, useEffect, useState } from 'react'
import { useHistory } from 'react-router'

import { IdentifiedAddress, validators } from '@gecogvidanto/shared'

import { Address, SignIn, ValidableInput } from '../../components'
import { ValidationEvent } from '../../hooks'
import { routes } from '../../routes'
import { useGameStore, useLangStore, useUserStore } from '../../stores'
import useNewReducer, { TargetName, setSending, updateValidation, updateValue } from './NewReducer'

const useStyles = makeStyles((theme: Theme) => ({
  paper: {
    padding: theme.spacing(1),
  },
  container: {
    ...theme.gecogvidanto.form.style.container,
    maxWidth: theme.gecogvidanto.form.pageWidth,
  },
  form: {
    ...theme.gecogvidanto.form.style.form,
  },
  submit: {
    ...theme.gecogvidanto.form.style.submit,
  },
}))

/**
 * Page used to create a new game.
 */
const New: FunctionComponent = observer(() => {
  const classes = useStyles()
  const history = useHistory()
  const { lang } = useLangStore()
  const { logged } = useUserStore()
  const { game, create, loadIfNeeded } = useGameStore()
  const [state, dispatch] = useNewReducer()

  // Data
  const valid: boolean = !!logged && state.locationNameValid && !state.sending

  // Events
  const onLocationChange = useCallback(
    (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
      dispatch(updateValue(e.target.name as TargetName.LocationName, e.target.value))
    },
    [dispatch]
  )
  const onAddressChange = useCallback(
    (value: string | IdentifiedAddress): void => {
      dispatch(updateValue(TargetName.Address, value))
    },
    [dispatch]
  )
  const onValueChange = useCallback(
    (e: ChangeEvent<{ name?: string; value: unknown }>): void => {
      dispatch(
        updateValue(e.target.name as TargetName.RoundsPerSet | TargetName.RoundLength, e.target.value)
      )
    },
    [dispatch]
  )
  const onValueValidate = useCallback(
    (e: ValidationEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
      dispatch(updateValidation(e.target.name as TargetName, e.valid))
    },
    [dispatch]
  )
  const onSubmit = useCallback(
    async (e: SyntheticEvent<any>): Promise<void> => {
      e.preventDefault()
      if (valid) {
        dispatch(setSending(true))
        try {
          await create(
            { name: state.locationName || undefined, address: state.address || undefined },
            state.roundsPerSet,
            state.roundLength
          )
        } catch {
          dispatch(setSending(false))
        }
      }
    },
    [valid, dispatch, create, state.locationName, state.address, state.roundsPerSet, state.roundLength]
  )

  // Manage game
  const [firstCall, setFirstCall] = useState(true)
  useEffect(() => {
    if (firstCall) {
      loadIfNeeded()
      setFirstCall(false)
    } else {
      game && history.push(routes.PLAYGAME.build({ id: game._id }))
    }
  }, [firstCall, game, history, loadIfNeeded])

  // Render
  const roundsPerSetValues: JSX.Element[] = [8, 10].map(value => (
    <MenuItem button key={value} value={value}>
      {value}
    </MenuItem>
  ))
  const roundLengthValues = [3, 4, 5].map(value => (
    <MenuItem button key={value} value={value}>
      {lang.minutes(value)}
    </MenuItem>
  ))
  return (
    <Paper className={classes.paper}>
      <Typography variant="h5" gutterBottom>
        {lang.pageNewGame()}
      </Typography>
      <Paper className={classes.container}>
        <form className={classes.form}>
          <ValidableInput
            id={TargetName.LocationName}
            name={TargetName.LocationName}
            label={lang.pageNewGameLocationName()}
            value={state.locationName || ''}
            autoFocus
            fullWidth
            validators={validators.location}
            onChange={onLocationChange}
            onValidate={onValueValidate}
          />
          <Address
            id={TargetName.Address}
            name={TargetName.Address}
            label={lang.pageNewGameLocationAddress()}
            defaultValue={state.address}
            margin="normal"
            fullWidth
            onChange={onAddressChange}
          />
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor={TargetName.RoundsPerSet}>{lang.pageNewGameRoundsPerSet()}</InputLabel>
            <Select
              id={TargetName.RoundsPerSet}
              name={TargetName.RoundsPerSet}
              value={state.roundsPerSet}
              onChange={onValueChange}
            >
              {...roundsPerSetValues}
            </Select>
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor={TargetName.RoundLength}>{lang.pageNewGameRoundLength()}</InputLabel>
            <Select
              id={TargetName.RoundLength}
              name={TargetName.RoundLength}
              value={state.roundLength}
              onChange={onValueChange}
            >
              {roundLengthValues}
            </Select>
          </FormControl>
          <Button
            type="submit"
            disabled={!valid}
            onClick={onSubmit}
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            {lang.ok()}
          </Button>
        </form>
      </Paper>
      <SignIn open={!logged} />
    </Paper>
  )
})

export default New
