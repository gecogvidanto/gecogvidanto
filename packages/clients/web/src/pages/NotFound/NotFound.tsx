/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Button, Paper, Theme, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { LinkHome } from '../../components'
import { useServerContext } from '../../context'
import { useLangStore } from '../../stores'

const useStyles = makeStyles((theme: Theme) => ({
  paper: {
    margin: theme.spacing(3),
    padding: theme.spacing(1),
  },
}))

/**
 * Not found page.
 */
const NotFound: FunctionComponent = observer(() => {
  const classes = useStyles()
  const serverContext = useServerContext()
  const { lang } = useLangStore()

  // Render
  if (serverContext) {
    serverContext.statusCode = 404
  }
  return (
    <Paper className={classes.paper}>
      <Typography variant="h5" gutterBottom>
        {lang.pageNotFound()}
      </Typography>
      <Typography>{lang.pageNotFoundLost()}</Typography>
      <Button component={LinkHome}>{lang.backToSiteHome()}</Button>
    </Paper>
  )
})

export default NotFound
