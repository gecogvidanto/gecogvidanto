/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Avatar, TableCell, TableRow, Theme } from '@material-ui/core'
// eslint-disable-next-line import/no-internal-modules
import { PaletteColor } from '@material-ui/core/styles/createPalette'
import { makeStyles } from '@material-ui/styles'
import classNames from 'classnames'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'
import { useHistory } from 'react-router'

import { Game } from '@gecogvidanto/shared'

import { useEventCallback, useUserData } from '../../../hooks'
import { routes } from '../../../routes'
import { useLangStore } from '../../../stores'
import { Gravatar } from '../../../tools'

const PICTURE_SIZE = 40

function linearGradient(color: PaletteColor): string {
  return `linear-gradient(to right, ${color.dark}, ${color.light}, ${color.dark})`
}

const useStyles = makeStyles((theme: Theme) => ({
  gameNew: {
    color: theme.palette.game.empty.contrastText,
    backgroundColor: theme.palette.game.empty.main,
    '&$hover:hover': {
      background: linearGradient(theme.palette.game.empty),
    },
  },
  gameInProgress: {
    color: theme.palette.game.inProgress.contrastText,
    backgroundColor: theme.palette.game.inProgress.main,
    '&$hover:hover': {
      background: linearGradient(theme.palette.game.inProgress),
    },
  },
  gameFinished: {
    color: theme.palette.game.finished.contrastText,
    backgroundColor: theme.palette.game.finished.main,
    '&$hover:hover': {
      background: linearGradient(theme.palette.game.finished),
    },
  },
  gameCloseable: {
    color: theme.palette.game.closeable.contrastText,
    backgroundColor: theme.palette.game.closeable.main,
    '&$hover:hover': {
      background: linearGradient(theme.palette.game.closeable),
    },
  },
  gameClosed: {
    color: theme.palette.game.closed.contrastText,
    backgroundColor: theme.palette.game.closed.main,
    '&$hover:hover': {
      background: linearGradient(theme.palette.game.closed),
    },
  },
  hover: {},
  tableCell: {
    color: 'inherit',
  },
  avatarTableCell: {
    paddingTop: 0,
    paddingBottom: 0,
  },
}))

export interface GameRowProps {
  game: Game
}

/**
 * Home page.
 */
const GameRow: FunctionComponent<GameRowProps> = observer(({ game }) => {
  const classes = useStyles()
  const { lang } = useLangStore()
  const history = useHistory()
  const setCount: number = game.roundSets.length
  const user = useUserData(game.masterId)
  const gravatar: Gravatar | undefined = user && new Gravatar(user.email)
  const playerCount: number = game.players.filter(
    player => player.roundArrived === 0 && player.roundLeft === 0
  ).length

  // Calculate data
  let status: keyof typeof classes
  if (setCount === 0 || (setCount === 1 && game.roundSets[0].currentRound === 0)) {
    status = 'gameNew'
  } else if (game.closed) {
    status = 'gameClosed'
  } else if (setCount >= 2 && game.roundSets[setCount - 1].currentRound > game.roundsPerSet) {
    status = 'gameCloseable'
  } else if (game.roundSets[setCount - 1].currentRound > game.roundsPerSet) {
    status = 'gameFinished'
  } else {
    status = 'gameInProgress'
  }
  const rowClasses = {
    root: classes[status],
    hover: classes.hover,
  }
  const location =
    typeof game.location.address === 'object'
      ? `${game.location.address.city} (${game.location.address.countryCode})`
      : undefined

  // Events
  const onGameSelect = useEventCallback(() => history.push(routes.GAME.build({ id: game._id! })))

  // Render
  return (
    <TableRow classes={rowClasses} hover onClick={onGameSelect}>
      <TableCell className={classNames(classes.tableCell, classes.avatarTableCell)}>
        {gravatar && <Avatar src={gravatar.getImageUrl(PICTURE_SIZE)} />}
      </TableCell>
      <TableCell className={classes.tableCell}>{lang._displayDate(new Date(game.start))}</TableCell>
      <TableCell className={classes.tableCell}>{location}</TableCell>
      <TableCell className={classes.tableCell}>{setCount}</TableCell>
      <TableCell className={classes.tableCell}>
        {game.players.length} ({playerCount})
      </TableCell>
    </TableRow>
  )
})

export default GameRow
