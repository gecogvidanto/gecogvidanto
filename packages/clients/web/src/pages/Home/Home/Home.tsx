/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Fab, Paper, Theme, Typography } from '@material-ui/core'
import RefreshIcon from '@material-ui/icons/Refresh'
import { makeStyles } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, useEffect, useMemo, useState } from 'react'

import { Game } from '@gecogvidanto/shared'

import { Waiting } from '../../../components'
import { useServerContext } from '../../../context'
import { UserDataCache, useEventCallback, useFetcher, useNewGameMenu } from '../../../hooks'
import { useLangStore } from '../../../stores'
import { api, notifyError, preload } from '../../../tools'
import Games from '../Games'

const NO_GAME: Game[] = []
const useStyles = makeStyles((theme: Theme) => ({
  paper: {
    padding: theme.spacing(1),
  },
  fab: {
    position: 'absolute',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
}))

/**
 * Home page.
 */
const Home: FunctionComponent = observer(() => {
  const classes = useStyles()
  const serverContext = useServerContext()
  const { lang } = useLangStore()
  useNewGameMenu()

  // Games
  const preloaded = useMemo<Game[] | undefined>(() => {
    const readAllGames = preload(serverContext, 'game', 'search')
    return readAllGames && readAllGames()
  }, [serverContext])
  const [reload, setReload] = useState<boolean | undefined>(preloaded ? undefined : true)
  const [loaded, { loading, error }] = useFetcher(reload ? api.game.search : undefined, NO_GAME)
  const games = preloaded || loaded

  // Manage status change
  useEffect(() => {
    if (reload === false) {
      setReload(true)
    }
  }, [reload])

  // Show error if any
  useEffect(() => {
    if (error) {
      notifyError(error, lang)
    }
  }, [error, lang])

  // Events
  const onRefreshContent = useEventCallback((): void => {
    setReload(false)
  })

  // Render
  return (
    <Paper className={classes.paper}>
      <Typography variant="h5" gutterBottom>
        {lang.pageIndex()}
      </Typography>
      <Waiting active={loading}>
        <UserDataCache>
          <Games games={games} />
        </UserDataCache>
      </Waiting>
      <Fab color="primary" aria-label={lang.refresh()} className={classes.fab} onClick={onRefreshContent}>
        <RefreshIcon />
      </Fab>
    </Paper>
  )
})

export default Home
