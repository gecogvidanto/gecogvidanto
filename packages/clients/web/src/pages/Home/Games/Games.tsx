/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Table, TableBody, TableCell, TableHead, TableRow, Typography } from '@material-ui/core'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { Game } from '@gecogvidanto/shared'

import { useLangStore } from '../../../stores'
import GameRow from '../GameRow'

export interface GamesProps {
  games: Game[]
}

/**
 * Home page.
 */
const Games: FunctionComponent<GamesProps> = observer(({ games }) => {
  const { lang } = useLangStore()

  if (games.length) {
    return (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell>{lang.pageIndexDate()}</TableCell>
            <TableCell>{lang.pageIndexLocation()}</TableCell>
            <TableCell>{lang.pageIndexSets()}</TableCell>
            <TableCell>{lang.pageIndexPlayers()}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {games.map(game => (
            <GameRow key={game._id} game={game} />
          ))}
        </TableBody>
      </Table>
    )
  } else {
    return <Typography>{lang.pageIndexNoGame()}</Typography>
  }
})

export default Games
