/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { useObserver } from 'mobx-react-lite'
import * as React from 'react'
import { ChangeEvent, FunctionComponent, useCallback, useMemo } from 'react'

import { AsyncValidator, validators } from '@gecogvidanto/shared'

import { ValidableInput } from '../../../components'
import { ValidationEvent } from '../../../hooks'
import { useLangStore } from '../../../stores'
import { Mode, TargetName, updateValue, useProfileReducer, validateField } from '../Profile'
import NameValidator from './NameValidator'

export interface NameProps {
  paramsId: string | undefined
}

/**
 * The name field.
 */
/* (JSX element) */
const Name: FunctionComponent<NameProps> = ({ paramsId }) => {
  const { lang } = useLangStore()
  const [state, dispatch] = useProfileReducer()
  const userId = paramsId || state.user._id
  const nameValidators = useMemo<AsyncValidator[]>(() => [new NameValidator(userId)], [userId])
  const onValueChange = useCallback(
    (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
      dispatch(updateValue(e.target.name as TargetName, e.target.value))
    },
    [dispatch]
  )
  const onValueValidate = useCallback(
    (e: ValidationEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
      dispatch(validateField(e.target.name as TargetName, e.valid))
    },
    [dispatch]
  )
  return useObserver(() => (
    <ValidableInput
      id={TargetName.Name}
      name={TargetName.Name}
      label={lang.pageProfileName()}
      value={state.user.name}
      autoFocus
      readOnly={state.mode === Mode.Show}
      required={state.mode !== Mode.Show}
      fullWidth
      validators={validators.name}
      asyncValidators={nameValidators}
      onChange={onValueChange}
      onValidate={onValueValidate}
    />
  ))
}
export default Name
