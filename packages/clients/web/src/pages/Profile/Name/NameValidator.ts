/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { AsyncValidator, User } from '@gecogvidanto/shared'

import { langType } from '../../../locale'
import { api } from '../../../tools'

export default class NameValidator implements AsyncValidator<langType, []> {
  public readonly params: [] = []

  public constructor(private readonly id: string | undefined) {}

  public async validate(value: string): Promise<'pageProfileNameDuplicate' | undefined> {
    const users: User[] = await api.user.search(undefined, value)
    if (users.length > 0 && users[0]._id !== this.id) {
      return 'pageProfileNameDuplicate'
    } else {
      return undefined
    }
  }
}
