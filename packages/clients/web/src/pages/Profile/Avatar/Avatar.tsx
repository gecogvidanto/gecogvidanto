/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Paper, Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { Gravatar } from '../../../tools'
import { Mode, useProfileReducer } from '../Profile'

const AVATAR_SIZE = 200

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    width: `${AVATAR_SIZE}px`,
    height: `${AVATAR_SIZE}px`,
    marginLeft: theme.spacing(1),
  },
}))

/**
 * The avatar.
 */
/* (JSX element) */
const Avatar: FunctionComponent = () => {
  const classes = useStyles()
  const [state] = useProfileReducer()
  const gravatar = new Gravatar(state.user.email)
  const imageType: 'mp' | 'identicon' = state.mode === Mode.Create ? 'mp' : 'identicon'
  return (
    <Paper className={classes.container}>
      <a target="_blank" rel="noopener noreferrer" href={gravatar.getProfileUrl()}>
        <img
          alt={state.user.name}
          src={gravatar.getImageUrl(AVATAR_SIZE, imageType)}
          width={AVATAR_SIZE}
          height={AVATAR_SIZE}
        />
      </a>
    </Paper>
  )
}

export default Avatar
