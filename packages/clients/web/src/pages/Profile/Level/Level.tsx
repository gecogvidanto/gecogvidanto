/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { FormControl, InputLabel, MenuItem, Select } from '@material-ui/core'
import { useObserver } from 'mobx-react-lite'
import * as React from 'react'
import { ChangeEvent, FunctionComponent, useCallback } from 'react'

import { CertLevel, Enum } from '@gecogvidanto/shared'

import { useLangStore } from '../../../stores'
import { Mode, TargetName, updateValue, useProfileReducer } from '../Profile'

/**
 * The level field.
 */
/* (JSX element) */
const Level: FunctionComponent = () => {
  const { lang } = useLangStore()
  const [state, dispatch] = useProfileReducer()
  const onValueChange = useCallback(
    (e: ChangeEvent<{ name?: string; value: unknown }>): void => {
      dispatch(
        updateValue(e.target.name as TargetName, CertLevel[e.target.value as keyof typeof CertLevel])
      )
    },
    [dispatch]
  )
  return useObserver(() =>
    state.mode === Mode.Create ? null : (
      <FormControl margin="normal" disabled={state.mode !== Mode.Edit} fullWidth>
        <InputLabel htmlFor={TargetName.Level}>{lang.pageProfileLevel()}</InputLabel>
        <Select
          id={TargetName.Level}
          name={TargetName.Level}
          value={Enum.getName(CertLevel, state.user.level as CertLevel)}
          onChange={onValueChange}
        >
          {Enum.getAllNames(CertLevel).map(value => (
            <MenuItem key={value} value={value}>
              {lang._displayCertLevel(value)}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    )
  )
}
export default Level
