/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Paper, Theme, Typography } from '@material-ui/core'
import { makeStyles, useTheme } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, SyntheticEvent, useCallback, useEffect, useMemo } from 'react'
import { useHistory, useParams } from 'react-router'

import { User } from '@gecogvidanto/shared'

import { Waiting } from '../../../components'
import { useServerContext } from '../../../context'
import { useEventCallback, useFetcher, useNewGameMenu, useReCaptcha } from '../../../hooks'
import { routes } from '../../../routes'
import { useLangStore, useSdataStore, useUserStore } from '../../../stores'
import { api, notificationManager, notifyError, preload } from '../../../tools'
import Account from '../Account'
import Avatar from '../Avatar'
import Email from '../Email'
import Level from '../Level'
import Name from '../Name'
import Password from '../Password'
import PasswordCheck from '../PasswordCheck'
import PasswordReset from '../PasswordReset'
import Submit from '../Submit'
import { ProfileReducerProvider } from './context'
import Mode from './Mode'
import useProfileReducer, { resetMode, resetUser, setLoading, setSending } from './ProfileReducer'

const useStyles = makeStyles((theme: Theme) => ({
  paper: {
    padding: theme.spacing(1),
  },
  container: {
    display: 'flex',
    flexDirection: 'row',
  },
  paperContainer: {
    ...theme.gecogvidanto.form.style.container,
    maxWidth: theme.gecogvidanto.form.pageWidth,
  },
  form: {
    ...theme.gecogvidanto.form.style.form,
  },
}))

/**
 * The profile page may have different aspects, depending on:
 * - who the connected user is,
 * - if an identifier, different from logged user, is provided.
 *
 * |       | Administrator | Identified | Anonymous   |
 * |-------|---------------|------------|-------------|
 * | id    | Full edit     | Show       | Show        |
 * | no id | Edit self     | Edit self  | Create user |
 *
 * - When in **show** mode, password is not displayed, other fields cannot be edited. There is no validation button.
 * - When in **create user** mode, level is not displayed, other fields are empty, expecting user to fill them.
 * - When in **edit self** mode, level cannot be modified, password is shown empty and will only be updated if filled.
 * - When in **full edit** mode, password is replaced with a reset password button.
 */
const Profile: FunctionComponent = observer(() => {
  const theme = useTheme<Theme>()
  const classes = useStyles()
  const { lang } = useLangStore()
  const { captchaKey } = useSdataStore()
  const { logged, refresh } = useUserStore()
  const history = useHistory()
  const { id: paramsId } = useParams<{ id?: string }>()
  const serverContext = useServerContext()
  const [state, dispatch] = useProfileReducer(logged, paramsId)
  useNewGameMenu()

  // Reset if mode change
  useEffect(() => {
    dispatch(resetMode(logged, paramsId))
  }, [dispatch, logged, paramsId])

  // Manage captcha
  const getToken = useReCaptcha(
    state.mode === Mode.Create ? captchaKey : undefined,
    'g-re-captcha',
    'userCreate'
  )

  // Load user data
  {
    const preloaded = useMemo(() => {
      const readUser = preload(serverContext, 'user', 'read')
      return state.mode === Mode.Self ? logged! : (paramsId && readUser && readUser(paramsId)) || undefined
    }, [logged, paramsId, serverContext, state.mode])
    const loadUser = useCallback(() => api.user.read(paramsId!), [paramsId])
    const [loaded, { loading, error }] = useFetcher<User>(
      paramsId && (!preloaded || preloaded._id !== paramsId) ? loadUser : undefined
    )
    const user = loaded || preloaded
    useEffect(() => {
      dispatch(setLoading(loading))
    }, [dispatch, loading])
    useEffect(() => {
      if (error) {
        notifyError(error, lang)
      }
    }, [error, lang])
    useEffect(() => {
      dispatch(resetUser(user))
    }, [dispatch, user])
  }

  // Calculate validity
  const valid: boolean = state.fieldsValid && (state.mode !== Mode.Create || !captchaKey || !!getToken)

  // Submit profile
  const onSubmit = useEventCallback(
    async (e: SyntheticEvent<any>): Promise<void> => {
      e.preventDefault()
      if (valid) {
        const validUser: User = {
          ...state.user,
          account: state.user.account || undefined,
        }
        dispatch(setSending(true))
        try {
          let result: User
          if (validUser._id) {
            result = await api.user.update(validUser, state.password || undefined)
            refresh(result)
            notificationManager.emitSuccess(lang.userUpdateSuccess(result.name))
          } else {
            const token: string = getToken ? await getToken() : ''
            result = await api.user.create(token, validUser, state.password!)
            notificationManager.emitInfo(lang.userCreated(result.name))
          }
          if (validUser.email !== result.email) {
            notificationManager.emitInfo(lang.userCheckEmail())
          }
          validUser._id || setTimeout(() => history.push(routes.HOME.build()))
        } catch (error) {
          notifyError(error, lang)
        } finally {
          dispatch(setSending(false))
        }
        notificationManager.emitSuccess(lang.pageProfileDataSent())
      }
    }
  )

  // Render
  const title =
    state.mode === Mode.Create
      ? lang.pageProfileSignUp(theme.gecogvidanto.applicationName)
      : lang.pageProfile()
  return (
    <ProfileReducerProvider value={[state, dispatch]}>
      <Paper className={classes.paper}>
        <Typography variant="h5" gutterBottom>
          {title}
        </Typography>
        <Waiting active={state.loading || state.sending}>
          <div className={classes.container}>
            <Paper className={classes.paperContainer}>
              <form className={classes.form}>
                <Name paramsId={paramsId} />
                <Email paramsId={paramsId} />
                <Password />
                <PasswordCheck />
                <Level />
                <Account />
                <PasswordReset />
                <Submit valid={valid} onSubmit={onSubmit} />
              </form>
            </Paper>
            <Avatar />
          </div>
        </Waiting>
      </Paper>
    </ProfileReducerProvider>
  )
})

export default Profile
