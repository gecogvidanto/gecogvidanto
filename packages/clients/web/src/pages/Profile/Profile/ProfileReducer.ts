/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Dispatch, useReducer } from 'react'

import { CertLevel, User } from '@gecogvidanto/shared'

import Mode from './Mode'

/**
 * The state used for the component.
 */
export interface State {
  mode: Mode
  user: User
  password: string
  passwordCheck: string
  emailValid: boolean
  nameValid: boolean
  accountValid: boolean
  passwordValid: boolean
  passwordCheckValid: boolean
  fieldsValid: boolean
  passwordReset: boolean
  showPassword: boolean
  loading: boolean
  sending: boolean
}

/**
 * The target component names.
 */
export const enum TargetName {
  Name = 'name',
  Email = 'email',
  Password = 'password',
  PasswordCheck = 'password-check',
  PasswordReset = 'password-reset',
  Level = 'level',
  Account = 'account',
}

/**
 * Type of actions which can be applied to the state.
 */
const enum ActionType {
  ResetMode,
  ResetUser,
  SetPasswordVisibility,
  UpdateValue,
  ValidateField,
  SetLoading,
  SetSending,
}

/**
 * Action used to reset the mode.
 */
interface ResetModeAction {
  type: ActionType.ResetMode
  payload: {
    logged: User | undefined
    paramsId: string | undefined
  }
}

/**
 * Build an action used to reset the mode.
 *
 * @param logged - The currently logged user.
 * @param paramsId - The identifier given in parameters.
 * @returns The corresponding action.
 */
export function resetMode(logged: User | undefined, paramsId: string | undefined): ResetModeAction {
  return { type: ActionType.ResetMode, payload: { logged, paramsId } }
}

/**
 * Action used to reset the loaded user.
 */
interface ResetUserAction {
  type: ActionType.ResetUser
  payload: User | undefined
}

/**
 * Build an action used to reset the user.
 *
 * @param user - The user data to use.
 * @returns The corresponding action.
 */
export function resetUser(user: User | undefined): ResetUserAction {
  return { type: ActionType.ResetUser, payload: user }
}

/**
 * Action used to set the password visibility.
 */
interface SetPasswordVisibilityAction {
  type: ActionType.SetPasswordVisibility
  payload: boolean
}

/**
 * Build an action used to set the password visibility.
 *
 * @param visible - Indicate the password visibility.
 * @returns The corresponding action.
 */
export function setPasswordVisibility(visible: boolean): SetPasswordVisibilityAction {
  return { type: ActionType.SetPasswordVisibility, payload: visible }
}

/**
 * Action used to update a string value in the form.
 */
interface UpdateValueAction {
  type: ActionType.UpdateValue
  target: TargetName
  payload: any
}

/**
 * Build an action used to update the value of a field.
 *
 * @param target - The field which value is to be updated.
 * @param value - The new value to set to field.
 * @returns The corresponding action.
 */
export function updateValue(target: TargetName, value: unknown): UpdateValueAction {
  return { type: ActionType.UpdateValue, target, payload: value }
}

/**
 * Action used to indicate that a field is valid or not.
 */
interface ValidateFieldAction {
  type: ActionType.ValidateField
  target: TargetName
  payload: boolean
}

/**
 * Build an action used to indicate that a field is valid or not.
 *
 * @param target - The field which value is to be updated.
 * @param valid - Indicate if the field is valid or not.
 * @returns The corresponding action.
 */
export function validateField(target: TargetName, valid: boolean): ValidateFieldAction {
  return { type: ActionType.ValidateField, target, payload: valid }
}

/**
 * Action used to indicate that user is currently being loaded.
 */
interface SetLoadingAction {
  type: ActionType.SetLoading
  payload: boolean
}

/**
 * Build an action used to indicate that user is currently being loaded.
 *
 * @param loading - Indicate that user is currently being loaded.
 * @returns The corresponding action.
 */
export function setLoading(loading: boolean): SetLoadingAction {
  return { type: ActionType.SetLoading, payload: loading }
}

/**
 * Action used to indicate that data are being sent.
 */
interface SetSendingAction {
  type: ActionType.SetSending
  payload: boolean
}

/**
 * Build an action used to indicate that data are being sent.
 *
 * @param sending - Indicate that data are being sent.
 * @returns The corresponding action.
 */
export function setSending(sending: boolean): SetSendingAction {
  return { type: ActionType.SetSending, payload: sending }
}

/**
 * The reducer action.
 */
export type Action =
  | ResetModeAction
  | ResetUserAction
  | SetPasswordVisibilityAction
  | UpdateValueAction
  | ValidateFieldAction
  | SetLoadingAction
  | SetSendingAction

/**
 * The initial state.
 */
const initialState = {
  user: {
    email: '',
    name: '',
    level: CertLevel.GameMaster,
  },
  password: '',
  passwordCheck: '',
  passwordReset: false,
  showPassword: false,
  loading: false,
  sending: false,
}

/**
 * Calculate the profile mode.
 *
 * @param logged - The logged user.
 * @param paramsId - The identifier of the user to edit as per query parameters.
 * @returns The calculated mode.
 */
function calculateMode(logged: User | undefined, paramsId: string | undefined): Mode {
  if (logged) {
    if (!paramsId || logged._id === paramsId) {
      return Mode.Self
    } else if (logged.level === CertLevel.Administrator) {
      return Mode.Edit
    } else {
      return Mode.Show
    }
  } else {
    if (!paramsId) {
      return Mode.Create
    } else {
      return Mode.Show
    }
  }
}

/**
 * Initialize the state.
 *
 * @param mode - The profile mode.
 * @returns The initial state.
 */
function init(mode: Mode): State {
  return {
    ...initialState,
    mode,
    emailValid: mode !== Mode.Create,
    nameValid: mode !== Mode.Create,
    accountValid: mode !== Mode.Create,
    passwordValid: mode !== Mode.Create,
    passwordCheckValid: mode !== Mode.Create,
    fieldsValid: mode !== Mode.Create,
  }
}

/**
 * The reducer function. Treat all actions to update the state.
 *
 * @param state - The initial state.
 * @param action - The action to be executed.
 * @returns The new state.
 */
function reducer(state: State, action: Action): State {
  switch (action.type) {
    case ActionType.ResetMode:
      {
        const mode = calculateMode(action.payload.logged, action.payload.paramsId)
        if (mode !== state.mode) {
          return init(mode)
        }
      }
      break
    case ActionType.ResetUser:
      if (action.payload) {
        return {
          ...state,
          user: action.payload,
          emailValid: true,
          nameValid: true,
          accountValid: true,
          passwordValid: true,
          passwordCheckValid: true,
          fieldsValid: true,
        }
      }
      break
    case ActionType.SetPasswordVisibility:
      if (state.showPassword !== action.payload) {
        return {
          ...state,
          showPassword: action.payload,
          fieldsValid:
            state.nameValid &&
            state.emailValid &&
            state.accountValid &&
            state.passwordValid &&
            (action.payload || state.passwordCheckValid),
        }
      }
      break
    case ActionType.UpdateValue:
      switch (action.target) {
        case TargetName.Name:
          if (state.user.name !== action.payload) {
            return { ...state, user: { ...state.user, name: action.payload } }
          }
          break
        case TargetName.Email:
          if (state.user.email !== action.payload) {
            return { ...state, user: { ...state.user, email: action.payload } }
          }
          break
        case TargetName.Password:
          if (state.password !== action.payload) {
            return { ...state, password: action.payload }
          }
          break
        case TargetName.PasswordCheck:
          if (state.passwordCheck !== action.payload) {
            return { ...state, passwordCheck: action.payload }
          }
          break
        case TargetName.PasswordReset:
          if (state.passwordReset !== action.payload) {
            return { ...state, passwordReset: action.payload }
          }
          break
        case TargetName.Level:
          if (state.user.level !== action.payload) {
            return { ...state, user: { ...state.user, level: action.payload } }
          }
          break
        case TargetName.Account:
          if (state.user.account !== action.payload) {
            return { ...state, user: { ...state.user, account: action.payload } }
          }
          break
      }
      break
    case ActionType.ValidateField:
      switch (action.target) {
        case TargetName.Name:
          if (state.nameValid !== action.payload) {
            return {
              ...state,
              nameValid: action.payload,
              fieldsValid:
                action.payload &&
                state.emailValid &&
                state.accountValid &&
                state.passwordValid &&
                (state.showPassword || state.passwordCheckValid),
            }
          }
          break
        case TargetName.Email:
          if (state.emailValid !== action.payload) {
            return {
              ...state,
              emailValid: action.payload,
              fieldsValid:
                state.nameValid &&
                action.payload &&
                state.accountValid &&
                state.passwordValid &&
                (state.showPassword || state.passwordCheckValid),
            }
          }
          break
        case TargetName.Account:
          if (state.accountValid !== action.payload) {
            return {
              ...state,
              accountValid: action.payload,
              fieldsValid:
                state.nameValid &&
                state.emailValid &&
                action.payload &&
                state.passwordValid &&
                (state.showPassword || state.passwordCheckValid),
            }
          }
          break
        case TargetName.Password:
          if (state.passwordValid !== action.payload) {
            return {
              ...state,
              passwordValid: action.payload,
              fieldsValid:
                state.nameValid &&
                state.emailValid &&
                state.accountValid &&
                action.payload &&
                (state.showPassword || state.passwordCheckValid),
            }
          }
          break
        case TargetName.PasswordCheck:
          if (state.passwordCheckValid !== action.payload) {
            return {
              ...state,
              passwordCheckValid: action.payload,
              fieldsValid:
                state.nameValid &&
                state.emailValid &&
                state.accountValid &&
                state.passwordValid &&
                (state.showPassword || action.payload),
            }
          }
          break
      }
      break
    case ActionType.SetLoading:
      if (state.loading !== action.payload) {
        return { ...state, loading: action.payload }
      }
      break
    case ActionType.SetSending:
      if (state.sending !== action.payload) {
        return { ...state, sending: action.payload }
      }
      break
    default:
      // eslint-disable-next-line no-case-declarations
      const error: never = action
      throw new Error(`Unexpected type in action ${error}`)
  }
  return state
}

/**
 * Use the component reducer.
 *
 * @param logged - The currently logged user.
 * @param paramsId - The identifier given in parameters.
 * @returns The same values as for `React.useReducer`.
 */
export default function useProfileReducer(
  logged: User | undefined,
  paramsId: string | undefined
): [State, Dispatch<Action>] {
  return useReducer(reducer, calculateMode(logged, paramsId), init)
}
