/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Button, Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { useObserver } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, SyntheticEvent } from 'react'

import { useLangStore } from '../../../stores'
import { Mode, useProfileReducer } from '../Profile'

const useStyles = makeStyles((theme: Theme) => ({
  submit: {
    ...theme.gecogvidanto.form.style.submit,
  },
}))

export interface SubmitProps {
  valid: boolean
  onSubmit: (e: SyntheticEvent<any>) => void
}

/**
 * The submit button.
 */
/* (JSX element) */
const Submit: FunctionComponent<SubmitProps> = ({ valid, onSubmit }) => {
  const classes = useStyles()
  const { lang } = useLangStore()
  const [state] = useProfileReducer()
  return useObserver(() =>
    state.mode === Mode.Show ? null : (
      <Button
        type="submit"
        disabled={!valid}
        onClick={onSubmit}
        fullWidth
        variant="contained"
        color="primary"
        className={classes.submit}
      >
        {lang.ok()}
      </Button>
    )
  )
}

export default Submit
