/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { FormControlLabel, Switch } from '@material-ui/core'
import { useObserver } from 'mobx-react-lite'
import * as React from 'react'
import { ChangeEvent, FunctionComponent, useCallback } from 'react'

import { useLangStore } from '../../../stores'
import { Mode, TargetName, updateValue, useProfileReducer } from '../Profile'

/**
 * The reset password switch.
 */
/* (JSX element) */
const PasswordReset: FunctionComponent = () => {
  const { lang } = useLangStore()
  const [state, dispatch] = useProfileReducer()
  const onValueChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>, checked: boolean): void => {
      dispatch(updateValue(e.target.name as TargetName, checked))
    },
    [dispatch]
  )
  const control = (
    <Switch
      id={TargetName.PasswordReset}
      name={TargetName.PasswordReset}
      color="primary"
      checked={state.passwordReset}
      onChange={onValueChange}
    />
  )
  return useObserver(() =>
    state.mode === Mode.Edit ? (
      <FormControlLabel control={control} label={lang.pageProfilePasswordReset()} />
    ) : null
  )
}
export default PasswordReset
