/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { useObserver } from 'mobx-react-lite'
import * as React from 'react'
import { ChangeEvent, FunctionComponent, useCallback } from 'react'

import { Validator, allowEmpty, validators } from '@gecogvidanto/shared'

import { ValidablePassword } from '../../../components'
import { ValidationEvent } from '../../../hooks'
import { useLangStore } from '../../../stores'
import {
  Mode,
  TargetName,
  setPasswordVisibility,
  updateValue,
  useProfileReducer,
  validateField,
} from '../Profile'

/**
 * The password field.
 */
/* (JSX element) */
const Password: FunctionComponent = () => {
  const { lang } = useLangStore()
  const [state, dispatch] = useProfileReducer()
  let passwordValidators: Validator[] = validators.password
  state.mode !== Mode.Create && (passwordValidators = allowEmpty(passwordValidators))
  const onValueChange = useCallback(
    (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
      dispatch(updateValue(e.target.name as TargetName, e.target.value))
    },
    [dispatch]
  )
  const onValueValidate = useCallback(
    (e: ValidationEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
      dispatch(validateField(e.target.name as TargetName, e.valid))
    },
    [dispatch]
  )
  const onToggleVisibility = useCallback(
    (visible: boolean): void => {
      dispatch(setPasswordVisibility(visible))
    },
    [dispatch]
  )
  return useObserver(() =>
    [Mode.Show, Mode.Edit].includes(state.mode) ? null : (
      <ValidablePassword
        id={TargetName.Password}
        name={TargetName.Password}
        label={lang.password()}
        placeholder={state.mode !== Mode.Create ? lang.unchanged() : undefined}
        value={state.password}
        required={state.mode === Mode.Create}
        fullWidth
        visibility={state.showPassword}
        onToggleVisibility={onToggleVisibility}
        validators={passwordValidators}
        onChange={onValueChange}
        onValidate={onValueValidate}
      />
    )
  )
}

export default Password
