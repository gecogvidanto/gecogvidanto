/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { useObserver } from 'mobx-react-lite'
import * as React from 'react'
import { ChangeEvent, FunctionComponent, useCallback } from 'react'

import { IdentityValidator } from '@gecogvidanto/shared'

import { ValidableInput } from '../../../components'
import { ValidationEvent } from '../../../hooks'
import { useLangStore } from '../../../stores'
import { Mode, TargetName, updateValue, useProfileReducer, validateField } from '../Profile'

/**
 * The password control field.
 */
/* (JSX element) */
const PasswordCheck: FunctionComponent = () => {
  const { lang } = useLangStore()
  const [state, dispatch] = useProfileReducer()
  const passwordCheckValidators = [new IdentityValidator(state.password || '')]
  const onValueChange = useCallback(
    (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
      dispatch(updateValue(e.target.name as TargetName, e.target.value))
    },
    [dispatch]
  )
  const onValueValidate = useCallback(
    (e: ValidationEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
      dispatch(validateField(e.target.name as TargetName, e.valid))
    },
    [dispatch]
  )
  return useObserver(() =>
    [Mode.Show, Mode.Edit].includes(state.mode) || state.showPassword ? null : (
      <ValidableInput
        id={TargetName.PasswordCheck}
        name={TargetName.PasswordCheck}
        label={lang.pageProfilePasswordCheck()}
        type="password"
        value={state.passwordCheck}
        required={state.mode === Mode.Create}
        fullWidth
        validators={passwordCheckValidators}
        onChange={onValueChange}
        onValidate={onValueValidate}
      />
    )
  )
}

export default PasswordCheck
