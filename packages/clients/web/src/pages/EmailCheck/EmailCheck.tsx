/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Button, Paper, Theme, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, useCallback, useEffect, useMemo, useState } from 'react'
import { useParams } from 'react-router'

import { EmailCheck as EmailCheckObject } from '@gecogvidanto/shared'

import { LinkHome, Waiting } from '../../components'
import { useServerContext } from '../../context'
import { useFetcher } from '../../hooks'
import { useLangStore } from '../../stores'
import { api, notifyError, preload } from '../../tools'

const enum CheckingStep {
  NotStarted,
  Checking,
  Succeeded,
  Failed,
}

const useStyles = makeStyles((theme: Theme) => ({
  paper: {
    margin: theme.spacing(3),
    padding: theme.spacing(1),
  },
}))

function useDecoded(emailParams: EmailCheckObject): EmailCheckObject {
  return useMemo(
    () => ({
      email: decodeURIComponent(emailParams.email),
      token: decodeURIComponent(emailParams.token),
    }),
    [emailParams.email, emailParams.token]
  )
}

/**
 * Email check page.
 */
const EmailCheck: FunctionComponent = observer(() => {
  const classes = useStyles()
  const { lang } = useLangStore()
  const serverContext = useServerContext()
  const emailParams = useDecoded(useParams<EmailCheckObject>())

  // Checking step
  const [step, setStep] = useState<CheckingStep>(
    (): CheckingStep => {
      const checkEmail = preload(serverContext, 'user', 'checkEmail')
      return checkEmail
        ? checkEmail(emailParams)
          ? CheckingStep.Succeeded
          : CheckingStep.Failed
        : CheckingStep.NotStarted
    }
  )

  // Manage validation call
  const emailValidation = useCallback(() => api.emailCheck.validate(emailParams), [emailParams])
  const [, { loading, error }] = useFetcher(
    [CheckingStep.NotStarted, CheckingStep.Checking].includes(step) ? emailValidation : undefined
  )

  // Manage step change
  useEffect(() => {
    if (step === CheckingStep.NotStarted && loading) {
      setStep(CheckingStep.Checking)
    } else if (step === CheckingStep.Checking && !loading) {
      if (error) {
        setStep(CheckingStep.Failed)
        notifyError(error, lang)
      } else {
        setStep(CheckingStep.Succeeded)
      }
    }
  }, [error, lang, loading, step])

  // Render
  let contentNode: JSX.Element
  switch (step) {
    case CheckingStep.NotStarted:
    case CheckingStep.Checking:
      contentNode = (
        <Waiting active>
          <Typography>{lang.pageEmailCheckPleaseWait()}</Typography>
        </Waiting>
      )
      break
    case CheckingStep.Succeeded:
      contentNode = (
        <>
          <Typography>{lang.pageEmailCheckSucceeded()}</Typography>
          <Button component={LinkHome}>{lang.backToSiteHome()}</Button>
        </>
      )
      break
    case CheckingStep.Failed:
      contentNode = (
        <>
          <Typography color="error">{lang.pageEmailCheckFailed()}</Typography>
          <Button component={LinkHome}>{lang.backToSiteHome()}</Button>
        </>
      )
      break
    default:
      // eslint-disable-next-line no-case-declarations
      const errorStep: never = step
      throw new Error(`Unknown step ${errorStep}`)
  }
  return (
    <Paper className={classes.paper}>
      <Typography variant="h5" gutterBottom>
        {lang.pageEmailCheck()}
      </Typography>
      {contentNode}
    </Paper>
  )
})

export default EmailCheck
