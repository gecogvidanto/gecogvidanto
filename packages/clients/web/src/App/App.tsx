/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { CssBaseline, Theme } from '@material-ui/core'
import { ThemeProvider } from '@material-ui/styles'
import * as React from 'react'
import { FunctionComponent } from 'react'
import { Route, Switch } from 'react-router-dom'

import { EmailCheck, HelpSheet, NotFound } from '../pages'
import { routes } from '../routes'
import { StoreSet, StoreSetProvider } from '../stores'
import AppContent from './AppContent'

export interface AppProps {
  /**
   * The theme of the application.
   */
  theme: Theme

  /**
   * The stores of the application.
   */
  storeSet: StoreSet
}

/**
 * Application entry point. Contains the main routes, which are above the layout.
 */
/* (JSX element) */
const App: FunctionComponent<AppProps> = ({ theme, storeSet }) => (
  <ThemeProvider theme={theme}>
    <StoreSetProvider value={storeSet}>
      <CssBaseline />
      <Switch>
        <Route path={routes.HELPSHEET.path}>
          <HelpSheet />
        </Route>
        <Route path={routes.EMAIL_CHECK.path}>
          <EmailCheck />
        </Route>
        <Route path={routes.NOT_FOUND.path}>
          <NotFound />
        </Route>
        <Route>
          <AppContent />
        </Route>
      </Switch>
    </StoreSetProvider>
  </ThemeProvider>
)
export default App
