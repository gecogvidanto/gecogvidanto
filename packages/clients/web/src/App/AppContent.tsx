/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as React from 'react'
import { FunctionComponent } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'

import Layout from '../Layout'
import { About, Home, New, Play, Profile, View } from '../pages'
import { routes } from '../routes'

/**
 * A component redirecting to not found page.
 */
/* (JSX element) */
const NotFoundRedirect: FunctionComponent = () => <Redirect to={routes.NOT_FOUND.build()} />

/**
 * Application content.
 */
/* (JSX element) */
const AppContent: FunctionComponent = () => (
  <Layout>
    <Switch>
      <Route exact path={routes.HOME.path}>
        <Home />
      </Route>
      <Route path={routes.PROFILE.path}>
        <Profile />
      </Route>
      <Route path={routes.GAME.path}>
        <View />
      </Route>
      <Route path={routes.PLAYGAME.path}>
        <Play />
      </Route>
      <Route path={routes.NEWGAME.path}>
        <New />
      </Route>
      <Route path={routes.ABOUT.path}>
        <About />
      </Route>
      <Route>
        <NotFoundRedirect />
      </Route>
    </Switch>
  </Layout>
)
export default AppContent
