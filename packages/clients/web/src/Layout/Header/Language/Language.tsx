/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Badge, IconButton, Menu, MenuItem } from '@material-ui/core'
import LanguageIcon from '@material-ui/icons/Language'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, MouseEvent, useCallback, useState } from 'react'

import { useLangStore } from '../../../stores'
import { UiUrlAnalyzer } from '../../../tools'

const DEFAULT_LANG = 'en'
const MENU_MAX_HEIGHT = '50%'

/**
 * Language selection.
 */
const Language: FunctionComponent = observer(() => {
  const { lang } = useLangStore()
  const { availables } = lang.$languageMap
  const active = availables.length > 1
  let selected = lang.$preferences.length > 0 ? lang.$preferences[0] : DEFAULT_LANG

  // State
  const [anchorEl, setAnchorEl] = useState<HTMLElement>()

  // Events
  const onMenuToggle = useCallback(
    (event: MouseEvent<HTMLElement>): void => {
      const target = event.currentTarget
      setAnchorEl(anchor => (!anchor && active ? target : undefined))
    },
    [active]
  )
  function handleSelectLanguage(langCode: string): () => Promise<void> {
    return async () => {
      setAnchorEl(undefined)
      lang.$changePreferences([langCode])
      if (typeof window !== 'undefined') {
        const analyzer = new UiUrlAnalyzer(window.location.pathname, availables)
        if (analyzer.langUrl.length > 0) {
          window.open(analyzer.cleanUrl, '_self')
        }
      }
    }
  }

  // Render
  const open = !!anchorEl
  function renderLanguage(): (langCode: string) => JSX.Element {
    return (langCode: string) => {
      const Lang: JSX.Element = (
        <MenuItem key={langCode} selected={langCode === selected} onClick={handleSelectLanguage(langCode)}>
          {lang.$languageMap.messages(langCode).$}
        </MenuItem>
      )
      return Lang
    }
  }
  if (selected.indexOf('_') > 0) {
    selected = selected.substring(0, selected.indexOf('_'))
  }
  let icon: JSX.Element = <LanguageIcon />
  if (active) {
    icon = (
      <Badge badgeContent={selected} color="secondary">
        {icon}
      </Badge>
    )
  }
  const paperProps = {
    style: {
      maxHeight: MENU_MAX_HEIGHT,
    },
  }
  return (
    <div>
      <IconButton
        color="inherit"
        onClick={onMenuToggle}
        disabled={availables.length <= 1}
        aria-owns={anchorEl && 'lang-menu'}
        aria-haspopup="true"
      >
        {icon}
      </IconButton>
      <Menu id="lang-menu" anchorEl={anchorEl} open={open} onClose={onMenuToggle} PaperProps={paperProps}>
        {availables.sort().map(renderLanguage())}
      </Menu>
    </div>
  )
})
export default Language
