/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {
  IconButton,
  ListItem,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText,
  Theme,
} from '@material-ui/core'
// eslint-disable-next-line import/no-internal-modules
import { SvgIconProps } from '@material-ui/core/SvgIcon'
import DeleteIcon from '@material-ui/icons/Delete'
import { useTheme } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { ComponentType, FunctionComponent, useCallback } from 'react'

import { useLangStore } from '../../../stores'
import { NotificationLevel } from '../../../tools'
import NotificationDescription from './NotificationDescription'

export interface NotificationProps {
  notification: NotificationDescription
  onRemove: (date: Date) => void
}

/**
 * A single notification.
 */
const Notification: FunctionComponent<NotificationProps> = observer(({ notification, onRemove }) => {
  const theme = useTheme<Theme>()
  const { lang } = useLangStore()

  // Events
  const onNotificationRemove = useCallback(() => {
    onRemove(notification.date)
  }, [notification, onRemove])

  // Render
  const { icons } = theme.gecogvidanto
  const Icon = ((): ComponentType<SvgIconProps> => {
    switch (notification.level) {
      case NotificationLevel.Success:
        return icons.success
      case NotificationLevel.Info:
        return icons.info
      case NotificationLevel.Warning:
        return icons.warning
      case NotificationLevel.Error:
        return icons.error
      default:
        // eslint-disable-next-line no-case-declarations
        const levelError: never = notification.level
        throw new Error(`Unexpected level ${levelError}`)
    }
  })()
  return (
    <ListItem disableGutters>
      <ListItemIcon>
        <Icon />
      </ListItemIcon>
      <ListItemText primary={notification.message} />
      <ListItemSecondaryAction>
        <IconButton aria-label={lang.delete()} onClick={onNotificationRemove}>
          <DeleteIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  )
})
export default Notification
