/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Badge, Button, IconButton, List, Popover } from '@material-ui/core'
// eslint-disable-next-line import/no-internal-modules
import { PopoverOrigin } from '@material-ui/core/Popover'
import ClearAllIcon from '@material-ui/icons/ClearAll'
import NotificationsIcon from '@material-ui/icons/Notifications'
import { makeStyles } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, MouseEvent, useCallback, useEffect, useRef, useState } from 'react'

import { useLangStore } from '../../../stores'
import { NotificationEvent, notificationManager } from '../../../tools'
import Notification from './Notification'
import NotificationDescription from './NotificationDescription'

const MENU_MAX_HEIGHT = '50%'
const MENU_WIDTH = 300
const useStyles = makeStyles({
  removeAllBlock: {
    display: 'flex',
  },
  pusher: {
    flexGrow: 1,
  },
})

/**
 * All notifications.
 */
const Notifications: FunctionComponent = observer(() => {
  const classes = useStyles()
  const { lang } = useLangStore()

  // State
  const [notifications, setNotifications] = useState<NotificationDescription[]>([])
  const [anchorEl, setAnchorEl] = useState<HTMLElement>()

  // Events
  const onNotify = useCallback((event: NotificationEvent): void => {
    const notification = { date: new Date(), ...event }
    setNotifications(previous => [notification, ...previous])
  }, [])
  const onRemoveAll = useCallback((): void => {
    setNotifications([])
    setAnchorEl(undefined)
  }, [])
  const onRemove = useCallback((date: Date): void => {
    setNotifications(previous => {
      const next = previous.filter(notification => notification.date.valueOf() !== date.valueOf())
      if (next.length === 0) {
        setAnchorEl(undefined)
      }
      return next
    })
  }, [])
  const onMenuToggle = useCallback(
    (event: MouseEvent<HTMLElement>): void => {
      const target = event.currentTarget
      setAnchorEl(previous => (!previous && notifications.length > 0 ? target : undefined))
    },
    [notifications.length]
  )

  // Stored notifications
  const firstStoredTest = useRef(true)
  useEffect(() => {
    if (typeof window !== 'undefined') {
      if (firstStoredTest.current) {
        firstStoredTest.current = false
        const stored = window.sessionStorage.getItem('notifications')
        if (stored) {
          try {
            const result = JSON.parse(stored) as NotificationDescription[]
            if (result.length > 0) {
              setNotifications(result)
            }
          } catch {
            // Ignored
          }
        }
      } else {
        window.sessionStorage.setItem('notifications', JSON.stringify(notifications))
      }
    }
  }, [notifications])

  // Listen to notification manager
  useEffect(() => {
    notificationManager.on(onNotify)
    return () => {
      notificationManager.off(onNotify)
    }
  }, [onNotify])

  // Render
  const open = !!anchorEl
  let icon: JSX.Element = <NotificationsIcon />
  if (notifications.length > 0) {
    icon = (
      <Badge badgeContent={notifications.length} color="secondary">
        {icon}
      </Badge>
    )
  }
  const anchorOrigin: PopoverOrigin = {
    vertical: 'top',
    horizontal: 'left',
  }
  const transformOrigin: PopoverOrigin = {
    vertical: 'top',
    horizontal: 'left',
  }
  const paperProps = {
    style: {
      maxHeight: MENU_MAX_HEIGHT,
      width: MENU_WIDTH,
    },
  }
  const notificationNodes: JSX.Element[] = notifications.map(notification => (
    <Notification
      key={notification.date.valueOf().toString()}
      notification={notification}
      onRemove={onRemove}
    />
  ))
  return (
    <div>
      <IconButton
        color="inherit"
        onClick={onMenuToggle}
        disabled={notifications.length === 0}
        aria-owns={anchorEl && 'notif-menu'}
        aria-haspopup="true"
      >
        {icon}
      </IconButton>
      <Popover
        id="notif-menu"
        anchorEl={anchorEl}
        anchorOrigin={anchorOrigin}
        transformOrigin={transformOrigin}
        open={open}
        onClose={onMenuToggle}
        PaperProps={paperProps}
      >
        <div className={classes.removeAllBlock}>
          <span className={classes.pusher} />
          <Button size="small" color="primary" aria-label={lang.removeAll()} onClick={onRemoveAll}>
            <ClearAllIcon />
          </Button>
        </div>
        <List dense>{...notificationNodes}</List>
      </Popover>
    </div>
  )
})
export default Notifications
