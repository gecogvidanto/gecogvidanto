/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Menu, MenuItem } from '@material-ui/core'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { useLangStore, useUserStore } from '../../../stores'

export interface UserMenuProps {
  id: string
  anchorEl: HTMLElement | undefined
  open: boolean
  onSignUp: () => void
  onSignIn: () => void
  onSignOut: () => void
  onProfile: () => void
  onRequestClose: () => void
  onExited: () => void
}

/**
 * The user menu.
 */
const UserMenu: FunctionComponent<UserMenuProps> = observer(
  ({ id, anchorEl, open, onSignUp, onSignIn, onSignOut, onProfile, onRequestClose, onExited }) => {
    const { lang } = useLangStore()
    const { logged } = useUserStore()

    // Render
    const menuNodes: JSX.Element[] = []
    if (logged) {
      menuNodes.push(
        <MenuItem key="sign-out" onClick={onSignOut}>
          {lang.signOut()}
        </MenuItem>
      )
      menuNodes.push(
        <MenuItem key="profile" onClick={onProfile}>
          {lang.pageProfile()}
        </MenuItem>
      )
    } else {
      menuNodes.push(
        <MenuItem key="sign-in" onClick={onSignIn}>
          {lang.signIn()}
        </MenuItem>
      )
      menuNodes.push(
        <MenuItem key="sign-up" onClick={onSignUp}>
          {lang.signUp()}
        </MenuItem>
      )
    }
    return (
      <Menu id={id} anchorEl={anchorEl} open={open} onClose={onRequestClose} onExited={onExited}>
        {...menuNodes}
      </Menu>
    )
  }
)

export default UserMenu
