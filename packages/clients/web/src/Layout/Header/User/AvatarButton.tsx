/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { IconButton } from '@material-ui/core'
import AnonymousIcon from '@material-ui/icons/AccountCircle'
import { makeStyles } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, MouseEvent } from 'react'

import { useUserStore } from '../../../stores'
import { Gravatar } from '../../../tools'

const PICTURE_SIZE = 40

const useStyles = makeStyles({
  avatar: {
    borderRadius: '50%',
  },
  avatarButton: {
    padding: '4px',
  },
})

export interface AvatarButtonProps {
  ariaOwns: string | undefined
  onRequestOpen: (event: MouseEvent<HTMLElement>) => void
}

/**
 * The user avatar button.
 */
const AvatarButton: FunctionComponent<AvatarButtonProps> = observer(({ ariaOwns, onRequestOpen }) => {
  const classes = useStyles()
  const { logged } = useUserStore()

  let className = ''
  let innerComponent: JSX.Element
  if (logged) {
    const gravatar = new Gravatar(logged.email)
    className = classes.avatarButton
    innerComponent = (
      <img
        className={classes.avatar}
        alt={logged.name}
        src={gravatar.getImageUrl(PICTURE_SIZE)}
        width={PICTURE_SIZE}
        height={PICTURE_SIZE}
      />
    )
  } else {
    innerComponent = <AnonymousIcon />
  }
  return (
    <IconButton
      className={className}
      color="inherit"
      onClick={onRequestOpen}
      aria-owns={ariaOwns}
      aria-haspopup="true"
    >
      {innerComponent}
    </IconButton>
  )
})
export default AvatarButton
