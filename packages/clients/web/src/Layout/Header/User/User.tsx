/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Theme, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent, MouseEvent, useCallback, useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'

import { SignIn } from '../../../components'
import { routes } from '../../../routes'
import { useLangStore, useUserStore } from '../../../stores'
import AvatarButton from './AvatarButton'
import UserMenu from './UserMenu'

const MENU_ID = 'login-menu'
const POPUP_ID = 'sign-in-popup'

const useStyles = makeStyles((theme: Theme) => ({
  userName: {
    marginRight: theme.spacing(1),
  },
}))

/**
 * Component managing connected user.
 */
const User: FunctionComponent = observer(() => {
  const classes = useStyles()
  const { lang } = useLangStore()
  const { logged, signOut } = useUserStore()
  const history = useHistory()

  // State
  const [anchorEl, setAnchorEl] = useState<HTMLElement>()
  const [popup, setPopup] = useState(false)
  const [nextStep, setNextStep] = useState<() => void>()
  const [askProfile, setAskProfile] = useState(false)

  // Events
  const onSignIn = useCallback((): void => {
    const nextAnchor = anchorEl
    setAnchorEl(undefined)
    setNextStep(() => {
      setAnchorEl(nextAnchor)
      setPopup(true)
    })
  }, [anchorEl])
  const onSignOut = useCallback((): void => {
    setAnchorEl(undefined)
    setNextStep(undefined)
    signOut()
  }, [signOut])
  const onUser = useCallback((): void => {
    setAnchorEl(undefined)
    setNextStep(() => setAskProfile(true))
  }, [])
  const onRequestOpen = useCallback((event: MouseEvent<HTMLElement>): void => {
    setAnchorEl(event.currentTarget)
    setPopup(false)
    setNextStep(undefined)
  }, [])
  const onRequestClose = useCallback((): void => {
    setAnchorEl(undefined)
    setNextStep(undefined)
  }, [])
  const onMenuExited = useCallback((): void => {
    nextStep && nextStep()
  }, [nextStep])

  // Go to profile page
  useEffect(() => {
    if (askProfile) {
      setAskProfile(false)
      history.push(routes.PROFILE.build())
    }
  }, [askProfile, history])

  // Render
  return (
    <>
      <Typography className={classes.userName} color="inherit" noWrap>
        {logged ? logged.name : lang.notConnected()}
      </Typography>
      <div>
        <AvatarButton ariaOwns={anchorEl && (popup ? POPUP_ID : MENU_ID)} onRequestOpen={onRequestOpen} />
        <UserMenu
          id={MENU_ID}
          anchorEl={anchorEl}
          open={!!anchorEl && !popup}
          onSignUp={onUser}
          onSignIn={onSignIn}
          onSignOut={onSignOut}
          onProfile={onUser}
          onRequestClose={onRequestClose}
          onExited={onMenuExited}
        />
        <SignIn id={POPUP_ID} open={!!anchorEl && popup} onClose={onRequestClose} />
      </div>
    </>
  )
})

export default User
