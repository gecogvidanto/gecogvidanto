/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { AppBar, IconButton, Theme, Toolbar, Typography } from '@material-ui/core'
import MenuIcon from '@material-ui/icons/Menu'
import { makeStyles, useTheme } from '@material-ui/styles'
import classNames from 'classnames'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { useLangStore } from '../../../stores'
import Language from '../Language'
import Notifications from '../Notifications'
import User from '../User'

const useStyles = makeStyles((theme: Theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: theme.gecogvidanto.menuWidth,
    width: `calc(100% - ${theme.gecogvidanto.menuWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  title: {
    margin: 0,
  },
  hiddenOpen: {
    display: 'none',
  },
  pushToRight: {
    flexGrow: 1,
  },
}))

export interface HeaderProps {
  openMenu: () => any
  menuOpen: boolean
}

/**
 * Header component.
 */
const Header: FunctionComponent<HeaderProps> = observer(({ openMenu, menuOpen }) => {
  const theme = useTheme<Theme>()
  const classes = useStyles()
  const { lang } = useLangStore()
  const buttonClassName = classNames(classes.menuButton, menuOpen && classes.hiddenOpen)
  const appClassName = classNames(classes.title, menuOpen && classes.hiddenOpen)

  // Render
  return (
    <AppBar position="absolute" className={classNames(classes.appBar, menuOpen && classes.appBarShift)}>
      <Toolbar disableGutters={!menuOpen} className={classes.toolbar}>
        <IconButton
          color="inherit"
          aria-label={lang.openMenu()}
          onClick={openMenu}
          className={buttonClassName}
        >
          <MenuIcon />
        </IconButton>
        <Typography component="h1" variant="h6" color="inherit" noWrap className={appClassName}>
          {theme.gecogvidanto.applicationName}
        </Typography>
        <div className={classes.pushToRight} />
        <User />
        <Language />
        <Notifications />
      </Toolbar>
    </AppBar>
  )
})
export default Header
