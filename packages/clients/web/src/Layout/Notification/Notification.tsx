/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { IconButton, Snackbar, Theme } from '@material-ui/core'
// eslint-disable-next-line import/no-internal-modules
import { SvgIconProps } from '@material-ui/core/SvgIcon'
import CloseIcon from '@material-ui/icons/Close'
import { makeStyles, useTheme } from '@material-ui/styles'
import classNames from 'classnames'
import { observer } from 'mobx-react-lite'
import * as React from 'react'
import { ComponentType, FunctionComponent, useCallback, useEffect, useRef, useState } from 'react'

import { useLangStore } from '../../stores'
import { notificationManager, NotificationEvent, NotificationLevel } from '../../tools'

/**
 * The minimum delay to show a notification if another one is in the queue.
 */
const SHOW_DELAY = 500

/**
 * The maximum delay to show a notification before auto hidding it. Must be greater than `SHOW_DELAY`.
 */
const AUTO_HIDE_DELAY = 6000

const useStyles = makeStyles((theme: Theme) => ({
  icon: {
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
  success: {
    color: theme.palette.success.dark,
  },
  info: {
    color: theme.palette.info.dark,
  },
  warning: {
    color: theme.palette.warning.dark,
  },
  error: {
    color: theme.palette.error.dark,
  },
}))

/**
 * The notification manager.
 */
const NotificationBar: FunctionComponent = observer(() => {
  const theme = useTheme<Theme>()
  const classes = useStyles()
  const { lang } = useLangStore()
  const eventQueue = useRef<NotificationEvent[]>([])
  const showTimer = useRef<number>()

  // State
  const [currentEvent, setCurrentEvent] = useState<NotificationEvent>()

  // Events
  const onShowTimeout = useCallback((): void => {
    showTimer.current = undefined
    if (eventQueue.current.length > 0) {
      setCurrentEvent(undefined)
    }
  }, [])
  const processQueue = useCallback((): void => {
    if (eventQueue.current.length > 0) {
      setCurrentEvent(eventQueue.current.shift())
      if (typeof window !== 'undefined') {
        showTimer.current = window.setTimeout(onShowTimeout, SHOW_DELAY)
      }
    }
  }, [onShowTimeout])
  const onNotify = useCallback(
    (event: NotificationEvent): void => {
      eventQueue.current.push(event)
      if (!currentEvent) {
        processQueue()
      } else if (showTimer.current === undefined) {
        setCurrentEvent(undefined)
      }
    },
    [currentEvent, processQueue]
  )
  const onCloseRequest = useCallback((_: any, reason?: string) => {
    if (reason !== 'clickaway') {
      setCurrentEvent(undefined)
    }
  }, [])
  const onExited = useCallback(() => {
    processQueue()
  }, [processQueue])

  // Listen to notifications
  useEffect(() => {
    notificationManager.on(onNotify)
    return () => {
      notificationManager.off(onNotify)
    }
  }, [onNotify])

  // Clean up
  useEffect(
    () => () => {
      eventQueue.current = []
      if (typeof window !== 'undefined' && showTimer.current) {
        window.clearTimeout(showTimer.current)
      }
    },
    []
  )

  // Get the style for the message
  function getStyleFor(
    level: NotificationLevel
  ): { icon: ComponentType<SvgIconProps>; class: keyof typeof classes } {
    const { icons } = theme.gecogvidanto
    switch (level) {
      case NotificationLevel.Success:
        return {
          icon: icons.success,
          class: 'success',
        }
      case NotificationLevel.Info:
        return { icon: icons.info, class: 'info' }
      case NotificationLevel.Warning:
        return {
          icon: icons.warning,
          class: 'warning',
        }
      case NotificationLevel.Error:
        return {
          icon: icons.error,
          class: 'error',
        }
      default:
        // eslint-disable-next-line no-case-declarations
        const levelError: never = level
        throw new Error(`Unexpected level ${levelError}`)
    }
  }

  // Render
  const open = !!currentEvent
  const contentProps = {
    'aria-describedby': 'notification-msg',
  }
  let icon: JSX.Element | undefined
  if (currentEvent) {
    const style = getStyleFor(currentEvent.level)
    const Icon = style.icon
    const className = classNames(classes.icon, classes[style.class])
    icon = <Icon className={className} color="primary" />
  }
  const messageNode: JSX.Element = (
    <span id="notification-msg" className={classes.message}>
      {icon}
      {currentEvent ? currentEvent.message : ''}
    </span>
  )
  const closeButtonNode: JSX.Element = (
    <IconButton key="close" aria-label={lang.close()} color="inherit" onClick={onCloseRequest}>
      <CloseIcon />
    </IconButton>
  )
  return (
    <Snackbar
      open={open}
      autoHideDuration={AUTO_HIDE_DELAY}
      onClose={onCloseRequest}
      onExited={onExited}
      ContentProps={contentProps}
      message={messageNode}
      action={closeButtonNode}
    />
  )
})
export default NotificationBar
