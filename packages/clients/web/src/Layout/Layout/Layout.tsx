/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import * as React from 'react'
import { FunctionComponent, useCallback, useState } from 'react'

import { MenuProvider } from '../../menu'
import Header from '../Header'
import MenuBar from '../MenuBar'
import Notification from '../Notification'

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    display: 'flex',
  },
  main: {
    display: 'flex',
    flexGrow: 1,
    flexDirection: 'column',
    height: '100vh',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    overflow: 'auto',
  },
  appBarSpacer: theme.mixins.toolbar,
}))

/**
 * Layout of the application.
 */
/* (JSX element) */
const Layout: FunctionComponent = ({ children }) => {
  const classes = useStyles()

  // State
  const [menuOpen, setMenuOpen] = useState<boolean>(false)

  // Events
  const onOpenMenu = useCallback(() => {
    setMenuOpen(true)
  }, [])
  const onCloseMenu = useCallback(() => {
    setMenuOpen(false)
  }, [])

  // Render
  return (
    <MenuProvider>
      <Notification />
      <div className={classes.root}>
        <Header openMenu={onOpenMenu} menuOpen={menuOpen} />
        <MenuBar closeMenu={onCloseMenu} open={menuOpen} />
        <main className={classes.main}>
          <div className={classes.appBarSpacer} />
          <div className={classes.content}>{children}</div>
        </main>
      </div>
    </MenuProvider>
  )
}
export default Layout
