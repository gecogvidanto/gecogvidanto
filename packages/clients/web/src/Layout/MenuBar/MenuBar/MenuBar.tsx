/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Divider, Drawer, IconButton, Theme, Typography } from '@material-ui/core'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import { makeStyles, useTheme } from '@material-ui/styles'
import classNames from 'classnames'
import { useObserver } from 'mobx-react-lite'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { useLangStore, useSdataStore } from '../../../stores'
import Menu from '../Menu'

const useStyles = makeStyles((theme: Theme) => ({
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: theme.gecogvidanto.menuWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
  },
  headerSpace: {
    display: 'flex',
    alignItems: 'center',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  headerTitle: {
    display: 'flex',
    flexDirection: 'column',
  },
  title: {
    margin: 0,
  },
  pushToRight: {
    flexGrow: 1,
  },
}))

export interface MenuBarProps {
  open: boolean
  closeMenu: () => any
}

const MenuBar: FunctionComponent<MenuBarProps> = ({ open, closeMenu }) => {
  const classes = useStyles()
  const theme = useTheme<Theme>()
  const { lang } = useLangStore()
  const { serverInfo } = useSdataStore()

  // Render
  const versionNode: JSX.Element | '' | undefined = serverInfo.version && (
    <Typography color="textSecondary" noWrap>
      {`v${serverInfo.version}`}
    </Typography>
  )
  const drawerClasses = {
    paper: classNames(classes.drawerPaper, !open && classes.drawerPaperClose),
  }
  return useObserver(() => (
    <Drawer variant="permanent" classes={drawerClasses} open={open}>
      <div className={classes.headerSpace}>
        <div className={classes.headerTitle}>
          <Typography component="h1" variant="h6" color="textPrimary" noWrap className={classes.title}>
            {theme.gecogvidanto.applicationName}
          </Typography>
          {versionNode}
        </div>
        <div className={classes.pushToRight} />
        <IconButton aria-label={lang.closeMenu()} onClick={closeMenu}>
          <ChevronLeftIcon />
        </IconButton>
      </div>
      <Divider />
      <Menu />
    </Drawer>
  ))
}
export default MenuBar
