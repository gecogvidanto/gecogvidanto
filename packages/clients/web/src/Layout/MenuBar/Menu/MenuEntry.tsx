/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { ListItemIcon, ListItemText, MenuItem } from '@material-ui/core'
// eslint-disable-next-line import/no-internal-modules
import { SvgIconProps } from '@material-ui/core/SvgIcon'
import { useObserver } from 'mobx-react-lite'
import * as React from 'react'
import { ComponentType, FunctionComponent, MouseEvent, useCallback } from 'react'

import { MenuEntryDescription, selectMenuEntry, useMenuAction } from '../../../menu'
import { useLangStore } from '../../../stores'

export interface MenuEntryProps {
  description: MenuEntryDescription
}

/**
 * A group of items in menu.
 */
/* (JSX element) */
const MenuEntry: FunctionComponent<MenuEntryProps> = ({ description }) => {
  const { lang } = useLangStore()
  const dispatch = useMenuAction()

  const onMenuSelect = useCallback(
    (event: MouseEvent<HTMLElement>): void => {
      dispatch(selectMenuEntry(event.currentTarget, description.action))
    },
    [description.action, dispatch]
  )

  // Render
  const Icon: ComponentType<SvgIconProps> = description.icon
  return useObserver(() => (
    <MenuItem onClick={onMenuSelect}>
      <ListItemIcon key="icon">
        <Icon />
      </ListItemIcon>
      <ListItemText key="text" primary={lang[description.title]()} />
    </MenuItem>
  ))
}
export default MenuEntry
