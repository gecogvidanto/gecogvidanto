/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Divider, List } from '@material-ui/core'
import * as React from 'react'
import { FunctionComponent } from 'react'

import { useMenuContent } from '../../../menu'
import MenuEntry from './MenuEntry'

/**
 * Display the main menu.
 */
/* (JSX element) */
const Menu: FunctionComponent = () => {
  const menu = useMenuContent()
  const nodes: JSX.Element[] = []
  menu
    .map((group, index) => ({
      group: group.map(item => <MenuEntry key={item.title} description={item} />),
      index,
    }))
    .filter(({ group }) => group.length !== 0)
    .forEach(({ group, index }) => {
      if (index > 0) {
        nodes.push(<Divider key={index} />)
      }
      nodes.push(<List key={`group-${index}`}>{...group}</List>)
    })
  return <>{nodes}</>
}
export default Menu
