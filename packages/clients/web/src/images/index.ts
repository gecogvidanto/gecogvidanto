/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
export { default as G1FaviconImage } from './g1-favicon.png'
export { default as HelpSheetImage } from './helpsheet.svg'
export { default as BlueMarkImage } from './mark-blue.svg'
export { default as GreenMarkImage } from './mark-green.svg'
export { default as RedMarkImage } from './mark-red.svg'
export { default as YellowMarkImage } from './mark-yellow.svg'
