/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import Intl, { LanguageMap } from 'intl-ts'

import { Game, User } from '@gecogvidanto/shared'

import { langType } from '../../locale'
import { ServerInfo } from '../../tools'
import GameStore from '../GameStore'
import LangStore from '../LangStore'
import SdataStore from '../SdataStore'
import UserStore from '../UserStore'
import StoreSet from './StoreSet'

/**
 * Create the application stores.
 *
 * @param serverInfo - Information about server.
 * @param captchaKey - The public key used for captcha.
 * @param languageMap - The language map.
 * @param langPreferences - The language preferences.
 * @param logged - The user currently logged.
 * @param game - The current game.
 * @returns The stores.
 */
export function buildStoreSet(
  serverInfo: ServerInfo,
  captchaKey: string,
  languageMap: LanguageMap<langType>,
  langPreferences: ReadonlyArray<string>,
  logged?: User,
  game?: Game
): StoreSet {
  const lang = new Intl(languageMap, langPreferences)
  return {
    staticStore: new SdataStore(serverInfo, captchaKey),
    langStore: new LangStore(lang),
    userStore: new UserStore(lang, logged),
    gameStore: new GameStore(lang, game),
  }
}
