/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as React from 'react'
import { FunctionComponent } from 'react'

import { GameProvider } from '../GameStore'
import { LangProvider } from '../LangStore'
import { SdataProvider } from '../SdataStore'
import { UserProvider } from '../UserStore'
import StoreSet from './StoreSet'

/**
 * Properties for the StoreSet component.
 */
export interface StoreSetProviderProps {
  value: StoreSet
}

/**
 * Provider for all application stores.
 */
/* (JSX element) */
const StoreSetProvider: FunctionComponent<StoreSetProviderProps> = ({ children, value }) => (
  <SdataProvider value={value.staticStore}>
    <LangProvider value={value.langStore}>
      <UserProvider value={value.userStore}>
        <GameProvider value={value.gameStore}>{children}</GameProvider>
      </UserProvider>
    </LangProvider>
  </SdataProvider>
)
export default StoreSetProvider
