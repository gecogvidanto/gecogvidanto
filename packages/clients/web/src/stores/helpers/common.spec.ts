/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { LanguageMap } from 'intl-ts'
import { IReactionDisposer, autorun } from 'mobx'
import { SinonStub } from 'sinon'

import { messages } from '../../locale'
import { NotificationEvent, notificationManager } from '../../tools'

export const languageMap = new LanguageMap(messages, 'en').merge<{ $: string }>({
  fr: { $: 'Français' },
  eo: { $: 'Esperanto' },
})

export const ERROR_MSG = '$client$connection' as const

interface DeferResult<TReturnValue> {
  resolve: (returnValue: TReturnValue) => any
  reject: (...args: any[]) => any
}

interface ChangeCounter {
  [name: string]: number
}

export function defer<TArgs extends any[], TReturnValue>(
  stub: SinonStub<TArgs, Promise<TReturnValue>>
): DeferResult<TReturnValue> {
  let resolve: DeferResult<TReturnValue>['resolve'] = () => undefined
  let reject: DeferResult<TReturnValue>['reject'] = () => undefined
  const promise = new Promise<TReturnValue>((res, rej) => {
    resolve = res
    reject = rej
  })
  stub.returns(promise)
  return { resolve, reject }
}

export function sendNotifications(): Promise<void> {
  return new Promise(resolve => setTimeout(() => setImmediate(() => setTimeout(resolve))))
}

export class NotificationStore {
  public readonly recordedNotifs: NotificationEvent[]

  public constructor() {
    this.recordedNotifs = []
    notificationManager.on(this.onNotification)
  }

  public onNotification = (event: NotificationEvent): void => {
    const { level, message } = event
    this.recordedNotifs.push({ level, message })
  }
}

export interface Observable {
  name: string
  getter: () => any
}

export class Observer {
  private readonly counters: ChangeCounter = {}
  private readonly records: string[] = []
  private readonly disposers: IReactionDisposer[] = []

  public constructor(observables: Observable[]) {
    observables.forEach(observable => this.disposers.push(autorun(this.countChanges(observable))))
  }

  public terminate(): string[] {
    this.disposers.forEach(disposer => disposer())
    while (this.disposers.length > 0) {
      this.disposers.pop()
    }
    return this.reset()
  }

  public reset(): string[] {
    for (const counter in this.counters) {
      this.counters[counter] = 0
    }
    const result = [...this.records]
    while (this.records.length > 0) {
      this.records.pop()
    }
    return result
  }

  public get changes(): ChangeCounter {
    const changes: ChangeCounter = {}
    for (const counter in this.counters) {
      this.counters[counter] !== 0 && (changes[counter] = this.counters[counter])
    }
    return changes
  }

  private countChanges(observable: Observable): () => void {
    this.counters[observable.name] = -1 // Set to 0 at initial call
    return (): void => {
      this.counters[observable.name]++
      this.records.push(`Reading content of ${observable.name}: ${observable.getter()}`)
    }
  }
}
