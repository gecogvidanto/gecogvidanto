/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Provider, createContext, useContext } from 'react'

export function createStore<T>(): {
  Provider: Provider<T | undefined>
  useStore: () => T
} {
  const context = createContext<T | undefined>(undefined)
  return {
    Provider: context.Provider,
    useStore: () => {
      const store = useContext(context)
      if (!store) {
        throw new Error('Trying to use uninitialized store')
      }
      return store
    },
  }
}
