/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import SdataStore from './SdataStore'

const PLUGIN = {
  module: 'the-module',
  name: 'module-name',
  description: '',
  database: false,
  ecoSysIds: [],
}
const CAPTCHA = 'Captcha-Key'

describe('SdataStore', function () {
  let sdataStore: SdataStore

  beforeEach('Initialize store', function () {
    sdataStore = new SdataStore({ plugins: [PLUGIN] }, CAPTCHA)
  })

  it('must store static data', async function () {
    expect(sdataStore.serverInfo.plugins).to.have.lengthOf(1)
    expect(sdataStore.serverInfo.plugins[0].name).to.equal(PLUGIN.name)
    expect(sdataStore.captchaKey).to.equal(CAPTCHA)
  })
})
