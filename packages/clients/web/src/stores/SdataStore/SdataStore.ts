/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { ServerInfo } from '../../tools'

/**
 * The static data store. This store does not need to be observable, it contains only application-wide
 * static data.
 */
export default class SdataStore {
  /**
   * Create the store.
   *
   * @param serverInfo - Information about server,.
   * @param captchaKey - The site key for the captcha.
   */
  public constructor(public readonly serverInfo: ServerInfo, public readonly captchaKey: string) {}
}
