/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import Intl from 'intl-ts'
import { computed, flow, observable, runInAction } from 'mobx'

import { Game, Location } from '@gecogvidanto/shared'

import { langType } from '../../locale'
import { api, notificationManager, notifyError } from '../../tools'

/**
 * The game store is used to manipulate the current game.
 */
export default class GameStore {
  @observable.ref
  private _game?: Game

  @observable
  private _loading = false

  /**
   * Create store.
   *
   * @param lang - The tools store.
   * @param game - The current game, if any.
   */
  public constructor(private readonly lang: Intl<langType>, game?: Game) {
    this._game = game
  }

  /**
   * @returns The current game, if any.
   */
  @computed
  public get game(): Game | undefined {
    return this._game
  }

  /**
   * @returns The current game, expected to exist. Throws an error if not.
   */
  @computed
  public get neededGame(): Game {
    if (!this._game) {
      throw new Error('No current game')
    }
    return this._game
  }

  /**
   * Indicate if game is currently being loaded.
   *
   * @returns The loading state.
   */
  @computed
  public get loading(): boolean {
    return this._loading
  }

  /**
   * Create a new game.
   *
   * @param location - The location of the game.
   * @param roundsPerSet - The count of rounds for each set.
   * @param roundLength - The length of each round in minutes.
   */
  public create: (location: Location, roundsPerSet: number, roundLength: number) => Promise<void> = async (
    location,
    roundsPerSet,
    roundLength
  ) => {
    await this.doAndUpdate(() => api.game.create(location, roundsPerSet, roundLength))
  }

  /**
   * Update location.
   *
   * @param location - The location of the game.
   */
  public updateLocation: (location: Location) => Promise<void> = async location => {
    const game = this.neededGame
    await this.doAndUpdate(() => api.game.update({ ...game, location }))
  }

  /**
   * Update summary.
   *
   * @param summary - The summary of the game.
   */
  public updateSummary: (summary: string) => Promise<void> = async summary => {
    const game = this.neededGame
    await this.doAndUpdate(() => api.game.update({ ...game, summary }))
  }

  /**
   * Load the requested game if it is not the current one.
   *
   * @param id - The game identifier, undefined to unload game.
   */
  public loadIfNeeded: (id?: string) => Promise<void> = async id => {
    if (this._game && this._game._id !== id) {
      runInAction(() => (this._game = undefined))
    }
    if (id && !this._game) {
      await this.doAndUpdate(() => api.game.read(id))
    }
  }

  /**
   * Add a player to the current game.
   *
   * @param name - The name to give to the player.
   */
  public addPlayer: (name: string) => Promise<void> = async name => {
    const game = this.neededGame
    await this.doAndUpdate(() => api.game.addPlayer(game, name))
  }

  /**
   * Update a player's name.
   *
   * @param player - The number associated to the player.
   * @param name - The name to give to the player.
   */
  public updatePlayer: (player: number, name: string) => Promise<void> = async (player, name) => {
    const game = this.neededGame
    await this.doAndUpdate(() => api.game.updatePlayer(game, player, name))
  }

  /**
   * Delete the player.
   *
   * @param player - The number associated to the player.
   */
  public deletePlayer: (player: number) => Promise<void> = async player => {
    const game = this.neededGame
    await this.doAndUpdate(() => api.game.deletePlayer(game, player))
  }

  /**
   * Add a new set to the current game.
   *
   * @param ecoSysId - The identifier of the economic system.
   */
  public addSet: (ecoSysId: string) => Promise<void> = async ecoSysId => {
    const game = this.neededGame
    await this.doAndUpdate(() => api.game.addSet(game, ecoSysId))
  }

  /**
   * Send the form and terminate round if round end form.
   *
   * @param data - The form data.
   * @param optionId - The form option identifier, if any.
   */
  public sendForm: (
    data: { [key: string]: string | number | boolean },
    optionId?: string
  ) => Promise<void> = async (data, optionId) => {
    const game = this.neededGame
    await this.doAndUpdate(() => api.game.sendForm(game, data, optionId))
  }

  /**
   * Record a technological break in current set.
   */
  public recordTechnologicalBreak: () => Promise<void> = async () => {
    const game = this.neededGame
    if (await this.doAndUpdate(() => api.game.recordTechnologicalBreak(game))) {
      notificationManager.emitSuccess(this.lang.gameTechBreakRecorded())
    }
  }

  /**
   * Close the game.
   */
  public close: () => Promise<void> = async () => {
    const game = this.neededGame
    if (await this.doAndUpdate(() => api.game.update({ ...game, closed: true }))) {
      notificationManager.emitSuccess(this.lang.gameClosed())
    }
  }

  /**
   * Execute the method which should update the current game.
   *
   * @param method - The method to execute.
   * @returns True if action finished successfully.
   */
  private doAndUpdate: (method: () => Promise<Game>) => Promise<boolean> = flow(function* doAndUpdate(
    this: GameStore,
    method: () => Promise<Game>
  ) {
    this._loading = true
    try {
      this._game = yield method()
      return true
    } catch (error) {
      notifyError(error, this.lang)
      return false
    } finally {
      this._loading = false
    }
  })
}
