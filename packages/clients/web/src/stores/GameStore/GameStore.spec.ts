/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import Intl from 'intl-ts'
import * as sinon from 'sinon'
import { SinonStub } from 'sinon'

import { ApiError } from '@gecogvidanto/client'
import { Game } from '@gecogvidanto/shared'

import { NotificationEvent, NotificationLevel, api } from '../../tools'
/* eslint-disable import/no-internal-modules */
import {
  ERROR_MSG,
  NotificationStore,
  Observer,
  defer,
  languageMap,
  sendNotifications,
} from '../helpers/common.spec'
import GameStore from './GameStore'

type Location = import('@gecogvidanto/shared').Location
const GAME_ID = 'gecoId'
const MASTER_ID = 'llzamenhof'
const LOCATION = { name: 'Here' }
const SUMMARY = 'Let’s sum up!'
const RPS = 10
const TLEN = 5
const GAME: Game = {
  _id: GAME_ID,
  start: Date.now(),
  masterId: MASTER_ID,
  location: LOCATION,
  roundsPerSet: RPS,
  roundLength: TLEN,
  players: [{ name: 'player', roundArrived: 0, roundLeft: 0 }],
  roundSets: [],
  characters: [],
  closed: false,
}
const RES_GAME: Game = { ...GAME, summary: SUMMARY }
const FORM_DATA = {
  key: 'value',
  num: 12,
}

const lang = new Intl(languageMap, [])
function observe(gameStore: GameStore): Observer {
  return new Observer([
    {
      name: 'loading',
      getter: () => gameStore.loading,
    },
    {
      name: 'game',
      getter: () => gameStore.game,
    },
  ])
}

describe('GameStore', function () {
  let gameStore: GameStore
  let recordedNotifs: NotificationEvent[]
  let observer: Observer

  beforeEach('Initialize store', function () {
    const notificationStore = new NotificationStore()
    recordedNotifs = notificationStore.recordedNotifs
    gameStore = new GameStore(lang, GAME)
    observer = observe(gameStore)
  })

  afterEach('Clean up', function () {
    observer.terminate()
  })

  async function doAndUpdate<TArgs extends any[]>(
    method: () => Promise<void>,
    stub: SinonStub<TArgs, Promise<Game>>,
    success: boolean
  ): Promise<void> {
    const deferred = defer(stub)
    method()
    expect(gameStore.loading, 'Game should be loading').to.be.true
    if (success) {
      deferred.resolve(RES_GAME)
      await sendNotifications()
      expect(gameStore.game).to.exist
      expect(gameStore.loading, 'Game should be loaded').to.be.false
      expect(gameStore.game!.summary).to.equal(SUMMARY)
    } else {
      deferred.reject(new ApiError(ERROR_MSG))
      await sendNotifications()
      expect(gameStore.loading, 'Game loading should be aborted').to.be.false
    }
  }

  describe('#create', function () {
    let create: SinonStub<[Location, number, number], Promise<Game>>

    beforeEach('Prepare doubles', function () {
      gameStore = new GameStore(lang)
      observer = observe(gameStore)
      create = sinon.stub(api.game, 'create')
    })

    afterEach('Restore doubles', function () {
      create.restore()
    })

    it('must create the game', async function () {
      await doAndUpdate(() => gameStore.create(LOCATION, RPS, TLEN), create, true)
      expect(create).to.have.been.calledOnceWith(LOCATION, RPS, TLEN)
      expect(recordedNotifs, 'Unexpected notification count').to.be.empty
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
        game: 1,
      })
    })

    it('must not create the game if server error', async function () {
      await doAndUpdate(() => gameStore.create(LOCATION, RPS, TLEN), create, false)
      expect(create).to.have.been.calledOnceWith(LOCATION, RPS, TLEN)
      expect(gameStore.game).to.be.undefined
      expect(recordedNotifs, 'Unexpected notification count').to.have.lengthOf(1)
      expect(recordedNotifs[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Error)
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
      })
    })
  })

  describe('#loadIfNeeded', function () {
    let read: SinonStub<[string], Promise<Game>>

    beforeEach('Prepare doubles', function () {
      read = sinon.stub(api.game, 'read')
    })

    afterEach('Restore doubles', function () {
      read.restore()
    })

    it('must load requested game if no game loaded', async function () {
      gameStore = new GameStore(lang)
      observer = observe(gameStore)
      await doAndUpdate(() => gameStore.loadIfNeeded(GAME_ID), read, true)
      expect(read).to.have.been.calledOnceWith(GAME_ID)
      expect(recordedNotifs, 'Unexpected notification count').to.be.empty
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
        game: 1,
      })
    })

    it('must load requested game if other than current', async function () {
      await doAndUpdate(() => gameStore.loadIfNeeded('new-id'), read, true)
      expect(read).to.have.been.calledOnceWith('new-id')
      expect(recordedNotifs, 'Unexpected notification count').to.be.empty
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
        game: 2,
      })
    })

    it('must unload game if asked for', async function () {
      await gameStore.loadIfNeeded()
      await sendNotifications()
      expect(read).to.not.have.been.called
      expect(gameStore.game).to.be.undefined
      expect(recordedNotifs, 'Unexpected notification count').to.be.empty
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        game: 1,
      })
    })

    it('must unload game if error', async function () {
      await doAndUpdate(() => gameStore.loadIfNeeded('new-id'), read, false)
      expect(read).to.have.been.calledOnceWith('new-id')
      expect(gameStore.game).to.be.undefined
      expect(recordedNotifs, 'Unexpected notification count').to.have.lengthOf(1)
      expect(recordedNotifs[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Error)
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
        game: 1,
      })
    })

    it('must not load game if already loaded', async function () {
      await gameStore.loadIfNeeded(GAME_ID)
      await sendNotifications()
      expect(read).to.not.have.been.called
      expect(recordedNotifs, 'Unexpected notification count').to.be.empty
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({})
    })

    it('must not unload game if already unloaded', async function () {
      gameStore = new GameStore(lang)
      observer = observe(gameStore)
      await gameStore.loadIfNeeded()
      await sendNotifications()
      expect(read).to.not.have.been.called
      expect(gameStore.game).to.be.undefined
      expect(recordedNotifs, 'Unexpected notification count').to.be.empty
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({})
    })
  })

  describe('#addPlayer', function () {
    let addPlayer: SinonStub<[Game, string], Promise<Game>>

    beforeEach('Prepare doubles', function () {
      addPlayer = sinon.stub(api.game, 'addPlayer')
    })

    afterEach('Restore doubles', function () {
      addPlayer.restore()
    })

    it('must add the player', async function () {
      await doAndUpdate(() => gameStore.addPlayer('player'), addPlayer, true)
      expect(addPlayer).to.have.been.calledOnceWith(GAME, 'player')
      expect(recordedNotifs, 'Unexpected notification count').to.be.empty
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
        game: 1,
      })
    })

    it('must not add player if error', async function () {
      await doAndUpdate(() => gameStore.addPlayer('player'), addPlayer, false)
      expect(addPlayer).to.have.been.calledOnceWith(GAME, 'player')
      expect(gameStore.game!.location).to.deep.equal(LOCATION)
      expect(recordedNotifs, 'Unexpected notification count').to.have.lengthOf(1)
      expect(recordedNotifs[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Error)
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
      })
    })
  })

  describe('#updatePlayer', function () {
    let updatePlayer: SinonStub<[Game, number, string], Promise<Game>>

    beforeEach('Prepare doubles', function () {
      updatePlayer = sinon.stub(api.game, 'updatePlayer')
    })

    afterEach('Restore doubles', function () {
      updatePlayer.restore()
    })

    it('must update the player', async function () {
      await doAndUpdate(() => gameStore.updatePlayer(0, 'player'), updatePlayer, true)
      expect(updatePlayer).to.have.been.calledOnceWith(GAME, 0, 'player')
      expect(recordedNotifs, 'Unexpected notification count').to.be.empty
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
        game: 1,
      })
    })

    it('must not update player if error', async function () {
      await doAndUpdate(() => gameStore.updatePlayer(0, 'player'), updatePlayer, false)
      expect(updatePlayer).to.have.been.calledOnceWith(GAME, 0, 'player')
      expect(gameStore.game!.location).to.deep.equal(LOCATION)
      expect(recordedNotifs, 'Unexpected notification count').to.have.lengthOf(1)
      expect(recordedNotifs[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Error)
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
      })
    })
  })

  describe('#deletePlayer', function () {
    let deletePlayer: SinonStub<[Game, number], Promise<Game>>

    beforeEach('Prepare doubles', function () {
      deletePlayer = sinon.stub(api.game, 'deletePlayer')
    })

    afterEach('Restore doubles', function () {
      deletePlayer.restore()
    })

    it('must remove the player', async function () {
      await doAndUpdate(() => gameStore.deletePlayer(0), deletePlayer, true)
      expect(deletePlayer).to.have.been.calledOnceWith(GAME, 0)
      expect(recordedNotifs, 'Unexpected notification count').to.be.empty
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
        game: 1,
      })
    })

    it('must not remove player if error', async function () {
      await doAndUpdate(() => gameStore.deletePlayer(0), deletePlayer, false)
      expect(deletePlayer).to.have.been.calledOnceWith(GAME, 0)
      expect(gameStore.game!.location).to.deep.equal(LOCATION)
      expect(recordedNotifs, 'Unexpected notification count').to.have.lengthOf(1)
      expect(recordedNotifs[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Error)
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
      })
    })
  })

  describe('#addSet', function () {
    let addSet: SinonStub<[Game, string], Promise<Game>>

    beforeEach('Prepare doubles', function () {
      addSet = sinon.stub(api.game, 'addSet')
    })

    afterEach('Restore doubles', function () {
      addSet.restore()
    })

    it('must add set to the game', async function () {
      await doAndUpdate(() => gameStore.addSet('eco-sys'), addSet, true)
      expect(addSet).to.have.been.calledOnceWith(GAME, 'eco-sys')
      expect(recordedNotifs, 'Unexpected notification count').to.be.empty
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
        game: 1,
      })
    })

    it('must not add set if error', async function () {
      await doAndUpdate(() => gameStore.addSet('eco-sys'), addSet, false)
      expect(addSet).to.have.been.calledOnceWith(GAME, 'eco-sys')
      expect(gameStore.game!.location).to.deep.equal(LOCATION)
      expect(recordedNotifs, 'Unexpected notification count').to.have.lengthOf(1)
      expect(recordedNotifs[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Error)
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
      })
    })
  })

  describe('#sendForm', function () {
    let execForm: SinonStub<
      [
        Game,
        {
          [key: string]: string | number | boolean
        },
        (string | undefined)?
      ],
      Promise<Game>
    >

    beforeEach('Prepare doubles', function () {
      execForm = sinon.stub(api.game, 'sendForm')
    })

    afterEach('Restore doubles', function () {
      execForm.restore()
    })

    it('must send form data', async function () {
      await doAndUpdate(() => gameStore.sendForm(FORM_DATA, 'option-id'), execForm, true)
      expect(execForm).to.have.been.calledOnceWith(GAME, FORM_DATA, 'option-id')
      expect(recordedNotifs, 'Unexpected notification count').to.be.empty
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
        game: 1,
      })
    })

    it('must not send form data if error', async function () {
      await doAndUpdate(() => gameStore.sendForm(FORM_DATA), execForm, false)
      expect(execForm).to.have.been.calledOnceWith(GAME, FORM_DATA)
      expect(gameStore.game!.location).to.deep.equal(LOCATION)
      expect(recordedNotifs, 'Unexpected notification count').to.have.lengthOf(1)
      expect(recordedNotifs[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Error)
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
      })
    })
  })

  describe('#recordTechnologicalBreak', function () {
    let recordTechnologicalBreak: SinonStub<[Game], Promise<Game>>

    beforeEach('Prepare doubles', function () {
      recordTechnologicalBreak = sinon.stub(api.game, 'recordTechnologicalBreak')
    })

    afterEach('Restore doubles', function () {
      recordTechnologicalBreak.restore()
    })

    it('must record technological break', async function () {
      await doAndUpdate(() => gameStore.recordTechnologicalBreak(), recordTechnologicalBreak, true)
      expect(recordTechnologicalBreak).to.have.been.calledOnceWith(GAME)
      expect(recordedNotifs, 'Unexpected notification count').to.have.lengthOf(1)
      expect(recordedNotifs[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Success)
      expect(recordedNotifs[0].message).to.match(/technological break recorded/i)
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
        game: 1,
      })
    })

    it('must not record technological break if error', async function () {
      await doAndUpdate(() => gameStore.recordTechnologicalBreak(), recordTechnologicalBreak, false)
      expect(recordTechnologicalBreak).to.have.been.calledOnceWith(GAME)
      expect(gameStore.game!.location).to.deep.equal(LOCATION)
      expect(recordedNotifs, 'Unexpected notification count').to.have.lengthOf(1)
      expect(recordedNotifs[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Error)
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
      })
    })
  })

  describe('#close', function () {
    let update: SinonStub<[Game], Promise<Game>>

    beforeEach('Prepare doubles', function () {
      update = sinon.stub(api.game, 'update')
    })

    afterEach('Restore doubles', function () {
      update.restore()
    })

    it('must close game', async function () {
      await doAndUpdate(() => gameStore.close(), update, true)
      expect(update).to.have.been.calledOnceWith({
        ...GAME,
        closed: true,
      })
      expect(recordedNotifs, 'Unexpected notification count').to.have.lengthOf(1)
      expect(recordedNotifs[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Success)
      expect(recordedNotifs[0].message).to.match(/game closed/i)
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
        game: 1,
      })
    })

    it('must not close game if error', async function () {
      await doAndUpdate(() => gameStore.close(), update, false)
      expect(update).to.have.been.calledOnceWith({
        ...GAME,
        closed: true,
      })
      expect(gameStore.game!.location).to.deep.equal(LOCATION)
      expect(recordedNotifs, 'Unexpected notification count').to.have.lengthOf(1)
      expect(recordedNotifs[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Error)
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
      })
    })

    it('must fail if no game loaded', async function () {
      gameStore = new GameStore(lang)
      observer = observe(gameStore)
      await expect(gameStore.close()).to.be.rejectedWith(/no current game/i)
    })
  })
})
