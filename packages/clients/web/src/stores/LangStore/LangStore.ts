/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import Intl from 'intl-ts'
import { reaction } from 'mobx'

import { UnassembledMessages } from '@gecogvidanto/shared'

import { langType } from '../../locale'
import { api, notifyError } from '../../tools'

/**
 * The language store.
 */
export default class LangStore {
  /**
   * Create the store.
   *
   * @param lang - The language.
   */
  public constructor(public readonly lang: Intl<langType>) {
    reaction(
      () => this.lang.$preferences,
      async preferences => {
        if (preferences.length > 0) {
          try {
            await api.lang.select(preferences[0])
          } catch (error) {
            notifyError(error, this.lang)
          }
        }
      }
    )
  }

  /**
   * Assemble the unassembled message to build a string.
   *
   * @param message - The message to assemble.
   * @returns The built string.
   */
  public assemble: <K extends keyof langType>(message: UnassembledMessages<langType>[K]) => string = <
    K extends keyof langType
  >(
    message: UnassembledMessages<langType>[K]
  ) => {
    let key: K
    let parameters: any[]
    if (typeof message === 'string') {
      key = (message as unknown) as K
      parameters = []
    } else {
      const paramMessage = (message as unknown) as {
        template: K
        parameters: any[]
      }
      key = paramMessage.template
      parameters = paramMessage.parameters
    }
    try {
      return (this.lang[key] as any)(...parameters)
    } catch {
      return `(no translation for ${key}[${parameters.join(', ')}])`
    }
  }
}
