/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import Intl from 'intl-ts'
import { runInAction } from 'mobx'
import * as sinon from 'sinon'
import { SinonStub } from 'sinon'

import { ApiError } from '@gecogvidanto/client'

import { NotificationEvent, NotificationLevel, api } from '../../tools'
/* eslint-disable import/no-internal-modules */
import {
  ERROR_MSG,
  NotificationStore,
  Observer,
  languageMap,
  sendNotifications,
} from '../helpers/common.spec'
import LangStore from './LangStore'

const langStore = new LangStore(new Intl(languageMap, []))

describe('LangStore', function () {
  let recordedNotifs: NotificationEvent[]
  let observer: Observer
  let langSelect: SinonStub<[string], Promise<void>>

  describe('update language preferences', function () {
    beforeEach('Initialize store', function () {
      langSelect = sinon.stub(api.lang, 'select')
      const notificationStore = new NotificationStore()
      recordedNotifs = notificationStore.recordedNotifs
      observer = new Observer([
        {
          name: 'lang',
          getter: () => langStore.lang.$(),
        },
      ])
      expect(langStore.lang.$preferences).to.be.empty
    })

    afterEach('Clean up', function () {
      runInAction(() => langStore.lang.$changePreferences([]))
      observer.terminate()
      langSelect.restore()
    })

    it('must modify default language on success', async function () {
      langSelect.resolves()
      runInAction(() => langStore.lang.$changePreferences(['eo']))
      await sendNotifications()
      expect(langSelect).to.have.been.calledOnceWith('eo')
      expect(langStore.lang.$preferences).to.have.lengthOf(1)
      expect(langStore.lang.$preferences[0], 'Unexpected preferences').to.equal('eo')
      expect(recordedNotifs, 'Unexpected notification count').to.be.empty
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        lang: 1,
      })
    })

    it('must modify default language and send notification on failure', async function () {
      langSelect.rejects(new ApiError(ERROR_MSG))
      runInAction(() => langStore.lang.$changePreferences(['fr']))
      await sendNotifications()
      expect(langSelect).to.have.been.calledOnceWith('fr')
      expect(langStore.lang.$preferences).to.have.lengthOf(1)
      expect(langStore.lang.$preferences[0], 'Unexpected preferences').to.equal('fr')
      expect(recordedNotifs, 'Unexpected notification count').to.have.lengthOf(1)
      expect(recordedNotifs[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Error)
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        lang: 1,
      })
    })
  })

  describe('#assemble', function () {
    it('must create string when no parameters', function () {
      const value = langStore.assemble('pageGameSetInProgress')
      expect(value).to.equal('Set in progress')
    })

    it('must create string with parameters', function () {
      const value = langStore.assemble({ template: 'pageGameSet', parameters: [9, 'yolo'] })
      expect(value).to.equal('Set #10: yolo')
    })
  })
})
