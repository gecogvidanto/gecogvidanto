/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import Intl from 'intl-ts'
import { action, computed, flow, observable } from 'mobx'

import { User } from '@gecogvidanto/shared'

import { langType } from '../../locale'
import { api, notificationManager, notifyError } from '../../tools'

/**
 * The user store is used to manipulate the logged user.
 */
export default class UserStore {
  @observable.ref
  private _logged?: User

  @observable
  private _loading = false

  /**
   * Create the store.
   *
   * @param lang - The language.
   * @param logged - The logged users, if any.
   */
  public constructor(private readonly lang: Intl<langType>, logged?: User) {
    this._logged = logged
  }

  /**
   * The logged user, if any.
   *
   * @returns The logged user.
   */
  @computed
  public get logged(): User | undefined {
    return this._logged
  }

  /**
   * Indicate if user is currently being loaded.
   *
   * @returns The loading state.
   */
  @computed
  public get loading(): boolean {
    return this._loading
  }

  /**
   * Sign-in for the user.
   *
   * @param email - The e-mail address.
   * @param password - The password.
   */
  public signIn: (email: string, password: string) => Promise<void> = flow(function* signIn(
    this: UserStore,
    email: string,
    password: string
  ) {
    this._loading = true
    try {
      this._logged = yield api.user.signIn(email, password)
      notificationManager.emitSuccess(this.lang.logged(this._logged!.name))
    } catch (error) {
      this._logged = undefined
      notifyError(error, this.lang)
    } finally {
      this._loading = false
    }
  }).bind(this)

  /**
   * Sign-out the current logged user.
   */
  public signOut: () => Promise<void> = action(async () => {
    this._logged = undefined
    try {
      await api.user.signOut()
      notificationManager.emitSuccess(this.lang.loggedOut())
      if (typeof window !== 'undefined') {
        window.sessionStorage.clear()
        setTimeout(() => (window.location.pathname = '/'))
      }
    } catch (error) {
      notifyError(error, this.lang)
    }
  })

  /**
   * Refresh the logged in user data.
   *
   * @param user - The user loaded from server if any.
   */
  public refresh: (user?: User) => Promise<void> = flow(function* (this: UserStore, user?: User) {
    if (this._logged && !this._loading) {
      if (user && user._id === this._logged._id) {
        this._logged = user
      } else {
        try {
          this._loading = true
          this._logged = yield api.user.read(this._logged._id!)
        } catch (error) {
          notifyError(error, this.lang)
        } finally {
          this._loading = false
        }
      }
    }
  }).bind(this)
}
