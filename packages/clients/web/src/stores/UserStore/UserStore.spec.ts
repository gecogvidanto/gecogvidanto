/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import Intl from 'intl-ts'
import * as sinon from 'sinon'
import { SinonStub } from 'sinon'

import { ApiError } from '@gecogvidanto/client'
import { CertLevel, User } from '@gecogvidanto/shared'

import { NotificationEvent, NotificationLevel, api } from '../../tools'
/* eslint-disable import/no-internal-modules */
import {
  ERROR_MSG,
  NotificationStore,
  Observer,
  defer,
  languageMap,
  sendNotifications,
} from '../helpers/common.spec'
import UserStore from './UserStore'

const USER: User = {
  _id: '0',
  email: 'god@heaven.sky',
  name: 'God',
  level: CertLevel.Administrator,
  account: 'IBAN',
}

const GOD = {
  email: USER.email,
  password: 'adam&eve',
}

const lang = new Intl(languageMap, [])
function observe(userStore: UserStore): Observer {
  return new Observer([
    {
      name: 'loading',
      getter: () => userStore.loading,
    },
    {
      name: 'logged',
      getter: () => userStore.logged,
    },
  ])
}

describe('UserStore', function () {
  let userStore: UserStore
  let recordedNotifs: NotificationEvent[]
  let observer: Observer
  let signIn: SinonStub<[string, string], Promise<User>>
  let signOut: SinonStub<[], Promise<void>>
  let read: SinonStub<[string], Promise<User>>

  beforeEach('Initialize store', function () {
    signIn = sinon.stub(api.user, 'signIn')
    signOut = sinon.stub(api.user, 'signOut')
    read = sinon.stub(api.user, 'read')
    const notificationStore = new NotificationStore()
    recordedNotifs = notificationStore.recordedNotifs
    userStore = new UserStore(lang, USER)
    observer = observe(userStore)
    expect(userStore.logged, 'Unexpected initial value for logged').to.exist
    expect(userStore.loading, 'Logged user should not be loading').to.be.false
  })

  afterEach('Clean up', function () {
    observer.terminate()
    read.restore()
    signOut.restore()
    signIn.restore()
  })

  describe('#signIn', function () {
    beforeEach('Create store without user', function () {
      userStore = new UserStore(lang)
      observer = observe(userStore)
      expect(userStore.logged).to.be.undefined
      while (recordedNotifs.length > 0) {
        recordedNotifs.pop()
      }
      observer.reset()
    })

    it('must get matching user if any', async function () {
      const resolve = defer(signIn).resolve
      userStore.signIn(GOD.email, GOD.password)
      await sendNotifications()
      expect(signIn).to.have.been.calledOnceWith(GOD.email, GOD.password)
      expect(signOut).to.not.have.been.called
      expect(read).to.not.have.been.called
      expect(userStore.loading, 'Logged user should be loading').to.be.true
      resolve(USER)
      await sendNotifications()
      expect(userStore.loading, 'Logged user should be loaded').to.be.false
      expect(userStore.logged).to.deep.equal(USER)
      expect(recordedNotifs, 'Unexpected notification count').to.have.lengthOf(1)
      expect(recordedNotifs[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Success)
      expect(recordedNotifs[0].message).to.match(/now logged in/i)
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
        logged: 1,
      })
    })

    it('must send notification if error', async function () {
      signIn.rejects(new ApiError(ERROR_MSG))
      userStore.signIn(GOD.email, GOD.password)
      await sendNotifications()
      expect(userStore.loading, 'Logged user should be loaded').to.be.false
      expect(userStore.logged).to.be.undefined
      expect(recordedNotifs, 'Unexpected notification count').to.have.lengthOf(1)
      expect(recordedNotifs[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Error)
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
      })
    })
  })

  describe('#signOut', function () {
    it('must invalidate user on success', async function () {
      signOut.resolves()
      const savedWindow = window
      try {
        ;(window as any) = undefined
        userStore.signOut()
        await sendNotifications()
        expect(signIn).to.not.have.been.called
        expect(signOut).to.have.been.calledOnce
        expect(read).to.not.have.been.called
        expect(userStore.logged).to.be.undefined
        expect(recordedNotifs, 'Unexpected notification count').to.have.lengthOf(1)
        expect(recordedNotifs[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Success)
        expect(recordedNotifs[0].message).to.match(/now logged out/i)
        expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
          logged: 1,
        })
      } finally {
        window = savedWindow
      }
    })

    it('must invalidate user and send notification on failure', async function () {
      signOut.rejects(new ApiError(ERROR_MSG))
      userStore.signOut()
      await sendNotifications()
      expect(signIn).to.not.have.been.called
      expect(signOut).to.have.been.calledOnce
      expect(read).to.not.have.been.called
      expect(userStore.logged).to.be.undefined
      expect(recordedNotifs, 'Unexpected notification count').to.have.lengthOf(1)
      expect(recordedNotifs[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Error)
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        logged: 1,
      })
    })
  })

  describe('#refresh', function () {
    it('must refresh user data', async function () {
      const resolve = defer(read).resolve
      userStore.refresh()
      await sendNotifications()
      expect(signIn).to.not.have.been.called
      expect(signOut).to.not.have.been.called
      expect(read).to.have.been.calledOnceWith(USER._id)
      expect(userStore.loading, 'Logged user should be loading').to.be.true
      resolve({ ...USER, name: 'Satan' })
      await sendNotifications()
      expect(userStore.loading, 'Logged user should be loaded').to.be.false
      expect(userStore.logged).to.exist
      expect((userStore.logged as User).name).to.equal('Satan')
      expect(recordedNotifs, 'Unexpected notification count').to.be.empty
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
        logged: 1,
      })
    })

    it('must not refresh user data if non existent or loading', async function () {
      userStore = new UserStore(lang)
      observer = observe(userStore)
      expect(userStore.logged).to.be.undefined
      userStore.refresh()
      await sendNotifications()
      expect(signIn).to.not.have.been.called
      expect(signOut).to.not.have.been.called
      expect(read).to.not.have.been.called
      expect(userStore.logged).to.be.undefined
      expect(observer.changes, 'First step observed changes').to.deep.equal({})
      const reject = defer(signIn).reject
      userStore.signIn(GOD.email, GOD.password)
      await sendNotifications()
      expect(observer.changes, 'Second step observed changes').to.deep.equal({
        loading: 1,
      })
      userStore.refresh()
      await sendNotifications()
      expect(read).to.not.have.been.called
      expect(recordedNotifs, 'Unexpected notification count').to.be.empty
      expect(observer.changes, 'Third step observed changes').to.deep.equal({
        loading: 1,
      })
      reject(new Error())
      await sendNotifications()
    })

    it('must notify if error', async function () {
      read.rejects(new ApiError(ERROR_MSG))
      userStore.refresh()
      await sendNotifications()
      expect(userStore.logged).to.deep.equal(USER)
      expect(recordedNotifs, 'Unexpected notification count').to.have.lengthOf(1)
      expect(recordedNotifs[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Error)
      expect(observer.changes, 'Unexpected observed changes').to.deep.equal({
        loading: 2,
      })
    })
  })
})
