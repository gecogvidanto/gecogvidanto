/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
type Mergeable<P extends any[]> = (...args: P) => void

/**
 * Create a function which is a merge of many function with similar signatures. Functions are called
 * successively.
 *
 * @param funcs - Functions to merge.
 * @returns The merged functions.
 */
export function mergeFunctions<P extends any[]>(...funcs: Array<Mergeable<P> | undefined>): Mergeable<P> {
  return (...args: P) => {
    funcs.forEach(func => func && func(...args))
  }
}
