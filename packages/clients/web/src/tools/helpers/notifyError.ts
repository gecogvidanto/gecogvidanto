/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import Intl from 'intl-ts'

import { ApiError } from '@gecogvidanto/client'

import { langType } from '../../locale'
import { notificationManager } from '../constants'

/**
 * Notify the error, depending on its shape.
 *
 * @param error - The error to check.
 * @param lang - The language for error messages if any.
 */
export function notifyError(error: unknown, lang: Intl<langType>): void {
  const message: string = error instanceof ApiError ? error.getMessage(lang) : lang.unknownError()
  notificationManager.emitError(message)
}
