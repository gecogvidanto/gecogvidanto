/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { DryContextType, ServerContext } from '../../context'

type ContextType = Required<DryContextType>

/**
 * Load values sent by the server at initialization.
 *
 * @param context - The server context.
 * @param a - The option to load.
 * @returns The value.
 */
export function preload<A extends keyof ContextType>(
  context: ServerContext | undefined,
  a: A
): ContextType[A] | undefined

/**
 * Load values sent by the server at initialization.
 *
 * @param context - The server context.
 * @param a - The option to load.
 * @param b - The sub-option if any.
 * @returns The value.
 */
export function preload<A extends keyof ContextType, B extends keyof ContextType[A]>(
  context: ServerContext | undefined,
  a: A,
  b: B
): ContextType[A][B] | undefined

/*
 * Implementation.
 */
export function preload<A extends keyof ContextType, B extends keyof ContextType[A]>(
  context: ServerContext | undefined,
  a: A,
  b?: B
): ContextType[A] | ContextType[A][B] | undefined {
  let stepA: ContextType[A] | undefined
  if (context) {
    stepA = context[a]
  } else if (
    typeof window !== 'undefined' &&
    window.__PRELOADED_STATE__ !== undefined &&
    window.__PRELOADED_STATE__[a] !== undefined
  ) {
    stepA = window.__PRELOADED_STATE__[a] as ContextType[A]
  }

  if (stepA !== undefined && b) {
    let stepB: ContextType[A][B] | undefined
    if (stepA[b] !== undefined) {
      stepB = stepA[b]
    }
    return stepB
  } else {
    return stepA
  }
}
