/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * Load a JavaScript script from an external source and executes it (execution is asynchronous).
 *
 * @param src - The `src` paramater to be given to the `script` tag, usually the URL to the script source.
 * @returns A promise giving a disposal function.
 */
export function loadScript(src: string): Promise<() => void> {
  return new Promise<() => void>((resolve, reject) => {
    let scriptElement = document.createElement('script')
    const dispose: () => void = () => {
      scriptElement.remove()
    }
    scriptElement.type = 'text/javascript'
    scriptElement.src = src
    scriptElement.async = true
    scriptElement.defer = true
    scriptElement.setAttribute(
      'nonce',
      (document.querySelector('meta[property="csp-nonce"]') as HTMLMetaElement).content
    )
    scriptElement.onload = () => resolve(dispose)
    scriptElement.onerror = event => reject(event)
    scriptElement = document.head.appendChild(scriptElement)
  })
}
