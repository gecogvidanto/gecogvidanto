/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * Generate a normalized random integer number. The gaussian generated curve is taken between around -3 and +3 (i.e.
 * more than 99% of the values). When failed, a uniform random number is choosen.
 *
 * @param min - The minimum border.
 * @param max - The maximum border.
 * @returns The gauss number.
 */
export function gaussRandom(min: number, max: number): number {
  const gap: number = max - min
  const mean: number = (min + max) / 2
  let result: number

  // First try Box-Muller method
  const r1 = 1 - Math.random()
  const r2 = 1 - Math.random()
  result = Math.sqrt(-2 * Math.log(r1)) * Math.cos(2 * Math.PI * r2)
  result = Math.round((result * gap) / 6 + mean)

  // For failing numbers, use uniform random
  if (result < min || result > max) {
    result = Math.floor(Math.random() * (gap + 1)) + min
  }

  return result
}
