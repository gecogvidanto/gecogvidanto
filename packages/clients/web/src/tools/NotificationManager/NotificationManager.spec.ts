/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback */
import { expect } from 'chai'

import NotificationManager, { NotificationEvent, NotificationLevel } from './NotificationManager'

class MemoryListener {
  public readonly memory: NotificationEvent[] = []

  public onNotification = (event: NotificationEvent): void => {
    this.memory.push(event)
  }
}

describe('NotificationManager', function () {
  let notificationManager: NotificationManager
  let listener: MemoryListener

  beforeEach('Reset data', function () {
    notificationManager = new NotificationManager()
    listener = new MemoryListener()
    notificationManager.on(listener.onNotification)
  })

  describe('#emitSuccess', function () {
    it('must emit success level notification', async function () {
      await notificationManager.emitSuccess('message')
      expect(listener.memory).to.have.lengthOf(1)
      expect(listener.memory[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Success)
      expect(listener.memory[0].message).to.equal('message')
    })
  })

  describe('#emitInfo', function () {
    it('must emit info level notification', async function () {
      await notificationManager.emitInfo('message')
      expect(listener.memory).to.have.lengthOf(1)
      expect(listener.memory[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Info)
      expect(listener.memory[0].message).to.equal('message')
    })
  })

  describe('#emitWarning', function () {
    it('must emit warning level notification', async function () {
      await notificationManager.emitWarning('message')
      expect(listener.memory).to.have.lengthOf(1)
      expect(listener.memory[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Warning)
      expect(listener.memory[0].message).to.equal('message')
    })
  })

  describe('#emitError', function () {
    it('must emit error level notification', async function () {
      await notificationManager.emitError('message')
      expect(listener.memory).to.have.lengthOf(1)
      expect(listener.memory[0].level, 'Unexpected notification level').to.equal(NotificationLevel.Error)
      expect(listener.memory[0].message).to.equal('message')
    })
  })
})
