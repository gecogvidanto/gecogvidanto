/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { AsyncEmitter } from '@gecogvidanto/shared'

/**
 * The levels for the notification.
 */
export const enum NotificationLevel {
  Success,
  Info,
  Warning,
  Error,
}

/**
 * The notification event.
 */
export interface NotificationEvent {
  level: NotificationLevel
  message: string
}

/**
 * Manager for the notifications of the application.
 */
export default class NotificationManager extends AsyncEmitter<NotificationEvent> {
  /**
   * Emit a success message.
   *
   * @param message - The message.
   * @returns A resolved promise when emit done.
   */
  public emitSuccess(message: string): Promise<void> {
    return this.emit({ level: NotificationLevel.Success, message })
  }

  /**
   * Emit an information message.
   *
   * @param message - The message.
   * @returns A resolved promise when emit done.
   */
  public emitInfo(message: string): Promise<void> {
    return this.emit({ level: NotificationLevel.Info, message })
  }

  /**
   * Emit a warning message.
   *
   * @param message - The message.
   * @returns A resolved promise when emit done.
   */
  public emitWarning(message: string): Promise<void> {
    return this.emit({ level: NotificationLevel.Warning, message })
  }

  /**
   * Emit an error message.
   *
   * @param message - The message.
   * @returns A resolved promise when emit done.
   */
  public emitError(message: string): Promise<void> {
    return this.emit({ level: NotificationLevel.Error, message })
  }
}
