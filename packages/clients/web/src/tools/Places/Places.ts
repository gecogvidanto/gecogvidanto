/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
// No direct import: all imports are dynamics
interface SimpleEvent {
  message: string
}
export type PlacesInstance = import('places.js').PlacesInstance
export type PlacesChangeEvent = import('places.js').ChangeEvent
export type PlacesErrorEvent = SimpleEvent
export type PlacesLimitEvent = SimpleEvent
type PlacesFunction = typeof import('places.js').default

/*
 * The places module, dynamically loaded.
 */
let _places: PlacesFunction | undefined

/**
 * The interface for managing `places.js`. The module is dynamically loaded.
 */
export default class Places {
  public readonly placesInstance: PlacesInstance

  public static async create(
    container: HTMLInputElement,
    appId: string | undefined,
    apiKey: string | undefined,
    type: 'address'
  ): Promise<Places> {
    if (!_places) {
      const _module = await import(/* webpackChunkName: "Places" */ 'places.js')
      _places = _module.default
    }
    return new Places(_places, container, appId, apiKey, type)
  }

  private constructor(
    private readonly places: PlacesFunction,
    private readonly container: HTMLInputElement,
    appId: string | undefined,
    apiKey: string | undefined,
    type: 'address'
  ) {
    const focused: boolean = this.container === document.activeElement
    this.placesInstance = this.places({
      container: this.container,
      appId,
      apiKey,
      type,
    })
    this.container.classList.remove('ap-input')
    focused && this.container.focus()
  }

  public close(): void {
    this.container.classList.add('ap-input')
    this.placesInstance.destroy()
  }
}
