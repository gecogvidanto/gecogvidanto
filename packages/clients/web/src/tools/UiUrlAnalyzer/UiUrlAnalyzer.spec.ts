/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import UiUrlAnalyzer from './UiUrlAnalyzer'

const AVAILABLE_LANGS: Array<{ code: string; lang: string; url: string }> = [
  { code: 'fr', lang: 'fr', url: '/_fr' },
  { code: 'fr-ca', lang: 'fr_ca', url: '/_fr_ca' },
  { code: 'en', lang: 'en', url: '/_en' },
  { code: 'eo', lang: 'eo', url: '/_eo' },
]
const TESTED_URLS: Array<{
  url: string
  lang: string
  end: string
  clean: string
}> = [
  // Classical non matching path
  {
    url: '/a/non/matching/path',
    lang: '',
    end: '/a/non/matching/path',
    clean: '/a/non/matching/path',
  },
  // Root path
  {
    url: '/',
    lang: '',
    end: '',
    clean: '/',
  },
  // Non available language path
  {
    url: '/_es',
    lang: '',
    end: '/_es',
    clean: '/_es',
  },
  // Root language path with extra /
  {
    url: '/_fr_ca/',
    lang: '/_fr_ca',
    end: '',
    clean: '/',
  },
  // Classical matching path
  {
    url: '/_en/with/full/path',
    lang: '/_en',
    end: '/with/full/path',
    clean: '/with/full/path',
  },
]
const availableLangs = AVAILABLE_LANGS.map(available => available.lang)
const testedUrls = TESTED_URLS.map(tested => ({
  ...tested,
  analyzer: new UiUrlAnalyzer(tested.url, availableLangs),
}))

describe('UiUrlAnalyzer', () => {
  describe('#langUrl', function () {
    testedUrls.map(tested =>
      it(`must match lang part of URL ${tested.url}`, function () {
        expect(tested.analyzer.langUrl).to.equal(tested.lang)
      })
    )
  })

  describe('#endUrl', function () {
    testedUrls.map(tested =>
      it(`must match end part of URL ${tested.url}`, function () {
        expect(tested.analyzer.endUrl).to.equal(tested.end)
      })
    )
  })

  describe('#cleanUrl', function () {
    testedUrls.map(tested =>
      it(`must match clean end part of URL ${tested.url}`, function () {
        expect(tested.analyzer.cleanUrl).to.equal(tested.clean)
      })
    )
  })

  describe('#alternates', function () {
    testedUrls.map(tested =>
      it(`must give all alternatives for ${tested.url}`, function () {
        tested.analyzer.alternates // Ask twice, it should be calculated once
        expect(tested.analyzer.alternates).to.deep.equal(
          AVAILABLE_LANGS.map(available => ({
            lang: available.code,
            url: available.url + tested.analyzer.endUrl,
          }))
        )
      })
    )
  })
})
