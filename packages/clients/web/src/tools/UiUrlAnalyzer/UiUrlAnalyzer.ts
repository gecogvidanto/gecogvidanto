/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Key, compile, pathToRegexp } from 'path-to-regexp'

/**
 * An alternate URL for a given language.
 */
export interface Alternate {
  lang: string
  url: string
}

const LANG_URL = '/\\_:lgCode'
const keys: Key[] = []
pathToRegexp(LANG_URL, keys)
const LANG_PARAM = keys[0].name as string

/**
 * Manage the UI URL.
 */
export default class UiUrlAnalyzer {
  /**
   * The URL used to identify language.
   */
  public static readonly LANG_URL: string = LANG_URL

  /**
   * The parameter name extracting language code from URL.
   */
  public static readonly LANG_PARAM: string = LANG_PARAM

  /**
   * The part of the URL containing the language.
   */
  public readonly langUrl: string

  /**
   * The part of the URL after the language string.
   */
  public readonly endUrl: string

  /**
   * The alternate paths for alternate languages.
   */
  private _alternates?: Alternate[]

  /**
   * Create the object.
   *
   * @param url - The URL to analyze.
   * @param availableLangs - The available language codes.
   */
  public constructor(url: string, private readonly availableLangs: string[]) {
    const langRegExp = pathToRegexp(UiUrlAnalyzer.LANG_URL, undefined, {
      end: false,
    })
    const match = langRegExp.exec(url)
    const [langMatch, lang] = match ? match : [undefined, undefined]
    const urlParts: [string, string] = ['', '']
    if (langMatch && lang && this.availableLangs.includes(lang)) {
      urlParts[0] = langMatch
      urlParts[1] = url.substring(langMatch.length)
    } else {
      urlParts[1] = url
    }
    ;[this.langUrl, this.endUrl] = urlParts.map(urlPart =>
      urlPart.endsWith('/') ? urlPart.slice(0, -1) : urlPart
    )
  }

  /**
   * @returns The URL without the language part.
   */
  public get cleanUrl(): string {
    return this.endUrl.length > 0 ? this.endUrl : '/'
  }

  /**
   * @returns The alternate URLs for available languages.
   */
  public get alternates(): Alternate[] {
    if (!this._alternates) {
      this._alternates = this.availableLangs.map(lgCode => ({
        lang: lgCode.replace('_', '-'),
        url: compile(UiUrlAnalyzer.LANG_URL)({ lgCode }) + this.endUrl,
      }))
    }
    return this._alternates
  }
}
