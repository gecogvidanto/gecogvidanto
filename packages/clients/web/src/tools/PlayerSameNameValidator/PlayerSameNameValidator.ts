/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Validator } from '@gecogvidanto/shared'

import { langType } from '../../locale'

export default class PlayerSameNameValidator implements Validator<langType, []> {
  public readonly params: [] = []

  public constructor(private readonly names: string[]) {}

  public validate(value: string): 'pageAddPlayersSameName' | undefined {
    if (this.names.includes(value)) {
      return 'pageAddPlayersSameName'
    } else {
      return undefined
    }
  }
}
