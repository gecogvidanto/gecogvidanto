/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { WsClientEventDefinition, WsServerEventDefinition, wsRoot } from '@gecogvidanto/shared'

import type * as SocketIOClientStatic from 'socket.io-client'

// Workaround for Webpack which cannot import functions from CommonJS
// eslint-disable-next-line @typescript-eslint/no-var-requires
const createClient: typeof SocketIOClientStatic = require('socket.io-client')

/**
 * The connection event.
 */
const CONNECT_EVENT = 'connect'

/**
 * Manage a web socket at client side.
 */
export default class WebSocket {
  /**
   * The socket.
   */
  private readonly socket: SocketIOClient.Socket

  /**
   * Create the socket.
   */
  public constructor() {
    this.socket = createClient({ path: `/${wsRoot}` })
  }

  /**
   * Add a callback function when connection established. Beware that this should be called immediately after creating
   * the object, or the connection event may be lost.
   *
   * @param callback - The callback function which will be called when connection.
   */
  public onConnect(callback: () => void): void {
    this.socket.on(CONNECT_EVENT, callback)
  }

  /**
   * Close the connection.
   */
  public close(): void {
    this.socket.close()
  }

  /**
   * Emit a new event.
   *
   * @param event - The event name.
   * @param args - The event arguments.
   */
  public emit<T extends keyof WsClientEventDefinition>(
    event: T,
    ...args: WsClientEventDefinition[T]
  ): void {
    this.socket.emit(event, ...args)
  }

  /**
   * Listen to an event.
   *
   * @param event - The event to listen to.
   * @param callback - The function to execute on event.
   */
  public on<T extends keyof WsServerEventDefinition>(
    event: T,
    callback: (...args: WsServerEventDefinition[T]) => void
  ): void {
    this.socket.on(event, callback)
  }
}
