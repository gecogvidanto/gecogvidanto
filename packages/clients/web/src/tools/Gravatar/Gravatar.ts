/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { MD5 } from 'spu-md5'

/**
 * Manage the Gravatar associated to given e-mail.
 */
export default class Gravatar {
  private md5Hash: string

  public constructor(email: string) {
    const md5 = new MD5()
    md5.update(Uint8Array.from(email.split('').map(c => c.charCodeAt(0))))
    this.md5Hash = md5.toString()
  }

  public getImageUrl(size: number, notExist: 'mp' | 'identicon' = 'identicon'): string {
    return `https://secure.gravatar.com/avatar/${this.md5Hash}?s=${size}&d=${notExist}`
  }

  public getProfileUrl(): string {
    return `https://www.gravatar.com/profile/${this.md5Hash}`
  }
}
