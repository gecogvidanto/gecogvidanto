/*
 * This file is part of @gecogvidanto/client-web.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable import/no-extraneous-dependencies */
import { join } from 'path'
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin'
import { Configuration, RuleSetRule } from 'webpack'

export const paths = {
  bundle: 'gecogvidanto',
  public: '/assets/',
  config: join(__dirname, 'tsconfig.bundle.json'),
}

const scopeRule: RuleSetRule['use'] = {
  loader: 'babel-loader',
  options: {
    presets: [
      [
        '@babel/preset-env',
        {
          modules: 'commonjs',
          corejs: 3,
          useBuiltIns: 'usage',
          targets: '> 0.25%, not dead',
        },
      ],
      '@babel/preset-react',
    ],
  },
}

const config: Configuration = {
  entry: ['./src/client.tsx', './src/images/g1-favicon.png', 'typeface-roboto'],
  output: {
    path: join(__dirname, 'dist', 'public'),
    publicPath: paths.public,
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [
          scopeRule,
          {
            loader: 'ts-loader',
            options: {
              compiler: 'ttypescript',
              configFile: paths.config,
            },
          },
        ],
      },
      {
        test: /\.m?js$/,
        exclude: /node_modules[/\\](?!@gecogvidanto[/\\])/,
        use: scopeRule,
      },
      {
        test: /\.(woff2?|png|svg|ogg)$/,
        sideEffects: true,
        use: {
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
          },
        },
      },
      {
        test: /\.css$/, // Needed for Roboto
        sideEffects: true,
        use: [
          {
            loader: 'style-loader',
            options: {
              injectType: 'singletonStyleTag',
              insert: (style: HTMLStyleElement): void => {
                style.setAttribute(
                  'nonce',
                  (document.querySelector('meta[property="csp-nonce"]') as HTMLMetaElement).content
                )
                document.head.appendChild(style)
              },
            },
          },
          'css-loader',
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    modules: ['node_modules', join(__dirname, 'node_modules')],
    plugins: [new TsconfigPathsPlugin({ configFile: paths.config })],
  },
}

export default config
