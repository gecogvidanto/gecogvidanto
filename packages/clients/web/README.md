# Web client for ĞecoĞvidanto

ĞecoĞvidanto is a tool used to help managing Ğeconomicus games. This module contains ĞecoĞvidanto client for web.

# Language/langue

Documents, messages, code (including variable names and comments), are in English.

Anyway, because French is my native language, all documents and important messages must also be provided in French. Other translations are welcome.

:fr: Une version française de ce document se trouve [ici](doc/fr/README.md).

# Installation

Installation is done using `npm install` command:

```bash
$ npm install --save @gecogvidanto/client-web
```

# License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

# Usage

The web client is a _Javascript_ application. It requires that you allow cookies and, of course, _Javascript_ execution for it. The language used for the application should be compatible with most browsers. If anyway you encounter a misfunctionning, try to update your browser to a most recent version, or even to use another browser.

To know how to use the web client, read its [manual](doc/en/Manual.md).

# Contributing

Even though I cannot guarantee a response time, please feel free to file an issue if you have any question or problem using the package.

_Pull Requests_ are welcome. You can, of course, submit corrections or improvements for code, but do not hesitate to also improve documentation, even for small spell or grammar errors.
