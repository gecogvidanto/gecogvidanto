/*
 * This file is part of @gecogvidanto/plugin.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import Intl, { Messages } from 'intl-ts'

import { Database, ModelList } from '../database'

/**
 * The server application must have all these data.
 * **Be aware that all these data may not be fully initialized when plugin constructor is called**, but must
 * be at the time the {@link GecoPlugin#ready()} method is called.
 */
export default interface ServerApp<M extends Messages = Messages> {
  readonly serverLang: Intl<M>
  readonly clientLang: Intl<M>
  readonly config: unknown
  readonly database: Database<ModelList>
}

/**
 * Check (type guard) if all given models are in database.
 *
 * @param database - The database to check.
 * @param models - The names of expected models.
 * @returns True or faulse, depending on result check.
 */
export function databaseHasModels<T extends ModelList>(
  database: Database<ModelList>,
  ...models: Array<keyof T>
): database is Database<T> {
  for (const model of models) {
    if (!(model in database.models)) {
      return false
    }
  }
  return true
}
