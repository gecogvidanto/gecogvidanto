/*
 * This file is part of @gecogvidanto/plugin.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import { databaseHasModels } from '.'
import { Database, ModelList } from '../database'

describe('databaseHasModels', () => {
  const database: Database<ModelList> = {
    models: {
      muggles: null as any,
      creatures: null as any,
      wands: null as any,
    },
  } as any

  it('must be true if all models are included', () => {
    const models = {
      creatures: null as any,
      muggles: null as any,
    }
    expect(databaseHasModels<typeof models>(database, 'creatures', 'muggles')).to.be.true
  })

  it('must be false if some models are missing', () => {
    const models = {
      creatures: null as any,
      plants: null as any,
    }
    expect(databaseHasModels<typeof models>(database, 'creatures', 'plants')).to.be.false
  })

  it('must be false if no models are included', () => {
    const models = {
      castles: null as any,
      trains: null as any,
    }
    expect(databaseHasModels<typeof models>(database, 'castles', 'trains')).to.be.false
  })
})
