/*
 * This file is part of @gecogvidanto/plugin.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
export interface Identifier {
  _id: string
}

interface Operator<T> {
  $lt?: T
  $lte?: T
  $gt?: T
  $gte?: T
  $ne?: T
  $in?: T[]
  $nin?: T[]
  $exists?: boolean
  $regex?: RegExp
}

interface LogicalOperator<T> {
  $and?: Array<Query<T>>
  $or?: Array<Query<T>>
  $not?: Query<T>
  $where?: (this: T) => boolean
}

/**
 * A query to select objects in database.
 */
export type Query<T> = Partial<T> | LogicalOperator<T> | { [P in keyof T]?: Operator<T[P]> }

/**
 * A projection for selected objects.
 */
export type Projection<T> = { [P in keyof T]?: 0 | 1 }

interface UpdateOperator<T> {
  $set?: Partial<T>
  $unset?: { [P in keyof T]?: true }
  $inc?: Partial<T>
  $min?: Partial<T>
  $max?: Partial<T>
}

/**
 * An update query.
 */
export type Update<T> = T | UpdateOperator<T>
