/*
 * This file is part of @gecogvidanto/plugin.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Identifier, Projection, Query, Update } from './CollectionTypes'

/**
 * A collection of data.
 */
export default interface Collection<T> {
  /**
   * Insert the document in database.
   *
   * @param newDoc - Document to insert.
   */
  insert(newDoc: T): Promise<Identifier & T>

  /**
   * Count the documents in database.
   *
   * @param query - Selection query.
   */
  count(query: Query<Identifier & T>): Promise<number>

  /**
   * Find documents in database.
   *
   * @param query - Selection query.
   * @param projection - Fields to select.
   */
  find(
    query: Query<Identifier & T>,
    projection?: Projection<Identifier & T>
  ): Promise<Array<Identifier & T>>

  /**
   * Find a single document in database.
   *
   * @param query - Selection query.
   * @param projection - Fields to select.
   */
  findOne(
    query: Query<Identifier & T>,
    projection?: Projection<Identifier & T>
  ): Promise<(Identifier & T) | undefined>

  /**
   * Update a document in database.
   *
   * @param query - Selection query.
   * @param updateQuery - Update query.
   * @param upsert - True to insert if no row selected (default is false).
   */
  update(query: Query<Identifier & T>, updateQuery: Update<T>, upsert?: boolean): Promise<number>

  /**
   * Remove a document in database.
   *
   * @param query - Selection query.
   */
  remove(query: Query<Identifier & T>): Promise<number>
}
