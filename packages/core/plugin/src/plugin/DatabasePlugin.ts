/*
 * This file is part of @gecogvidanto/plugin.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Database, ModelList, ModelTypeList } from '../database'
import GecoPlugin from './GecoPlugin'

/**
 * A ĞecoĞvidanto plugin managing a database.
 */
export default interface DatabasePlugin extends GecoPlugin {
  openDatabase<T extends ModelList>(types: ModelTypeList<T>): Database<T>
}

/**
 * Test (type guard) if the ĞecoĞvidanto plugin is a database plugin.
 *
 * @param plugin - The plugin to test.
 * @returns The test result.
 */
export function isDatabasePlugin(plugin: GecoPlugin): plugin is DatabasePlugin {
  return 'openDatabase' in plugin
}
