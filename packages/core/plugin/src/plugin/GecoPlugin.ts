/*
 * This file is part of @gecogvidanto/plugin.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { ConfigDescription } from 'confinode'

import { ModelTypeList } from '../database'
import { ServerApp } from '../server'

/**
 * A default ĞecoĞvidanto plugin.
 */
export default interface GecoPlugin {
  ready(): Promise<void> // Called after database and eco systems are loaded
}

/**
 * Messages found in the plugin.
 */
type GecoMessages = {
  en?: { $?: 'English' } & Partial<import('intl-ts').Messages>
} & {
  [key: string]: Partial<import('intl-ts').Messages>
}

/**
 * The class constructor for the plugin.
 */
type GecoPluginClass = new (serverApp: ServerApp) => GecoPlugin

/**
 * The static data of the plugin.
 */
interface GecoPluginData {
  readonly plugin: 'gecogvidanto-plugin-v1'
  readonly description: string
  readonly configuration?: ConfigDescription<any>
  readonly messages: Readonly<GecoMessages>
  readonly uiMessages: Readonly<GecoMessages>
  readonly models: Readonly<ModelTypeList<any>>
}

/**
 * The type of the plugin.
 */
export type GecoPluginType = GecoPluginClass & GecoPluginData

/**
 * Create a plugin based on given class and data.
 *
 * @param c - The plugin class.
 * @param d - The plugin data.
 * @returns The created plugin.
 */
export function createGecoPlugin(c: GecoPluginClass, d: GecoPluginData): GecoPluginType {
  return Object.assign(c, d)
}

/**
 * Test (type guard) if a given module contains a ĞecoĞvidanto plugin.
 *
 * @param mod - The loaded module.
 * @returns True or false depending on check result.
 */
export function isGecoPluginType(mod: unknown): mod is GecoPluginType {
  return typeof mod === 'function' && 'plugin' in mod && (mod as any).plugin === 'gecogvidanto-plugin-v1'
}
