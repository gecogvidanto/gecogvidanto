/*
 * This file is part of @gecogvidanto/plugin.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import { DatabasePlugin, GecoPlugin, GecoPluginType, createGecoPlugin, isDatabasePlugin } from '.'
import { Database, ModelList, ModelTypeList } from '../database'
import { ServerApp } from '../server'

describe('DatabasePlugin', () => {
  class FakeDatabase<T extends ModelList> implements Database<T> {
    public readonly models: T = undefined as any
    public async waitForStarted(): Promise<void> {
      // Nothing
    }
  }

  class FakeDatabasePlugin implements DatabasePlugin {
    public readonly models = {}

    public constructor(_serverApp: ServerApp) {
      // Don't do anything
    }

    public ready(): Promise<void> {
      return Promise.resolve()
    }

    public openDatabase<T extends ModelList>(_types: ModelTypeList<T>): Database<T> {
      return new FakeDatabase()
    }
  }

  class FakeSimplePlugin implements GecoPlugin {
    public readonly models = {}

    public constructor() {
      // Don't do anything
    }

    public ready(): Promise<void> {
      return Promise.resolve()
    }
  }

  describe('#isDatabasePlugin', () => {
    it('must detect database plugin for plugin with appropriate method', () => {
      const fakePluginType: GecoPluginType = createGecoPlugin(FakeDatabasePlugin, {
        plugin: 'gecogvidanto-plugin-v1',
        description: 'Fake database plugin',
        messages: {},
        uiMessages: {},
        models: {},
      })

      expect(isDatabasePlugin(new fakePluginType(undefined as any))).to.be.true
    })

    it('must not detect database plugin for plugin if no database method', () => {
      const fakePluginType: GecoPluginType = createGecoPlugin(FakeSimplePlugin, {
        plugin: 'gecogvidanto-plugin-v1',
        description: 'Fake database plugin',
        messages: {},
        uiMessages: {},
        models: {},
      })

      expect(isDatabasePlugin(new fakePluginType(undefined as any))).to.be.false
    })
  })
})
