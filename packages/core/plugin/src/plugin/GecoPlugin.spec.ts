/*
 * This file is part of @gecogvidanto/plugin.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import { GecoPlugin, GecoPluginType, ServerApp, createGecoPlugin, isGecoPluginType } from '..'

describe('GecoPlugin', () => {
  class FakePlugin implements GecoPlugin {
    public readonly models = {}

    public constructor(_serverApp: ServerApp) {
      // Don't do anything
    }

    public ready(): Promise<void> {
      return Promise.resolve()
    }
  }

  describe('#isGecoPluginType', () => {
    let modifiablePlugin: any

    beforeEach('Prepare plugin data', () => {
      // Be sure plugin is properly initialized
      const fakePlugin: GecoPluginType = createGecoPlugin(FakePlugin, {
        plugin: 'gecogvidanto-plugin-v1',
        description: 'Fake plugin',
        messages: {},
        uiMessages: {},
        models: {},
      })

      // Assign to a modifiable variable
      modifiablePlugin = fakePlugin
    })

    it('must detect plugin for module with appropriate signature', () => {
      expect(isGecoPluginType(modifiablePlugin)).to.be.true
    })

    it('must not detect plugin for module no plugin property', () => {
      delete modifiablePlugin.plugin
      expect(isGecoPluginType(modifiablePlugin)).to.be.false
    })

    it('must not detect plugin for bad value in module plugin property', () => {
      modifiablePlugin.plugin = 'not-a-plugin'
      expect(isGecoPluginType(modifiablePlugin)).to.be.false
    })
  })
})
