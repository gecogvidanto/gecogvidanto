/*
 * This file is part of @gecogvidanto/plugin.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import {
  EconomicSystemPlugin,
  GecoPlugin,
  GecoPluginType,
  createGecoPlugin,
  isEconomicSystemPlugin,
} from '.'
import { EconomicSystem } from '../economicsystem'
// eslint-disable-next-line import/no-internal-modules
import { FakeEconomicSystem } from '../economicsystem/EconomicSystem.spec'
import { ServerApp } from '../server'

/* eslint-disable import/no-internal-modules */

class FakeEconomicSystemPlugin implements EconomicSystemPlugin {
  public readonly models = {}

  public constructor(_serverApp: ServerApp) {
    // Don't do anything
  }

  public ready(): Promise<void> {
    return Promise.resolve()
  }

  public openEconomicSystems(): ReadonlyArray<EconomicSystem<any>> {
    return [new FakeEconomicSystem()]
  }
}

class FakeSimplePlugin implements GecoPlugin {
  public readonly models = {}

  public constructor() {
    // Don't do anything
  }

  public ready(): Promise<void> {
    return Promise.resolve()
  }
}

describe('EconomicSystemPlugin', () => {
  describe('#isEconomicSystemPlugin', () => {
    it('must detect economic system plugin for plugin with appropriate method', () => {
      const fakePluginType: GecoPluginType = createGecoPlugin(FakeEconomicSystemPlugin, {
        plugin: 'gecogvidanto-plugin-v1',
        description: 'Fake system plugin',
        messages: {},
        uiMessages: {},
        models: {},
      })

      expect(isEconomicSystemPlugin(new fakePluginType(undefined as any))).to.be.true
    })

    it('must not detect database plugin for plugin if no database method', () => {
      const fakePluginType: GecoPluginType = createGecoPlugin(FakeSimplePlugin, {
        plugin: 'gecogvidanto-plugin-v1',
        description: 'Fake database plugin',
        messages: {},
        uiMessages: {},
        models: {},
      })

      expect(isEconomicSystemPlugin(new fakePluginType(undefined as any))).to.be.false
    })
  })
})
