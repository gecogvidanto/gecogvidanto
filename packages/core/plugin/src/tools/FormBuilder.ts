/*
 * This file is part of @gecogvidanto/plugin.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {
  Aspect,
  DynFunction,
  Field,
  FieldGroup,
  FieldOptions,
  Form,
  FormPart,
  HelpSheet,
  LocalizedOption,
  PlaceContent,
  PureMessageKey,
  ServerField,
  UnassembledMessages,
  UnlockedArray,
  langType,
} from '@gecogvidanto/shared'

/**
 * Builder of a collection of selector choices.
 */
export class ChoiceCollector<T extends langType> {
  public readonly choices: LocalizedOption<T, keyof T>[] = []

  /**
   * Add a choice to the selector.
   *
   * @param choice - The choice to add.
   * @returns `this`.
   */
  public addChoice<K extends keyof T>(choice: LocalizedOption<T, K>): this {
    this.checkId(choice.id)
    this.choices.push(choice)
    return this
  }

  /**
   * Check choice identifier unicity. Throws an exception if identifier is not unique.
   *
   * @param id - The identifier to check.
   */
  protected checkId(id: string): void {
    if (this.choices.map(choice => choice.id).includes(id)) {
      throw new Error(`Duplicated option id ${id}`)
    }
  }
}

/**
 * Builder of a collection of form fields.
 */
export abstract class FieldCollector<T extends langType> {
  /**
   * Add a text field to the collector.
   *
   * @param name - The name of the field.
   * @param content - The initial content of the field.
   * @param (unnamed) - An object containing optional data.
   * @param (unnamed).options - The initial options of the field.
   * @param (unnamed).hasData - Indicate if the text field has data.
   * @returns `this`.
   */
  public addTextField<K extends keyof T & string>(
    name: string,
    content: UnassembledMessages<T>[K],
    { options, hasData = false }: { options?: FieldOptions; hasData?: boolean } = {}
  ): this {
    this.checkName(name)
    this.collect({
      type: 'text',
      name,
      content,
      options: options || {},
      hasData,
    })
    return this
  }

  /**
   * Add a text input field to the collector.
   *
   * @param name - The name of the field.
   * @param help - A help string that UI may display as label or placeholder.
   * @param content - The initial content of the field.
   * @param (unnamed) - An object containing optional fields.
   * @param (unnamed).options - The initial options of the field.
   * @param (unnamed).minLength - The minimal lenght or undefined if no minimum.
   * @param (unnamed).maxLength - The maximal lenght or undefined if no maximum.
   * @param (unnamed).hasData - Indicate if the text field has data.
   * @returns `this`.
   */
  public addTextInputField<K extends keyof T & string>(
    name: string,
    help: UnassembledMessages<T>[K],
    content: string,
    {
      options,
      minLength,
      maxLength,
      hasData = true,
    }: {
      options?: FieldOptions
      minLength?: number
      maxLength?: number
      hasData?: boolean
    } = {}
  ): this {
    this.checkName(name)
    this.collect({
      type: 'text-input',
      name,
      help,
      content,
      options: options || {},
      minLength,
      maxLength,
      hasData,
    })
    return this
  }

  /**
   * Add a number input field to the collector.
   *
   * @param name - The name of the field.
   * @param help - A help string that UI may display as label or placeholder.
   * @param content - The initial content of the field.
   * @param (unnamed) - An object containing the following optional fields.
   * @param (unnamed).options - The initial options of the field.
   * @param (unnamed).minValue - The minimal value of the field or undefined if no minimum.
   * @param (unnamed).maxValue - The maximal value of the field or undefined if no maximum.
   * @param (unnamed).incrementStep - The increment step if UI is displaying increment buttons.
   * @param (unnamed).hasData - Indicate if the text field has data.
   * @returns `this`.
   */
  public addNumberInputField<K extends keyof T & string>(
    name: string,
    help: UnassembledMessages<T>[K],
    content: number,
    {
      options,
      minValue,
      maxValue,
      incrementStep,
      hasData = true,
    }: {
      options?: FieldOptions
      minValue?: number
      maxValue?: number
      incrementStep?: number
      hasData?: boolean
    } = {}
  ): this {
    this.checkName(name)
    this.collect({
      type: 'number-input',
      name,
      help,
      content,
      options: options || {},
      minValue,
      maxValue,
      incrementStep,
      hasData,
    })
    return this
  }

  /**
   * Add a boolean input field to the collector.
   *
   * @param name - The name of the field.
   * @param help - A help string that UI may display as label or placeholder.
   * @param content - The initial content of the field.
   * @param (unnamed) - An object containing optional fields.
   * @param (unnamed).options - The initial options of the field.
   * @param (unnamed).hasData - Indicate if the text field has data.
   * @returns `this`.
   */
  public addBooleanInputField<K extends keyof T & string>(
    name: string,
    help: UnassembledMessages<T>[K],
    content: boolean,
    { options, hasData = true }: { options?: FieldOptions; hasData?: boolean } = {}
  ): this {
    this.checkName(name)
    this.collect({
      type: 'boolean-input',
      name,
      help,
      content,
      options: options || {},
      hasData,
    })
    return this
  }

  /**
   * Add a select input field to the collector.
   *
   * @param name - The name of the field.
   * @param help - A help string that UI may display as label or placeholder.
   * @param content - The initial content of the field.
   * @param choicesBuilder - Builder for the choices which can be selected.
   * @param (unnamed) - An object containing optional fields.
   * @param (unnamed).options - The initial options of the field.
   * @param (unnamed).hasData - Indicate if the text field has data.
   * @returns `this`.
   */
  public addSelectInputField<K extends keyof T & string>(
    name: string,
    help: UnassembledMessages<T>[K],
    content: string,
    choicesBuilder: (collector: ChoiceCollector<T>) => void,
    { options, hasData = true }: { options?: FieldOptions; hasData?: boolean } = {}
  ): this {
    this.checkName(name)
    const collector: ChoiceCollector<T> = new ChoiceCollector()
    choicesBuilder(collector)
    this.collect({
      type: 'select-input',
      name,
      help,
      content,
      choices: collector.choices,
      options: options || {},
      hasData,
    })
    return this
  }

  /**
   * Add the given field to the collector.
   *
   * @param field - The field to collect.
   */
  protected abstract collect(field: Field<T>): void

  /**
   * Check that the field name is unique in its form part. Must throw an exception if not.
   *
   * @param name - The name of the field.
   */
  protected abstract checkName(name: string): void
}

/**
 * Field collector used internally in builder.
 */
class InternalFieldCollector<T extends langType> extends FieldCollector<T> {
  /**
   * The collected fields.
   */
  public readonly fields: Field<T>[] = []

  /**
   * Create the collector.
   *
   * @param checkName - The method used to check field names.
   */
  public constructor(protected readonly checkName: (name: string) => void) {
    super()
  }

  /**
   * Collect the field.
   *
   * @param field - The field to collect.
   */
  protected collect(field: Field<T>): void {
    this.fields.push(field)
  }
}

/**
 * A builder for the forms. The builder check field name unicity. @see {@link #checkName()} for more
 * information.
 */
export default class FormBuilder<T extends langType, P = Record<string, unknown>> extends FieldCollector<
  T
> {
  /**
   * The dynamic function. This function **should** be compiled into appropriate ECMAScript level for the
   * client.
   */
  public dynFunction?: DynFunction<P>

  /**
   * The parameters to be used by the function.
   */
  public params?: P

  /**
   * The seen field names.
   */
  private readonly fieldNames: Map<string, Set<string>> = new Map()

  /**
   * The group currently being built.
   */
  private currentGroup: UnlockedArray<FieldGroup<T>> = {
    mandatory: false,
    fields: [],
  }

  /**
   * The part currently being built.
   */
  private currentPart: UnlockedArray<FormPart<T>> = {
    player: -Infinity,
    groups: [this.currentGroup],
  }

  /**
   * Parts of the form.
   */
  private readonly parts: Array<UnlockedArray<FormPart<T>>> = [this.currentPart]

  /**
   * Indicate that next fields will be inserted in given player part.
   *
   * @param player - The player identifier (-Infinity for all players).
   * @returns `this`.
   */
  public toPart(player: number): this {
    let newPart = this.parts.find(part => part.player === player)
    if (!newPart) {
      newPart = { player, groups: [{ mandatory: false, fields: [] }] }
      this.parts.push(newPart)
    }
    this.currentPart = newPart
    const { groups } = this.currentPart
    this.currentGroup = groups[groups.length - 1] as UnlockedArray<FieldGroup<T>>
    return this
  }

  /**
   * Create the group.
   *
   * @param mandatory - True if group is mandatory.
   * @param titleBuilder - A builder callback used to create a title.
   * @returns `this`.
   */
  public newGroup(mandatory: boolean, titleBuilder?: (collector: FieldCollector<T>) => void): this {
    const title: Field<T>[] = this.internallyCollect(titleBuilder)
    if (title.length > 1) {
      throw new Error('Only one field can be set as title group')
    } else if (title.length > 0) {
      this.currentGroup = {
        title: title[0],
        mandatory,
        fields: [],
      }
    } else {
      this.currentGroup = { mandatory, fields: [] }
    }
    this.currentPart.groups.push(this.currentGroup)
    return this
  }

  /**
   * Add strongly linked fields.
   *
   * @param fieldsBuilder - A callback used to build the fields.
   * @returns `this`.
   */
  public addLinkedFields(fieldsBuilder: (collector: FieldCollector<T>) => void): this {
    const fields: Field<T>[] = this.internallyCollect(fieldsBuilder)
    this.currentGroup.fields.push(fields)
    return this
  }

  /**
   * Add the values fields for the dying player.
   *
   * @param helpSheet - The help sheet for the fields aspect.
   * @returns `this`.
   */
  public addDyingValues(helpSheet: HelpSheet): this {
    return this.addDying(
      {
        title: '$shared$formValues',
        low: '$shared$formValuesLow',
        medium: '$shared$formValuesMedium',
        high: '$shared$formValuesHigh',
      },
      {
        title: ServerField.DyingValues,
        low: ServerField.LowValues,
        medium: ServerField.MediumValues,
        high: ServerField.HighValues,
      },
      helpSheet
    )
  }

  /**
   * Add the money fields for the dying player.
   *
   * @param helpSheet - The help sheet for the fields aspect.
   * @returns `this`.
   */
  public addDyingMoney(helpSheet: HelpSheet): this {
    return this.addDying(
      {
        title: '$shared$formMoney',
        low: '$shared$formMoneyLow',
        medium: '$shared$formMoneyMedium',
        high: '$shared$formMoneyHigh',
      },
      {
        title: ServerField.DyingMoney,
        low: ServerField.LowMoney,
        medium: ServerField.MediumMoney,
        high: ServerField.HighMoney,
      },
      helpSheet
    )
  }

  /**
   * Builds the form using prepared data.
   *
   * @returns The created form.
   */
  public build(): Form<T, P> {
    return {
      parts: this.parts
        .sort((a, b) => a.player - b.player)
        .map(part => ({
          ...part,
          groups: part.groups.filter(group => group.title || group.fields.length > 0),
        }))
        .filter(part => part.groups.length > 0),
      dynFunction: this.dynFunction,
      params: this.params || ({} as any),
    }
  }

  protected collect(field: Field<T>): void {
    this.currentGroup.fields.push(field)
  }

  /**
   * Check field name unicity accross part. Throws an exception if name is not unique.
   *
   * @param name - The name to check.
   */
  protected checkName(name: string): void {
    const partIdentifier: string =
      this.currentPart.player === -Infinity ? 'global' : String(this.currentPart.player)
    let fieldNames: Set<string> | undefined = this.fieldNames.get(partIdentifier)
    if (!fieldNames) {
      fieldNames = new Set()
      this.fieldNames.set(partIdentifier, fieldNames)
    }
    if (fieldNames.has(name)) {
      throw new Error(`Field name ${name} created multiple times for player ${partIdentifier}`)
    }
    fieldNames.add(name)
  }

  /**
   * Create a dying player result fields.
   *
   * @param label - The label to use.
   * @param label.title - The title label.
   * @param label.low - The low field label,.
   * @param label.medium - The medium field label,.
   * @param label.high - The high field label,.
   * @param fieldId - The field identifier to use.
   * @param fieldId.title - The title field identifier.
   * @param fieldId.low - The low field identifier.
   * @param fieldId.medium - The medium field identifier.
   * @param fieldId.high - The high field identifier.
   * @param helpSheet - The help sheet used to display colors.
   * @returns `this`.
   */
  private addDying(
    label: {
      title: PureMessageKey<langType>
      low: PureMessageKey<langType>
      medium: PureMessageKey<langType>
      high: PureMessageKey<langType>
    },
    fieldId: { title: string; low: string; medium: string; high: string },
    helpSheet: HelpSheet
  ): this {
    this.addTextField(fieldId.title, label.title as any).addLinkedFields(collector =>
      collector
        .addNumberInputField(fieldId.low, label.low as any, 0, {
          options: this.createOptions(helpSheet.low),
          minValue: 0,
        })
        .addNumberInputField(fieldId.medium, label.medium as any, 0, {
          options: this.createOptions(helpSheet.medium),
          minValue: 0,
        })
        .addNumberInputField(fieldId.high, label.high as any, 0, {
          options: this.createOptions(helpSheet.high),
          minValue: 0,
        })
    )
    return this
  }

  /**
   * Convert a place content into field options.
   *
   * @param content - The place content.
   * @returns `this`.
   */
  private createOptions(content: PlaceContent): FieldOptions {
    switch (content) {
      case PlaceContent.Empty:
        return { invisible: true }
      case PlaceContent.Red:
        return { aspect: Aspect.Red }
      case PlaceContent.Yellow:
        return { aspect: Aspect.Yellow }
      case PlaceContent.Green:
        return { aspect: Aspect.Green }
      case PlaceContent.Blue:
        return { aspect: Aspect.Blue }
      default:
        // eslint-disable-next-line no-case-declarations
        const contentValue: never = content
        throw new Error(`Unknown content value ${contentValue}`)
    }
  }

  /**
   * Internally collect fields.
   *
   * @param builder - The callback building the fields to collect.
   * @returns Collected fields.
   */
  private internallyCollect(builder?: (collector: FieldCollector<T>) => void): Field<T>[] {
    const collector: InternalFieldCollector<T> = new InternalFieldCollector(name => this.checkName(name))
    builder && builder(collector)
    return collector.fields
  }
}
