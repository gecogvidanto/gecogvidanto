/*
 * This file is part of @gecogvidanto/plugin.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { expect } from 'chai'

import { FormData } from '.'

const RESULT = {
  badValue: 'unused',
  badValueWith$: 'unused',
  forUser$0$: 'user 0',
  forUser$3$: 'user 3',
  forUser$1$: 'user 1',
  globalField$$: 'global',
  otherForUser$1$: 'other 1',
}

describe('FormData', () => {
  const data = new FormData(RESULT)

  it('must not have the bad values', () => {
    expect(data.getValuesFor(-Infinity), 'Bad value for global fields').to.not.have.any.keys(
      'badValue',
      'badValueWith$'
    )
    for (let id = 0; id < 6; id++) {
      expect(data.getValuesFor(id), `Bad value for player ${id}`).to.not.have.any.keys(
        'badValue',
        'badValueWith$'
      )
    }
  })

  it('must give appropriate player identifiers', () => {
    expect(data.playerIds).to.deep.equal([-Infinity, 0, 1, 3])
  })

  it('must have the appropriate key and values', () => {
    ;[0, 1, 3].forEach(id => {
      expect(data.getValuesFor(id)).to.have.any.keys('forUser')
      expect(data.getValuesFor(id).get('forUser')).to.equal(`user ${id}`)
    })
    expect(data.getValuesFor(1)).to.have.any.keys('otherForUser')
    expect(data.getValuesFor(1).get('otherForUser')).to.equal('other 1')
  })

  it('must not have the keys for other users', () => {
    expect(data.getValuesFor(-Infinity)).to.not.have.key('forUser')
    expect(data.getValuesFor(2)).to.not.have.key('forUser')
    expect(data.getValuesFor(3)).to.not.have.key('otherForUser')
    expect(data.getValuesFor(4)).to.not.have.key('forUser')
  })
})
