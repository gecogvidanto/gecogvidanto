/*
 * This file is part of @gecogvidanto/plugin.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import {
  Field,
  HelpSheet,
  PlaceContent,
  PureMessageKey,
  messages as sharedMessages,
} from '@gecogvidanto/shared'

import { FormBuilder } from '.'

const messages = {
  ...sharedMessages,
  fieldContent: 'content',
  otherContent: 'other',
  inputContent: 'input',
  enterText: 'enter text',
  enterNumber: 'enter number',
  aTrueString: 'this is true',
  selectMe: 'select me',
  pattern: (dummy: string) => `Mi${dummy}estas`,
  groupTitle: 'The group title',
}
type langType = typeof messages
const FIELD_NAME = 'field'
const SIMPLE_FIELD: [string, PureMessageKey<langType>] = [FIELD_NAME, 'fieldContent']

describe('FormBuilder', () => {
  it('must create needed parts once and only once', () => {
    const builder = new FormBuilder<langType>()
    builder.addTextField(...SIMPLE_FIELD)
    builder.toPart(2).addTextField(...SIMPLE_FIELD)
    builder.toPart(5).addTextField(...SIMPLE_FIELD)
    builder.toPart(2).addTextField('field2', 'otherContent', { options: { disabled: true } })
    const form = builder.build()
    expect(form.parts).to.have.lengthOf(3)
    expect(form.parts[0].player).to.equal(-Infinity)
    expect(form.parts[0].groups, 'Incorrect group count for all players').to.have.lengthOf(1)
    expect(form.parts[1].player).to.equal(2)
    expect(form.parts[1].groups, 'Incorrect group count for player 2').to.have.lengthOf(1)
    expect(form.parts[2].player).to.equal(5)
    expect(form.parts[2].groups, 'Incorrect group count for player 5').to.have.lengthOf(1)
  })

  it('must filter out empty groups or parts', () => {
    const builder = new FormBuilder<langType>()
    builder
      .toPart(1)
      .toPart(2)
      .toPart(3)
      .newGroup(false)
      .newGroup(true)
      .newGroup(false, collector => collector.addTextField(...SIMPLE_FIELD))
      .newGroup(true)
      .addTextField('field2', 'otherContent', { options: { hidden: true } })
    const form = builder.build()
    expect(form.parts).to.have.lengthOf(1)
    expect(form.parts[0].player).to.equal(3)
    expect(form.parts[0].groups, 'Incorrect group count').to.have.lengthOf(2)
  })

  it('must fail if twice same field name', () => {
    const builder = new FormBuilder<langType>()
    builder.newGroup(false, collector => collector.addTextField(...SIMPLE_FIELD))
    expect(() => builder.addTextField(...SIMPLE_FIELD)).to.throw(
      /field name .* created multiple times for/i
    )
  })

  it('must fail if twice same option choice', () => {
    const builder = new FormBuilder<langType>()
    expect(() =>
      builder.addSelectInputField('selectField', 'selectMe', 'me', collector =>
        collector
          .addChoice({
            id: 'me',
            description: { template: 'pattern', parameters: ['!'] },
          })
          .addChoice({ id: 'me', description: 'fieldContent' })
      )
    ).to.throw(/Duplicated option id/i)
  })

  it('must return parts ordered', () => {
    const builder = new FormBuilder<langType>()
    const modifiableParts: any[] = (builder as any).parts
    modifiableParts.pop()
    builder.toPart(8).addTextField(...SIMPLE_FIELD)
    builder.toPart(3).addTextField(...SIMPLE_FIELD)
    builder.toPart(-Infinity).addTextField(...SIMPLE_FIELD)
    builder.toPart(6).addTextField(...SIMPLE_FIELD)
    const form = builder.build()
    expect(form.parts).to.have.lengthOf(4)
    expect(form.parts[0].player).to.equal(-Infinity)
    expect(form.parts[1].player).to.equal(3)
    expect(form.parts[2].player).to.equal(6)
    expect(form.parts[3].player).to.equal(8)
  })

  it('must fail if many field in group title', () => {
    const builder = new FormBuilder<langType>()
    expect(() =>
      builder.newGroup(true, collector =>
        collector.addTextField(...SIMPLE_FIELD).addNumberInputField('numInput', 'inputContent', 0)
      )
    ).to.throw(/only one field/i)
  })

  it('must create linked fields', () => {
    const builder = new FormBuilder<langType>()
    builder.addLinkedFields(collector =>
      collector.addTextField(...SIMPLE_FIELD).addTextField('field2', 'otherContent')
    )
    const form = builder.build()
    const fields = form.parts[0].groups[0].fields
    expect(fields).to.have.lengthOf(1)
    expect(fields[0]).to.be.an('array')
    expect(fields[0]).to.have.lengthOf(2)
  })

  it('must render update function', () => {
    const builder = new FormBuilder<langType>()
    builder.dynFunction = () => {
      return () => {
        return
      }
    }
    const form = builder.build()
    expect(form.dynFunction).to.exist.and.to.be.a('function')
  })

  it('must create a field of each kind', () => {
    const builder = new FormBuilder<langType>()
    builder
      .addTextField('text', 'fieldContent')
      .addTextInputField('textInput', 'enterText', 'empty')
      .addNumberInputField('numberInput', 'enterNumber', 0)
      .addBooleanInputField('booleanInput', 'aTrueString', false)
      .addSelectInputField('selectField', 'selectMe', 'me', collector =>
        collector.addChoice({
          id: 'me',
          description: { template: 'pattern', parameters: ['!'] },
        })
      )
    const form = builder.build()
    expect(form.parts).to.have.lengthOf(1)
    const fields = form.parts[0].groups[0].fields
    expect(fields).to.have.lengthOf(5)
    expect((fields[0] as Field<langType>).type).to.equal('text')
    expect((fields[0] as Field<langType>).hasData).to.be.false
    expect((fields[1] as Field<langType>).type).to.equal('text-input')
    expect((fields[1] as Field<langType>).hasData).to.be.true
    expect((fields[2] as Field<langType>).type).to.equal('number-input')
    expect((fields[2] as Field<langType>).hasData).to.be.true
    expect((fields[3] as Field<langType>).type).to.equal('boolean-input')
    expect((fields[3] as Field<langType>).hasData).to.be.true
    expect((fields[4] as Field<langType>).type).to.equal('select-input')
    expect((fields[4] as Field<langType>).hasData).to.be.true
  })

  it('must create all fields for a dying player', () => {
    const builder = new FormBuilder<langType>()
    const helpSheetValues: HelpSheet = {
      low: PlaceContent.Red,
      medium: PlaceContent.Yellow,
      high: PlaceContent.Green,
      waiting: PlaceContent.Empty,
    }
    const helpSheetMoney: HelpSheet = {
      low: PlaceContent.Blue,
      medium: PlaceContent.Empty,
      high: PlaceContent.Red,
      waiting: PlaceContent.Yellow,
    }
    builder
      .toPart(1)
      .newGroup(true, collector => collector.addTextField('grouptitle', 'groupTitle'))
      .addDyingValues(helpSheetValues)
      .addDyingMoney(helpSheetMoney)
    const form = builder.build()
    const fields = form.parts[0].groups[0].fields
    expect(fields).to.have.lengthOf(4)
    expect((fields[0] as Field<langType>).type).to.equal('text')
    expect(fields[1]).to.be.an('array').and.to.have.lengthOf(3)
    expect((fields[2] as Field<langType>).type).to.equal('text')
    expect(fields[3]).to.be.an('array').and.to.have.lengthOf(3)
  })

  it('must throw error for unknown place content', () => {
    const builder = new FormBuilder<langType>()
    const helpSheet: HelpSheet = {
      low: undefined as any,
      medium: PlaceContent.Yellow,
      high: PlaceContent.Green,
      waiting: PlaceContent.Empty,
    }
    expect(() => builder.addDyingValues(helpSheet)).to.throw(/unknown/i)
  })
})
