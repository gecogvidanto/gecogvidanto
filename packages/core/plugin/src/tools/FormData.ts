/*
 * This file is part of @gecogvidanto/plugin.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { FormFieldIdentifier } from '@gecogvidanto/shared'

/**
 * Analyze the result of the form execution to provide easy access to form data.
 */
export default class FormData {
  /**
   * The parts of the form.
   */
  private readonly parts: Map<number, Map<string, string>> = new Map()

  /**
   * Create the analyzer with the data.
   *
   * @param data - The raw data.
   */
  public constructor(data: { [key: string]: string }) {
    for (const key in data) {
      const identifier = new FormFieldIdentifier(key)
      if (identifier.valid) {
        let fieldPart: Map<string, string> | undefined = this.parts.get(identifier.player)
        if (!fieldPart) {
          fieldPart = new Map()
          this.parts.set(identifier.player, fieldPart)
        }
        fieldPart.set(identifier.name, data[key])
      }
    }
  }

  /**
   * @returns The (sorted) identifiers of the players having data.
   */
  public get playerIds(): ReadonlyArray<number> {
    return Array.from(this.parts.keys()).sort((a, b) => a - b)
  }

  /**
   * Get the values for the given player. It consists of a map of key and values pairs, which may be empty.
   *
   * @param playerId - The player identifier, which may be -Infinity for global values.
   * @returns The values for the players.
   */
  public getValuesFor(playerId: number): Map<string, string> {
    return this.parts.get(playerId) || new Map()
  }
}
