/*
 * This file is part of @gecogvidanto/plugin.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Game, HelpSheet } from '@gecogvidanto/shared'

import ServerCharacter from './ServerCharacter'
import ServerSet from './ServerSet'

/**
 * A game coming from server.
 */
export default interface ServerGame extends Game {
  /**
   * The current running set.
   */
  readonly currentSet: ServerSet

  /**
   * The absolute current round for the whole game (including previous set rounds).
   */
  readonly absoluteCurrentRound: number

  /**
   * The players still on game (removing players who left the game).
   */
  readonly onGamePlayers: ReadonlyArray<number>

  /**
   * Players which are either dying, leaving or at end of set, so for which results will be needed.
   */
  readonly terminatingPlayers: ReadonlyArray<number>

  /**
   * The sets of the game. A fully played game should have at least 2 sets.
   */
  readonly roundSets: ReadonlyArray<ServerSet>

  /**
   * The characters in the game. Each set, a player is playing a character from birth to death, but
   * death/rebirth will likely be in the middle of the set. There may also be non player characters.
   */
  readonly characters: ReadonlyArray<ServerCharacter>

  /**
   * Add a non player character to the set.
   *
   * @returns The identifier of the NPC (always negative).
   */
  addNonPlayerCharacter(): number

  /**
   * Get the character for the given player in the current set.
   *
   * @param player - The player identifier, may be negative for a non-player character.
   */
  getCharacterFor(player: number): ServerCharacter

  /**
   * Build the help sheet for the values.
   */
  buildValuesHelpSheet(): HelpSheet
}
