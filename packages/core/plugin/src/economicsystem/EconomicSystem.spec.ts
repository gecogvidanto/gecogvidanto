/*
 * This file is part of @gecogvidanto/plugin.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { expect } from 'chai'
import Intl, { LanguageMap } from 'intl-ts'

import {
  HelpSheet,
  LocalizedOption,
  PlaceContent,
  UnassembledMessages,
  messages as sharedMessages,
} from '@gecogvidanto/shared'

import { EconomicSystem } from '.'
import { FormBuilder, FormData } from '../tools'

const messages = {
  ...sharedMessages,
  name: 'fake',
  npc: 'noone',
  option: 'option',
}
type langType = typeof messages

export class FakeEconomicSystem extends EconomicSystem<langType> {
  public constructor(value?: number) {
    super('fake', 'name', value)
  }
  public getNonPlayerCharacterName(): Promise<UnassembledMessages<langType>[keyof langType]> {
    return Promise.resolve(this.unassembledMessage('npc'))
  }
  public getMoneyHelpSheet(): Promise<HelpSheet> {
    return Promise.resolve({
      low: PlaceContent.Empty,
      medium: PlaceContent.Empty,
      high: PlaceContent.Empty,
      waiting: PlaceContent.Empty,
    })
  }
  public getOptions(): Promise<ReadonlyArray<LocalizedOption<langType, keyof langType>>> {
    return Promise.resolve([this.localizedOption({ id: 'option', description: 'option' })])
  }
  public getForm(builder: FormBuilder<langType>): Promise<FormBuilder<langType>> {
    return Promise.resolve(builder)
  }
  public execForm(data: FormData): Promise<FormData> {
    return Promise.resolve(data)
  }
  public terminateRound(): Promise<void> {
    return Promise.resolve()
  }
}

describe('EconomicSystem', () => {
  const lang = new Intl(new LanguageMap(messages, 'en'))

  it('must be created with appropriate given values', () => {
    const fake = new FakeEconomicSystem(6)
    expect(fake.id).to.equal('fake')
    expect((lang[fake.name] as any)()).to.equal('fake')
    expect(fake.valueCost).to.equal(6)
  })

  it('must be created with appropriate default values', () => {
    const fake = new FakeEconomicSystem()
    expect(fake.id).to.equal('fake')
    expect((lang[fake.name] as any)()).to.equal('fake')
    expect(fake.valueCost).to.equal(1)
  })

  it('must create unassembled message', async () => {
    const fake = new FakeEconomicSystem()
    await expect(fake.getNonPlayerCharacterName()).to.eventually.equal('npc')
  })

  it('must create localized option', async () => {
    const fake = new FakeEconomicSystem()
    await expect(fake.getOptions()).to.eventually.have.lengthOf(1)
  })
})
