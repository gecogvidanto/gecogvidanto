/*
 * This file is part of @gecogvidanto/plugin.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Character, Score, Unlocked } from '@gecogvidanto/shared'

type UnlockedScore = {
  [K in keyof Score]: K extends 'values' | 'money' ? Unlocked<Score[K]> : Score[K]
}

/**
 * A character coming from server.
 */
export default interface ServerCharacter extends Character {
  /**
   * The score for the current character at the current round. This will create an empty score if it does
   * not exist. Only the current score may be modifiable.
   *
   * Plugins should only modify NPC scores, as player scores are managed by the server.
   */
  readonly currentScore: UnlockedScore

  /**
   * Total score may be modified by plugins. During the set, total score is the count of low money, so take
   * care of the {@link EconomicSystem#valueCost}.
   */
  totalScore: number
}
