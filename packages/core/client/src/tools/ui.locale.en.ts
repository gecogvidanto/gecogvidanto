/*
 * This file is part of @gecogvidanto/client.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { createMessages } from 'intl-ts'

import { messages as sharedMessages } from '@gecogvidanto/shared'

/**
 * The messages for the shared library.
 */
export const messages = createMessages({
  ...sharedMessages,
  $client$connection: 'Failed to connect to ĞecoĞvidanto server',
  $client$credentials: 'Login failed: invalid credentials',
  $client$economicSystem: 'Economic system not found in the server',
  $client$emailCheck: 'No matching e-mail token found — Please restart the procedure',
  $client$gameClose: 'Cannot close game: not enough or unfinished sets',
  $client$gameNextRound: 'Current set is already finished',
  $client$gamePlayers: 'Too many players or multiple players with the same name',
  $client$gameSets: 'There is already an unfinished set in the game',
  $client$gameTechBreak: 'There is no set in progress for a technological break',
  $client$userExists: 'Another user is already registered with the same name or e-mail',
})

/**
 * The type for the messages.
 */
export type langType = typeof messages
