/*
 * This file is part of @gecogvidanto/client.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import axios, { AxiosPromise, AxiosRequestConfig } from 'axios'
import { compile } from 'path-to-regexp'
import * as qs from 'qs'

import { ApiDefinition, ApiMethod, apiRoutes } from '@gecogvidanto/shared'

// Transform API routes into functions
// eslint-disable-next-line @typescript-eslint/ban-types
type HttpClientRouteFunctionParameter<P> = P extends undefined ? {} : P

type HttpClientRouteFunction<U, D, R> = D extends undefined
  ? U extends undefined
    ? () => AxiosPromise<R>
    : (url: U) => AxiosPromise<R>
  : (url: HttpClientRouteFunctionParameter<U>, data: D) => AxiosPromise<R>

type HttpClientRoutes = {
  [P in keyof ApiDefinition]: HttpClientRouteFunction<
    ApiDefinition[P]['url'],
    ApiDefinition[P]['data'],
    ApiDefinition[P]['return']
  >
}

/**
 * The HTTP calls which can be made to the server.
 */
export const httpClient: Readonly<HttpClientRoutes> = Object.keys(apiRoutes).reduce((wrapper, route) => {
  const routeName = route as keyof ApiDefinition
  const fn = (url?: any, data?: any): AxiosPromise<any> => {
    const config: AxiosRequestConfig = {}
    config.method = apiRoutes[routeName].method
    config.url = compile(apiRoutes[routeName].fullUrl)(url || {})
    if (data) {
      switch (apiRoutes[routeName].method) {
        case ApiMethod.Post:
        case ApiMethod.Put:
        case ApiMethod.Patch:
          config.data = data
          break
        default:
          config.params = data
          config.paramsSerializer = params => qs.stringify(params)
      }
    }
    const csrfToken: string =
      document.querySelector('meta[name="csrf-token"]')?.getAttribute('content') || ''
    csrfToken && (config.headers = { 'CSRF-Token': csrfToken })
    return axios(config)
  }
  wrapper[routeName] = fn as any
  return wrapper
}, {} as HttpClientRoutes)
