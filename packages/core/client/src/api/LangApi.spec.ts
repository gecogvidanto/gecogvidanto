/*
 * This file is part of @gecogvidanto/client.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback */
import { expect } from 'chai'
import * as moxios from 'moxios'

import { apiRoutes } from '@gecogvidanto/shared'

import { LangApi } from '..'
import { GLOBAL_ERROR_MSG, QueryData, Request, executeQuery, extractError } from './common.spec'

describe('LangApi', function () {
  this.slow(400)
  let langApi: LangApi

  beforeEach('Initialize API and Moxios', () => {
    moxios.install()
    langApi = new LangApi()
  })

  afterEach('Clean up', () => {
    moxios.uninstall()
  })

  describe('#select', () => {
    let exec: Promise<void>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => langApi.select('eo'))
      request = await executeQuery(apiRoutes.lang)
    })

    it('must modify default language on success', async () => {
      expect(request.queryData.url.langCode, 'Unexpected lang code').to.equal('eo')
      await request.respondWith({ status: 200 })
      await expect(exec).to.be.fulfilled
    })

    it('must modify default language and send notification on failure', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })
})
