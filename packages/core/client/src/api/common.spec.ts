/*
 * This file is part of @gecogvidanto/client.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import Intl, { LanguageMap } from 'intl-ts'
import * as moxios from 'moxios'
import { Key, pathToRegexp } from 'path-to-regexp'

import { ApiMethod } from '@gecogvidanto/shared'

import { ApiError, langType, messages } from '..'

export const GLOBAL_ERROR_MSG = /failed to connect/i

const lang: Intl<langType> = new Intl(new LanguageMap(messages, 'en'))

export function extractError<R>(method: () => Promise<R>): Promise<R> {
  const result: Promise<R> = new Promise<R>((resolve, reject) => {
    method()
      .then(value => resolve(value))
      .catch(error => {
        if (error instanceof ApiError) {
          reject(new Error(error.getMessage(lang)))
        } else {
          reject(error)
        }
      })
  })
  result.catch(ignore => ignore) // Prevent unhandled warning
  return result
}

export interface QueryData {
  queryData: {
    url: any
    query: any
    post: any
  }
}

export interface Item {
  response?: any
  status?: number
}

export interface Request {
  respondWith(res: Item): Promise<Record<string, unknown>>
}

export interface Tracker {
  reset(): void
  count(): number
  mostRecent(): Request
}

export async function executeQuery(params: {
  readonly fullUrl: string
  readonly method: ApiMethod
}): Promise<Request & QueryData> {
  await new Promise<void>(resolve => moxios.wait(resolve))
  const result = moxios.requests.mostRecent()
  const keys: Key[] = []
  const urlResult = pathToRegexp(params.fullUrl, keys).exec(result.config.url!)
  if (urlResult === null) {
    throw new Error(`Query URL: ${result.config.url} does not match expected ${params.fullUrl}`)
  }
  if (params.method !== result.config.method) {
    throw new Error(`Query method: ${result.config.method} is not expected ${params.method}`)
  }

  const fullResult: Request & QueryData = result as any
  fullResult.queryData = {} as any

  fullResult.queryData.url = keys.reduce((prev, key, index) => {
    prev[key.name] = decodeURIComponent(urlResult[index + 1])
    return prev
  }, {} as any)
  fullResult.queryData.query = result.config.params || {}
  fullResult.queryData.post = JSON.parse(result.config.data || '{}')

  return fullResult
}
