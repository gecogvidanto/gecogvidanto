/*
 * This file is part of @gecogvidanto/client.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import Intl from 'intl-ts'

import { PureMessageKey } from '@gecogvidanto/shared'

import { langType } from '../tools'

/**
 * An error in the API, given a message key.
 */
export default class ApiError<T extends langType = langType> extends Error {
  public constructor(private readonly messageKey: PureMessageKey<T>) {
    super()

    // Because of Error special behavior
    Object.setPrototypeOf(this, new.target.prototype)
  }

  public getMessage(lang: Intl<T>): string {
    return (lang as Intl<any>)[this.messageKey]()
  }
}

/**
 * @returns A new server connection error.
 */
export function buildConnectionError(): ApiError {
  return new ApiError('$client$connection')
}
