/*
 * This file is part of @gecogvidanto/client.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback */
import { expect } from 'chai'
import * as moxios from 'moxios'

import {
  Form,
  Game,
  HelpSheet,
  IdentifiedAddress,
  LocalizedOption,
  PlaceContent,
  UnassembledMessages,
  apiRoutes,
} from '@gecogvidanto/shared'

import { GameApi } from '..'
import { GLOBAL_ERROR_MSG, QueryData, Request, executeQuery, extractError } from './common.spec'

const messages = {
  option: 'local name',
}
type langType = typeof messages & { $: string }

const GAME_ID = 'gecoId'
const MASTER_ID = 'llzamenhof'
const ADDRESS: IdentifiedAddress = {
  value: 'Middle of the lake, Annecy, France',
  city: 'Annecy',
  countryCode: 'fr',
  latitude: 45.862252,
  longitude: 6.162632,
}
const LOCATION = { name: 'Location', address: ADDRESS }
const RPS = 10
const TLEN = 5
const SUMMARY = 'What a good game !'
const GAME: Game = {
  start: Date.now(),
  masterId: MASTER_ID,
  location: LOCATION,
  roundsPerSet: RPS,
  roundLength: TLEN,
  summary: SUMMARY,
  players: [],
  roundSets: [],
  characters: [],
  closed: false,
}
const HELP_SHEET: HelpSheet = {
  low: PlaceContent.Red,
  medium: PlaceContent.Yellow,
  high: PlaceContent.Green,
  waiting: PlaceContent.Empty,
}
const GAME_OPTION: LocalizedOption<langType, 'option'> = {
  id: 'option-id',
  description: 'option',
}
const GAME_FORM: Form<any> = {
  parts: [
    {
      player: -Infinity,
      groups: [],
    },
    {
      player: 0,
      groups: [],
    },
  ],
  params: {},
}

describe('GameApi', function () {
  this.slow(400)
  let gameApi: GameApi

  beforeEach('Initialize API and Moxios', () => {
    moxios.install()
    gameApi = new GameApi()
  })

  afterEach('Clean up', () => {
    moxios.uninstall()
  })

  describe('#create', () => {
    let exec: Promise<Game>
    let request: Request & QueryData

    describe('with location', () => {
      beforeEach('Call API', async () => {
        exec = extractError(() => gameApi.create(LOCATION, RPS, TLEN))
        request = await executeQuery(apiRoutes.gameCreate)
      })

      it('must create the game', async () => {
        expect(request.queryData.post).to.deep.equal({
          locationName: LOCATION.name,
          ...LOCATION.address,
          roundsPerSet: RPS,
          roundLength: TLEN,
        })
        await request.respondWith({ status: 200, response: GAME })
        await expect(exec).to.eventually.deep.equal(GAME)
      })

      it('must not create the game if server error', async () => {
        await request.respondWith({ status: 500 })
        await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
      })
    })

    describe('without location', () => {
      it('must create game with simple address', async () => {
        exec = extractError(() => gameApi.create({ address: 'Here' }, RPS, TLEN))
        request = await executeQuery(apiRoutes.gameCreate)
        expect(request.queryData.post).to.deep.equal({
          value: 'Here',
          roundsPerSet: RPS,
          roundLength: TLEN,
        })
        await request.respondWith({ status: 200, response: GAME })
        await expect(exec).to.eventually.deep.equal(GAME)
      })

      it('must update game without location', async () => {
        exec = extractError(() => gameApi.create({}, RPS, TLEN))
        request = await executeQuery(apiRoutes.gameCreate)
        expect(request.queryData.post).to.deep.equal({
          roundsPerSet: RPS,
          roundLength: TLEN,
        })
        await request.respondWith({ status: 200, response: GAME })
        await expect(exec).to.eventually.deep.equal(GAME)
      })
    })
  })

  describe('#update', () => {
    let exec: Promise<Game>
    let request: Request & QueryData

    describe('with full location', () => {
      beforeEach('Call API', async () => {
        exec = extractError(() => gameApi.update({ _id: GAME_ID, ...GAME, closed: true }))
        request = await executeQuery(apiRoutes.gameUpdate)
      })

      it('must close game', async () => {
        expect(request.queryData.url).to.deep.equal({ id: GAME_ID })
        expect(request.queryData.post).to.deep.equal({
          locationName: LOCATION.name,
          ...LOCATION.address,
          closed: true,
        })
        await request.respondWith({ status: 200, response: GAME })
        await expect(exec).to.eventually.deep.equal(GAME)
      })

      it('must display error if cannot close game', async () => {
        await request.respondWith({ status: 409 })
        await expect(exec).to.be.rejectedWith(/not enough or unfinished sets/i)
      })

      it('must not update game if server error', async () => {
        await request.respondWith({ status: 500 })
        await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
      })
    })

    describe('without location', () => {
      it('must update game with simple address', async () => {
        const game: Game = { ...GAME, location: { address: 'Here' } }
        exec = extractError(() =>
          gameApi.update({
            _id: GAME_ID,
            ...game,
            summary: undefined,
          })
        )
        request = await executeQuery(apiRoutes.gameUpdate)
        expect(request.queryData.url).to.deep.equal({ id: GAME_ID })
        expect(request.queryData.post).to.deep.equal({ value: 'Here' })
        await request.respondWith({ status: 200, response: GAME })
        await expect(exec).to.eventually.deep.equal(GAME)
      })

      it('must update game without location', async () => {
        const game: Game = { ...GAME, location: {} }
        exec = extractError(() =>
          gameApi.update({
            _id: GAME_ID,
            ...game,
            summary: undefined,
          })
        )
        request = await executeQuery(apiRoutes.gameUpdate)
        expect(request.queryData.url).to.deep.equal({ id: GAME_ID })
        expect(request.queryData.post).to.deep.equal({})
        await request.respondWith({ status: 200, response: GAME })
        await expect(exec).to.eventually.deep.equal(GAME)
      })
    })
  })

  describe('#search', () => {
    let exec: Promise<Game[]>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => gameApi.search())
      request = await executeQuery(apiRoutes.gameSearch)
    })

    it('must search all available games', async () => {
      await request.respondWith({ status: 200, response: [GAME] })
      await expect(exec).to.eventually.have.lengthOf(1)
    })

    it('must show error if cannot read games', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })

  describe('#read', () => {
    let exec: Promise<Game>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => gameApi.read(GAME_ID))
      request = await executeQuery(apiRoutes.gameRead)
    })

    it('must read requested game', async () => {
      expect(request.queryData.url).to.deep.equal({ id: GAME_ID })
      await request.respondWith({ status: 200, response: GAME })
      await expect(exec).to.eventually.deep.equal(GAME)
    })

    it('must not read the game if server error', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })

  describe('#addPlayer', () => {
    let exec: Promise<Game>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => gameApi.addPlayer({ _id: GAME_ID, ...GAME }, 'player-name'))
      request = await executeQuery(apiRoutes.gamePlayerAdd)
    })

    it('must add player to the game', async () => {
      expect(request.queryData.url).to.deep.equal({ id: GAME_ID })
      expect(request.queryData.post).to.deep.equal({ name: 'player-name' })
      await request.respondWith({ status: 200, response: GAME })
      await expect(exec).to.eventually.deep.equal(GAME)
    })

    it('must display error if problem', async () => {
      await request.respondWith({ status: 409 })
      await expect(exec).to.be.rejectedWith(/too many players or .* same name/i)
    })

    it('must not add player if server error', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })

  describe('#updatePlayer', () => {
    let exec: Promise<Game>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => gameApi.updatePlayer({ _id: GAME_ID, ...GAME }, 12, 'player-name'))
      request = await executeQuery(apiRoutes.gamePlayerUpdate)
    })

    it('must update player name', async () => {
      expect(request.queryData.url).to.deep.equal({
        id: GAME_ID,
        player: '12',
      })
      expect(request.queryData.post).to.deep.equal({ name: 'player-name' })
      await request.respondWith({ status: 200, response: GAME })
      await expect(exec).to.eventually.deep.equal(GAME)
    })

    it('must display error if problem', async () => {
      await request.respondWith({ status: 409 })
      await expect(exec).to.be.rejectedWith(/too many players or .* same name/i)
    })

    it('must not update player if server error', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })

  describe('#deletePlayer', () => {
    let exec: Promise<Game>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => gameApi.deletePlayer({ _id: GAME_ID, ...GAME }, 12))
      request = await executeQuery(apiRoutes.gamePlayerDelete)
    })

    it('must remove player from the game', async () => {
      expect(request.queryData.url).to.deep.equal({
        id: GAME_ID,
        player: '12',
      })
      await request.respondWith({ status: 200, response: GAME })
      await expect(exec).to.eventually.deep.equal(GAME)
    })

    it('must not remove player if server error', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })

  describe('#addSet', () => {
    let exec: Promise<Game>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => gameApi.addSet({ _id: GAME_ID, ...GAME }, 'libre'))
      request = await executeQuery(apiRoutes.gameSetAdd)
    })

    it('must add set to the game', async () => {
      expect(request.queryData.url).to.deep.equal({ id: GAME_ID })
      expect(request.queryData.post).to.deep.equal({ ecoSysId: 'libre' })
      await request.respondWith({ status: 200, response: GAME })
      await expect(exec).to.eventually.deep.equal(GAME)
    })

    it('must display error if unfinished set', async () => {
      await request.respondWith({ status: 409 })
      await expect(exec).to.be.rejectedWith(/there is already an unfinished set/i)
    })

    it('must not add set if server error', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })

  describe('#readNpcName', () => {
    let exec: Promise<UnassembledMessages<langType>[keyof langType]>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => gameApi.readNpcName<langType>({ _id: GAME_ID, ...GAME }, 12, -1))
      request = await executeQuery(apiRoutes.gameNpcName)
    })

    it('must read NPC name', async () => {
      expect(request.queryData.url).to.deep.equal({
        id: GAME_ID,
        roundSet: '12',
        character: '-1',
      })
      await request.respondWith({ status: 200, response: 'pikachu' })
      await expect(exec).to.eventually.equal('pikachu')
    })

    it('must not read NPC name if server error', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })

  describe('#readValuesHelpSheet', () => {
    let exec: Promise<HelpSheet>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => gameApi.readValuesHelpSheet({ _id: GAME_ID, ...GAME }))
      request = await executeQuery(apiRoutes.gameHelpSheetValues)
    })

    it('must read help sheet for values', async () => {
      expect(request.queryData.url).to.deep.equal({ id: GAME_ID })
      await request.respondWith({ status: 200, response: HELP_SHEET })
      await expect(exec).to.eventually.deep.equal(HELP_SHEET)
    })

    it('must not read help sheet if server error', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })

  describe('#readMoneyHelpSheet', () => {
    let exec: Promise<HelpSheet>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => gameApi.readMoneyHelpSheet({ _id: GAME_ID, ...GAME }))
      request = await executeQuery(apiRoutes.gameHelpSheetMoney)
    })

    it('must read help sheet for money', async () => {
      expect(request.queryData.url).to.deep.equal({ id: GAME_ID })
      await request.respondWith({ status: 200, response: HELP_SHEET })
      await expect(exec).to.eventually.deep.equal(HELP_SHEET)
    })

    it('must not read help sheet if server error', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })

  describe('#readOptions', () => {
    let exec: Promise<LocalizedOption<langType, keyof langType>[]>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => gameApi.readOptions<langType>({ _id: GAME_ID, ...GAME }))
      request = await executeQuery(apiRoutes.gameOptionsCreate)
    })

    it('must read options for current round', async () => {
      expect(request.queryData.url).to.deep.equal({ id: GAME_ID })
      await request.respondWith({ status: 200, response: [GAME_OPTION] })
      await expect(exec).to.eventually.have.lengthOf(1)
    })

    it('must not read options if server error', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })

  describe('#readForm', () => {
    let exec: Promise<Form<any>>
    let request: Request & QueryData
    const serializedForm = {
      parts: GAME_FORM.parts.map(part => ({
        player: part.player === -Infinity ? null : part.player,
        groups: part.groups,
      })),
      params: GAME_FORM.params,
    }

    beforeEach('Call API', async () => {
      exec = extractError(() => gameApi.readForm({ _id: GAME_ID, ...GAME }))
      request = await executeQuery(apiRoutes.gameFormCreate)
    })

    it('must read form for current round', async () => {
      expect(request.queryData.url).to.deep.equal({ id: GAME_ID })
      await request.respondWith({ status: 200, response: serializedForm })
      await expect(exec).to.eventually.deep.equal(GAME_FORM)
    })

    it('must read update function if provided', async () => {
      expect(request.queryData.url).to.deep.equal({ id: GAME_ID })
      await request.respondWith({
        status: 200,
        response: {
          ...serializedForm,
          dynFunction: 'return function(){return}',
        },
      })
      const form = await exec
      expect(form.dynFunction).to.be.a('function')
    })

    it('must not care of non working functions', async () => {
      expect(request.queryData.url).to.deep.equal({ id: GAME_ID })
      await request.respondWith({
        status: 200,
        response: { ...serializedForm, dynFunction: 'throw Error()' },
      })
      await expect(exec).to.eventually.deep.equal(GAME_FORM)
    })

    it('must not read form if server error', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })

  describe('#sendForm', () => {
    let exec: Promise<Game>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() =>
        gameApi.sendForm({ _id: GAME_ID, ...GAME }, { formName: 'name' }, 'option-id')
      )
      request = await executeQuery(apiRoutes.gameFormSend)
    })

    it('must send form result', async () => {
      expect(request.queryData.url).to.deep.equal({ id: GAME_ID })
      expect(request.queryData.post).to.deep.equal({
        formName: 'name',
        optionId: 'option-id',
      })
      await request.respondWith({ status: 200, response: GAME })
      await expect(exec).to.eventually.deep.equal(GAME)
    })

    it('must display error if set is finished', async () => {
      await request.respondWith({ status: 200, response: GAME })
      exec = extractError(() => gameApi.sendForm({ _id: GAME_ID, ...GAME }, { formName: 'name' }))
      request = await executeQuery(apiRoutes.gameFormSend)
      await request.respondWith({ status: 409 })
      await expect(exec).to.be.rejectedWith(/already finished/i)
    })

    it('must not terminate round if server error', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })

  describe('#recordTechnologicalBreak', () => {
    let exec: Promise<Game>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => gameApi.recordTechnologicalBreak({ _id: GAME_ID, ...GAME }))
      request = await executeQuery(apiRoutes.gameTechnologicalBreak)
    })

    it('must record technological break', async () => {
      expect(request.queryData.url).to.deep.equal({ id: GAME_ID })
      await request.respondWith({ status: 200, response: GAME })
      await expect(exec).to.eventually.deep.equal(GAME)
    })

    it('must display error if no set in progress', async () => {
      await request.respondWith({ status: 409 })
      await expect(exec).to.be.rejectedWith(/no set in progress/i)
    })

    it('must not record technological break if server error', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })
})
