/*
 * This file is part of @gecogvidanto/client.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { EmailCheck, HttpError } from '@gecogvidanto/shared'

import ApiError, { buildConnectionError } from './ApiError'
import { httpClient as http } from './helpers'

/**
 * The API for EmailCheck.
 */
export default class EmailCheckApi {
  /**
   * Validate (or not) the e-mail address.
   *
   * @param emailCheck - The e-mail checking data.
   */
  public async validate(emailCheck: EmailCheck): Promise<void> {
    try {
      await http.emailValidate(emailCheck)
    } catch (error) {
      if (!error.response || error.response.status !== HttpError.Unauthorized) {
        throw buildConnectionError()
      } else {
        throw new ApiError('$client$emailCheck')
      }
    }
  }
}
