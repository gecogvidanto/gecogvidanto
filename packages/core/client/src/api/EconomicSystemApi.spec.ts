/*
 * This file is part of @gecogvidanto/client.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback */
import { expect } from 'chai'
import * as moxios from 'moxios'

import { EconomicSystem, apiRoutes } from '@gecogvidanto/shared'

import { EconomicSystemApi } from '..'
import { GLOBAL_ERROR_MSG, QueryData, Request, executeQuery, extractError } from './common.spec'

const messages = {
  ecoName: 'Economic system',
  otherEcoName: 'Another one',
}
type langType = typeof messages & { $: string }

const XCHG_DATA: EconomicSystem<langType> = {
  id: 'xchg',
  name: 'ecoName',
  valueCost: 3,
}
const XCHG_SYS: EconomicSystem<langType>[] = [
  XCHG_DATA,
  { id: 'xchg2', name: 'otherEcoName', valueCost: 1 },
]

describe('EconomicSystemApi', function () {
  this.slow(400)
  let economicSystemApi: EconomicSystemApi

  beforeEach('Initialize API and Moxios', async () => {
    moxios.install()
    economicSystemApi = new EconomicSystemApi()
  })

  afterEach('Clean up', () => {
    moxios.uninstall()
  })

  describe('#search', () => {
    let exec: Promise<EconomicSystem<langType>[]>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => economicSystemApi.search())
      request = await executeQuery(apiRoutes.economicSystemSearch)
    })

    it('must send economic system identifiers', async () => {
      await request.respondWith({ status: 200, response: XCHG_SYS })
      await expect(exec).to.eventually.deep.equal(XCHG_SYS)
    })

    it('must send error if server error', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })

  describe('#read', () => {
    let exec: Promise<{ name: keyof langType; valueCost: number }>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => economicSystemApi.read<langType>(XCHG_DATA.id))
      request = await executeQuery(apiRoutes.economicSystemRead)
    })

    it('must send economic system name', async () => {
      expect(request.queryData.url).to.deep.equal({ id: XCHG_DATA.id })
      await request.respondWith({ status: 200, response: XCHG_DATA })
      await expect(exec).to.eventually.deep.equal(XCHG_DATA)
    })

    it('must send error if server error', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })

    it('must send specific error if system not found', async () => {
      await request.respondWith({ status: 404 })
      await expect(exec).to.be.rejectedWith(/not found/i)
    })
  })
})
