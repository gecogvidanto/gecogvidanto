/*
 * This file is part of @gecogvidanto/client.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { HttpError, User } from '@gecogvidanto/shared'

import ApiError, { buildConnectionError } from './ApiError'
import { httpClient as http } from './helpers'

/**
 * The user API is used to manipulate users.
 */
export default class UserApi {
  /**
   * Sign-in for the user.
   *
   * @param email - The e-mail address.
   * @param password - The password.
   * @returns The signed-in user.
   */
  public async signIn(email: string, password: string): Promise<User> {
    try {
      const response = await http.userSignIn({}, { email, password })
      return response.data
    } catch (error) {
      if (!error.response || error.response.status !== HttpError.Unauthorized) {
        throw buildConnectionError()
      } else {
        throw new ApiError('$client$credentials')
      }
    }
  }

  /**
   * Sign-out the current logged user.
   */
  public async signOut(): Promise<void> {
    try {
      await http.userSignOut()
    } catch {
      throw buildConnectionError()
    }
  }

  /**
   * Create a new user with the given password.
   *
   * @param captchaToken - The captcha token.
   * @param user - The user.
   * @param password - The password.
   * @returns The created user.
   */
  public async create(captchaToken: string, user: User, password: string): Promise<User> {
    try {
      const response = await http.userCreate({}, { ...user, password, captchaToken })
      return response.data
    } catch (error) {
      if (!error.response || error.response.status !== HttpError.Conflict) {
        throw buildConnectionError()
      } else {
        throw new ApiError('$client$userExists')
      }
    }
  }

  /**
   * Search users matching properties or all the users.
   *
   * @param email - Get only the user with the given e-mail.
   * @param name - Get only the user with the given name.
   * @returns The matching users.
   */
  public async search(email?: string, name?: string): Promise<User[]> {
    try {
      const response = await http.userSearch({}, { email, name })
      return response.data
    } catch {
      throw buildConnectionError()
    }
  }

  /**
   * Read the requested user.
   *
   * @param id - The user identifier.
   * @returns The matching user.
   */
  public async read(id: string): Promise<User> {
    try {
      const response = await http.userRead({ id })
      return response.data
    } catch {
      throw buildConnectionError()
    }
  }

  /**
   * Update the user.
   *
   * @param user - The user, with its identifier and new data.
   * @param password - The password to update, if any.
   * @returns The updated user.
   */
  public async update(user: User, password?: string): Promise<User> {
    try {
      const response = await http.userUpdate({ id: user._id! }, { ...user, password })
      return response.data
    } catch (error) {
      if (!error.response || error.response.status !== HttpError.Conflict) {
        throw buildConnectionError()
      } else {
        throw new ApiError('$client$userExists')
      }
    }
  }

  /**
   * Delete the user.
   *
   * @param user - The user to delete.
   */
  public async delete(user: User): Promise<void> {
    try {
      await http.userDelete({ id: user._id! })
    } catch {
      throw buildConnectionError()
    }
  }
}
