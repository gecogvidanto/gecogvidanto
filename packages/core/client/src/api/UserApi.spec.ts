/*
 * This file is part of @gecogvidanto/client.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback */
import { expect } from 'chai'
import * as moxios from 'moxios'

import { CertLevel, User, apiRoutes } from '@gecogvidanto/shared'

import { UserApi } from '..'
import { GLOBAL_ERROR_MSG, QueryData, Request, executeQuery, extractError } from './common.spec'

const ALL_USERS: User[] = [
  {
    _id: '0',
    email: 'god@heaven.sky',
    name: 'God',
    level: CertLevel.Administrator,
    account: 'IBAN',
  },
  {
    _id: 'id-1',
    email: 'heidi@alpes.ch',
    name: 'Heidi',
    level: CertLevel.GameMaster,
  },
]

const STRANGER: User = {
  _id: 'phone',
  email: 'et@space.far',
  name: 'E.T.',
  level: CertLevel.GameMaster,
  account: 'public-key',
}

const GOD = {
  email: ALL_USERS[0].email,
  password: 'adam&eve',
}

const PASSWORD = 'SuperVerySecret'

describe('UserApi', function () {
  this.slow(400)
  let userApi: UserApi

  beforeEach('Initialize API and Moxios', () => {
    moxios.install()
    userApi = new UserApi()
  })

  afterEach('Clean up', () => {
    moxios.uninstall()
  })

  describe('#signIn', () => {
    let exec: Promise<User>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => userApi.signIn(GOD.email, GOD.password))
      request = await executeQuery(apiRoutes.userSignIn)
    })

    it('must get matching user if any', async () => {
      expect(request.queryData.post).to.deep.equal({
        email: GOD.email,
        password: GOD.password,
      })
      await request.respondWith({ status: 200, response: ALL_USERS[0] })
      await expect(exec).to.eventually.deep.equal(ALL_USERS[0])
    })

    it('must send notification if bad credentials', async () => {
      await request.respondWith({ status: 401 })
      await expect(exec).to.be.rejectedWith(/invalid credentials/i)
    })

    it('must send notification if other error', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })

  describe('#signOut', () => {
    let exec: Promise<void>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => userApi.signOut())
      request = await executeQuery(apiRoutes.userSignOut)
    })

    it('must be fulfilled on success', async () => {
      await request.respondWith({ status: 200 })
      await expect(exec).to.be.fulfilled
    })

    it('must send notification on failure', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })

  describe('#create', () => {
    let exec: Promise<User>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => userApi.create('captcha', STRANGER, PASSWORD))
      request = await executeQuery(apiRoutes.userCreate)
    })

    it('must send created user', async () => {
      expect(request.queryData.post).to.deep.equal({
        ...STRANGER,
        password: PASSWORD,
        captchaToken: 'captcha',
      })
      await request.respondWith({ status: 200, response: STRANGER })
      await expect(exec).to.eventually.deep.equal(STRANGER)
    })

    it('must send error if creation failed', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })

    it('must send specific error if user exists', async () => {
      await request.respondWith({ status: 409 })
      await expect(exec).to.be.rejectedWith(/already registered/i)
    })
  })

  describe('#search', () => {
    it('must return user list if success', async () => {
      const exec = extractError(() => userApi.search())
      const request = await executeQuery(apiRoutes.userSearch)
      await request.respondWith({ status: 200, response: ALL_USERS })
      const users = await exec
      expect(users).to.have.lengthOf(ALL_USERS.length)
      for (let index = 0; index < ALL_USERS.length; index++) {
        expect(users[index], `Unexpected element #${index}`).to.deep.equal(ALL_USERS[index])
      }
    })

    it('must send values if provided', async () => {
      const exec = extractError(() => userApi.search(STRANGER.email, STRANGER.name))
      const request = await executeQuery(apiRoutes.userSearch)
      expect(request.queryData.query).to.deep.equal({
        email: STRANGER.email,
        name: STRANGER.name,
      })
      await request.respondWith({ status: 200, response: [STRANGER] })
      const users = await exec
      expect(users).to.have.lengthOf(1)
    })

    it('must send message if error', async () => {
      const exec = extractError(() => userApi.search())
      const request = await executeQuery(apiRoutes.userSearch)
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })

  describe('#read', () => {
    it('must return user if success', async () => {
      const exec = extractError(() => userApi.read(STRANGER._id!))
      const request = await executeQuery(apiRoutes.userRead)
      await request.respondWith({ status: 200, response: STRANGER })
      const user = await exec
      expect(user).to.deep.equal(STRANGER)
    })

    it('must send message if error', async () => {
      const exec = extractError(() => userApi.read(STRANGER._id!))
      const request = await executeQuery(apiRoutes.userRead)
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })

  describe('#update', () => {
    it('must send given user values', async () => {
      const exec = userApi.update(STRANGER)
      const request = await executeQuery(apiRoutes.userUpdate)
      expect(request.queryData.post).to.deep.equal(STRANGER)
      await request.respondWith({ status: 200, response: STRANGER })
      await expect(exec).to.eventually.deep.equal(STRANGER)
    })

    it('must send given user values with password', async () => {
      const exec = userApi.update(STRANGER, PASSWORD)
      const request = await executeQuery(apiRoutes.userUpdate)
      expect(request.queryData.post).to.deep.equal({
        ...STRANGER,
        password: PASSWORD,
      })
      await request.respondWith({ status: 200, response: STRANGER })
      await expect(exec).to.eventually.deep.equal(STRANGER)
    })

    it('must send error notification if update failed', async () => {
      const exec = extractError(() => userApi.update(STRANGER))
      const request = await executeQuery(apiRoutes.userUpdate)
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })

    it('must send specific error if user exists', async () => {
      const exec = extractError(() => userApi.update(STRANGER))
      const request = await executeQuery(apiRoutes.userUpdate)
      await request.respondWith({ status: 409 })
      await expect(exec).to.be.rejectedWith(/already registered/i)
    })
  })

  describe('#delete', () => {
    let exec: Promise<void>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => userApi.delete(STRANGER))
      request = await executeQuery(apiRoutes.userDelete)
    })

    it('must send id of user to remove', async () => {
      expect(request.queryData.url.id).to.equal(STRANGER._id)
      await request.respondWith({ status: 200 })
      await expect(exec).to.be.fulfilled
    })

    it('must send error if deletion failed', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })
  })
})
