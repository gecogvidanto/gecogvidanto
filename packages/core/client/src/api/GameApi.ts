/*
 * This file is part of @gecogvidanto/client.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import {
  ApiDefinition,
  Form,
  Game,
  HelpSheet,
  HttpError,
  IdentifiedAddress,
  LocalizedOption,
  Location,
  UnassembledMessages,
  Unlocked,
} from '@gecogvidanto/shared'

import ApiError, { buildConnectionError } from './ApiError'
import { httpClient as http } from './helpers'

/**
 * The game API is used to manipulate games.
 */
export default class GameApi {
  /**
   * Create a new game.
   *
   * @param location - The location of the game.
   * @param roundsPerSet - The count of rounds for each set.
   * @param roundLength - The length of each round in minutes.
   * @returns The created game.
   */
  public async create(location: Location, roundsPerSet: number, roundLength: number): Promise<Game> {
    try {
      const address: IdentifiedAddress | { value?: string } = location.address
        ? typeof location.address === 'string'
          ? { value: location.address }
          : location.address
        : {}
      const data: ApiDefinition['gameCreate']['data'] = {
        ...address,
        locationName: location.name,
        roundsPerSet,
        roundLength,
      }
      // Must cast to any because TypeScript does not understand the type correctly
      const response = await http.gameCreate({}, data as any)
      return response.data
    } catch {
      throw buildConnectionError()
    }
  }

  /**
   * Update the game (location, summary and closed state).
   *
   * @param game - The game to update.
   * @returns The updated game.
   */
  public async update(game: Game): Promise<Game> {
    let data: ApiDefinition['gameUpdate']['data'] = {}
    if (game.location.address && typeof game.location.address === 'object') {
      data = game.location.address
    } else {
      data.value = game.location.address
    }
    data.locationName = game.location.name
    data.summary = game.summary
    game.closed && (data.closed = true)
    try {
      // Must cast to any because TypeScript does not understand the type correctly
      const response = await http.gameUpdate({ id: game._id! }, data as any)
      return response.data
    } catch (error) {
      if (!error.response || !game.closed || error.response.status !== HttpError.Conflict) {
        throw buildConnectionError()
      } else {
        throw new ApiError('$client$gameClose')
      }
    }
  }

  /**
   * Search all available games.
   *
   * @returns The available games.
   */
  public async search(): Promise<Game[]> {
    try {
      const response = await http.gameSearch()
      return response.data
    } catch {
      throw buildConnectionError()
    }
  }

  /**
   * Read the requested game.
   *
   * @param id - The game identifier.
   * @returns The matching game.
   */
  public async read(id: string): Promise<Game> {
    try {
      const response = await http.gameRead({ id })
      return response.data
    } catch {
      throw buildConnectionError()
    }
  }

  /**
   * Add a player to the game.
   *
   * @param game - The game to which to add player.
   * @param name - The name to give to the player.
   * @returns The modified game.
   */
  public async addPlayer(game: Game, name: string): Promise<Game> {
    try {
      const response = await http.gamePlayerAdd({ id: game._id! }, { name })
      return response.data
    } catch (error) {
      if (!error.response || error.response.status !== HttpError.Conflict) {
        throw buildConnectionError()
      } else {
        throw new ApiError('$client$gamePlayers')
      }
    }
  }

  /**
   * Update a player.
   *
   * @param game - The game to which to add player.
   * @param player - The number associated to the player.
   * @param name - The name to give to the player.
   * @returns The modified game.
   */
  public async updatePlayer(game: Game, player: number, name: string): Promise<Game> {
    try {
      const response = await http.gamePlayerUpdate({ id: game._id!, player }, { name })
      return response.data
    } catch (error) {
      if (!error.response || error.response.status !== HttpError.Conflict) {
        throw buildConnectionError()
      } else {
        throw new ApiError('$client$gamePlayers')
      }
    }
  }

  /**
   * Delete player. If player already played, simply indicate he left the game.
   *
   * @param game - The game which player left.
   * @param player - The number associated to the player.
   * @returns The modified game.
   */
  public async deletePlayer(game: Game, player: number): Promise<Game> {
    try {
      const response = await http.gamePlayerDelete({ id: game._id!, player })
      return response.data
    } catch {
      throw buildConnectionError()
    }
  }

  /**
   * Add a new set to the game.
   *
   * @param game - The game to which to add set.
   * @param ecoSysId - The identifier of the economic system.
   * @returns The modified game.
   */
  public async addSet(game: Game, ecoSysId: string): Promise<Game> {
    try {
      const response = await http.gameSetAdd({ id: game._id! }, { ecoSysId })
      return response.data
    } catch (error) {
      if (!error.response || error.response.status !== HttpError.Conflict) {
        throw buildConnectionError()
      } else {
        throw new ApiError('$client$gameSets')
      }
    }
  }

  /**
   * Read the name of a non player character.
   *
   * @param game - The game for which to read NPC name.
   * @param roundSet - The set for which to read NPC name.
   * @param character - The NPC identifier, always negative.
   * @returns The name of the character.
   */
  public async readNpcName<T extends import('intl-ts').Messages>(
    game: Game,
    roundSet: number,
    character: number
  ): Promise<UnassembledMessages<T>[keyof T]> {
    try {
      const response = await http.gameNpcName({
        id: game._id!,
        roundSet,
        character,
      })
      return response.data as any
    } catch (e) {
      throw buildConnectionError()
    }
  }

  /**
   * Read the help sheet for values for the game.
   *
   * @param game - The game for which to read help sheet.
   * @returns The help sheet.
   */
  public async readValuesHelpSheet(game: Game): Promise<HelpSheet> {
    try {
      const response = await http.gameHelpSheetValues({ id: game._id! })
      return response.data
    } catch {
      throw buildConnectionError()
    }
  }

  /**
   * Read the help sheet for money for the game.
   *
   * @param game - The game for which to read help sheet.
   * @returns The help sheet.
   */
  public async readMoneyHelpSheet(game: Game): Promise<HelpSheet> {
    try {
      const response = await http.gameHelpSheetMoney({ id: game._id! })
      return response.data
    } catch {
      throw buildConnectionError()
    }
  }

  /**
   * Read the options for the game.
   *
   * @param game - The game for which to read options.
   * @returns The options.
   */
  public async readOptions<T extends import('intl-ts').Messages>(
    game: Game
  ): Promise<LocalizedOption<T, keyof T>[]> {
    try {
      const response = await http.gameOptionsCreate({ id: game._id! })
      return response.data
    } catch {
      throw buildConnectionError()
    }
  }

  /**
   * Read the form for the game.
   *
   * @param game - The game for which to read form.
   * @param optionId - The option identifier, if any.
   * @returns The form.
   */
  public async readForm(game: Game, optionId?: string): Promise<Form<any, any>> {
    try {
      const response = await http.gameFormCreate({ id: game._id! }, { optionId })
      const form: Unlocked<Form<any, any>> = {
        parts: response.data.parts.map(part => ({
          player: typeof part.player === 'number' ? part.player : -Infinity,
          groups: part.groups,
        })),
        params: response.data.params,
      }
      const dynFunction = response.data.dynFunction
      try {
        if (dynFunction) {
          const functionContainer = Function(dynFunction)
          form.dynFunction = functionContainer()
        }
      } catch {
        // Don't care if dynamic function not available
      }
      return form
    } catch {
      throw buildConnectionError()
    }
  }

  /**
   * Send the form, and terminate the round if no option identifier.
   *
   * @param game - The game for which to terminate round.
   * @param data - The form data.
   * @param optionId - The option identifier, if any.
   * @returns The modified game.
   */
  public async sendForm(
    game: Game,
    data: { [key: string]: string | number | boolean },
    optionId?: string
  ): Promise<Game> {
    try {
      const response = await http.gameFormSend(
        { id: game._id! },
        // eslint-disable-next-line prefer-object-spread
        Object.assign({}, data, { optionId })
      )
      return response.data
    } catch (error) {
      if (optionId || !error.response || error.response.status !== HttpError.Conflict) {
        throw buildConnectionError()
      } else {
        throw new ApiError('$client$gameNextRound')
      }
    }
  }

  /**
   * Record a technological break in current set of game.
   *
   * @param game - The game for which to record technological break.
   * @returns The modified game.
   */
  public async recordTechnologicalBreak(game: Game): Promise<Game> {
    try {
      const response = await http.gameTechnologicalBreak({ id: game._id! })
      return response.data
    } catch (error) {
      if (!error.response || error.response.status !== HttpError.Conflict) {
        throw buildConnectionError()
      } else {
        throw new ApiError('$client$gameTechBreak')
      }
    }
  }
}
