/*
 * This file is part of @gecogvidanto/client.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback */
import { expect } from 'chai'
import * as moxios from 'moxios'

import { EmailCheck, apiRoutes } from '@gecogvidanto/shared'

import { EmailCheckApi } from '..'
import { GLOBAL_ERROR_MSG, QueryData, Request, executeQuery, extractError } from './common.spec'

const EMAIL_CHECK: EmailCheck = {
  email: 'nothing@nowhere.no',
  token: 'ASecretToken',
}

describe('EmailCheckApi', function () {
  this.slow(400)
  let emailCheckApi: EmailCheckApi

  beforeEach('Initialize API and Moxios', async () => {
    moxios.install()
    emailCheckApi = new EmailCheckApi()
  })

  afterEach('Clean up', () => {
    moxios.uninstall()
  })

  describe('#validate', () => {
    let exec: Promise<void>
    let request: Request & QueryData

    beforeEach('Call API', async () => {
      exec = extractError(() => emailCheckApi.validate(EMAIL_CHECK))
      request = await executeQuery(apiRoutes.emailValidate)
    })

    it('must send validation data', async () => {
      expect(request.queryData.url).to.deep.equal(EMAIL_CHECK)
      await request.respondWith({ status: 200 })
      await expect(exec).to.be.fulfilled
    })

    it('must send error if server error', async () => {
      await request.respondWith({ status: 500 })
      await expect(exec).to.be.rejectedWith(GLOBAL_ERROR_MSG)
    })

    it('must send specific error if verification failed', async () => {
      await request.respondWith({ status: 401 })
      await expect(exec).to.be.rejectedWith(/no matching e-mail/i)
    })
  })
})
