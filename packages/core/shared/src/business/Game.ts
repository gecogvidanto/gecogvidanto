/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import Character from './Character'
import Gecobject from './Gecobject'
import Player from './Player'
import RoundSet from './RoundSet'

export interface IdentifiedAddress {
  value: string
  city?: string
  countryCode?: string
  latitude: number
  longitude: number
}

export interface Location {
  name?: string
  address?: IdentifiedAddress | string
}

/**
 * A ĞecoĞvidanto game.
 */
export default interface Game extends Gecobject {
  /**
   * The start time (in ms since EPOCH).
   */
  readonly start: number

  /**
   * Identifier of the game master.
   */
  readonly masterId: string

  /**
   * Location of where the game is played.
   */
  location: Location

  /**
   * A summary of the game, can contain anything in any language.
   */
  summary?: string

  /**
   * Count of rounds per set, should be 8 or 10.
   */
  readonly roundsPerSet: number

  /**
   * The lenght of a round in minutes, should be between 3 and 5.
   */
  readonly roundLength: number

  /**
   * The players of the game. Even if not whished, players may arrive after game start and leave before game end.
   */
  readonly players: ReadonlyArray<Player>

  /**
   * The sets of the game. A fully played game should have at least 2 sets.
   */
  readonly roundSets: ReadonlyArray<RoundSet>

  /**
   * The characters in the game. Each set, a player is playing a character from birth to death, but death/rebirth will
   * likely be in the middle of the set. There may also be non player characters.
   */
  readonly characters: ReadonlyArray<Character>

  /**
   * Indicate if the game is closed, no more sets will be played.
   */
  closed: boolean
}
