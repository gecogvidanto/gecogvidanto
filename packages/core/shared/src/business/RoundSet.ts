/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * A (round) set of a game for a given economic system.
 */
export default interface RoundSet {
  /**
   * The unique identifier of economic system used for this set.
   */
  readonly ecoSysId: string

  /**
   * The current round of this set, 0 if not started yet, {@link Game.roundsPerSet} + 1 if finished.
   */
  readonly currentRound: number

  /**
   * Rounds where a technological break occured.
   */
  readonly techBreakRounds: ReadonlyArray<number>
}
