/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import Score from './Score'

/**
 * A character in a game set.
 */
export default interface Character {
  /**
   * The identifier of the set this character belongs to.
   */
  readonly roundSet: number

  /**
   * The identifier of the player, or negative for non player character.
   */
  readonly player: number

  /**
   * The generation of the player, i.e. The round at end of which the player will die.
   */
  readonly generation?: number

  /**
   * The detailed score of the player per round. There may not be score for all rounds.
   */
  readonly score: ReadonlyArray<Score>

  /**
   * The total score of the player. This score contains the points in low money count during the game, and in low value
   * count when game is finished. A division may happen at the end of the game, but the score will not be visible to end
   * user until the end of the game, so the user will always see the score in low value.
   */
  readonly totalScore: number
}
