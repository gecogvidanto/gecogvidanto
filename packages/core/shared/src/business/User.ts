/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import Gecobject from './Gecobject'

/**
 * The certification levels.
 */
export enum CertLevel {
  GameMaster = 'Master',
  Administrator = 'Admin',
}

/**
 * An identified user of the ĞecoĞvidanto application.
 */
export default interface User extends Gecobject {
  /**
   * The email of the user, used as connection identifier.
   */
  email: string

  /**
   * The name or pseudonyme of the user.
   */
  name: string

  /**
   * The certification level of the user.
   */
  level: CertLevel

  /**
   * The user account public key.
   */
  account?: string
}
