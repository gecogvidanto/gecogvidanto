/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * A player of a game.
 */
export default interface Player {
  /**
   * The name or pseudonyme of the player.
   */
  name: string

  /**
   * The round the player arrived, should be 0 for most players.
   */
  readonly roundArrived: number

  /**
   * The round the player left, should be 0 (did not left) for most players.
   */
  readonly roundLeft: number
}
