/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import Intl, { Messages } from 'intl-ts'

export type ValidatorMessage<T extends Messages, P extends any[]> = {
  [K in keyof T]: Intl<T>[K] extends (...args: P) => string ? K : never
}[keyof T]

/**
 * A validator interface.
 */
export default interface Validator<T extends Messages = any, P extends any[] = any[]> {
  readonly params: P
  validate: (value: string) => ValidatorMessage<T, P> | undefined
}

/**
 * A validator based on regular expression.
 */
export class RegExpValidator<T extends Messages, P extends any[]> implements Validator<T, P> {
  public constructor(
    private readonly regExp: RegExp,
    private readonly msgKey: ValidatorMessage<T, P>,
    public readonly params: P
  ) {}

  public validate(value: string): ValidatorMessage<T, P> | undefined {
    if (this.regExp.test(value)) {
      return undefined
    } else {
      return this.msgKey
    }
  }
}
