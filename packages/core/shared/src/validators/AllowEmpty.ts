/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import Validator, { ValidatorMessage } from './Validator'

/**
 * A wrapper which adds emptiness to validator allowed values.
 */
export default class AllowEmpty<T extends import('intl-ts').Messages, P extends any[]>
  implements Validator<T, P> {
  public readonly params: P

  public constructor(private validator: Validator<T, P>) {
    this.params = validator.params
  }

  public validate(value: string): ValidatorMessage<T, P> | undefined {
    if (value.length === 0) {
      return undefined
    } else {
      return this.validator.validate(value)
    }
  }
}

/**
 * Wrap all validators of the given array into an {@link #AllowEmpty}.
 *
 * @param validators - The validators to wrap.
 * @returns The same validator which will allow an empty value.
 */
export function allowEmpty(validators: Validator[]): Validator[] {
  return validators.map(validator => new AllowEmpty(validator))
}
