/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { langType } from '../tools'
import Validator from './Validator'

/**
 * A validator for a minimum value length.
 */
export default class MinLength implements Validator<langType, [number]> {
  public readonly params: [number]

  /**
   * Create the validator.
   *
   * @param minLength - The minimum allowed length.
   */
  public constructor(private readonly minLength: number) {
    this.params = [minLength]
  }

  public validate(value: string): '$shared$minLength' | undefined {
    if (value.length < this.minLength) {
      return '$shared$minLength'
    } else {
      return undefined
    }
  }
}
