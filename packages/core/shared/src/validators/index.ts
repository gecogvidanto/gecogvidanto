/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { CertLevel } from '../business'
import EmailValidator from './Email'
import ListValidator from './List'
import MaxLengthValidator from './MaxLength'
import MinLengthValidator from './MinLength'
import NonEmptyValidator from './NonEmpty'
import StringEnumValidator from './StringEnum'

export { default as Validator, RegExpValidator } from './Validator'
export { default as AsyncValidator } from './AsyncValidator'
export { default as AllowEmptyValidator, allowEmpty } from './AllowEmpty'
export { default as IdentityValidator } from './Identity'
export { default as ListValidator } from './List'
export { default as NonEmptyValidator } from './NonEmpty'
export { default as MinLengthValidator } from './MinLength'
export { default as MaxLengthValidator } from './MaxLength'
export { default as EmailValidator } from './Email'
export { default as StringEnumValidator } from './StringEnum'
export { default as NumberValueValidator } from './NumberValue'
export { validate, asyncValidate } from './validate'

export const validators = {
  name: [new MinLengthValidator(3)],
  email: [new NonEmptyValidator(), new EmailValidator()],
  account: [new MaxLengthValidator(512)],
  password: [new MinLengthValidator(6), new MaxLengthValidator(64)],
  certLevel: [new StringEnumValidator(CertLevel)],
  location: [new MaxLengthValidator(64)],
  roundsPerSet: [new ListValidator((8).toString(), (10).toString())],
  roundLength: [new ListValidator((3).toString(), (4).toString(), (5).toString())],
  playerName: [new MinLengthValidator(1), new MaxLengthValidator(32)],
}
