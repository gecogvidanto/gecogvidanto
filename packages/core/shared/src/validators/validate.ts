/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import Intl, { Messages } from 'intl-ts'

import AsyncValidator from './AsyncValidator'
import Validator, { ValidatorMessage } from './Validator'

/**
 * Validate the value with the validator.
 *
 * @param value - The value to validate.
 * @param validator - The validator.
 * @returns True if the value is validated, false otherwise.
 */
export function validate(value: string, validator: Validator): boolean

/**
 * Validate the value with the validator.
 *
 * @param value - The value to validate.
 * @param validator - The validator.
 * @param lang - The internationalization object.
 * @returns Undefined if the value is validated, an error message otherwise.
 */
export function validate<T extends Messages, P extends any[]>(
  value: string,
  validator: Validator<T, P>,
  lang: Intl<T>
): string | undefined

/*
 * Validation implementation.
 */
export function validate<T extends Messages, P extends any[]>(
  value: string,
  validator: Validator<T, P>,
  lang?: Intl<T>
): boolean | string | undefined {
  return validateResult(validator.validate(value), validator.params, lang)
}

/**
 * Validate the value with the async validator.
 *
 * @param value - The value to validate.
 * @param validator - The validator.
 * @returns True if the value is validated, false otherwise.
 */
export function asyncValidate(value: string, validator: AsyncValidator): Promise<boolean>

/**
 * Validate the value with the async validator.
 *
 * @param value - The value to validate.
 * @param validator - The validator.
 * @param lang - The internationalization object.
 * @returns Undefined if the value is validated, an error message otherwise.
 */
export function asyncValidate<T extends Messages, P extends any[]>(
  value: string,
  validator: AsyncValidator<T, P>,
  lang: Intl<T>
): Promise<string | undefined>

/*
 * Async validation implementation.
 */
export async function asyncValidate<T extends Messages, P extends any[]>(
  value: string,
  validator: AsyncValidator<T, P>,
  lang?: Intl<T>
): Promise<boolean | string | undefined> {
  return validateResult(await validator.validate(value), validator.params, lang)
}

/**
 * Validate the result into either a boolean or a string (or undefined), depending on if lang is provided.
 *
 * @param result - The result to convert, which is either a message property or undefined.
 * @param params - The parameters of the validator.
 * @param lang - The language to use.
 * @returns Either a boolean or a string or undefined.
 */
function validateResult<T extends Messages, P extends any[]>(
  result: ValidatorMessage<T, P> | undefined,
  params: P,
  lang?: Intl<T>
): boolean | string | undefined {
  if (result === undefined) {
    return lang ? undefined : true
  } else {
    if (lang) {
      return lang[result](...params)
    } else {
      return false
    }
  }
}
