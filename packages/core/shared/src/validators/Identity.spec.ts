/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import Intl, { LanguageMap } from 'intl-ts'

import { IdentityValidator, validate } from '.'
import { messages } from '../tools'

describe('IdentityValidator', function () {
  const validator = new IdentityValidator('Tested-Value')
  const lang = new Intl(new LanguageMap(messages))

  it('must be valid if given value is equal to tested one', function () {
    expect(validate('Tested-Value', validator)).to.be.true
    expect(validate('Tested-Value', validator, lang)).to.be.undefined
  })

  it('must be invalid if given value is not equal to tested one', function () {
    expect(validate('any string', validator)).to.be.false
    expect(validate('any string', validator, lang)).to.be.a('string')
  })
})
