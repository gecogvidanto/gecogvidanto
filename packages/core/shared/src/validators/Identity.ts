/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { langType } from '../tools'
import Validator from './Validator'

/**
 * A validator for identity value. Mostly useful for hidden password control.
 */
export default class Identity implements Validator<langType, []> {
  public readonly params: [] = []

  /**
   * Create the validator.
   *
   * @param comparison - The string to compare value to.
   */
  public constructor(private readonly comparison: string) {}

  public validate(value: string): '$shared$nonMatching' | undefined {
    if (value === this.comparison) {
      return undefined
    } else {
      return '$shared$nonMatching'
    }
  }
}
