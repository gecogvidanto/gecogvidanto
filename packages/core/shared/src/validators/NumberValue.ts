/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { langType } from '../tools'
import Validator from './Validator'

/**
 * A validator for number values.
 */
export default class NumberValue implements Validator<langType, []> {
  public readonly params: [] = []

  /**
   * Create the validator.
   *
   * @param minValue - The minimum value of the field or undefined if no minimum.
   * @param maxValue - The maximum value of the field or undefined if no maximum.
   */
  public constructor(private readonly minValue?: number, private readonly maxValue?: number) {}

  public validate(value: string): '$shared$nonMatching' | undefined {
    const converted = Number(value)
    if (
      isNaN(converted) ||
      (this.minValue !== undefined && converted < this.minValue) ||
      (this.maxValue !== undefined && converted > this.maxValue)
    ) {
      return '$shared$nonMatching'
    } else {
      return undefined
    }
  }
}
