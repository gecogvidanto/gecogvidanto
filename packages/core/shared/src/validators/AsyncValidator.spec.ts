/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import Intl, { LanguageMap } from 'intl-ts'

import { AsyncValidator, asyncValidate } from '.'
import { messages } from '../tools'

class Validator implements AsyncValidator<typeof messages, []> {
  public readonly params: [] = []

  public async validate(value: string): Promise<'$shared$nonMatching' | undefined> {
    await new Promise(resolve => setTimeout(() => resolve(), 50))
    if (value === 'no') {
      return '$shared$nonMatching'
    } else {
      return undefined
    }
  }
}

describe('AsyncValidator', function () {
  const validator = new Validator()
  const lang = new Intl(new LanguageMap(messages))

  it('must be valid if given value expected', function () {
    expect(asyncValidate('yes', validator)).to.eventually.be.true
    expect(asyncValidate('yes', validator, lang)).to.eventually.be.undefined
  })

  it('must be invalid if given value is no', function () {
    expect(asyncValidate('no', validator)).to.eventually.be.false
    expect(asyncValidate('no', validator, lang)).to.eventually.be.a('string')
  })
})
