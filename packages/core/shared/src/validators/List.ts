/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { langType } from '../tools'
import Validator from './Validator'

/**
 * A validator for a maximum value length.
 */
export default class MaxLength implements Validator<langType, [string]> {
  public readonly params: [string]
  private readonly values: ReadonlySet<string>

  /**
   * Create the validator.
   *
   * @param values - The allowed values.
   */
  public constructor(...values: string[]) {
    this.params = [values.join(', ')]
    this.values = new Set(values)
  }

  public validate(value: string): '$shared$notInList' | undefined {
    if (this.values.has(value)) {
      return undefined
    } else {
      return '$shared$notInList'
    }
  }
}
