/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import Intl, { LanguageMap } from 'intl-ts'

import { NumberValueValidator, validate } from '.'
import { messages } from '../tools'

describe('NumberValueValidator', function () {
  const lang = new Intl(new LanguageMap(messages))

  describe('with no parameter', function () {
    const validator = new NumberValueValidator()

    it('must be valid if given value is any number', function () {
      expect(validate('-12', validator)).to.be.true
      expect(validate('-12', validator, lang)).to.be.undefined
      expect(validate('0', validator)).to.be.true
      expect(validate('0', validator, lang)).to.be.undefined
      expect(validate('20736', validator)).to.be.true
      expect(validate('20736', validator, lang)).to.be.undefined
    })

    it('must be invalid if given value is not a number', function () {
      expect(validate('twelve', validator)).to.be.false
      expect(validate('twelve', validator, lang)).to.be.a('string')
    })
  })

  describe('with minimum value', function () {
    const validator = new NumberValueValidator(0)

    it('must be valid if given value is minimum or more', function () {
      expect(validate('0', validator)).to.be.true
      expect(validate('0', validator, lang)).to.be.undefined
      expect(validate('42', validator)).to.be.true
      expect(validate('42', validator, lang)).to.be.undefined
    })

    it('must be invalid if given value is below minimum', function () {
      expect(validate('-1', validator)).to.be.false
      expect(validate('-1', validator, lang)).to.be.a('string')
    })
  })

  describe('with maximum value', function () {
    const validator = new NumberValueValidator(undefined, 0)

    it('must be valid if given value is maximum or less', function () {
      expect(validate('-42', validator)).to.be.true
      expect(validate('-42', validator, lang)).to.be.undefined
      expect(validate('0', validator)).to.be.true
      expect(validate('0', validator, lang)).to.be.undefined
    })

    it('must be invalid if given value is below minimum', function () {
      expect(validate('1', validator)).to.be.false
      expect(validate('1', validator, lang)).to.be.a('string')
    })
  })
})
