/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import { AllowEmptyValidator, EmailValidator, MinLengthValidator, Validator, allowEmpty, validate } from '.'

describe('AllowEmptyValidator', function () {
  const validators: Validator[] = [new EmailValidator(), new MinLengthValidator(2)]

  it('must refuse emptiness if empty validator is not wrapped', function () {
    expect(validate('', validators[0])).to.be.false
    expect(validate('', validators[1])).to.be.false
  })

  it('must accept emptiness with usage of AllowEmptyValidator', function () {
    expect(validate('', new AllowEmptyValidator(validators[0]))).to.be.true
    expect(validate('', new AllowEmptyValidator(validators[1]))).to.be.true
  })

  it('must use default validator for non empty values', function () {
    expect(validate('me@home.here', new AllowEmptyValidator(validators[0]))).to.be.true
    expect(validate('not an @mail', new AllowEmptyValidator(validators[0]))).to.be.false
    expect(validate('long entry', new AllowEmptyValidator(validators[1]))).to.be.true
    expect(validate('a', new AllowEmptyValidator(validators[1]))).to.be.false
  })

  it('must accept emptiness with call to allowEmpty', function () {
    const emptyValidators = allowEmpty(validators)
    expect(validate('', emptyValidators[0])).to.be.true
    expect(validate('', emptyValidators[1])).to.be.true
  })
})
