/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback */
import { expect } from 'chai'

import { DynField, DynFieldEvent } from '.'
import { Listener } from '../tools'
import FieldCore from './FieldCore'

const FIELD: FieldCore = {
  name: 'field',
  content: 'content',
  options: {},
  hasData: true,
}

function wait(): Promise<void> {
  return new Promise<void>(resolve => {
    setTimeout(() => resolve(), 1)
  })
}

describe('DynField', function () {
  let field: DynField
  const eventStore: DynFieldEvent[] = []
  const listener: Listener<DynFieldEvent> = (event: DynFieldEvent) => {
    eventStore.push(event)
  }

  beforeEach('Initialize dynamic field', function () {
    field = new DynField(12, FIELD)
    expect(field.player).to.equal(12)
    expect(field.name).to.equal(FIELD.name)
    expect(field.content).to.equal(FIELD.content)
    expect(field.options).to.deep.equal(FIELD.options)
    field.on(listener)
    while (eventStore.length) {
      eventStore.pop()
    }
  })

  describe('#aspect', function () {
    it('must not send event if value is same', async function () {
      field.options = FIELD.options
      await wait()
      expect(eventStore).to.have.lengthOf(0)
    })

    it('must send event if value is modified', async function () {
      field.options = { disabled: true }
      await wait()
      expect(eventStore, 'Unexpected event count').to.have.lengthOf(1)
      expect(eventStore[0].field).to.equal(field)
    })
  })

  describe('#content', function () {
    it('must not send event if value is not modified', async function () {
      field.content = 'content'
      await wait()
      expect(eventStore).to.have.lengthOf(0)
    })

    it('must send event if value is modified', async function () {
      field.content = 'modified'
      await wait()
      expect(eventStore, 'Unexpected event count').to.have.lengthOf(1)
      expect(eventStore[0].field).to.equal(field)
    })
  })
})
