/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { AsyncEmitter } from '../tools'
import FieldCore, { FieldOptions } from './FieldCore'

/**
 * Event emitted by dynamic fields when programmatically modified.
 */
export interface DynFieldEvent {
  /**
   * The field which was modified.
   */
  field: DynField
}

/**
 * A dynamic field which may be used in a form.
 */
export default class DynField extends AsyncEmitter<DynFieldEvent> {
  public readonly name: string
  public readonly hasData: boolean
  private _content: unknown
  private _options: FieldOptions

  public constructor(public readonly player: number, data: FieldCore) {
    super()
    this.name = data.name
    this.hasData = data.hasData
    this._content = data.content
    this._options = data.options
  }

  public get content(): unknown {
    return this._content
  }

  public set content(value: unknown) {
    if (value !== this._content) {
      this._content = value
      this.emit({ field: this })
    }
  }

  public get options(): FieldOptions {
    return this._options
  }

  public set options(value: FieldOptions) {
    if (value !== this._options) {
      this._options = value
      this.emit({ field: this })
    }
  }
}
