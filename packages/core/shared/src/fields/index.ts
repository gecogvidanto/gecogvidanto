/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { default as BooleanInputField } from './BooleanInput'
import { default as NumberInputField } from './NumberInput'
import { default as SelectInputField } from './SelectInput'
import { default as TextField } from './Text'
import { default as TextInputField } from './TextInput'

export type Field<T extends import('intl-ts').Messages> =
  | TextField<T>
  | TextInputField<T>
  | NumberInputField<T>
  | BooleanInputField<T>
  | SelectInputField<T>

export { Aspect, FieldOptions } from './FieldCore'
export { default as DynField, DynFieldEvent } from './DynField'
