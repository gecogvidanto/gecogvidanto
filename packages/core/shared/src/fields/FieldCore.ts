/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Aspect of the field. The way the field is really rendered depends on the client.
 */
export const enum Aspect {
  /**
   * An information field, usually green or blue.
   */
  Info,

  /**
   * A warning field, usually orange or yellow.
   */
  Warning,

  /**
   * An error field, usually red.
   */
  Error,

  /**
   * A field used for red values or banknotes.
   */
  Red,

  /**
   * A field used for yellow values or banknotes.
   */
  Yellow,

  /**
   * A field used for green values or banknotes.
   */
  Green,

  /**
   * A field used for blue values or banknotes.
   */
  Blue,
}

export interface FieldOptions {
  /**
   * If true, the field should not be displayed (no room left for it).
   */
  hidden?: boolean

  /**
   * If true (and not hidden), the field should not be visible (but space is reserved for it).
   */
  invisible?: boolean

  /**
   * If true, the field should not be editable.
   */
  disabled?: boolean

  /**
   * If set, field should have a special aspect.
   */
  aspect?: Aspect
}

/**
 * The core of a field.
 */
export default interface FieldCore {
  /**
   * Name used to identify the field.
   */
  readonly name: string

  /**
   * The (initial) content of the field. Type should be restricted by inherited interfaces.
   */
  content: unknown

  /**
   * The display options for the field.
   */
  options: FieldOptions

  /**
   * Indicate if the field has important data which needs to be sent to server. If false, field is only to
   * be displayed.
   */
  hasData: boolean
}
