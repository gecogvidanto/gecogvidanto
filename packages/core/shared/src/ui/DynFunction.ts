/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { DynField } from '../fields'

/**
 * Usefull tools given by the client to the dynamic function.
 */
export interface Toolbox {
  /**
   * This helper search for field matching player and name, and if found, execute the callback with this
   * field as argument. If no matching field found, callback is not executed.
   *
   * Implementations may manage a cache system for fast field retrieval if this function is called many
   * times.
   */
  readonly forField: (player: number, name: string) => (callback: (field: DynField) => void) => void
}

/**
 * If client uses dynamic function, initialization function **will** be called after form was first
 * rendered.
 *
 * **Note:** in some specific cases, initialization may also be called after some data have been set by the
 * user in the form (for example, if user language is changing).
 */
export type InitFunction = () => void

/**
 * If client uses dynamic function, update function **will** be called by client when a field is updated.
 *
 * **Note:** update function **will only** be called if the field is modified by the user (meaning it will
 * **not** be called if modified by the update function itself or by the initialization function).
 *
 * This function is called with:
 * - the modified field;
 * - the previous value of the field.
 */
export type UpdateFunction = (field: DynField, previous: unknown) => void

/**
 * This dynamic function may be provided by a plugin in order to dynamically manage the fields of a form.
 * Even if this function is provided, **client may choose not to use it at all** (especially for security
 * reasons). This function will be executed in its own context.
 *
 * For all those reasons:
 * - forms **must** work even if the dynamic function is not used;
 * - dynamic function and all its content **must** be fully autonomous, meaning it must not require any code
 * execution outside of the dynamic function itself (and provided parameters),
 * **even code from its own module** (be careful of polyfill that compiler may insert in module at
 * compilation time);
 * - dynamic function **should** work on data provided as parameters and never touch any other more global
 * variables.
 *
 * If used by the client, this function **will be called** once and only once after a new form (and
 * therefore a new dynamic function) is received and rendered to the screen. The provided parameters are:
 * - the dynamic fields created for the form;
 * - the parameters shipped with the form;
 * - the toolbox provided by the client.
 */
export type DynFunction<T> = (
  fields: ReadonlyArray<DynField>,
  params: Readonly<T>,
  toolbox: Toolbox
) => InitFunction & UpdateFunction
