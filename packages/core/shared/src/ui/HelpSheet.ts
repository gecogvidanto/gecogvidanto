/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * The content which can be put a each place.
 */
export const enum PlaceContent {
  Empty,
  Red,
  Yellow,
  Green,
  Blue,
}

/**
 * A help sheet, either for values or for money.
 */
export default interface HelpSheet {
  readonly low: PlaceContent
  readonly medium: PlaceContent
  readonly high: PlaceContent
  readonly waiting: PlaceContent
}
