/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Field } from '../fields'
import { DynFunction } from './DynFunction'

/**
 * Fields created and used by the server. Note that field names all are starting with exclamation mark, which may be
 * used to identify them, provided that plugin does not do the same.
 *
 * Note: server only create these fields for players. Plugins may use them for non player characters.
 */
export const enum ServerField {
  /**
   * The main title for a dying player.
   */
  Dying = '!dyingTitle',

  /**
   * The title for values results.
   */
  DyingValues = '!dyingValuesTitle',

  /**
   * The count of low values.
   */
  LowValues = '!lowValues',

  /**
   * The count of medium values.
   */
  MediumValues = '!mediumValues',

  /**
   * The count of high values.
   */
  HighValues = '!highValues',

  /**
   * The title for money results.
   */
  DyingMoney = '!dyingMoneyTitle',

  /**
   * The count of low money banknotes.
   */
  LowMoney = '!lowMoney',

  /**
   * The count of medium money banknotes.
   */
  MediumMoney = '!mediumMoney',

  /**
   * The count of high money banknotes.
   */
  HighMoney = '!highMoney',
}

/**
 * A group of fields. Fields in a group are logically linked, i.e. All fields for a dying player result (count of values
 * and count of banknotes). A field group may be mandatory, meaning that the game master should look at it, or not,
 * meaning that game master may or may not use it. Client may display mandatory groups differently or not.
 */
export interface FieldGroup<T extends import('intl-ts').Messages> {
  /**
   * An optional field that may be used as group title.
   */
  readonly title?: Field<T>

  /**
   * Indicate if the group must be viewed by the game master.
   */
  readonly mandatory: boolean

  /**
   * The fields contained in the group. If fields are in an inner array, they may have a strong link and should be
   * displayed as close as possible (in same line of form, for example).
   */
  readonly fields: ReadonlyArray<Field<T> | ReadonlyArray<Field<T>>>
}

/**
 * A part of a form, i.e. Fields for a given player (or global fields if no player identifier).
 */
export interface FormPart<T extends import('intl-ts').Messages> {
  /**
   * The player identifier, can be negative for NPC or -Infinity for global group.
   */
  readonly player: number

  /**
   * The groups in this form part.
   */
  readonly groups: ReadonlyArray<FieldGroup<T>>
}

/**
 * A form which will be defined by server to be displayed by clients. In a form, field names must be unique  accross all
 * groups of a given player, and each part must have a different player identifier. Some client may also display parts
 * in the order they are shown.
 *
 * Some field names are used by the server (see {@link #ServerField}). Plugins must not use these names for their own
 * purpose. It is advised that plugin specific field names do not start with an exclamation mark. The server will, of
 * course, manage these fields, even if they also may be used by plugins for some specific operations.
 */
export default interface Form<T extends import('intl-ts').Messages, P = Record<string, unknown>> {
  /**
   * The parts of the form, grouped by concerned users.
   */
  readonly parts: ReadonlyArray<FormPart<T>>

  /**
   * The dynamic function, used to dynamically update the form.
   */
  readonly dynFunction?: DynFunction<P>

  /**
   * An object containing parameters which may be used by the update function.
   */
  readonly params: P
}

/**
 * A form part where player identifier can be undefined (-Infinity is not serializable).
 */
export type SerializableFormPart = {
  [K in keyof FormPart<any>]: FormPart<any>[K] extends number
    ? FormPart<any>[K] | undefined
    : FormPart<any>[K]
}

/**
 * A form where:
 * - parts are serializable parts,
 * - dynamic functions are strings.
 */
export type SerializableForm = {
  [K in keyof Form<any, any>]-?: Form<any, any>[K] extends DynFunction<any> | undefined
    ? string | null
    : Form<any, any>[K] extends ReadonlyArray<FormPart<any>>
    ? ReadonlyArray<SerializableFormPart>
    : Form<any, any>[K]
}
