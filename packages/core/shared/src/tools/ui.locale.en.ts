/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { createMessages } from 'intl-ts'

/**
 * The messages for the shared library.
 */
export const messages = createMessages({
  $: 'English',
  $shared$empty: 'The field cannot be empty',
  $shared$formMoney: 'Indicate banknotes count',
  $shared$formMoneyHigh: 'High banknotes',
  $shared$formMoneyLow: 'Low banknotes',
  $shared$formMoneyMedium: 'Medium banknotes',
  $shared$formValues: 'Indicate values count',
  $shared$formValuesHigh: 'High values',
  $shared$formValuesLow: 'Low values',
  $shared$formValuesMedium: 'Medium values',
  $shared$invalidEmail: 'The value must be a valid e-mail address',
  $shared$maxLength: (length: number) => `The value must be at most ${length} characters long`,
  $shared$minLength: (length: number) => `The value must be at least ${length} characters long`,
  $shared$nonMatching: 'The value does not match',
  $shared$notInList: (valid: string) => `The value must be one of ${valid}`,
})

/**
 * The type for the messages.
 */
export type langType = typeof messages
