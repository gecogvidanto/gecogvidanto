/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Extract the properties which are functions.
 */
export type PickFunctions<T> = {
  [K in keyof T]: T[K] extends (...args: any[]) => any ? T[K] : never
}

/**
 * Convert a type containing functions to the same type with return types instead of functions.
 */
export type ReturnTypes<T> = {
  [P in keyof T]: T[P] extends (...args: any[]) => infer R ? R : T[P]
}

/**
 * Transform all properties into string.
 */
export type Stringized<T> = { [P in keyof T]: string }

/**
 * Add the ability to set all properties as undefined.
 */
export type Undefinedable<T> = { [P in keyof T]: T[P] | undefined }

/**
 * Remove the readonly from the type properties.
 */
export type Unlocked<T> = { -readonly [P in keyof T]: T[P] }

/**
 * Remove the readonly from the arrays.
 */
export type UnlockedArray<T> = {
  [P in keyof T]: T[P] extends ReadonlyArray<infer U> ? U[] : T[P]
}

/**
 * An unassembled from of the messages (template function and parameters are stored appart). The purpose of
 * such message is for when a client should display a message with parameters given by the server. It must
 * be the client's responsability to build the strings because the user could ask for a language change.
 */
export type UnassembledMessages<T extends import('intl-ts').Messages> = {
  [K in keyof T]: T[K] extends (...args: infer P) => any
    ? {
        template: K
        parameters: P
      }
    : K
}

/**
 * The keys of pure messages of the language type. Pure messages are the ones not taking parameters. Note
 * that even if it is legal to define a pure message as a function taking no parameter instead of a string,
 * such defined messages will not be considered pure. Note that this is compatible with an
 * `UnassembledMessages<T>[K]`.
 */
export type PureMessageKey<T extends import('intl-ts').Messages> = {
  [K in keyof T]: T[K] extends string ? K : never
}[keyof T]
