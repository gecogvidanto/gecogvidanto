/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * A listener for a given event.
 */
export type Listener<E> = (event: E) => void

/**
 * An emitter of asynchronous events.
 */
export default class AsyncEmitter<E> {
  private listeners: Array<Listener<E>> = []

  /**
   * Add a listener to the emitter.
   *
   * @param listener - The listener.
   * @returns `this`.
   */
  public on(listener: Listener<E>): this {
    return this.addListener(listener)
  }

  /**
   * Add a listener to the emitter.
   *
   * @param listener - The listener.
   * @returns `this`.
   * @see #on
   */
  public addListener(listener: Listener<E>): this {
    this.listeners.push(listener)
    return this
  }

  /**
   * Remove the listener from the emitter.
   *
   * @param listener - The listener.
   * @returns `this`.
   */
  public off(listener: Listener<E>): this {
    return this.removeListener(listener)
  }

  /**
   * Remove the listener from the emitter.
   *
   * @param listener - The listener.
   * @returns `this`.
   * @see #off
   */
  public removeListener(listener: Listener<E>): this {
    this.listeners = this.listeners.filter(l => l !== listener)
    return this
  }

  /**
   * Emit an event to all listener.
   *
   * @param e - The event.
   * @returns `this`.
   */
  public emit(e: E): Promise<void> {
    return new Promise<void>(resolve => {
      setTimeout(() => {
        this.listeners.forEach(l => l(e))
        resolve()
      })
    })
  }
}
