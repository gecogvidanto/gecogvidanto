/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback */
import { expect } from 'chai'

import { AsyncEmitter } from '..'

interface MemoryEvent {
  data: string
}

class MemoryListener {
  public readonly memory: MemoryEvent[] = []

  public onNotification = (event: MemoryEvent): void => {
    this.memory.push(event)
  }
}

describe('AsyncEmitter', function () {
  let emitter: AsyncEmitter<MemoryEvent>
  let listener: MemoryListener
  let otherListener: MemoryListener
  const event: MemoryEvent = {
    data: 'message',
  }

  beforeEach('Reset data', function () {
    emitter = new AsyncEmitter()
    listener = new MemoryListener()
    otherListener = new MemoryListener()
    emitter.on(listener.onNotification)
  })

  describe('#on', function () {
    it('must set first listener', async function () {
      await emitter.emit(event)
      expect(listener.memory).to.have.lengthOf(1)
      expect(listener.memory[0]).to.equal(event)
    })

    it('must add new listener', async function () {
      emitter.on(otherListener.onNotification)
      emitter.on(otherListener.onNotification)
      await emitter.emit(event)
      expect(listener.memory, 'Unexpected first listener length').to.have.lengthOf(1)
      expect(otherListener.memory, 'Unexpected other listener length').to.have.lengthOf(2)
    })
  })

  describe('#off', function () {
    it('must remove listener', async function () {
      emitter.on(otherListener.onNotification)
      await emitter.emit(event)
      emitter.off(otherListener.onNotification)
      await emitter.emit(event)
      expect(listener.memory, 'Unexpected first listener length').to.have.lengthOf(2)
      expect(otherListener.memory, 'Unexpected other listener length').to.have.lengthOf(1)
    })
  })

  describe('#emit', function () {
    it('must emit events', async function () {
      const otherEvent: MemoryEvent = {
        data: 'Other message',
      }
      await emitter.emit(event)
      await emitter.emit(otherEvent)
      expect(listener.memory).to.have.lengthOf(2)
      expect(listener.memory[0], 'Unexpected first event value').to.equal(event)
      expect(listener.memory[1], 'Unexpected other event value').to.equal(otherEvent)
    })
  })
})
