/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import Enum from './Enum'

// A pure enumeration
enum EnumPure {
  PureA,
  PureB,
  PureC,
}

// An initialized enumeration
enum EnumInit {
  InitA = 0,
  InitB = 12,
  InitC = 2,
}

// A string enumeration
enum EnumString {
  StringA = 'initA',
  StringB = 'initB',
  StringC = 'initC',
}

describe('Enum', function () {
  describe('#getAllNames', function () {
    it('must return all names of a pure enumeration', function () {
      expect(Enum.getAllNames(EnumPure)).to.have.members(['PureA', 'PureB', 'PureC'])
    })
    it('must return all names of an initialized enumeration', function () {
      expect(Enum.getAllNames(EnumInit)).to.have.members(['InitA', 'InitB', 'InitC'])
    })
    it('must return all names of a string enumeration', function () {
      expect(Enum.getAllNames(EnumString)).to.have.members(['StringA', 'StringB', 'StringC'])
    })
  })

  describe('#getName', function () {
    it('must return matching name of a pure enumeration', function () {
      expect(Enum.getName(EnumPure, 2)).to.equal('PureC')
    })
    it('must return matching name of an initialized enumeration', function () {
      expect(Enum.getName(EnumInit, 12)).to.equal('InitB')
    })
    it('must return matching name of a string enumeration', function () {
      expect(Enum.getName(EnumString, 'initA' as EnumString)).to.equal('StringA')
    })
    it('must return undefined if no matching name of a pure enumeration', function () {
      expect(Enum.getName(EnumPure, -1)).to.be.undefined
    })
    it('must return undefined if no matching name of an initialized enumeration', function () {
      expect(Enum.getName(EnumInit, 1)).to.be.undefined
    })
    it('must return undefined if no matching name of a string enumeration', function () {
      expect(Enum.getName(EnumString, 'unknown' as EnumString)).to.be.undefined
    })
  })
})
