/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Class containing static helpers for enum objects.
 */
export default class Enum {
  /**
   * Get all the names (actually keys) of the enumeration item. This will only return names defined in the
   * enumeration, not the indices. This method can be used with any enumeration object, be it number or
   * string.
   *
   * @param enumObject - The enumeration object to extract names from.
   * @returns An array containing all enumeration names.
   */
  public static getAllNames<T>(enumObject: T): Array<keyof T & string> {
    return Object.keys(enumObject).filter(key => isNaN(parseInt(key, 10))) as any
  }

  /**
   * Get the enumeration name for the given initialization value. This method can only be used with string
   * enumerations as retrieving the name of a number enumeration item is trivial.
   *
   * @param enumObject - The enumeration object in which to search for names.
   * @param initValue - The initialization value of the name.
   * @returns The found name or undefined if no matching name found.
   */
  public static getName<T>(enumObject: T, initValue: T[keyof T]): (keyof T & string) | undefined {
    const found = Object.entries(enumObject).find(([_, value]) => value === initValue)
    return found ? (found[0] as any) : undefined
  }
}
