/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import { FormFieldIdentifier } from '..'

const FIELD_NAME = 'fieldName'
const GLOBAL_IDENTIFIER = 'fieldName$$'
const PLAYER_IDENTIFIER = (player: number): string => `fieldName$${player}$`

describe('FormFieldIdentifier', function () {
  it('must create identifier for global field', function () {
    const identifier = new FormFieldIdentifier(-Infinity, FIELD_NAME)
    expect(identifier.valid).to.be.true
    expect(identifier.player).to.equal(-Infinity)
    expect(identifier.name).to.equal(FIELD_NAME)
    expect(identifier.identifier).to.equal(GLOBAL_IDENTIFIER)
  })

  new Array<number>(0, 1, 3, 8).forEach(player => {
    it(`must create identifier for player #${player} field`, function () {
      const identifier = new FormFieldIdentifier(player, FIELD_NAME)
      expect(identifier.valid).to.be.true
      expect(identifier.player).to.equal(player)
      expect(identifier.name).to.equal(FIELD_NAME)
      expect(identifier.identifier).to.equal(PLAYER_IDENTIFIER(player))
    })
  })

  it('must identify global field', function () {
    const identifier = new FormFieldIdentifier(GLOBAL_IDENTIFIER)
    expect(identifier.valid).to.be.true
    expect(identifier.player).to.equal(-Infinity)
    expect(identifier.name).to.equal(FIELD_NAME)
    expect(identifier.identifier).to.equal(GLOBAL_IDENTIFIER)
  })

  new Array<number>(0, 1, 3, 8).forEach(player => {
    it(`must identify player#${player} field`, function () {
      const identifier = new FormFieldIdentifier(PLAYER_IDENTIFIER(player))
      expect(identifier.valid).to.be.true
      expect(identifier.player).to.equal(player)
      expect(identifier.name).to.equal(FIELD_NAME)
      expect(identifier.identifier).to.equal(PLAYER_IDENTIFIER(player))
    })
  })

  it('must reject invalid identifiers', function () {
    const identifier = new FormFieldIdentifier('invalid')
    expect(identifier.valid).to.be.false
  })
})
