/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * When sent from client to server, form input fields are calculated a new identifier, made by appending the
 * dollar character, followed by the player identifier if defined, followed by another dollar sign, to the
 * initial field name. So, with a field called “name” and player identifier of 3, the field identifier will
 * be “name$3$”, and for a global field called “count” (player identifier is -Infinity), the final field
 * identifier will be “count$$”.
 *
 * This class manages the conversions between the two ways of identifing the fields.
 */
export default class FormFieldIdentifier {
  /**
   * Indicate if the given identifier was valid. Other values may not be used if false.
   */
  public readonly valid: boolean

  /**
   * The identifier of the player, the NPC or global (-Infinity).
   */
  public readonly player: number

  /**
   * The field name.
   */
  public readonly name: string

  /**
   * The field identifier.
   */
  public readonly identifier: string

  /**
   * Create a new form field identifier from a field identifier.
   *
   * @param identifier - The field identifier (containing both player identifier and field name).
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public constructor(identifier: string)

  /**
   * Create a new form field identifier from both player identifier and field name.
   *
   * @param player - The player identifier, or undefined for global field.
   * @param field - The field name.
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public constructor(player: number, field: string)

  /**
   * Constructor implementation.
   *
   * @param idOrPlayer - Field or player identifier.
   * @param field - Field name.
   */
  public constructor(idOrPlayer: string | number, field?: string) {
    if (typeof idOrPlayer === 'string') {
      this.identifier = idOrPlayer
      const lastSeparator = idOrPlayer.lastIndexOf('$', idOrPlayer.length - 2)
      if (idOrPlayer.endsWith('$') && lastSeparator >= 0) {
        this.name = idOrPlayer.substring(0, lastSeparator)
        const player: string = idOrPlayer.substring(lastSeparator + 1, idOrPlayer.length - 1)
        this.player = player.length ? Number(player) : -Infinity
        this.valid = !isNaN(this.player)
      } else {
        this.name = ''
        this.player = NaN
        this.valid = false
      }
    } else {
      this.player = idOrPlayer
      this.name = field!
      const player = idOrPlayer === -Infinity ? '' : String(idOrPlayer)
      this.identifier = field! + '$' + player + '$'
      this.valid = true
    }
  }
}
