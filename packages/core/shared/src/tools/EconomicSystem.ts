/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { PureMessageKey } from './types'

/**
 * The basic data of an economical system.
 */
export default interface EconomicSystem<T extends import('intl-ts').Messages> {
  /**
   * A unique identifier, used to retrieve the economical system used for a given set.
   */
  readonly id: string

  /**
   * The language entry containing the name of the economic system.
   */
  readonly name: PureMessageKey<T>

  /**
   * The cost (in money unit) of a single low value.
   */
  readonly valueCost: number
}
