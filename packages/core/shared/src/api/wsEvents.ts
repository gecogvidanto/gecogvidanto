/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { HelpSheet } from '../ui'

/**
 * Events sent by the server.
 */
export interface WsServerEventDefinition {
  /**
   * Called when values help sheet may need to be updated. Parameters are:
   * - the help sheet,
   * - the factor multiplication for low value, which depends on value cost and technological break count.
   */
  valuesHelpSheet: [HelpSheet, number]
  /**
   * Called when money help sheet may need to be updated. Parameters are:
   * - the help sheet.
   */
  moneyHelpSheet: [HelpSheet]
}

/**
 * Names of available server events.
 */
export type WsServerEventName = keyof WsServerEventDefinition

/**
 * Events sent by the client.
 */
export interface WsClientEventDefinition {
  register: [string, WsServerEventName]
}

/**
 * Names of available client events.
 */
export type WsClientEventName = keyof WsClientEventDefinition

/**
 * The root path where web socket is open.
 */
export const wsRoot = 'ws'
