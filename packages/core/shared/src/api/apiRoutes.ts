/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { EmailCheck, Game, IdentifiedAddress, User } from '../business'
import { EconomicSystem, LocalizedOption, UnassembledMessages } from '../tools'
import { SerializableForm, HelpSheet } from '../ui'

/**
 * The captcha action types.
 */
export type CaptchaAction = 'userCreate'

/**
 * The API definition.
 */
export interface ApiDefinition {
  lang: {
    url: { langCode: string }
    data: undefined
    return: void
  }
  userSignIn: {
    url: undefined
    data: { email: string; password: string }
    return: User
  }
  userSignOut: {
    url: undefined
    data: undefined
    return: void
  }
  userCreate: {
    url: undefined
    data: User & { password: string; captchaToken: string }
    return: User
  }
  userSearch: {
    url: undefined
    data: { email?: string; name?: string }
    return: User[]
  }
  userRead: {
    url: { id: string }
    data: undefined
    return: User
  }
  userUpdate: {
    url: { id: string }
    data: User & { password?: string }
    return: User
  }
  userDelete: {
    url: { id: string }
    data: undefined
    return: void
  }
  emailValidate: {
    url: EmailCheck
    data: undefined
    return: void
  }
  economicSystemSearch: {
    url: undefined
    data: undefined
    return: EconomicSystem<any>[]
  }
  economicSystemRead: {
    url: { id: string }
    data: undefined
    return: EconomicSystem<any>
  }
  gameCreate: {
    url: undefined
    data: (IdentifiedAddress | { value?: string }) & {
      locationName?: string
      roundsPerSet: number
      roundLength: number
    }
    return: Game
  }
  gameUpdate: {
    url: { id: string }
    data: (IdentifiedAddress | { value?: string }) & {
      locationName?: string
      summary?: string
      closed?: true
    }
    return: Game
  }
  gameSearch: {
    url: undefined
    data: undefined
    return: Game[]
  }
  gameRead: {
    url: { id: string }
    data: undefined
    return: Game
  }
  gamePlayerAdd: {
    url: { id: string }
    data: { name: string }
    return: Game
  }
  gamePlayerUpdate: {
    url: { id: string; player: number }
    data: { name: string }
    return: Game
  }
  gamePlayerDelete: {
    url: { id: string; player: number }
    data: undefined
    return: Game
  }
  gameSetAdd: {
    url: { id: string }
    data: { ecoSysId: string }
    return: Game
  }
  gameNpcName: {
    url: { id: string; roundSet: number; character: number }
    data: undefined
    return: UnassembledMessages<any>[any]
  }
  gameHelpSheetValues: {
    url: { id: string }
    data: undefined
    return: HelpSheet
  }
  gameHelpSheetMoney: {
    url: { id: string }
    data: undefined
    return: HelpSheet
  }
  gameOptionsCreate: {
    url: { id: string }
    data: undefined
    return: LocalizedOption<any, any>[]
  }
  gameFormCreate: {
    url: { id: string }
    data: { optionId?: string }
    return: SerializableForm
  }
  gameFormSend: {
    url: { id: string }
    data: { [key: string]: string | number | boolean } & { optionId?: string }
    return: Game
  }
  gameTechnologicalBreak: {
    url: { id: string }
    data: undefined
    return: Game
  }
}

/**
 * The available methods.
 */
export const enum ApiMethod {
  Get = 'get',
  Delete = 'delete',
  Post = 'post',
  Put = 'put',
  Patch = 'patch',
}

const declaredApiRoutes: {
  readonly [name in keyof ApiDefinition]: {
    readonly method: ApiMethod
    readonly url: string
  }
} = {
  lang: {
    method: ApiMethod.Patch,
    url: 'lang/:langCode',
  },
  userSignIn: {
    method: ApiMethod.Post,
    url: 'user/sign-in',
  },
  userSignOut: {
    method: ApiMethod.Patch,
    url: 'user/sign-out',
  },
  userCreate: {
    method: ApiMethod.Post,
    url: 'user',
  },
  userSearch: {
    method: ApiMethod.Get,
    url: 'users/:email?/:name?',
  },
  userRead: {
    method: ApiMethod.Get,
    url: 'user/:id',
  },
  userUpdate: {
    method: ApiMethod.Put,
    url: 'user/:id',
  },
  userDelete: {
    method: ApiMethod.Delete,
    url: 'user/:id',
  },
  emailValidate: {
    method: ApiMethod.Get,
    url: 'email/:email/:token',
  },
  economicSystemSearch: {
    method: ApiMethod.Get,
    url: 'economicSystems',
  },
  economicSystemRead: {
    method: ApiMethod.Get,
    url: 'economicSystem/:id',
  },
  gameCreate: {
    method: ApiMethod.Post,
    url: 'game',
  },
  gameUpdate: {
    method: ApiMethod.Patch,
    url: 'game/:id',
  },
  gameSearch: {
    method: ApiMethod.Get,
    url: 'games',
  },
  gameRead: {
    method: ApiMethod.Get,
    url: 'game/:id',
  },
  gamePlayerAdd: {
    method: ApiMethod.Post,
    url: 'game/:id/player',
  },
  gamePlayerUpdate: {
    method: ApiMethod.Put,
    url: 'game/:id/player/:player',
  },
  gamePlayerDelete: {
    method: ApiMethod.Delete,
    url: 'game/:id/player/:player',
  },
  gameSetAdd: {
    method: ApiMethod.Post,
    url: 'game/:id/set',
  },
  gameNpcName: {
    method: ApiMethod.Get,
    url: 'game/:id/set/:roundSet/npc/:character',
  },
  gameHelpSheetValues: {
    method: ApiMethod.Get,
    url: 'game/:id/helpsheet/values',
  },
  gameHelpSheetMoney: {
    method: ApiMethod.Get,
    url: 'game/:id/helpsheet/money',
  },
  gameOptionsCreate: {
    method: ApiMethod.Get,
    url: 'game/:id/options',
  },
  gameFormCreate: {
    method: ApiMethod.Get,
    url: 'game/:id/form',
  },
  gameFormSend: {
    method: ApiMethod.Post,
    url: 'game/:id/form',
  },
  gameTechnologicalBreak: {
    method: ApiMethod.Post,
    url: 'game/:id/techBreak',
  },
}

export type ApiRoutes = {
  [P in keyof ApiDefinition]: {
    readonly method: ApiMethod
    readonly fullUrl: string
    readonly subUrl: string
  }
}

/**
 * The root of the API.
 */
export const apiRoot = 'api'

/**
 * The routes for the API.
 */
const apiRoutes: Readonly<ApiRoutes> = Object.keys(declaredApiRoutes).reduce((routes, route) => {
  const routeName = route as keyof ApiDefinition
  routes[routeName] = {
    method: declaredApiRoutes[routeName].method,
    fullUrl: `/${apiRoot}/${declaredApiRoutes[routeName].url}`,
    subUrl: `/${declaredApiRoutes[routeName].url}`,
  }
  return routes
}, {} as ApiRoutes)

export default apiRoutes
