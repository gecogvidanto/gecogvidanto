/*
 * This file is part of @gecogvidanto/shared.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback */
import { ApiDefinition, ApiMethod, apiRoutes } from '.'

/* eslint-disable @typescript-eslint/ban-types */
/**
 * This will not compile if interface does not conform to signature.
 */
export interface RoutesType extends ApiDefinition {
  readonly [route: string]: {
    readonly url: object | undefined
    readonly data: object | undefined
    readonly return: any
  }
}
/* eslint-enable @typescript-eslint/ban-types */

describe('apiRoutes', function () {
  it('must not define twice same url and method', function () {
    const seen: { [key: string]: { url: string; method: ApiMethod } } = {}
    for (const route in apiRoutes) {
      const routeName = route as keyof ApiDefinition
      for (const key in seen) {
        if (
          seen[key].url === apiRoutes[routeName].subUrl &&
          seen[key].method === apiRoutes[routeName].method
        ) {
          throw new Error(`Both keys ${route} and ${key} defines same URL and method`)
        }
      }
      seen[route] = {
        url: apiRoutes[routeName].subUrl,
        method: apiRoutes[routeName].method,
      }
    }
  })
})
