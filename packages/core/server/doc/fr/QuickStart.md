# Guide de démarrage rapide

L'installation peut aussi bien se faire sur un serveur connecté à Internet et fonctionnant en 24/24 7/7 que sur un petit ordinateur individuel. Dans le premier cas, il permettra aux animateurs de se connecter depuis n'importe où pour créer et animer une partie. Dans le second cas, il peut permettre à un individu de gérer des parties sur son poste en connectant un navigateur à l'adresse `https://localhost`. Dans les deux cas, l'installation requiert les mêmes étapes.

# Installer l'application

Pour installer l'application :

1. Récupérez l'archive contenant le serveur et copiez-là dans le répertoire de votre choix.
2. Décompressez l'archive. Cela va créer un répertoire `package` que vous pouvez renommer à votre convenance.
3. Allez dans ce répertoire et installez les dépendances nécessaire à l'aide de la commande `npm install --production` ou `yarn install --production`.

# Installer des extensions

Vous devrez ensuite installer quelques extensions. Il vous faudra au minimum une extension « base de données » et deux systèmes économiques. Vous pourrez également installer d'autres systèmes économiques ou des extensions linguistiques. Par exemple, avec `npm` :

```bash
$ npm install @gecogvidanto/plugin-nedb
$ npm install @gecogvidanto/plugin-basesys
$ npm install @gecogvidanto/plugin-locale-fr
```

Si vous préférez utiliser `yarn` :

```bash
$ yarn add @gecogvidanto/plugin-nedb
$ yarn add @gecogvidanto/plugin-basesys
$ yarn add @gecogvidanto/plugin-locale-fr
```

# Configurer

Le serveur ne répond qu'à des requêtes HTTPS. Vous aurez besoin de fournir un certificat SSL avec sa clé privée afin de faire fonctionner la sécurité. Si vous n'en avez pas, rechercher sur Internet pour un exemple de « comment créer un certificat auto-signé » (utiliser OpenSSL sera probablement la méthode la plus simple). Alternativement, si vous voulez créer un serveur publique avec une certification sûre, automatique et gratuite, vous pouvez regarder https://letsencrypt.org/.

Le serveur requiert un fichier de configuration. Regardez [le document spécifique](Config.md) pour savoir comment écrire ce fichier.

Référez-vous à la documentation de chaque extension pour voir ses options de configurations particulières.

Un fichier de configuration typique pour utiliser l'application uniquement localement en tant qu'utilisateur simple pourrait être :

```yaml
server:
  port: 8443
  keyFile: /home/user/tls/host.key
  certFile: /home/user/tls/host.cert
```

Dans cet exemple, le port utilisé est le 8443 afin de ne pas nécessiter d'être un superutilisateur. Les autres entrées indiquent où se trouvent les fichiers de clé et de certificat.

# Démarrer

Démarrer l'application se fait depuis le répertoire où le serveur est installé. Il s'agit du répertoire contenant le fichier `package.json`.

Lorsque tout est prêt, vous pouvez démarrer le serveur :

```bash
$ npm start
```

Si vous préférez utiliser `yarn` :

```bash
$ yarn start
```

Si le fichier de configuration se trouve dans un autre répertoire, précisez-le en argument :

```
$ yarn start C:\GG\Config.json
```

# Utiliser

Ouvrez un navigateur à l'adresse du serveur. Si vous avez un fichier de configuration similaire à celui donné en exemple ci-dessus, cette adresse devrait être https://localhost:8443.

Lorsque vous vous connectez à une nouvelle installation, vous devriez vous autentifier en tant qu'administrateur :

- `courriel`: admin@gecogvidanto.org
- `mot de passe`: admin\*

N'oubliez pas de modifier ce mot de passe !

Référez-vous à la documentation du client que vous souhaitez utiliser. La documentation du client web (client par défaut lorsque vous vous connectez au serveur) se trouve avec le paquet `@geconomicus/client-web`.
