# Guide de configuration

L'application requiert un fichier de configuration pour fonctionner. Ce fichier est chargé par [confinode](https://github.com/slune-org/confinode/blob/master/doc/fr/usermanual.md). La liste des noms de fichiers recherchés par défaut est augmentée de:

- `/etc/gecogvidanto/gecogvidanto.conf`, au format JSON ou YAML ;
- `/etc/gecogvidanto.conf`, au format JSON ou YAML ;
- `/etc/gecogvidanto/gecogvidanto.*`, avec n'importe quelle extension supportée ;
- `/etc/gecogvidanto.*`, avec n'importe quelle extension supportée ;

Veuillez vous référer au [manuel utilisateur de confinode](https://github.com/slune-org/confinode/blob/master/doc/fr/usermanual.md#recherche-de-la-configuration) pour la liste des noms de fichiers par défaut et les extensions supportées. Il est également possible de spécifier un autre nom de fichier au démarrage de l'application :

```bash
$ npm run start ./config.json
```

# Serveur

L'entrée `server` contrôle la façon dont le serveur HTTPS fonctionnera :

- `host` permet d'indiquer l'interface que le serveur va écouter. Si cette valeur n'est pas précisée, le serveur écoute sur toutes les interfaces.
- `port` est le numéro de port que le serveur va écouter. Par défaut : `443` (port HTTPS par défaut).
- `keyFile` est le fichier contenant la clé privée utilisée pour les communications sécurisées (HTTPS). **Obligatoire** (vous aurez peut-être besoin de générer un certificat auto-signé).
- `certFile` est le fichier contenant le certificat utilisé pour les communications sécurisées (HTTPS). **Obligatoire** (vous aurez peut-être besoin de générer un certificat auto-signé).
- `remoteHost` permet d'indiquer le nom de l'hôte visible par les utilisateurs. Par défaut, c'est la valeur de `host` qui sera utilisée. Si `host` est également vide, la valeur `localhost` sera utilisée. Pour un serveur utilisable à distance, au moins l'une des options `host` ou `remoteHost` devrait être précisée.
- `remotePort` est le numéro de port visible par les utilisateurs (utile en cas de serveur mandataire inverse). Par défaut, prend la valeur de `port`.

# Sécurité

L'entrée `security` permet de fixer les paramètres de sécurité :

- `sessionKey` est une clé utilisée pour signer les cookies de session. Une clé aléatoire est calculée si aucune n'est fournie.
- `maxFailures` est le nombre maximum de tentatives continues de connexions échouées avant l'invalidation du compte de l'utilisateur. Un utilisateur avec un compte invalide ne peut que demander un nouveau mot de passe à l'aide de son adresse électronique. La valeur par défaut est `3`.
- `validity` est une rubrique contenant les durées maximales de validité. Elle peut contenir :
  - `session` est la durée maximale en secondes d'une session inactive. Cette valeur peut être `0` pour une session qui n'expire jamais. Par défaut : `14400` (4 heures).
  - `email` est le nombre maximum de secondes qu'un utilisateur peut prendre pour valider une nouvelle adresse électronique. La valeur par défaut est `86400` (24 heures).

# Définition du Captcha

L'entrée `reCaptcha` contient les paramètres pour le contrôle du ReCaptcha Google :

- `siteKey` est la clé (publique) du site, utilisée par le client. La valeur par défaut est vide, désactivant le Captcha. Si dans ce cas la clé secrète est fournie, cela empêchera la création de nouveaux utilisateurs.
- `secretKey` est la clé secrète qui ne devrait jamais être envoyée au client. La valeur par défaut est vide, désactivant totalement le controle.
- `threshold` est la valeur de seuil en pourcentage en dessous duquel la requête est rejetée. La valeur par défaut is `50`.

# Courriel

L'entrée `email` controle la façon dont le serveur envoie des courriers électroniques :

- `from` indique d'où viennent les courriels. Par défaut : `"ĞecoĞvidanto server" <no-reply@gecogvidanto.org>`.
- `prefix` est une chaine de caractères ajoutée en début des sujets des courriels. Vide par défaut.
- `config` est la [configuration du transporteur](https://nodemailer.com/usage/), comme attendu par _Nodemailer_. **Obligatoire**

# Journalisation

L'entrée `logging` est utilisée pour indiquer les paramètres de journalisation. Un journal par défaut est créé pour afficher les messages dans la console, mais d'autres peuvent être ajoutés. Les journaux de ĞecoĞvidanto sont basés sur les niveaux _npm_ (voir [ici](https://github.com/winstonjs/winston#logging-levels) pour plus d'informations).

- `level` est le niveau minimum que doit avoir un message pour être affiché par le journal par défaut, ou `silent` pour totalement couper l'affichage par défaut. Par défaut: `info`.
- `color` indique si les couleurs doivent être utilisées par le journal par défaut. Le défaut est `true` (vrai).
- L'entrée `loggers` est un tableau de journaux additionnels.

## Journaux

L'entrée `loggers` peut être utilisée pour paramétrer des journaux additionnels optionnels. Chaque journal peut avoir les propriétés suivantes :

- `type` pour le type de transport utilisé. Les valeurs actuellement supportées sont `console` ou `file` (fichier). Cette valeur est **obligatoire**.
- `level` pour le niveau minimum pour qu'un message soit affiché par le journal. Par défaut : `info`.
- `strict` pour indiquer si le journal doit uniquement gérer les message du niveau donné, et ainsi exclure les messages de niveau inférieur. La valeur par défaut est `false` (faux).
- `formats` peut optionnellement être utilisé pour ajouter des informations aux messages. Voir [ici](https://github.com/winstonjs/logform) pour plus d'informations sur les formats. Les formats actuellement supportés sont :
  - `align`.
  - `colorize`, avec les options booléennes `all` (défaut: `false`), `level` (défaut: `true`) et `message` (défaut: `false`).
  - `label`, avec l'option chaine de caractère `label` (**obligatoire**) et l'option booléenne `message` (défaut: `true`).
  - `padLevels`, avec l'option chaine de caractères `filler` (défaut: espace).
  - `timestamp`, avec l'option chaine de caractères facultative `format`.
- `output` permet de paramétrer la sortie, en utilisant les informations du message, y compris celles ajoutées par l'entrée `formats`. Deux propriétés sont attendues :
  - `type` est le type de sortie utilisée, soit `json`, soit `printf`. Le défaut est `printf`.
  - `format` est le format utilisé pour la sortie. Si `type` est `json`, cela peut être un nombre indiquant le nombre d'espace à utiliser pour l'indentation, ou une chaine indiquant les caractères à utiliser pour l'indentation. Si `type` est `printf`, il s'agit de la chaine de caractères à afficher, qui peut contenir des valeurs de messages avec cette syntaxe : `${messageValue}`. La valeur par défaut est `${message}`.
- `options` pour les options supplémentaires à donner au transport. Voir [ici](https://github.com/winstonjs/winston/blob/master/docs/transports.md#winston-core) pour les informations sur les options de transport.

# Parties

L'entrée `game` définie le comportement des parties. Elle peut contenir les propriétés suivantes :

- `maxPlayers` qui contient le nombre maximum de joueurs actifs dans une partie. Le défaut est `20`.

# Extensions

L'entrée `plugins` contient le paramétrage des extensions installées. Référez-vous à la documentation des extensions pour plus d'informations.

# Exemple

```yaml
server:
  host: gecogvidanto.org
  keyFile: /etc/gecogvidanto/tls/host.key
  certFile: /etc/gecogvidanto/tls/host.cert
security:
  validity:
    session: 3600
email:
  config:
    host: localhost
    port: 25
logging:
  loggers:
    - type: console
      level: warning
      strict: true
      formats:
        align: {}
        label:
          label: WARN
        timestamp: {}
      output:
        format: >-
          ${timestamp}: ${level} ${message}
plugins:
  nedb:
    directory: /var/lib/gecogvidanto
```
