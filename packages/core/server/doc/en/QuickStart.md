# Quick start guide

Installation can either be made on a server connected to the Internet and powered 24-7 or on a small personnal computer. In the first case, game masters will be able to connect from anywhere in order to create and master a game. In the latter case, individual will be able to manage games on its personnal computer, connecting through a browser at URL `https://localhost`. In both cases, installation requires same steps.

# Install application

In order to install application:

1. Get the archive containing the server and copy it in the folder of your choice.
2. Uncompress archive. It will create a directory called `package` which you can rename at your will.
3. Go in this directory and install needed dependencies with the command `npm install --production` or `yarn install --production`.

# Install plugins

You must then install some plugins. You will need at least a “Database” plugin and two economic systems. You also may install other economic systems or linguistic plugins. For example, with `npm`:

```bash
$ npm install @gecogvidanto/plugin-nedb
$ npm install @gecogvidanto/plugin-basesys
$ npm install @gecogvidanto/plugin-locale-fr
```

If you prefer to use `yarn`:

```bash
$ yarn add @gecogvidanto/plugin-nedb
$ yarn add @gecogvidanto/plugin-basesys
$ yarn add @gecogvidanto/plugin-locale-fr
```

# Configure

Server only responds to HTTPS requests. You will need to provide an SSL certificate and its private key in order to make the security work. If you don't have any, search the web for an example on “how to create a self-signed certificate” (using OpenSSL will probably be the simplest method). Alternatively, if you want to create a public server with a safe, automatic and free certification, you can have a look at https://letsencrypt.org/.

Server requires a configuration file. Read [the specific document](Config.md) to know how to write this file.

See documentation of each plugin to see its specific configuration options.

A typical configuration file to use the application only locally and as a simple user would be:

```yaml
server:
  port: 8443
  keyFile: /home/user/tls/host.key
  certFile: /home/user/tls/host.cert
```

In this example, the used port is 8443 in order not to require to be superuser. The other entries are where the key and certificate files are located.

# Start up

Starting the application is made from the directory where the server is installed. This is the directory containing the file `package.json`.

When everything is ready, you can start the server:

```bash
$ npm start
```

If you prefer to use `yarn`:

```bash
$ yarn start
```

If the configuration file is in another directory, indicate it as a parameter:

```
$ yarn start C:\GG\Config.json
```

# Use

Open a browser to the server address. If you have a configuration file looking like the one given in example above, this address should be https://localhost:8443.

When you connect to a new installation, you should authenticate as the administrator:

- `e-mail`: admin@gecogvidanto.org
- `password`: admin\*

Do not forget to change this password!

See the documentation of the client that you which to use. The documentation of the web client (default client when you connect to server) is with the `@geconomicus/client-web` package.
