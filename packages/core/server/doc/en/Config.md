# Configuration guide

Application requires a configuration file. This file is loaded by [confinode](https://github.com/slune-org/confinode/blob/master/doc/en/usermanual.md). The default searched file name list is augmented with:

- `/etc/gecogvidanto/gecogvidanto.conf`, in JSON or YAML format;
- `/etc/gecogvidanto.conf`, in JSON or YAML format;
- `/etc/gecogvidanto/gecogvidanto.*`, with any supported extension;
- `/etc/gecogvidanto.*`, with any supported extension.

Please refer to [confinode user manual](https://github.com/slune-org/confinode/blob/master/doc/en/usermanual.md#configuration-search) for the default file name list and supported extensions. It is also possible to specify another file name when starting the application:

```bash
$ npm run start ./config.json
```

# Server

The `server` entry controls how the HTTPS server will work:

- `host` is used to specify the interface the server will listen to. If not specified, the server will listen to all interfaces.
- `port` is the port number the server will be listening to. Default: `443` (default HTTPS port).
- `keyFile` is the file containing the private key used for secured (HTTPS) communications. **Mandatory** (you may need to generate a self-signed certificate).
- `certFile` is the file containing the certificate used for secured (HTTPS) communications. **Mandatory** (you may need to generate a self-signed certificate).
- `remoteHost` is used to specify the host name visible by the users. By default, `host` value is used. If `host` is also empty, `localhost` will be used. For a remote usable server, at least one of `host` or `remoteHost` options should be specified.
- `remotePort` is the port number visible by users (useful in case of reverse proxy). By default, takes the value of `port`.

# Security

The `security` entry is used to set the security parameters:

- `sessionKey` is a key used to sign session cookie. A random key is calculated if none provided.
- `maxFailures` is the maximum number of continuous failed tries before the user account gets invalid. A user with an invalid account can only require a new password through its e-mail address. Default is `3`.
- `validity` is used to set the maximum validity periods. It can contain:
  - `session` is the maximum age of an idle session in seconds. This value may be `0` for a never expiring session. Default: `14400` (4 hours).
  - `email` is the number of seconds a user can take to validate a new e-mail address. Default is `86400` (24 hours).

# Captcha definition

The `reCaptcha` entry contains parameters for Google ReCaptcha control:

- `siteKey` is the (public) key of the site, used at client side. Defaults to empty string which will deactivate client captcha. If, in this case, the secret key is provided, this will prevent new user creation.
- `secretKey` is the secret key which should never be sent to client. Defaults to empty string which will deactivate server control.
- `threshold` is the threshold in percentage under which a query is rejected. Default is `50`.

# Email

The `email` entry controls how e-mails can be sent by the server:

- `from` indicates where the e-mail comes from. Default: `"ĞecoĞvidanto server" <no-reply@gecogvidanto.org>`.
- `prefix` is a string appended to the subject of the e-mails. Empty by default.
- `config` is the [transport configuration](https://nodemailer.com/usage/) as expected by _Nodemailer_. **Mandatory**.

# Logging

The `logging` entry is used to set the logging parameters. A default logger is created to display messages in the console, but others can be added. ĞecoĞvidanto loggers are based in _npm_ levels (see [here](https://github.com/winstonjs/winston#logging-levels) for more information).

- `level` is the minimum level for a message to be displayed by the default logger, or `silent` to totally mute default logger. Default: `info`.
- `color` indicates if colors should be used with default logger. Default is `true`.
- The `loggers` entry is an array of additional loggers.

## Loggers

The `loggers` entry may be used to set optional additional loggers. Each logger may have the following properties:

- `type` for the type of the transport used. Currently supported values are `console` or `file`. This value is **mandatory**.
- `level` for the minimum level for a message to be displayed by the logger. Default: `info`.
- `strict` to indicate if the logger should only care of messages of the given level, and so exclude messages of lower level. Default is `false`.
- `formats` can optionally be used to add information to the messages. See [here](https://github.com/winstonjs/logform) for more information on formats. Supported formats are currently:
  - `align`.
  - `colorize`, with the boolean options `all` (default: `false`), `level` (default: `true`) and `message` (default: `false`).
  - `label`, with the string option `label` (**required**) and the boolean option `message` (default: `true`).
  - `padLevels`, with the string option `filler` (default: space).
  - `timestamp`, with the optional string option `format`.
- `output` can be set to customize the output, using message information, including the one added by the `formats` entry. Two properties are expected:
  - `type` is the type of the output used, either `json` or `printf`. Default is `printf`.
  - `format` is the format used for the output. If `type` is `json`, it may be a number indicating the count of spaces to use for indentation, or a string indicating the characters to use for indentation. If `type` is `printf`, it is the string to be displayed, which may contain message values using this syntax: `${messageValue}`. The default value is `${message}`
- `options` for the additional options to give to transport. See [here](https://github.com/winstonjs/winston/blob/master/docs/transports.md#winston-core) for information on transport options.

# Games

The `game` entry defines the behavior of the games. It can contain the following properties:

- `maxPlayers` which contains the maximum count of active players in a single game. Default is `20`.

# Plugins

The `plugins` entry contains values depending on installed plugins. See plugins documentation for more information.

# Example

```yaml
server:
  host: gecogvidanto.org
  keyFile: /etc/gecogvidanto/tls/host.key
  certFile: /etc/gecogvidanto/tls/host.cert
security:
  validity:
    session: 3600
email:
  config:
    host: localhost
    port: 25
logging:
  loggers:
    - type: console
      level: warning
      strict: true
      formats:
        align: {}
        label:
          label: WARN
        timestamp: {}
      output:
        format: >-
          ${timestamp}: ${level} ${message}
plugins:
  nedb:
    directory: /var/lib/gecogvidanto
```
