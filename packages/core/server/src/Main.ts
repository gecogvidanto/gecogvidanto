/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import Intl, { LanguageMap } from 'intl-ts'
import * as winston from 'winston'

import { langType as uiLangType, messages as uiMessages } from '@gecogvidanto/client-web'
import {
  Database,
  DatabasePlugin,
  EconomicSystem,
  GecoPluginType,
  ModelTypeList,
  isDatabasePlugin,
  isEconomicSystemPlugin,
} from '@gecogvidanto/plugin'

import AppData, { LoadedPlugin, ServerInfo, app } from './AppData'
import { Config, ConfigBuilder, configureLogger } from './config'
import { ServerModels, serverModels } from './database'
import Initializer, { InitializerData, RawPlugin } from './Initializer'
import { Server } from './server'
import { Mailer, langType, messages } from './tools'

/**
 * The main class, configuring and starting-up the server.
 */
export default class Main implements AppData {
  private readonly initialData: Promise<InitializerData>
  private rawPlugins: ReadonlyArray<RawPlugin> = []
  private _serverInfo?: ServerInfo
  private _serverLang?: Intl<langType>
  private _clientLang?: Intl<langType>
  private _uiLangMap?: LanguageMap<uiLangType>
  private _config?: Config
  private _mailer?: Mailer
  private _dbModels?: ModelTypeList<ServerModels>
  private _database?: Database<ServerModels>
  private _economicSystems?: ReadonlyArray<EconomicSystem<langType>>

  public constructor() {
    configureLogger()
    const initializer = new Initializer()
    this.initialData = initializer.data
    app.data = this
  }

  public get serverInfo(): ServerInfo {
    return this.tryGetInternal('serverInfo', this._serverInfo)
  }

  public get plugins(): ReadonlyArray<LoadedPlugin> {
    return this.rawPlugins.map(plugin => ({
      name: plugin.name,
      description: plugin.type.description,
      plugin: this.tryGetInternal('plugins', plugin.instance),
    }))
  }

  public get serverLang(): Intl<langType> {
    return this.tryGetInternal('serverLang', this._serverLang)
  }

  public get clientLang(): Intl<langType> {
    return this.tryGetInternal('clientLang', this._clientLang)
  }

  public get uiLangMap(): LanguageMap<uiLangType> {
    return this.tryGetInternal('uiLangMap', this._uiLangMap)
  }

  public get config(): Config {
    return this.tryGetInternal('config', this._config)
  }

  public get mailer(): Mailer {
    return this.tryGetInternal('mailer', this._mailer)
  }

  public get database(): Database<ServerModels> {
    return this.tryGetInternal('database', this._database)
  }

  public get economicSystems(): ReadonlyArray<EconomicSystem<langType>> {
    return this.tryGetInternal('economicSystems', this._economicSystems)
  }

  public async start(): Promise<void> {
    try {
      const initialData = await this.initialData
      this.rawPlugins = initialData.rawPlugins
      this._serverInfo = initialData.serverInfo
      if (
        this.initLangs() &&
        (await this.createConfig()) &&
        this.initLogger() &&
        this.initModels() &&
        this.initMailer() &&
        this.initPlugins() &&
        this.initDatabase() &&
        this.initEconomicSystems() &&
        (await this.startAll())
      ) {
        const res = await this.serveRequests()
        // eslint-disable-next-line no-process-exit
        process.exit(res ? 0 : 1)
      } else {
        // eslint-disable-next-line no-process-exit
        process.exit(1)
      }
    } catch (error) {
      winston.error(error.message || String(error))
      // eslint-disable-next-line no-process-exit
      process.exit(1)
    }
  }

  private tryGetInternal<T>(name: keyof Main, variable: T | undefined): T {
    if (!variable) {
      throw new Error(`Internal error: ${name} is not initialized yet`)
    }
    return variable
  }

  private initLangs(): boolean {
    winston.debug('Loading language strings')
    const pluginTypes: GecoPluginType[] = this.rawPlugins.map(plugin => plugin.type)
    const langMap: LanguageMap<langType> = pluginTypes.reduce(
      (languageMap, plugin) => languageMap.merge(plugin.messages),
      new LanguageMap(messages, 'en')
    )
    this._clientLang = new Intl(langMap)
    this._serverLang = new Intl(this._clientLang, [process.env.LANG || ''])
    this._uiLangMap = pluginTypes.reduce(
      (languageMap, plugin) => languageMap.merge(plugin.uiMessages),
      new LanguageMap(uiMessages, 'en')
    )
    return true
  }

  private async createConfig(): Promise<boolean> {
    winston.debug(this.serverLang.initPhaseConfiguration())
    const configBuilder = this.rawPlugins
      .filter(plugin => !!plugin.type.configuration)
      .reduce(
        (previous, plugin) => previous.addPluginDescription(plugin.name, plugin.type.configuration!),
        new ConfigBuilder()
      )
    const readConfig = await configBuilder.build()
    if (readConfig) {
      this._config = readConfig
    }
    return !!readConfig
  }

  private initLogger(): boolean {
    winston.debug(this.serverLang.initPhaseLogger())
    configureLogger(this.config.logging)
    return true
  }

  private initModels(): boolean {
    winston.debug(this.serverLang.initPhaseModels())
    this._dbModels = { ...serverModels }
    this.rawPlugins
      .map(plugin => plugin.type)
      .forEach(pluginType => {
        Object.assign(this._dbModels, pluginType.models)
      })
    return true
  }

  private initMailer(): boolean {
    winston.debug(this.serverLang.initPhaseMailer())
    this._mailer = new Mailer()
    return true
  }

  private initPlugins(): boolean {
    winston.debug(this.serverLang.initPhasePlugins())
    this.rawPlugins.forEach(plugin => (plugin.instance = new plugin.type(this as any)))
    return true
  }

  private initDatabase(): boolean {
    winston.debug(this.serverLang.initPhaseDatabase())
    const databasePlugins: DatabasePlugin[] = []
    this.rawPlugins.forEach(plugin => {
      const instance = plugin.instance!
      if (isDatabasePlugin(instance)) {
        databasePlugins.push(instance)
        this.serverInfo.plugins = [
          ...this.serverInfo.plugins,
          {
            module: plugin.module,
            name: plugin.name,
            description: plugin.type.description,
            version: plugin.data.version,
            database: true,
            ecoSysIds: [],
          },
        ]
      }
    })

    if (databasePlugins.length === 0) {
      winston.error(this.serverLang.databaseNoPlugin())
      return false
    } else if (databasePlugins.length > 1) {
      winston.error(this.serverLang.pluginMultipleDatabase())
      return false
    }

    this._database = databasePlugins[0].openDatabase(this._dbModels!)
    return true
  }

  private initEconomicSystems(): boolean {
    winston.debug(this.serverLang.initPhaseEconomicSystems())
    const economicSystems: EconomicSystem<langType>[] = []
    this.rawPlugins.forEach(plugin => {
      const instance = plugin.instance!
      if (isEconomicSystemPlugin(instance)) {
        const instanceEconomicSystems = instance.openEconomicSystems()
        economicSystems.push(...instanceEconomicSystems)
        this.serverInfo.plugins = [
          ...this.serverInfo.plugins,
          {
            module: plugin.module,
            name: plugin.name,
            description: plugin.type.description,
            version: plugin.data.version,
            database: false,
            ecoSysIds: instanceEconomicSystems.map(ecoSys => ecoSys.id),
          },
        ]
      }
    })

    if (economicSystems.length < 2) {
      winston.error(this.serverLang.economicSystemNotEnough())
      return false
    }

    this._economicSystems = economicSystems
    return true
  }

  private async startAll(): Promise<boolean> {
    winston.debug(this.serverLang.initPhaseStart())
    // Terminate server information
    this.serverInfo.plugins = [
      ...this.serverInfo.plugins,
      ...this.rawPlugins
        .filter(
          plugin => this.serverInfo.plugins.findIndex(registered => registered.module === plugin.module) < 0
        )
        .map(plugin => ({
          module: plugin.module,
          name: plugin.name,
          description: plugin.type.description,
          version: plugin.data.version,
          database: false,
          ecoSysIds: [],
        })),
    ]

    // Controls
    const { reCaptcha } = this.config
    if (!!reCaptcha.siteKey && !reCaptcha.secretKey) {
      winston.error(this.serverLang.reCaptchaConfigError())
    } else if (!reCaptcha.siteKey && !!reCaptcha.secretKey) {
      winston.warn(this.serverLang.reCaptchaPreventNewUsers())
    }

    // Wait for everything is ready
    try {
      if (!(await this.mailer.usable)) {
        winston.warn(this.serverLang.mailerNotConfigured())
      }
      await this.database.waitForStarted()
      await Promise.all(this.rawPlugins.map(plugin => plugin.instance!.ready()))
      return true
    } catch (error) {
      winston.error(error.message || String(error))
      return false
    }
  }

  private async serveRequests(): Promise<boolean> {
    const { host, port } = this.config.server
    const server = new Server()
    try {
      await server.listen(port, host)
      await new Promise(resolve => {
        async function close(): Promise<void> {
          await server.close()
          resolve()
        }
        process.once('SIGINT', close)
        process.once('SIGTERM', close)
      })
      winston.info(this.serverLang.terminated())
      return true
    } catch (error) {
      winston.error(error.message || String(error))
      return false
    }
  }
}
