/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { randomBytes } from 'crypto'
import { RequestHandler } from 'express'

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Express {
    interface Request {
      nonce?: string
    }
  }
}

/**
 * Generate the nonce to be used by script and style.
 *
 * @param req - The request.
 * @param _ - (unused).
 * @param next - The next handler.
 */
export const nonceGenerator: RequestHandler = (req, _, next) => {
  req.nonce = randomBytes(16).toString('hex')
  next()
}
