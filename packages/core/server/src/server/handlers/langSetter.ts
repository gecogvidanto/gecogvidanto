/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { RequestHandler } from 'express'

import { app } from '../../AppData'

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Express {
    interface Request {
      uiLangPreferences?: string[]
    }
  }
}

/**
 * Set the internal language parameter.
 *
 * @param req - The request.
 * @param _ - (unused).
 * @param next - The next handler.
 */
export const langSetter: RequestHandler = (req, _, next) => {
  const languages: undefined | string | string[] = req.session!.lang || req.headers['accept-language']
  let preferences: string[] = []
  if (languages) {
    preferences = typeof languages === 'string' ? languages.split(',') : languages
  }
  req.uiLangPreferences = preferences.map(lang => lang.split(';')[0].trim())
  app.data.clientLang.$changePreferences(req.uiLangPreferences, false)
  next()
}
