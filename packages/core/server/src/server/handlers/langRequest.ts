/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { RequestHandler } from 'express'

import { langRoutes } from '@gecogvidanto/client-web'

import { app } from '../../AppData'

/**
 * Analyse if URL contains a requested language.
 *
 * @param req - The request.
 * @param _ - (unused).
 * @param next - The next handler.
 */
export const langRequest: RequestHandler = (req, _, next) => {
  const lang: string = req.params[langRoutes.param]
  if (app.data.uiLangMap.availables.includes(lang)) {
    req.session!.lang = lang
  }
  next()
}
