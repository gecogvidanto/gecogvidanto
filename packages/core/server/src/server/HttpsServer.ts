/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as bodyParser from 'body-parser'
import * as cors from 'cors'
import * as csurf from 'csurf'
import * as express from 'express'
import { Express } from 'express'
import { readFileSync } from 'fs'
import * as helmet from 'helmet'
import { Server, createServer } from 'https'
import { Socket } from 'net'
import * as winston from 'winston'

import { externalSources, langRoutes, staticRoutes } from '@gecogvidanto/client-web'

import { ServerApi, apiPath } from '../api'
import { app as gapp } from '../AppData'
import Passport from '../Passport'
import { langRequest, langSetter, logger, nonceGenerator } from './handlers'
import PageServer from './PageServer'
import SocketServer from './SocketServer'

/*
 * Class managing the HTTPS server.
 */
export default class HttpsServer {
  private readonly server: Server
  private readonly socketServer: SocketServer

  public constructor() {
    const app: Express = express()

    // Prepare query
    app.use(logger)
    app.use(cors({ origin: false }))
    app.use(helmet({ referrerPolicy: { policy: 'same-origin' } }))
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: false }))

    // Configure static paths
    app.use(staticRoutes.url, express.static(staticRoutes.path))

    // Configure session
    const passport = new Passport()
    app.use(passport.handlers)
    app.use(csurf())

    // Configure lang
    app.use(langRoutes.url, langRequest)
    app.use(langSetter)

    // Configure API
    const serverApi = new ServerApi(passport)
    app.use(apiPath, serverApi.api)

    // Configure page router
    app.use(
      nonceGenerator,
      helmet.contentSecurityPolicy({
        directives: {
          defaultSrc: ["'self'"],
          scriptSrc: ["'self'", req => `'nonce-${req.nonce}'`, "'unsafe-eval'", ...externalSources.scripts],
          styleSrc: ["'self'", req => `'nonce-${req.nonce}'`, ...externalSources.styles],
          imgSrc: ["'self'", ...externalSources.images],
          connectSrc: ["'self'", ...externalSources.data],
        },
      })
    )
    const pageServer = new PageServer(serverApi)
    app.use(pageServer.onRequest)

    // Create server
    const { keyFile, certFile } = gapp.data.config.server
    this.server = createServer(
      {
        key: readFileSync(keyFile),
        cert: readFileSync(certFile),
      },
      app
    )
    this.socketServer = new SocketServer(this.server, serverApi)
  }

  public listen(port: number, hostname: string): Promise<void> {
    this.socketServer.listen()
    return new Promise((resolve, reject) => {
      this.server
        .listen(port, hostname || undefined)
        .once('listening', () => {
          winston.info(gapp.data.serverLang.serverRunning(hostname, port))
          resolve()
        })
        .once('error', (err: Error & { code: string }) => {
          if (err.code === 'EADDRINUSE') {
            reject(new Error(gapp.data.serverLang.serverPortInUse(port)))
          } else if (err.code === 'EACCES') {
            reject(new Error(gapp.data.serverLang.serverPortPermissions(port)))
          } else {
            reject(err)
          }
        })
        .on('connection', (socket: Socket) => {
          winston.debug(gapp.data.serverLang.serverNewConnection(socket.remoteAddress!))
        })
        .once('close', () => {
          winston.verbose(gapp.data.serverLang.serverStopped())
          this.socketServer.close()
        })
    })
  }

  public close(): Promise<void> {
    return new Promise(resolve => {
      this.server.close(() => {
        resolve()
      })
    })
  }
}
