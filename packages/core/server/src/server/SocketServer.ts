/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Server as HttpsServer } from 'https'
import * as createServer from 'socket.io'
import { Server, Socket } from 'socket.io'
import * as winston from 'winston'

import { EconomicSystem } from '@gecogvidanto/plugin'
import {
  HelpSheet,
  WsClientEventDefinition,
  WsClientEventName,
  WsServerEventDefinition,
  WsServerEventName,
  wsRoot,
} from '@gecogvidanto/shared'

import { ServerApi, EMPTY_HELP_SHEET, GameApiEvent, GameApiEventType } from '../api'
import { app } from '../AppData'
import { Game } from '../business'
import { langType } from '../tools'

/**
 * The connection event.
 */
const CONNECT_EVENT = 'connection'

/**
 * The disconnection event.
 */
const DISCONNECT_EVENT = 'disconnect'

/**
 * Manage the client incoming web socket.
 */
class WebSocket {
  /**
   * Create the socket manager.
   *
   * @param socket - The real socket.
   */
  public constructor(private readonly socket: Socket) {}

  /**
   * @returns The identifier of the socket.
   */
  public get id(): string {
    return this.socket.id
  }

  /**
   * Add a callback on disconnection event.
   *
   * @param callback - The callback to call when socket disconnected.
   */
  public onDisconnect(callback: () => void): void {
    this.socket.on(DISCONNECT_EVENT, callback)
  }

  /**
   * Emit a new event.
   *
   * @param event - The event name.
   * @param args - The event arguments.
   */
  public emit<T extends keyof WsServerEventDefinition>(
    event: T,
    ...args: WsServerEventDefinition[T]
  ): void {
    this.socket.emit(event, ...args)
  }

  /**
   * Listen to an event.
   *
   * @param event - The event to listen to.
   * @param callback - The function to execute on event.
   */
  public on<T extends keyof WsClientEventDefinition>(
    event: T,
    callback: (...args: WsClientEventDefinition[T]) => void
  ): void {
    this.socket.on(event, callback as (...args: any[]) => void)
  }
}

/**
 * Class managing the Web Socket server for listening to game changes.
 */
export default class SocketServer {
  /**
   * The inner server.
   */
  private readonly server: Server

  /**
   * Listeners are stored in a 3 dimension map in order to sort them by:
   * 1. Game identifier;
   * 1. Event name;
   * 1. Socket identifier.
   */
  private readonly listeners: Map<string, Map<WsServerEventName, Map<string, WebSocket>>> = new Map()

  public constructor(httpsServer: HttpsServer, private readonly serverApi: ServerApi) {
    this.server = createServer(httpsServer, { path: `/${wsRoot}` })
  }

  /**
   * Start the socket server.
   *
   * @returns `this`.
   */
  public listen(): this {
    this.serverApi.game.on(this.onGameChanged)
    this.server.on(CONNECT_EVENT, this.onConnect)
    return this
  }

  /**
   * Terminate the socket server.
   *
   * @returns `this`.
   */
  public close(): this {
    this.serverApi.game.off(this.onGameChanged)
    return this
  }

  /**
   * Callback for game changed event.
   *
   * @param e - The event.
   */
  private onGameChanged: (e: GameApiEvent) => Promise<void> = async e => {
    // Search for game listeners
    const gameListeners: Map<WsServerEventName, Map<string, WebSocket>> | undefined = this.listeners.get(
      e.game._id
    )
    if (!gameListeners || gameListeners.size === 0) {
      return
    }

    // Select action
    switch (e.type) {
      case GameApiEventType.NewRound:
        this.manageMoneyHelpSheet(e.game, gameListeners)
        break
      case GameApiEventType.TechBreak:
        this.manageValuesHelpSheet(e.game, gameListeners)
        break
      default:
        // eslint-disable-next-line no-case-declarations
        const eType: never = e.type
        throw new Error(`Unknown game API event ${eType}`)
    }
  }

  /**
   * Event for client connection received.
   *
   * @param realSocket - The real used socket.
   */
  private onConnect: (realSocket: Socket) => void = realSocket => {
    winston.debug(app.data.serverLang.serverNewWSConnection())
    const socket = new WebSocket(realSocket)
    socket.onDisconnect(() => {
      this.listeners.forEach(gameListeners =>
        gameListeners.forEach(eventListeners => eventListeners.delete(socket.id))
      )
    })
    new Array<WsClientEventName>('register').forEach(clientEvent => {
      switch (clientEvent) {
        case 'register':
          socket.on(clientEvent, (id: string, eventName: WsServerEventName) =>
            this.register(socket, id, eventName)
          )
          break
        default:
          // eslint-disable-next-line no-case-declarations
          const eName: never = clientEvent
          throw new Error(`Unknown WS client event ${eName}`)
      }
    })
  }

  /**
   * Register a new client as listener.
   *
   * @param socket - The web socket.
   * @param id - The game identifier.
   * @param eventName - Name of the event to register for.
   */
  private async register(socket: WebSocket, id: string, eventName: WsServerEventName): Promise<void> {
    // Prepare
    const game = await Game.find(id)
    winston.silly(app.data.serverLang.serverWSRegister(id, eventName))
    if (!game) {
      return
    }

    // Search or create listener map
    let gameListeners: Map<WsServerEventName, Map<string, WebSocket>> | undefined = this.listeners.get(id)
    if (!gameListeners) {
      gameListeners = new Map()
      this.listeners.set(id, gameListeners)
    }
    let eventListeners: Map<string, WebSocket> | undefined = gameListeners.get(eventName)
    if (!eventListeners) {
      eventListeners = new Map()
      gameListeners.set(eventName, eventListeners)
    }
    eventListeners.set(socket.id, socket)

    // Initialization
    switch (eventName) {
      case 'valuesHelpSheet':
        this.manageValuesHelpSheet(game, socket)
        break
      case 'moneyHelpSheet':
        this.manageMoneyHelpSheet(game, socket)
        break
      default:
        // eslint-disable-next-line no-case-declarations
        const eName: never = eventName
        winston.debug(app.data.serverLang.serverWSUnknown(eName))
    }
  }

  /**
   * Send message for values help sheet event.
   *
   * @param game - The game which has changed.
   * @param destinations - The destinations of the message.
   */
  private manageValuesHelpSheet(
    game: Game,
    destinations: Map<WsServerEventName, Map<string, WebSocket>> | WebSocket
  ): void {
    const realDestinations = this.extractDestinations(destinations, 'valuesHelpSheet')
    if (!realDestinations) {
      return
    }
    if (game.setInProgress) {
      const economicSystem: EconomicSystem<langType> = app.data.economicSystems.find(
        value => value.id === game.currentSet.ecoSysId
      )!
      const helpSheet: HelpSheet =
        game.currentSet.currentRound === 0 ? EMPTY_HELP_SHEET : game.buildValuesHelpSheet()
      const costFactor: number =
        economicSystem.valueCost * Math.pow(2, game.currentSet.techBreakRounds.length)
      realDestinations.forEach(socket => socket.emit('valuesHelpSheet', helpSheet, costFactor))
    }
  }

  /**
   * Send message for money help sheet event.
   *
   * @param game - The game which has changed.
   * @param destinations - The destinations of the message.
   */
  private async manageMoneyHelpSheet(
    game: Game,
    destinations: Map<WsServerEventName, Map<string, WebSocket>> | WebSocket
  ): Promise<void> {
    const realDestinations = this.extractDestinations(destinations, 'moneyHelpSheet')
    if (!realDestinations) {
      return
    }
    if (game.setInProgress) {
      const economicSystem: EconomicSystem<langType> = app.data.economicSystems.find(
        value => value.id === game.currentSet.ecoSysId
      )!
      const helpSheet: HelpSheet =
        game.currentSet.currentRound === 0 ? EMPTY_HELP_SHEET : await economicSystem.getMoneyHelpSheet(game)
      realDestinations.forEach(socket => socket.emit('moneyHelpSheet', helpSheet))
    }
  }

  /**
   * Extract the destinations of the event.
   *
   * @param listenersOrSocket - Either the game listenes or a web socket.
   * @param event - The event to search for.
   * @returns Either the destination map or undefined if no destination.
   */
  private extractDestinations(
    listenersOrSocket: Map<WsServerEventName, Map<string, WebSocket>> | WebSocket,
    event: WsServerEventName
  ): Map<string, WebSocket> | undefined {
    if (listenersOrSocket instanceof WebSocket) {
      return new Map([[listenersOrSocket.id, listenersOrSocket]])
    } else {
      const result = listenersOrSocket.get(event)
      return result && (result.size > 0 ? result : undefined)
    }
  }
}
