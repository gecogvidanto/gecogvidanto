/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Request, Response } from 'express'
import * as winston from 'winston'

import { ServerContext, buildPage, buildStoreSet, contentType } from '@gecogvidanto/client-web'
import {
  EconomicSystem as EconomicSystemDoc,
  EmailCheck as EmailCheckDoc,
  Game as GameDoc,
  HttpError,
  User as UserDoc,
} from '@gecogvidanto/shared'

import { ServerApi } from '../api'
import { app as gapp } from '../AppData'
import { EconomicSystemHelper } from '../business'
import { langType } from '../tools'

/**
 * Class serving the HTML pages.
 */
export default class PageServer {
  public constructor(private readonly serverApi: ServerApi) {}

  /**
   * Handler for the page requests.
   *
   * @param req - The request.
   * @param res - The response.
   */
  public onRequest: (req: Request, res: Response) => Promise<void> = async (req, res) => {
    const staticContext = new ServerContext()
    const context = this.prepareContext(req, staticContext)
    try {
      const htmlContent = await this.preparePageContent(context, staticContext, req, res)
      if (staticContext.url) {
        const status = staticContext.statusCode || 302
        res.redirect(status, staticContext.url)
      } else {
        const status = staticContext.statusCode || 200
        res.status(status).contentType(contentType).send(htmlContent)
      }
    } catch (error) {
      winston.error(error.message || error)
      error.stack && winston.debug(error.stack)
      res.status(HttpError.Server).send()
    }
  }

  private async preparePageContent(
    context: ReturnType<typeof buildStoreSet>,
    staticContext: ServerContext,
    req: Request,
    res: Response
  ): Promise<string> {
    // Initialize data pre-loaders
    ServerContext.dataProviders = {
      userReader: this.userReader(req, res),
      emailChecker: this.emailChecker(req, res),
      gamesSearcher: this.gamesSearcher(req, res),
      economicSystemsSearcher: this.economicSystemsSearcher(),
    }

    // Render page
    let htmlContent: string
    let needRender = false
    do {
      htmlContent = await buildPage(req.url, req.csrfToken(), req.nonce!, context, staticContext)
      try {
        needRender = await staticContext.loadingData
      } catch (error) {
        if (typeof error === 'number') {
          staticContext.statusCode = error
        } else {
          throw error
        }
      }
    } while (needRender && staticContext.statusCode === undefined)
    return htmlContent
  }

  private userReader(req: Request, res: Response): (id: string) => Promise<UserDoc> {
    return async (id: string): Promise<UserDoc> =>
      this.serverApi.user.onRead.exec(req, res, {
        url: { id },
        data: undefined,
      })
  }

  private emailChecker(req: Request, res: Response): (emailCheck: EmailCheckDoc) => Promise<boolean> {
    return async (emailCheck: EmailCheckDoc): Promise<boolean> => {
      try {
        await this.serverApi.emailCheck.onValidate.exec(req, res, {
          url: emailCheck,
          data: undefined,
        })
        return true
      } catch (error) {
        return false
      }
    }
  }

  private gamesSearcher(req: Request, res: Response): () => Promise<GameDoc[]> {
    return async (): Promise<GameDoc[]> =>
      this.serverApi.game.onSearch.exec(req, res, {
        url: undefined,
        data: undefined,
      })
  }

  private economicSystemsSearcher(): () => Promise<EconomicSystemDoc<any>[]> {
    return (): Promise<EconomicSystemDoc<langType>[]> =>
      Promise.resolve(
        gapp.data.economicSystems.map(economicSystem => EconomicSystemHelper.extractDoc(economicSystem))
      )
  }

  private prepareContext(req: Request, context: ServerContext): ReturnType<typeof buildStoreSet> {
    // Prepare context
    context.sdata.serverInfo = gapp.data.serverInfo
    context.sdata.captchaKey = gapp.data.config.reCaptcha.siteKey
    context.lang.preferences = req.uiLangPreferences!
    context.lang.languageMap = gapp.data.uiLangMap
    let loggedUser: UserDoc | undefined
    if (req.user) {
      loggedUser = req.user.cloneToDoc()
      context.user.logged = loggedUser
    }
    const game = undefined // TODO

    // Build context
    return buildStoreSet(
      gapp.data.serverInfo,
      gapp.data.config.reCaptcha.siteKey,
      gapp.data.uiLangMap,
      req.uiLangPreferences!,
      loggedUser,
      game
    )
  }
}
