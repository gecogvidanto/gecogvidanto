/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { EconomicSystem, Model } from '@gecogvidanto/plugin'

import { app } from './AppData'
import { langType } from './tools'

export function addModel(modelName: string, model: Model): void {
  safeModels()[modelName] = model
}

export function addConfig(value: unknown, ...keys: string[]): void {
  const parent = keys.slice(0, -1).reduce((config, key) => {
    if (!(key in config)) {
      config[key] = {}
    }
    return config[key]
  }, safeConfig())
  parent[keys[keys.length - 1]] = value
}

export function addEconomicSystem(economicSystem: EconomicSystem<langType>): void {
  safeEconomicSystems().push(economicSystem)
}

function safeModels(): any {
  const database = safeDatabase()
  if (!database.models) {
    database.models = {}
  }
  return database.models
}

function safeConfig(): any {
  const data = safeData()
  if (!data.config) {
    data.config = {}
  }
  return data.config
}

function safeDatabase(): any {
  const data = safeData()
  if (!data.database) {
    data.database = {}
  }
  return data.database
}

function safeEconomicSystems(): any {
  const data = safeData()
  if (!data.economicSystems) {
    data.economicSystems = []
  }
  return data.economicSystems
}

function safeData(): any {
  if (!app.data) {
    app.data = {} as any
  }
  return app.data
}
