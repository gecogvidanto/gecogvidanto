/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { RequestHandler } from 'express'
import * as expressSession from 'express-session'
import * as passport from 'passport'
import { Strategy as LocalStrategy } from 'passport-local'

import { app } from '../AppData'
import { User as GUser } from '../business'

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Express {
    // eslint-disable-next-line @typescript-eslint/no-empty-interface
    interface User extends GUser {}
  }
}

/*
 * Passport management class.
 */
export default class Passport {
  private session: RequestHandler

  public constructor() {
    const security = app.data.config.security
    this.session = expressSession({
      name: 'gecogvidanto-session',
      resave: false,
      saveUninitialized: true,
      store: app.data.database.models.session,
      secret: security.sessionKey,
      cookie: {
        secure: true,
        httpOnly: true,
        maxAge: security.validity.session * 1000 || undefined,
      },
    })
    passport.serializeUser(this.onSerializeUser)
    passport.deserializeUser(this.onDeserializeUser)
    passport.use('local', new LocalStrategy({ usernameField: 'email' }, this.onVerifyConnection))
  }

  public get handlers(): RequestHandler[] {
    return [this.session, passport.initialize(), passport.session()]
  }

  public authenticate(): RequestHandler[] {
    return [
      passport.authenticate('local'),
      (req, res) => {
        res.send(req.user!.cloneToDoc())
      },
    ]
  }

  private onSerializeUser = (user: GUser, done: (err: Error | null, id?: string) => void): void => {
    done(null, user.cloneToDoc()._id)
  }

  private onDeserializeUser = (id: string, done: (err: Error | null, user?: GUser) => void): void => {
    GUser.find(id)
      .then(user => {
        if (user) {
          return done(null, user)
        } else {
          return done(new Error('User not found'))
        }
      })
      .catch(err => done(new Error(err)))
  }

  private onVerifyConnection = (
    username: string,
    password: string,
    done: (error: Error | null, user?: GUser | false) => void
  ): void => {
    if (username.length === 0 || password.length === 0) {
      done(null, false)
    } else {
      GUser.findConnection(username, password)
        .then(user => {
          done(null, user ? user : false)
        })
        .catch(err => {
          done(new Error(err))
        })
    }
  }
}
