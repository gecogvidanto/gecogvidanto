/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { resolve as resolvePath } from 'path'
import * as winston from 'winston'

import { GecoPlugin, GecoPluginType, isGecoPluginType } from '@gecogvidanto/plugin'

import { ServerInfo } from './AppData'
import { ModuleReader, ModuleData } from './tools'

export interface RawPlugin {
  module: string
  data: ModuleData
  name: string
  type: GecoPluginType
  instance?: GecoPlugin
}

export interface InitializerData {
  readonly serverInfo: ServerInfo
  readonly rawPlugins: ReadonlyArray<RawPlugin>
}

/**
 * The initializer class, called at server startup.
 */
export default class Initializer {
  public readonly data: Promise<InitializerData>

  public constructor() {
    this.data = (async () => {
      const serverInfo: ServerInfo = { plugins: [] }
      const moduleData: ModuleData = await new ModuleReader(resolvePath(__dirname, '..')).data
      winston.info(
        `Starting ĞecoĞvidanto server (${moduleData.name || 'undefined name'}) — version ${
          moduleData.version || '(unknown)'
        }`
      )
      serverInfo.name = moduleData.name
      serverInfo.version = moduleData.version

      const rawPlugins: RawPlugin[] = []
      const plugins = await Promise.all(
        moduleData.dependencies.map(dependency => Initializer.readDependency(dependency))
      )
      plugins.forEach(plugin => plugin && rawPlugins.push(plugin))

      return { serverInfo, rawPlugins }
    })()
  }

  /**
   * Read the dependency to see if it is a ĞecoĞvidanto server plugin.
   *
   * @param dependency - The dependency to read.
   * @returns The plugin module or undefined in not a plugin.
   */
  private static async readDependency(dependency: string): Promise<RawPlugin | undefined> {
    try {
      const type = await import(dependency)
      if (isGecoPluginType(type)) {
        const name = dependency.replace(/(?:(?:\W?\bgecogvidanto\b\W?)|(?:\W?\bplugin\b\W?))+/g, '')
        winston.info(`Found plugin: ${type.description} (${name}) in module ${dependency}`)
        const data: ModuleData = await new ModuleReader(dependency).data
        return {
          module: dependency,
          data,
          name,
          type,
        }
      }
    } catch (e) {
      // Silently fail if dependency cannot be loaded
    }
    return undefined
  }
}
