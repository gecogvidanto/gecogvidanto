/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import { RoundSet } from '.'

const IDX = 1
const RPS = 10

describe('RoundSet', function () {
  const ECO_SYS = 'economic system'
  let roundSet: RoundSet

  beforeEach('Create default set', function () {
    roundSet = new RoundSet(RPS, IDX, ECO_SYS)
  })

  describe('#constructor', function () {
    it('must be created with default values for unspecified ones', function () {
      expect(roundSet.ecoSysId).to.equal(ECO_SYS)
      expect(roundSet.techBreakRounds).to.be.empty
      expect(roundSet.currentRound).to.equal(0)
    })

    it('must be created correctly from doc', function () {
      const newSet = new RoundSet(RPS, IDX, {
        ecoSysId: 'eco-sys',
        currentRound: 9,
        techBreakRounds: [7, 9],
      })
      const setDoc = newSet.cloneToDoc()
      expect(setDoc).to.have.all.keys('ecoSysId', 'currentRound', 'techBreakRounds')
      expect(setDoc.ecoSysId).to.equal('eco-sys')
      expect(setDoc.currentRound).to.equal(9)
      expect(setDoc.techBreakRounds).to.have.ordered.members([7, 9])
    })
  })

  describe('#currentRound', function () {
    it('must accept to set current round to same or increased value', function () {
      roundSet.currentRound = 0
      expect(roundSet.currentRound).to.equal(0)
      roundSet.currentRound = 1
      expect(roundSet.currentRound).to.equal(1)
    })

    it('must refuse to set current round to any other value', function () {
      expect(() => (roundSet.currentRound = 2)).to.throw(/rounds can only be increased/i)
    })

    it('must refuse to modify current round if set finished', function () {
      while (roundSet.currentRound <= RPS) {
        roundSet.currentRound++
      }
      expect(() => roundSet.currentRound++).to.throw(/cannot be modified/i)
    })
  })

  describe('#addTechBreak', function () {
    it('must add technological breaks when asked', function () {
      roundSet.currentRound++
      roundSet.addTechBreak()
      roundSet.currentRound++
      roundSet.currentRound++
      roundSet.addTechBreak()
      expect(roundSet.techBreakRounds).to.have.members([1, 3])
    })

    it('must refuse to set technological breaks if set not started', function () {
      expect(() => roundSet.addTechBreak()).to.throw(/not started/i)
    })

    it('must refuse to set technological breaks if set finished', function () {
      while (roundSet.currentRound <= RPS) {
        roundSet.currentRound++
      }
      expect(() => roundSet.addTechBreak()).to.throw(/cannot be modified/i)
    })
  })

  describe('#finished', function () {
    it('must indicate if set is finished', function () {
      while (roundSet.currentRound <= RPS) {
        expect(roundSet.finished).to.be.false
        roundSet.currentRound++
      }
      expect(roundSet.finished).to.be.true
    })
  })

  describe('#cloneToDoc', function () {
    it('must provide doc', function () {
      while (roundSet.currentRound < 4) {
        roundSet.currentRound++
      }
      roundSet.addTechBreak()
      while (roundSet.currentRound < 6) {
        roundSet.currentRound++
      }
      roundSet.addTechBreak()
      roundSet.currentRound++
      const setDoc = roundSet.cloneToDoc()
      expect(setDoc).to.have.all.keys('ecoSysId', 'currentRound', 'techBreakRounds')
      expect(setDoc.ecoSysId).to.equal(ECO_SYS)
      expect(setDoc.currentRound).to.equal(7)
      expect(setDoc.techBreakRounds).to.have.ordered.members([4, 6])
    })
  })
})
