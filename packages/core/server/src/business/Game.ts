/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { FormBuilder, FormData, ServerGame } from '@gecogvidanto/plugin'
import {
  Character as CharacterDoc,
  Game as GameDoc,
  HelpSheet,
  Location,
  PlaceContent,
} from '@gecogvidanto/shared'

import { app } from '../AppData'
import { GameModel } from '../database'
import { GameForm } from '../tools'
import Character, { GameData } from './Character'
import Gecobject from './Gecobject'
import Player from './Player'
import RoundSet from './RoundSet'

function model(): GameModel {
  return app.data.database.models.game
}
function maxPlayers(): number {
  return app.data.config.game.maxPlayers
}

/**
 * A game.
 */
export default class Game extends Gecobject<GameDoc> implements ServerGame {
  public readonly masterId: string
  public readonly roundsPerSet: number
  public readonly roundLength: number

  private _start: number
  private _location: Location
  private _summary?: string
  private _closed: boolean
  private _players: Player[]
  private readonly _roundSets: RoundSet[]
  private _characters: Character[]

  /**
   * Create a game from the given document.
   *
   * @param game - The document.
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public constructor(game: GameDoc)

  /**
   * Create a game from data.
   *
   * @param masterId - The identifier of the game master.
   * @param location - The location of the game.
   * @param roundsPerSet - The count of rounds for each set.
   * @param roundLength - The lenght of each round in minutes.
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public constructor(masterId: string, location: Location, roundsPerSet: number, roundLength: number)

  /*
   * Constructor implementation.
   */
  public constructor(
    masterIdOrDoc: string | GameDoc,
    location?: Location,
    roundsPerSet?: number,
    roundLength?: number
  ) {
    super(typeof masterIdOrDoc === 'string' ? undefined : masterIdOrDoc)
    if (typeof masterIdOrDoc === 'string') {
      this._start = Date.now()
      this.masterId = masterIdOrDoc
      this._location = location!
      this._summary = undefined
      this.roundsPerSet = roundsPerSet!
      this.roundLength = roundLength!
      this._closed = false
      this._players = []
      this._roundSets = []
      this._characters = []
    } else {
      this._start = masterIdOrDoc.start
      this.masterId = masterIdOrDoc.masterId
      this._location = masterIdOrDoc.location
      this._summary = masterIdOrDoc.summary
      this.roundsPerSet = masterIdOrDoc.roundsPerSet
      this.roundLength = masterIdOrDoc.roundLength
      this._closed = masterIdOrDoc.closed
      this._players = masterIdOrDoc.players.map(player => new Player(() => this._closed, player))
      this._roundSets = masterIdOrDoc.roundSets.map(
        (set, index) => new RoundSet(this.roundsPerSet, index, set)
      )
      this._characters = masterIdOrDoc.characters.map(character => this.buildCharacter(character))
    }
  }

  /**
   * Search available games.
   *
   * @returns Available games.
   */
  public static async search(): Promise<Game[]> {
    return (await model().search()).map(game => new Game(game))
  }

  /**
   * Read the requested game.
   *
   * @param _id - The identifier of the game.
   * @returns The game, if found.
   */
  public static async find(_id: string): Promise<Game | undefined> {
    const game = await model().find(_id)
    return game ? new Game(game) : undefined
  }

  public get start(): number {
    return this._start
  }

  public get location(): Location {
    return this._location
  }

  public set location(value: Location) {
    this.requireModifiable()
    this._location = value
  }

  public get summary(): string | undefined {
    return this._summary
  }

  public set summary(value: string | undefined) {
    this.requireModifiable()
    this._summary = value
  }

  public get closed(): boolean {
    return this._closed
  }

  public set closed(value: boolean) {
    if (value) {
      if (this.roundSets.length < 2) {
        throw new Error('Cannot close a game with less than 2 sets')
      } else if (this.setInProgress) {
        throw new Error('Cannot close a game if sets are not finished')
      }
      this._closed = true
    } else if (this._closed) {
      throw new Error('Cannot re-open a closed game')
    }
  }

  public get players(): ReadonlyArray<Player> {
    return this._players
  }

  /**
   * Add a player to the game.
   *
   * @param name - The name of the player to add to the game.
   */
  public addPlayer(name: string): void {
    this.requireModifiable()
    if (this.onGamePlayers.length >= maxPlayers()) {
      throw new Error('Maximum count of active players reached')
    }
    if (this._players.find(player => player.name === name) !== undefined) {
      throw new Error('Multiple players with same name')
    }
    this._players.push(new Player(() => this._closed, name, this.absoluteCurrentRound))
    if (this.setInProgress) {
      if (this.currentSet.currentRound === 0) {
        this.resetSet()
      } else {
        const roundSet = this._roundSets.length - 1
        this._characters.push(this.buildCharacter(roundSet, this._players.length - 1, this.roundsPerSet))
      }
    }
  }

  /**
   * Update the name of the given player.
   *
   * @param player - The identifier of the player to update.
   * @param name - The name of the player to add to the game.
   */
  public updatePlayer(player: number, name: string): void {
    this.requireModifiable()
    if (player < 0 || player >= this._players.length) {
      throw new Error('No such player')
    }
    if (this._players.find((_player, index) => _player.name === name && index !== player) !== undefined) {
      throw new Error('Multiple players with same name')
    }
    this._players[player].name = name
  }

  /**
   * Remove a player from the game. If player already played, simply indicate that he left.
   *
   * @param player - The identifier of the player to delete.
   */
  public deletePlayer(player: number): void {
    this.requireModifiable()
    if (player < 0 || player >= this._players.length) {
      throw new Error('No such player')
    }
    if (this._players[player].roundArrived === this.absoluteCurrentRound) {
      this._players = this._players.filter((_player, index) => player !== index)
      if (this.setInProgress) {
        if (this.currentSet.currentRound === 0) {
          this.resetSet()
        } else {
          this._characters = this._characters
            .filter(character => character.player !== player)
            .map(character => (character.player > player ? character.shiftForDeletedPlayer() : character))
        }
      }
    } else {
      this._players[player].roundLeft = this.absoluteCurrentRound
    }
  }

  public get onGamePlayers(): ReadonlyArray<number> {
    return this._players
      .map((player, index) => ({ index, player }))
      .filter(({ player }) => player.roundLeft === 0 || player.roundLeft >= this.absoluteCurrentRound)
      .map(({ index }) => index)
  }

  public get terminatingPlayers(): ReadonlyArray<number> {
    return this.onGamePlayers.filter(
      player =>
        this.currentSet.currentRound === this.roundsPerSet ||
        this.getCharacterFor(player).generation === this.currentSet.currentRound ||
        (this.players[player].roundLeft !== 0 &&
          this.players[player].roundLeft === this.absoluteCurrentRound)
    )
  }

  public get roundSets(): ReadonlyArray<RoundSet> {
    return this._roundSets
  }

  public addSet(ecoSysId: string): void {
    // Controls
    this.requireModifiable()
    if (this.setInProgress) {
      throw new Error('Cannot create new set if previous one is not finished')
    }

    // Create set
    const players = this.onGamePlayers
    const setNumber = this._roundSets.length
    this._roundSets.push(new RoundSet(this.roundsPerSet, setNumber, ecoSysId))

    // Reset time if first set
    if (setNumber === 0) {
      this._start = Date.now()
    }

    // Calculate random generation for each player
    const deathPerRound = Math.trunc(players.length / this.roundsPerSet)
    const playerGenerations = new Array(players.length).fill(0)
    for (let generation = 0; generation < this.roundsPerSet; generation++) {
      for (let i = 0; i < deathPerRound; i++) {
        let playNum = Math.floor(Math.random() * (players.length - generation * deathPerRound - i))
        playNum = playerGenerations.findIndex(value => value === 0 && playNum-- <= 0)
        playerGenerations[playNum] = generation + 1
      }
    }

    // Spread rest of players over rounds
    const overSized: Set<number> = new Set()
    playerGenerations.forEach((value, index, generations) => {
      if (value === 0) {
        value = Math.floor(Math.random() * this.roundsPerSet + 1)
        while (overSized.has(value)) {
          if (++value > this.roundsPerSet) {
            value = 1
          }
        }
        generations[index] = value
        overSized.add(value)
      }
    })

    // Create characters for players
    playerGenerations
      .map((generation, index) => this.buildCharacter(setNumber, players[index], generation))
      .forEach(character => this._characters.push(character))
  }

  public get currentSet(): RoundSet {
    this.requireSetInProgress()
    return this.roundSets[this.roundSets.length - 1]
  }

  public get characters(): ReadonlyArray<Character> {
    return this._characters
  }

  public addNonPlayerCharacter(): number {
    this.requireSetInProgress()
    const npcId =
      this._characters
        .filter(character => character.roundSet === this._roundSets.length - 1)
        .reduce((prevId, character) => Math.min(prevId, character.player), 0) - 1
    const setId = this._roundSets.length - 1
    this._characters.push(this.buildCharacter(setId, npcId))
    return npcId
  }

  public getCharacterFor(player: number): Character {
    this.requireSetInProgress()
    const found = this._characters.find(
      character => character.roundSet === this._roundSets.length - 1 && character.player === player
    )
    if (!found) {
      throw new Error(`No character found for player ${player}`)
    }
    return found
  }

  public buildValuesHelpSheet(): HelpSheet {
    this.requireSetInProgress()
    const values = [PlaceContent.Red, PlaceContent.Yellow, PlaceContent.Green, PlaceContent.Blue]
    let index = this.currentSet.techBreakRounds.length
    const low: PlaceContent = values[index++ % values.length]
    const medium: PlaceContent = values[index++ % values.length]
    const high: PlaceContent = values[index++ % values.length]
    const waiting: PlaceContent = values[index++ % values.length]
    return { low, medium, high, waiting }
  }

  /**
   * Build the form for the current round.
   *
   * @param moneyHelpSheet - The money help sheet definition.
   * @returns The form builder.
   */
  public buildForm(moneyHelpSheet: HelpSheet): FormBuilder<any> {
    const gameForm: GameForm = new GameForm(this)
    return gameForm.build(moneyHelpSheet)
  }

  /**
   * Analyze the form result.
   *
   * @param data - The form data.
   */
  public analyzeFormResult(data: FormData): void {
    const gameForm: GameForm = new GameForm(this)
    gameForm.analyze(data)
  }

  /**
   * Save (create or update) the game.
   */
  public async save(): Promise<void> {
    const game = this.cloneToDoc()
    if (!this.persistent) {
      this._id = await model().create(game)
    } else {
      await model().update(game)
    }
  }

  /**
   * Delete the game from database.
   */
  public async delete(): Promise<void> {
    if (this.persistent) {
      await model().delete(this._id)
    }
  }

  /**
   * Get the absolute current round, meaning counting all rounds of all finished sets.
   *
   * @returns Current round.
   */
  public get absoluteCurrentRound(): number {
    let result = this._roundSets.length * this.roundsPerSet
    if (this.setInProgress) {
      const currentRound = this._roundSets[this._roundSets.length - 1].currentRound
      result += currentRound - this.roundsPerSet
    }
    return result
  }

  /**
   * @returns True if the last set is in progress.
   */
  public get setInProgress(): boolean {
    return !!(
      this._roundSets.length &&
      this._roundSets[this._roundSets.length - 1].currentRound <= this.roundsPerSet
    )
  }

  /**
   * Create a clone of the object in a simple document.
   *
   * @param safe - If safe, some values of current set will not be shown in characters.
   * @returns The pure document.
   */
  public cloneToDoc(safe = false): GameDoc {
    const result: GameDoc = {
      ...super.cloneToDoc(),
      start: this._start,
      masterId: this.masterId,
      location: this._location,
      roundsPerSet: this.roundsPerSet,
      roundLength: this.roundLength,
      players: this._players.map(player => player.cloneToDoc()),
      roundSets: this._roundSets.map(set => set.cloneToDoc()),
      characters: this._characters.map(character => character.cloneToDoc(safe)),
      closed: this._closed,
    }
    this._summary && (result.summary = this._summary)
    return result
  }

  /**
   * Reset the last set, meaning delete it and recreate it with the new player configuration.
   */
  private resetSet(): void {
    const roundSet = this._roundSets.pop()
    this._characters = this._characters.filter(
      character => character.roundSet < this._roundSets.length || character.player < 0
    )
    this.addSet(roundSet!.ecoSysId)
  }

  /**
   * Build the character using doc.
   *
   * @param character - The character document.
   */
  private buildCharacter(character: CharacterDoc): Character

  /**
   * Build the character using data.
   *
   * @param roundSet - The set number (index in game set array).
   * @param player - The player number (if >= 0, index in game player array, if < 0, quasi-index in set NPC
   *   array).
   * @param generation - Generation (round of death) of the character.
   */
  private buildCharacter(roundSet: number, player: number, generation?: number): Character

  /*
   * Implementation.
   */
  private buildCharacter(setOrDoc: CharacterDoc | number, player?: number, generation?: number): Character {
    const setId = typeof setOrDoc === 'number' ? setOrDoc : setOrDoc.roundSet
    const gameDataFunction = (): GameData | undefined => {
      if (setId === this._roundSets.length - 1 && this.setInProgress) {
        return {
          currentRound: this.currentSet.currentRound,
          techBreakCount: this.currentSet.techBreakRounds.length,
        }
      } else {
        return undefined
      }
    }
    const economicSystem = app.data.economicSystems.find(
      value => value.id === this._roundSets[setId].ecoSysId
    )
    if (!economicSystem && setId === this._roundSets.length - 1 && this.setInProgress) {
      throw new Error('Current economic system not found')
    }
    const valueCost = economicSystem ? economicSystem.valueCost : 1
    return typeof setOrDoc === 'number'
      ? new Character(gameDataFunction, valueCost, setOrDoc, player!, generation)
      : new Character(gameDataFunction, valueCost, setOrDoc)
  }

  /**
   * Test if set is in progress and fail if not.
   */
  private requireSetInProgress(): void {
    if (!this.setInProgress) {
      throw new Error('No set in progress')
    }
  }

  /**
   * Ensure the game is modifiable. Throw an exception if it is not.
   */
  private requireModifiable(): void {
    if (this._closed) {
      throw new Error('Closed game cannot be modified')
    }
  }
}
