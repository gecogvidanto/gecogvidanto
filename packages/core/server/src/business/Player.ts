/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Player as PlayerDoc } from '@gecogvidanto/shared'

/**
 * A player.
 */
export default class Player implements PlayerDoc {
  public readonly roundArrived: number

  private _name: string
  private _roundLeft: number

  /**
   * Create a player from the given document.
   *
   * @param locked - Indicate if object is locked (no modification possible).
   * @param player - The document.
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public constructor(locked: () => boolean, player: PlayerDoc)

  /**
   * Create a player from data.
   *
   * @param locked - Indicate if object is locked (no modification possible).
   * @param name - The name or pseudo of the player.
   * @param roundArrived - The round the player arrived in the game.
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public constructor(locked: () => boolean, name: string, roundArrived: number)

  /*
   * Constructor implementation.
   */
  public constructor(
    private readonly locked: () => boolean,
    nameOrDoc: string | PlayerDoc,
    roundArrived?: number
  ) {
    if (typeof nameOrDoc === 'string') {
      this._name = nameOrDoc
      this.roundArrived = roundArrived!
      this._roundLeft = 0
    } else {
      this._name = nameOrDoc.name
      this.roundArrived = nameOrDoc.roundArrived
      this._roundLeft = nameOrDoc.roundLeft
    }
  }

  public get name(): string {
    return this._name
  }

  public set name(value: string) {
    this.requireModifiable()
    this._name = value
  }

  public get roundLeft(): number {
    return this._roundLeft
  }

  public set roundLeft(value: number) {
    this.requireModifiable()
    if (this._roundLeft && value !== this._roundLeft) {
      throw new Error('Player already left')
    }
    this._roundLeft = value
  }

  /**
   * Create a clone of the object in a simple document.
   *
   * @returns The pure document.
   */
  public cloneToDoc(): PlayerDoc {
    const result: PlayerDoc = {
      name: this._name,
      roundArrived: this.roundArrived,
      roundLeft: this._roundLeft,
    }
    return result
  }

  /**
   * Ensure the player is modifiable. Throw an exception if it is not.
   */
  private requireModifiable(): void {
    if (this.locked()) {
      throw new Error('Closed game player cannot be modified')
    }
  }
}
