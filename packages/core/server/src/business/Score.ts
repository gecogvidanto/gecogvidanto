/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { EntityCount as EntityCountDoc, Score as ScoreDoc } from '@gecogvidanto/shared'

/**
 * Entity count, with low, medium and high count.
 */
export class EntityCount implements EntityCountDoc {
  private _low: number
  private _medium: number
  private _high: number

  /**
   * Create an entity count.
   *
   * @param updateScore - A function used to update the total score, will throw an exception if the score is
   *   locked.
   * @param entityCount - The document containing entity count.
   */
  public constructor(private readonly updateScore: (value: number) => void, entityCount?: EntityCountDoc) {
    if (entityCount === undefined) {
      this._low = 0
      this._medium = 0
      this._high = 0
    } else {
      this._low = entityCount.low
      this._medium = entityCount.medium
      this._high = entityCount.high
    }
  }

  public get low(): number {
    return this._low
  }

  public set low(value: number) {
    this._low = this.calculateValue(this._low, value, 1)
  }

  public get medium(): number {
    return this._medium
  }

  public set medium(value: number) {
    this._medium = this.calculateValue(this._medium, value, 2)
  }

  public get high(): number {
    return this._high
  }

  public set high(value: number) {
    this._high = this.calculateValue(this._high, value, 4)
  }

  /**
   * @returns True if entity count is null.
   */
  public get zero(): boolean {
    return this._low === 0 && this._medium === 0 && this._high === 0
  }

  /**
   * Create a clone of the object in a simple document.
   *
   * @returns The pure document.
   */
  public cloneToDoc(): EntityCountDoc {
    const result: EntityCountDoc = {
      low: this._low,
      medium: this._medium,
      high: this._high,
    }
    return result
  }

  /**
   * Do all the needed stuff when updating a value and return the value to set, or throw an exception if
   * value must not be set.
   *
   * @param current - The current value.
   * @param value - The value to set.
   * @param factor - The multiplying factor for the total score.
   * @returns The value.
   */
  private calculateValue(current: number, value: number, factor: 1 | 2 | 4): number {
    if (value !== current) {
      this.updateScore(value * factor)
    }
    return value
  }
}

/**
 * Use to distinguish values and money scores.
 */
export const enum ScoreType {
  Values,
  Money,
}

/**
 * A score card.
 */
export default class Score implements ScoreDoc {
  public readonly round: number
  public readonly values: EntityCount
  public readonly money: EntityCount

  /**
   * Create a score.
   *
   * @param updateScore - A function used to update the total score, will throw an exception if the score is
   *   locked.
   * @param roundOrScore - Either the round number or the document containing score data.
   */
  public constructor(
    updateScore: (value: number, type: ScoreType, round: number) => void,
    roundOrScore: number | ScoreDoc
  ) {
    if (typeof roundOrScore === 'number') {
      this.round = roundOrScore
      this.values = new EntityCount(this.buildScoreFunction(updateScore, ScoreType.Values))
      this.money = new EntityCount(this.buildScoreFunction(updateScore, ScoreType.Money))
    } else {
      this.round = roundOrScore.round
      this.values = new EntityCount(
        this.buildScoreFunction(updateScore, ScoreType.Values),
        roundOrScore.values
      )
      this.money = new EntityCount(
        this.buildScoreFunction(updateScore, ScoreType.Money),
        roundOrScore.money
      )
    }
  }

  /**
   * @returns True if score is null.
   */
  public get zero(): boolean {
    return this.values.zero && this.money.zero
  }

  /**
   * Create a clone of the object in a simple document.
   *
   * @returns The pure document.
   */
  public cloneToDoc(): ScoreDoc {
    const result: ScoreDoc = {
      round: this.round,
      values: this.values.cloneToDoc(),
      money: this.money.cloneToDoc(),
    }
    return result
  }

  /**
   * Build the entity update score function.
   *
   * @param updateScore - A function used to update the total score, will throw an exception if the score is locked.
   * @param type - The type of the score.
   * @returns The function used to update score.
   */
  private buildScoreFunction(
    updateScore: (value: number, type: ScoreType, round: number) => void,
    type: ScoreType
  ): (value: number) => void {
    const round: number = this.round
    return (value: number) => updateScore(value, type, round)
  }
}
