/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as BCrypt from 'bcrypt'

import { CertLevel, User as UserDoc } from '@gecogvidanto/shared'

import { app } from '../AppData'
import { UserModel, ModelUser } from '../database'
import Gecobject from './Gecobject'

function model(): UserModel {
  return app.data.database.models.user
}
function maxTries(): number {
  return app.data.config.security.maxFailures
}
const SALT_ROUNDS = 10

/**
 * A user of the game.
 */
export default class User extends Gecobject<UserDoc> implements UserDoc {
  public email: string
  public name: string
  public level: CertLevel
  public account?: string

  /**
   * Create a user from the given document.
   *
   * @param user - The document.
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public constructor(user: UserDoc)

  /**
   * Create a user from data.
   *
   * @param email - The e-mail address, may be empty, especially if not checked yet.
   * @param name - The name or pseudonyme of the user.
   * @param account - The public key of user account.
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public constructor(email: string, name: string, account?: string)

  /*
   * Constructor implementation.
   */
  public constructor(emailOrDoc: string | UserDoc, name?: string, account?: string) {
    super(typeof emailOrDoc === 'string' ? undefined : emailOrDoc)
    if (typeof emailOrDoc === 'string') {
      this.email = emailOrDoc
      this.name = name!
      this.level = CertLevel.GameMaster
      this.account = account
    } else {
      this.email = emailOrDoc.email
      this.name = emailOrDoc.name
      this.level = emailOrDoc.level
      this.account = emailOrDoc.account
    }
  }

  /**
   * Search users.
   *
   * @param email - Search only users matching e-mail if provided.
   * @param name - Search only users matching name if provided.
   * @returns The found users.
   */
  public static async search(email?: string, name?: string): Promise<User[]> {
    return (await model().search(email, name))
      .filter(user => !!user.email)
      .map(user => new User(user as ModelUser & { email: string }))
  }

  /**
   * Get the user matching the given identifier.
   *
   * @param _id - The user identifier.
   * @returns The user if found.
   */
  public static async find(_id: string): Promise<User | undefined> {
    const data = await model().find(_id)
    data && !data.email && (data.email = '')
    return data ? new User(data as ModelUser & { email: string }) : undefined
  }

  /**
   * Get the user matching the connection parameters.
   *
   * @param email - The user email.
   * @param password - The user password.
   * @returns The user if found.
   */
  public static async findConnection(email: string, password: string): Promise<User | undefined> {
    const users = await model().search(email)
    if (users.length === 1 && users[0].failures! < maxTries()) {
      const match: boolean = await BCrypt.compare(password, users[0].password!)
      if (match) {
        if (users[0].failures !== 0) {
          await model().updateFailures(users[0]._id, true)
        }
        return new User(users[0] as ModelUser & { email: string })
      } else {
        await model().updateFailures(users[0]._id, false)
      }
    }
    return undefined
  }

  /**
   * Remove users with no email (not fully created).
   *
   * @param saveIds - The identifiers of users not to be removed (waiting for confirmation).
   */
  public static async removeFailedCreations(saveIds: string[]): Promise<void> {
    await model().removeFailedCreations(saveIds)
  }

  /**
   * Save the user to the database.
   *
   * @param password - The password if it needs to be changed.
   */
  public async save(password?: string): Promise<void> {
    const user = await this.cloneForModel(password)
    if (!this.persistent) {
      if (!password) {
        throw new Error('Password is required for user creation')
      }
      user.failures = 0
      this._id = await model().create(user)
    } else {
      await model().update(user)
    }
  }

  /**
   * Check that the user has unique properties.
   *
   * @returns True if unique.
   */
  public async checkUnicity(): Promise<boolean> {
    const user = await this.cloneForModel()
    return model().checkUnicity(user)
  }

  /**
   * Delete the user.
   */
  public async delete(): Promise<void> {
    if (this.persistent) {
      await model().delete(this._id)
    }
  }

  /**
   * Create a clone of the object in a simple document.
   *
   * @returns The pure document.
   */
  public cloneToDoc(): UserDoc {
    const result: UserDoc = {
      ...super.cloneToDoc(),
      email: this.email,
      name: this.name,
      level: this.level,
    }
    this.account && (result.account = this.account)
    return result
  }

  /**
   * Create the data structure for database.
   *
   * @param password - The password if to be recorded.
   * @returns The data structure.
   */
  private async cloneForModel(password?: string): Promise<ModelUser> {
    const modelUser: ModelUser = this.cloneToDoc()
    if (this.email.length === 0) {
      delete modelUser.email
    }
    if (password) {
      modelUser.password = await BCrypt.hash(password, SALT_ROUNDS)
    }
    return modelUser
  }
}
