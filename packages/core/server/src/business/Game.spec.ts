/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import * as sinon from 'sinon'
import { SinonStub } from 'sinon'

import { EconomicSystem } from '@gecogvidanto/plugin'
import { Location, PlaceContent } from '@gecogvidanto/shared'

import { Character, Game, RoundSet } from '.'
import { addConfig, addEconomicSystem, addModel } from '../AppData.spec'
import { GameModel } from '../database'
import { langType } from '../tools'

type Identifier = import('@gecogvidanto/plugin').Identifier
type ModelGame = import('@gecogvidanto/shared').Game
const model = new GameModel(undefined as any)
addModel('game', model)
const MAX_PLAYERS = 30
addConfig(MAX_PLAYERS, 'game', 'maxPlayers')
const FAKE_ECOSYS: EconomicSystem<langType> = {
  id: 'fake-sys',
  valueCost: 2,
} as any
const FAKE_LIBRESYS: EconomicSystem<langType> = {
  id: 'libre-sys',
  valueCost: 3,
} as any
const FAKE_DEBTSYS: EconomicSystem<langType> = {
  id: 'debt-sys',
  valueCost: 1,
} as any
addEconomicSystem(FAKE_ECOSYS)
addEconomicSystem(FAKE_LIBRESYS)
addEconomicSystem(FAKE_DEBTSYS)

const MASTER_ID = 'master-id'
const LOCATION: Location = { name: '54a RML', address: 'Tero' }
const ROUND_PS = 8
const ROUND_L = 4

function setSetValueTo(roundSet: RoundSet, value: number): void {
  while (roundSet.currentRound < value) {
    roundSet.currentRound++
  }
}

function addSetsAndCloseGame(game: Game): void {
  game.addSet(FAKE_ECOSYS.id)
  setSetValueTo(game.currentSet, ROUND_PS + 1)
  game.addSet(FAKE_ECOSYS.id)
  setSetValueTo(game.currentSet, ROUND_PS + 1)
  game.closed = true
}

function checkRandom(characters: ReadonlyArray<Character>): boolean {
  const playersPerRound: number[] = new Array(ROUND_PS).fill(0)
  characters.forEach(character => {
    character.generation && playersPerRound[character.generation - 1]++
  })
  // eslint-disable-next-line id-blacklist
  let minValue = Number.MAX_SAFE_INTEGER
  let maxValue = 0
  playersPerRound.forEach(players => {
    if (players < minValue) {
      minValue = players
    }
    if (players > maxValue) {
      maxValue = players
    }
  })
  return maxValue - minValue <= 1
}

describe('Game', function () {
  let game: Game

  beforeEach('Create default game', function () {
    game = new Game(MASTER_ID, LOCATION, ROUND_PS, ROUND_L)
  })

  describe('#contructor', function () {
    it('must create game with appropriate values', function () {
      expect(game.start).to.be.within(Date.now() - 1000, Date.now())
      expect(game.masterId).to.equal(MASTER_ID)
      expect(game.location).to.equal(LOCATION)
      expect(game.summary).to.not.be.ok
      expect(game.roundsPerSet).to.equal(ROUND_PS)
      expect(game.roundLength).to.equal(ROUND_L)
      expect(game.players).to.be.empty
      expect(game.roundSets).to.be.empty
      expect(game.characters).to.be.empty
      expect(game.closed).to.be.false
    })

    it('must be created correctly from doc', function () {
      const START = Date.now() - 1728
      game = new Game({
        start: START,
        masterId: 'obey-your-master',
        location: { address: 'Atlantide' },
        summary: 'This was great!',
        roundsPerSet: 8,
        roundLength: 3,
        players: [
          {
            name: 'Zorro',
            roundArrived: 0,
            roundLeft: 0,
          },
        ],
        roundSets: [
          {
            ecoSysId: FAKE_LIBRESYS.id,
            currentRound: ROUND_PS + 1,
            techBreakRounds: [7],
          },
        ],
        characters: [
          {
            roundSet: 0,
            player: 0,
            score: [],
            totalScore: 816,
          },
        ],
        closed: true,
      })
      const gameDoc = game.cloneToDoc()
      expect(gameDoc).to.have.all.keys(
        'start',
        'masterId',
        'location',
        'summary',
        'roundsPerSet',
        'roundLength',
        'players',
        'roundSets',
        'characters',
        'closed'
      )
      expect(gameDoc.start).to.equal(START)
      expect(gameDoc.masterId).to.equal('obey-your-master')
      expect(gameDoc.location).to.deep.equal({ address: 'Atlantide' })
      expect(gameDoc.summary).to.equal('This was great!')
      expect(gameDoc.roundsPerSet).to.equal(8)
      expect(gameDoc.roundLength).to.equal(3)
      expect(gameDoc.players).to.have.lengthOf(1)
      expect(gameDoc.players[0].name).to.equal('Zorro')
      expect(gameDoc.roundSets).to.have.lengthOf(1)
      expect(gameDoc.roundSets[0].ecoSysId).to.equal(FAKE_LIBRESYS.id)
      expect(gameDoc.roundSets[0].currentRound).to.be.gt(ROUND_PS)
      expect(gameDoc.roundSets[0].techBreakRounds).to.have.lengthOf(1)
      expect(gameDoc.characters).to.have.lengthOf(1)
      expect(gameDoc.closed).to.be.true
      expect(() => (game.players[0].name = 'Batman')).to.throw(/cannot be modified/i)
    })
  })

  describe('#search', function () {
    let readStub: SinonStub<[], Promise<(ModelGame & Identifier)[]>>

    beforeEach('Create doubles', function () {
      readStub = sinon.stub(model, 'search')
    })

    afterEach('Clean up', function () {
      readStub.restore()
    })

    it('must request data from database', async function () {
      readStub.resolves([{ _id: 'game-id', ...game.cloneToDoc() }])
      const games = await Game.search()
      expect(readStub).to.have.been.calledOnce
      expect(games).to.have.lengthOf(1)
      expect(games[0]._id).to.equal('game-id')
    })
  })

  describe('#find', function () {
    let findStub: SinonStub<[string], Promise<(ModelGame & Identifier) | undefined>>

    beforeEach('Create doubles', function () {
      findStub = sinon.stub(model, 'find')
    })

    afterEach('Clean up', function () {
      findStub.restore()
    })

    it('must return existing game', async function () {
      findStub.resolves({ _id: 'game-id', ...game.cloneToDoc() })
      const readGame = await Game.find('id')
      expect(findStub).to.have.been.calledOnceWith('id')
      expect(readGame).to.exist
      expect(readGame!._id).to.equal('game-id')
    })

    it('must return undefined if no matching game', async function () {
      findStub.resolves(undefined)
      const readGame = await Game.find('id')
      expect(findStub).to.have.been.calledOnceWith('id')
      expect(readGame).to.not.exist
    })
  })

  describe('#location', function () {
    it('must accept modification unless game closed', function () {
      expect(() => (game.location = { name: 'Hejme' })).to.not.throw()
      addSetsAndCloseGame(game)
      expect(() => (game.location = { name: 'Alie' })).to.throw(/cannot be modified/i)
    })
  })

  describe('#summary', function () {
    it('must accept modification unless game closed', function () {
      expect(() => (game.summary = 'Once upon a time')).to.not.throw()
      addSetsAndCloseGame(game)
      expect(() => (game.summary = 'in a faraway land')).to.throw(/cannot be modified/i)
    })
  })

  describe('#closed', function () {
    it('must close the game', function () {
      game.addSet(FAKE_DEBTSYS.id)
      setSetValueTo(game.roundSets[0], ROUND_PS + 1)
      game.addSet(FAKE_LIBRESYS.id)
      setSetValueTo(game.roundSets[1], ROUND_PS + 1)
      expect(() => (game.closed = true)).to.not.throw()
    })

    it('must refuse to close a game with too few sets', function () {
      game.closed = false
      expect(() => (game.closed = true)).to.throw(/cannot close a game with less than 2 sets/i)
      expect(game.closed).to.be.false
      game.addSet(FAKE_DEBTSYS.id)
      setSetValueTo(game.roundSets[0], ROUND_PS + 1)
      expect(() => (game.closed = true)).to.throw(/cannot close a game with less than 2 sets/i)
      expect(game.closed).to.be.false
    })

    it('must refuse to close a game with unfinished sets', function () {
      game.closed = false
      game.addSet(FAKE_DEBTSYS.id)
      setSetValueTo(game.roundSets[0], ROUND_PS + 1)
      game.addSet(FAKE_LIBRESYS.id)
      setSetValueTo(game.roundSets[1], ROUND_PS + 1)
      game.addSet(FAKE_ECOSYS.id)
      setSetValueTo(game.roundSets[1], ROUND_PS)
      expect(() => (game.closed = true)).to.throw(/cannot close a game if sets are not finished/i)
      expect(game.closed).to.be.false
    })

    it('must refuse to re-open a closed game', function () {
      game.addSet(FAKE_DEBTSYS.id)
      setSetValueTo(game.roundSets[0], ROUND_PS + 1)
      game.addSet(FAKE_LIBRESYS.id)
      setSetValueTo(game.roundSets[1], ROUND_PS + 1)
      game.closed = true
      expect(game.closed).to.be.true
      expect(() => (game.closed = false)).to.throw(/cannot re-open a closed game/i)
      expect(game.closed).to.be.true
    })
  })

  describe('#players', function () {
    it('must be modifiable unless closed', function () {
      game.addPlayer('Superman')
      expect(() => (game.players[0].name = 'Spiderman')).to.not.throw()
      addSetsAndCloseGame(game)
      expect(() => (game.players[0].name = 'Batman')).to.throw(/cannot be modified/i)
    })
  })

  describe('#addPlayer', function () {
    it('must add player to the game', function () {
      game.addPlayer('player')
      expect(game.players).to.have.lengthOf(1)
      expect(game.players[0].name).to.equal('player')
      expect(game.roundSets).to.be.empty
      expect(game.characters).to.be.empty
    })

    it('must add players to the current set', function () {
      game.addSet(FAKE_ECOSYS.id)
      setSetValueTo(game.roundSets[0], ROUND_PS + 1)
      game.addSet(FAKE_ECOSYS.id)
      game.roundSets[1].currentRound++
      expect(game.roundSets).to.have.lengthOf(2)
      expect(game.characters).to.be.empty
      for (let i = 0; i < ROUND_PS; i++) {
        game.addPlayer(`player${i}`)
      }
      expect(game.players).to.have.lengthOf(ROUND_PS)
      expect(game.characters).to.have.lengthOf(ROUND_PS)
      game.characters.forEach(character => {
        expect(character.roundSet).to.equal(1)
        expect(character.generation).to.equal(ROUND_PS)
      })
    })

    it('must add players to unstarted first set', function () {
      game.addSet(FAKE_ECOSYS.id)
      expect(game.roundSets).to.have.lengthOf(1)
      expect(game.characters).to.be.empty
      for (let i = 0; i < ROUND_PS; i++) {
        game.addPlayer(`player${i}`)
      }
      expect(game.players).to.have.lengthOf(ROUND_PS)
      expect(game.characters).to.have.lengthOf(ROUND_PS)
      const generations: number[] = []
      game.characters.forEach(character => {
        expect(character.roundSet).to.equal(0)
        if (character.player >= 0) {
          expect(generations).to.not.include(character.generation!)
          generations.push(character.generation!)
        }
      })
    })

    it('must add players to unstarted last set', function () {
      game.addSet(FAKE_DEBTSYS.id)
      game.addNonPlayerCharacter()
      setSetValueTo(game.roundSets[0], ROUND_PS + 1)
      game.addSet(FAKE_LIBRESYS.id)
      game.addNonPlayerCharacter()
      game.addNonPlayerCharacter()
      expect(game.roundSets).to.have.lengthOf(2)
      expect(game.characters).to.have.lengthOf(3)
      for (let i = 0; i < ROUND_PS; i++) {
        game.addPlayer(`player${i}`)
      }
      expect(game.players).to.have.lengthOf(ROUND_PS)
      expect(game.characters).to.have.lengthOf(ROUND_PS + 3)
      const generations: number[] = []
      game.characters.forEach(character => {
        if (character.player >= 0) {
          expect(character.roundSet).to.equal(1)
          expect(generations).to.not.include(character.generation!)
          generations.push(character.generation!)
        }
      })
    })

    it('must fail if adding too many players', function () {
      for (let i = 0; i < MAX_PLAYERS; i++) {
        game.addPlayer(`player${i}`)
      }
      expect(() => game.addPlayer('overflow')).to.throw(/maximum .* reached/i)
    })

    it('must be possible to add new player if some left', function () {
      for (let i = 0; i < MAX_PLAYERS; i++) {
        game.addPlayer(`player${i}`)
      }
      game.addSet(FAKE_ECOSYS.id)
      game.currentSet.currentRound++
      game.players[0].roundLeft = game.currentSet.currentRound
      game.currentSet.currentRound++
      expect(() => game.addPlayer('last minute')).not.to.throw()
    })

    it('must fail if player with same name already exist', function () {
      game.addPlayer('Albus Dumbledore')
      expect(() => game.addPlayer('Albus Dumbledore')).to.throw(/same name/i)
    })

    it('must refuse to add player if game closed', function () {
      expect(() => game.addPlayer('Harry Potter')).to.not.throw()
      addSetsAndCloseGame(game)
      expect(() => game.addPlayer('Hermione Granger')).to.throw(/cannot be modified/i)
    })
  })

  describe('#updatePlayer', function () {
    it('must update the player', function () {
      game.addPlayer('Harry Potter')
      game.updatePlayer(0, 'Ron Weasley')
      expect(game.players[0].name).to.equal('Ron Weasley')
    })

    it('must fail if player does not exist', function () {
      game.addPlayer('Albus Dumbledore')
      expect(() => game.updatePlayer(1, 'Lord Voldemort')).to.throw(/no such player/i)
    })

    it('must fail if player with same name already exist', function () {
      game.addPlayer('Harry Potter')
      game.addPlayer('Ron Weasley')
      expect(() => game.updatePlayer(1, 'Harry Potter')).to.throw(/same name/i)
    })

    it('must refuse modification if game closed', function () {
      game.addPlayer('Hermione Granger')
      addSetsAndCloseGame(game)
      expect(() => game.updatePlayer(0, 'Luna Lovegood')).to.throw(/cannot be modified/i)
    })
  })

  describe('#deletePlayer', function () {
    it('must make the player leave at current round', function () {
      game.addPlayer('Harry Potter')
      game.addSet(FAKE_ECOSYS.id)
      game.currentSet.currentRound++
      game.deletePlayer(0)
      expect(game.players[0].roundLeft).to.equal(1)
    })

    it('must delete player if arrived at this round', function () {
      game.addPlayer('Ron Weasley')
      game.deletePlayer(0)
      game.addSet(FAKE_ECOSYS.id)
      game.currentSet.currentRound++
      game.addPlayer('Luna Lovegood')
      game.deletePlayer(0)
      expect(game.players).to.have.lengthOf(0)
    })

    it('must fail if player does not exist', function () {
      game.addPlayer('Albus Dumbledore')
      game.addSet(FAKE_ECOSYS.id)
      game.currentSet.currentRound++
      expect(() => game.deletePlayer(1)).to.throw(/no such player/i)
    })

    it('must refuse modification if game closed', function () {
      game.addPlayer('Hermione Granger')
      addSetsAndCloseGame(game)
      expect(() => game.deletePlayer(0)).to.throw(/cannot be modified/i)
    })
  })

  describe('#onGamePlayers', function () {
    it('must give the list of non-left players', function () {
      for (let i = 0; i < 6; i++) {
        game.addPlayer(`player${i}`)
      }
      game.addSet(FAKE_ECOSYS.id)
      setSetValueTo(game.roundSets[0], 3)
      game.players[2].roundLeft = 2
      game.players[3].roundLeft = 2
      expect(game.onGamePlayers).to.have.lengthOf(4)
      expect(game.onGamePlayers).to.have.members([0, 1, 4, 5])
    })
  })

  describe('#addSet', function () {
    for (let playerCt = 1; playerCt <= 3 * ROUND_PS; playerCt++) {
      it(`must add set to the game when there are ${playerCt} players`, function () {
        for (let i = 0; i < playerCt; i++) {
          game.addPlayer(`player${i}`)
        }
        expect(game.players).to.have.lengthOf(playerCt)
        game.addSet(FAKE_ECOSYS.id)
        for (let i = 0; i < 3; i++) {
          game.addNonPlayerCharacter()
        }
        expect(game.roundSets).to.have.lengthOf(1)
        expect(game.characters).to.have.lengthOf(playerCt + 3)
        expect(checkRandom(game.characters)).to.be.true
      })
    }

    it('must refuse to create set if current one is not finished', function () {
      game.addSet(FAKE_DEBTSYS.id)
      game.roundSets[0].currentRound++
      expect(() => game.addSet(FAKE_LIBRESYS.id)).to.throw(/cannot create .* not finished/i)
    })

    it('must refuse to create set if game closed', function () {
      addSetsAndCloseGame(game)
      expect(() => game.addSet(FAKE_ECOSYS.id))
    })

    it('must fail at character creation if current eco system not found', function () {
      expect(
        () =>
          new Game({
            start: Date.now(),
            masterId: 'dummy',
            location: {},
            roundsPerSet: 10,
            roundLength: 5,
            players: [
              {
                name: 'Zorro',
                roundArrived: 0,
                roundLeft: 0,
              },
            ],
            roundSets: [
              {
                ecoSysId: 'not-exist',
                currentRound: 5,
                techBreakRounds: [3],
              },
            ],
            characters: [
              {
                roundSet: 0,
                player: 0,
                score: [],
                totalScore: 1,
              },
            ],
            closed: false,
          })
      ).to.throw(/not found/i)
    })

    it('must not fail at character creation if finished eco system not found', function () {
      expect(
        () =>
          new Game({
            start: Date.now(),
            masterId: 'dummy',
            location: {},
            roundsPerSet: 10,
            roundLength: 5,
            players: [
              {
                name: 'Zorro',
                roundArrived: 0,
                roundLeft: 0,
              },
            ],
            roundSets: [
              {
                ecoSysId: 'not-exist',
                currentRound: ROUND_PS + 1,
                techBreakRounds: [7],
              },
              {
                ecoSysId: FAKE_ECOSYS.id,
                currentRound: 3,
                techBreakRounds: [],
              },
            ],
            characters: [
              {
                roundSet: 0,
                player: 0,
                score: [],
                totalScore: 1,
              },
            ],
            closed: false,
          })
      ).to.not.throw()
    })
  })

  describe('#currentSet', function () {
    it('must give the set in progress', function () {
      game.addSet(FAKE_ECOSYS.id)
      expect(game.currentSet).to.equal(game.roundSets[0])
    })

    it('must fail if no set in progress', function () {
      expect(() => game.currentSet).to.throw()
      game.addSet(FAKE_ECOSYS.id)
      setSetValueTo(game.roundSets[0], ROUND_PS + 1)
      expect(() => game.currentSet).to.throw()
    })
  })

  describe('#characters', function () {
    let character: Character

    beforeEach('Prepare players', function () {
      game.addPlayer('Stephen King')
      game.addSet(FAKE_ECOSYS.id)
      character = game.characters[0]
    })

    it('must accept modifications of score only for current round', function () {
      expect(() => (character.currentScore.values.low = 1)).to.not.throw()
      expect(() => (character.score[0].money.medium = 2)).to.not.throw()
      expect(character.score[0].values.low).to.equal(1)
      expect(character.score[0].money.medium).to.equal(2)
      game.currentSet.currentRound++
      expect(() => (character.currentScore.values.high = 3)).to.not.throw()
      game.currentSet.currentRound++
      expect(() => (character.score[1].money.low = 4)).to.throw(/cannot be modified/i)
      expect(character.score[1].values.high).to.equal(3)
      expect(character.score[1].money.low).to.equal(0)
    })

    it('must accept modifications of total score only for non finished round', function () {
      expect(() => character.totalScore++).to.not.throw()
      game.currentSet.currentRound++
      expect(() => character.totalScore++).to.not.throw()
      setSetValueTo(game.currentSet, ROUND_PS)
      expect(() => character.totalScore++).to.not.throw()
      game.currentSet.currentRound++
      expect(() => character.totalScore++).to.throw(/cannot be modified/i)
    })
  })

  describe('#addNonPlayerCharacter', function () {
    it('must create NPC for required sets', function () {
      game.addSet(FAKE_DEBTSYS.id)
      expect(game.addNonPlayerCharacter()).to.equal(-1)
      game.roundSets[0].currentRound++
      expect(game.addNonPlayerCharacter()).to.equal(-2)
      setSetValueTo(game.roundSets[0], ROUND_PS + 1)
      game.addSet(FAKE_LIBRESYS.id)
      expect(game.addNonPlayerCharacter()).to.equal(-1)
      game.roundSets[1].currentRound++
      expect(game.addNonPlayerCharacter()).to.equal(-2)
    })

    it('must fail if no set in progress', function () {
      expect(() => game.addNonPlayerCharacter()).to.throw()
      game.addSet(FAKE_ECOSYS.id)
      setSetValueTo(game.roundSets[0], ROUND_PS + 1)
      expect(() => game.addNonPlayerCharacter()).to.throw()
    })
  })

  describe('#getCharacterFor', function () {
    beforeEach('Add players to game', function () {
      for (let i = 0; i < ROUND_PS; i++) {
        game.addPlayer(`player${i}`)
      }
      game.addSet(FAKE_ECOSYS.id)
      game.addNonPlayerCharacter()
    })

    it('must give appropriate character for given player', function () {
      for (let i = 0; i < ROUND_PS; i++) {
        expect(game.getCharacterFor(i).player).to.equal(i)
      }
    })

    it('must give appropriate character for given NPC', function () {
      expect(game.getCharacterFor(-1).player).to.equal(-1)
    })

    it('must fail if no character found', function () {
      expect(() => game.getCharacterFor(ROUND_PS + 1)).to.throw(/no character found/i)
      expect(() => game.getCharacterFor(-2)).to.throw(/no character found/i)
    })

    it('must fail if no set in progress', function () {
      setSetValueTo(game.roundSets[0], ROUND_PS + 1)
      expect(() => game.getCharacterFor(0)).to.throw()
    })
  })

  describe('#getValuesHelpSheet', function () {
    it('must return cyclic value', function () {
      game.addSet(FAKE_ECOSYS.id)
      game.currentSet.currentRound++
      expect(game.buildValuesHelpSheet()).to.deep.equal({
        low: PlaceContent.Red,
        medium: PlaceContent.Yellow,
        high: PlaceContent.Green,
        waiting: PlaceContent.Blue,
      })
      game.currentSet.addTechBreak()
      game.currentSet.currentRound++
      expect(game.buildValuesHelpSheet()).to.deep.equal({
        low: PlaceContent.Yellow,
        medium: PlaceContent.Green,
        high: PlaceContent.Blue,
        waiting: PlaceContent.Red,
      })
    })
  })

  describe('#save', function () {
    let createStub: SinonStub<[Readonly<ModelGame>], Promise<string>>
    let updateStub: SinonStub<[Readonly<ModelGame>], Promise<void>>

    beforeEach('Create doubles', function () {
      createStub = sinon.stub(model, 'create')
      updateStub = sinon.stub(model, 'update')
    })

    afterEach('Clean up', function () {
      updateStub.restore()
      createStub.restore()
    })

    it('must create new game', async function () {
      createStub.resolves('game-id')
      await expect(game.save()).to.be.fulfilled
      expect(game._id).to.equal('game-id')
      expect(createStub).to.have.been.calledOnce
      expect(updateStub).to.not.have.been.called
    })

    it('must update existing game', async function () {
      game._id = 'game-id'
      updateStub.resolves()
      await expect(game.save()).to.be.fulfilled
      expect(createStub).to.not.have.been.called
      expect(updateStub).to.have.been.calledOnce
    })
  })

  describe('#delete', function () {
    let deleteStub: SinonStub<[string], Promise<void>>

    beforeEach('Create doubles', function () {
      deleteStub = sinon.stub(model, 'delete')
    })

    afterEach('Clean up', function () {
      deleteStub.restore()
    })

    it('must delete game from database', async function () {
      game._id = 'game-id'
      deleteStub.resolves()
      await expect(game.delete()).to.be.fulfilled
      expect(deleteStub).to.have.been.calledOnce
    })

    it('must not do anything if game is not persistent', async function () {
      await expect(game.delete()).to.be.fulfilled
      expect(deleteStub).to.not.have.been.called
    })
  })

  describe('#absoluteCurrentRound', function () {
    it('must give the absolute current round', function () {
      game.addSet(FAKE_DEBTSYS.id)
      expect(game.absoluteCurrentRound).to.equal(0)
      game.currentSet.currentRound++
      expect(game.absoluteCurrentRound).to.equal(1)
      setSetValueTo(game.currentSet, 4)
      expect(game.absoluteCurrentRound).to.equal(4)
      setSetValueTo(game.currentSet, ROUND_PS)
      expect(game.absoluteCurrentRound).to.equal(ROUND_PS)
      game.currentSet.currentRound++
      expect(game.absoluteCurrentRound).to.equal(ROUND_PS)
      game.addSet(FAKE_LIBRESYS.id)
      expect(game.absoluteCurrentRound).to.equal(ROUND_PS)
      game.currentSet.currentRound++
      expect(game.absoluteCurrentRound).to.equal(ROUND_PS + 1)
      setSetValueTo(game.currentSet, ROUND_PS)
      expect(game.absoluteCurrentRound).to.equal(2 * ROUND_PS)
    })
  })

  describe('#setInProgress', function () {
    it('must indicate if set is in progress', function () {
      expect(game.setInProgress).to.be.false
      game.addSet(FAKE_DEBTSYS.id)
      expect(game.setInProgress).to.be.true
      game.currentSet.currentRound++
      expect(game.setInProgress).to.be.true
      setSetValueTo(game.currentSet, ROUND_PS)
      expect(game.setInProgress).to.be.true
      game.currentSet.currentRound++
      expect(game.setInProgress).to.be.false
      game.addSet(FAKE_LIBRESYS.id)
      expect(game.setInProgress).to.be.true
    })
  })

  describe('#cloneToDoc', function () {
    it('must provide doc', function () {
      game = new Game('new-id', { name: 'Belejo' }, 10, 5)
      game.addPlayer('Tic')
      game.addPlayer('Tac')
      game.addSet(FAKE_DEBTSYS.id)
      setSetValueTo(game.roundSets[0], 7)
      game.roundSets[0].addTechBreak()
      setSetValueTo(game.roundSets[0], 11)
      game.addSet(FAKE_LIBRESYS.id)
      game.addNonPlayerCharacter()
      setSetValueTo(game.roundSets[1], 11)
      game.closed = true
      const gameDoc = game.cloneToDoc()
      expect(gameDoc).to.have.all.keys(
        'start',
        'masterId',
        'location',
        'roundsPerSet',
        'roundLength',
        'players',
        'roundSets',
        'characters',
        'closed'
      )
      expect(gameDoc.start)
        .to.be.gte(Date.now() - 1000)
        .and.lte(Date.now())
      expect(gameDoc.masterId).to.equal('new-id')
      expect(gameDoc.location).to.deep.equal({ name: 'Belejo' })
      expect(gameDoc.roundsPerSet).to.equal(10)
      expect(gameDoc.roundLength).to.equal(5)
      expect(gameDoc.players).to.have.lengthOf(2)
      expect(gameDoc.players[0].name).to.equal('Tic')
      expect(gameDoc.roundSets).to.have.lengthOf(2)
      expect(gameDoc.roundSets[0].ecoSysId).to.equal(FAKE_DEBTSYS.id)
      expect(gameDoc.roundSets[1].currentRound).to.be.gt(ROUND_PS)
      expect(gameDoc.roundSets[0].techBreakRounds).to.have.lengthOf(1)
      expect(gameDoc.characters).to.have.lengthOf(5)
      expect(gameDoc.closed).to.be.true
    })
  })
})
