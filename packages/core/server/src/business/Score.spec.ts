/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import { Score } from '.'
import { ScoreType } from './Score'

describe('Score', function () {
  let score: Score
  const totalScore: number[] = Array(2).fill(0)
  const updateScore = (value: number, type: ScoreType): number => (totalScore[type] += value)

  beforeEach('Create default score card', function () {
    totalScore[ScoreType.Values] = 0
    totalScore[ScoreType.Money] = 0
    score = new Score(updateScore, 1)
  })

  describe('#contructor', function () {
    it('must set score to 0 until modified', function () {
      expect(score.round).to.equal(1)
      expect(score.values.low).to.equal(0)
      expect(score.values.medium).to.equal(0)
      expect(score.values.high).to.equal(0)
      expect(score.money.low).to.equal(0)
      expect(score.money.medium).to.equal(0)
      expect(score.money.high).to.equal(0)
      expect(totalScore[ScoreType.Values]).to.equal(0)
      expect(totalScore[ScoreType.Money]).to.equal(0)
    })

    it('must be created correctly from doc', function () {
      score = new Score(updateScore, {
        round: 3,
        values: {
          low: 12,
          medium: 24,
          high: 36,
        },
        money: {
          low: 48,
          medium: 60,
          high: 72,
        },
      })
      const scoreDoc = score.cloneToDoc()
      expect(scoreDoc).to.have.all.keys('round', 'values', 'money')
      expect(scoreDoc.round).to.equal(3)
      expect(scoreDoc.values.low).to.equal(12)
      expect(scoreDoc.values.medium).to.equal(24)
      expect(scoreDoc.values.high).to.equal(36)
      expect(scoreDoc.money.low).to.equal(48)
      expect(scoreDoc.money.medium).to.equal(60)
      expect(scoreDoc.money.high).to.equal(72)
      expect(totalScore[ScoreType.Values]).to.equal(0)
      expect(totalScore[ScoreType.Money]).to.equal(0)
    })
  })

  describe('#(data count)', function () {
    it('must set appropriate score to appropriate value', function () {
      score.values.low = 1
      score.values.medium = 2
      score.values.high = 4
      score.money.low = 8
      score.money.medium = 16
      score.money.high = 32
      expect(score.values.low).to.equal(1)
      expect(score.values.medium).to.equal(2)
      expect(score.values.high).to.equal(4)
      expect(score.money.low).to.equal(8)
      expect(score.money.medium).to.equal(16)
      expect(score.money.high).to.equal(32)
      expect(totalScore[ScoreType.Values]).to.equal(21)
      expect(totalScore[ScoreType.Money]).to.equal(168)
    })
  })

  describe('#zero', function () {
    it('must be true if all values are null', function () {
      expect(score.values.zero, 'Unexpected values score').to.be.true
      expect(score.money.zero, 'Unexpected money score').to.be.true
      expect(score.zero, 'Unexpected score').to.be.true
    })

    it('must be false if any values is not null', function () {
      score.values.medium = 1
      expect(score.values.zero, 'Unexpected values score').to.be.false
      expect(score.money.zero, 'Unexpected money score').to.be.true
      expect(score.zero, 'Unexpected score').to.be.false
    })
  })

  describe('#cloneToDoc', function () {
    it('must provide doc', function () {
      score.values.low = 1
      score.values.medium = 2
      score.values.high = 4
      score.money.low = 8
      score.money.medium = 16
      score.money.high = 32
      const scoreDoc = score.cloneToDoc()
      expect(scoreDoc).to.have.all.keys('round', 'values', 'money')
      expect(scoreDoc.round).to.equal(1)
      expect(scoreDoc.values.low).to.equal(1)
      expect(scoreDoc.values.medium).to.equal(2)
      expect(scoreDoc.values.high).to.equal(4)
      expect(scoreDoc.money.low).to.equal(8)
      expect(scoreDoc.money.medium).to.equal(16)
      expect(scoreDoc.money.high).to.equal(32)
    })
  })
})
