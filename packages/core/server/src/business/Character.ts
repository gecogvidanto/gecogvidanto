/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { ServerCharacter } from '@gecogvidanto/plugin'
import { Character as CharacterDoc } from '@gecogvidanto/shared'

import Score, { ScoreType } from './Score'

export interface GameData {
  currentRound: number
  techBreakCount: number
}

/**
 * A character.
 */
export default class Character implements ServerCharacter {
  public readonly roundSet: number
  public readonly player: number
  public readonly generation?: number
  public readonly score: Score[]

  private _totalScore: number
  private scoreIsMoney: boolean

  /**
   * Create a character from the given document.
   *
   * @param gameData - Function to get game data for the current set.
   * @param valueCost - The cost of a value in money given by the economic system.
   * @param character - The document.
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public constructor(gameData: () => GameData | undefined, valueCost: number, character: CharacterDoc)

  /**
   * Create a character from data.
   *
   * @param gameData - Function to get game data for the current set.
   * @param valueCost - The cost of a value in money given by the economic system.
   * @param roundSet - The set number (index in game set array).
   * @param player - The player number (if >= 0, index in game player array, if < 0, quasi-index in set NPC array).
   * @param generation - Generation (round of death) of the character.
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public constructor(
    gameData: () => GameData | undefined,
    valueCost: number,
    roundSet: number,
    player: number,
    generation?: number
  )

  /*
   * Constructor implementation.
   */
  public constructor(
    private readonly gameData: () => GameData | undefined,
    private readonly valueCost: number,
    setOrDoc: number | CharacterDoc,
    player?: number,
    generation?: number
  ) {
    if (typeof setOrDoc === 'number') {
      this.roundSet = setOrDoc
      this.player = player!
      this.generation = generation
      this.score = []
      this._totalScore = 0
    } else {
      this.roundSet = setOrDoc.roundSet
      this.player = setOrDoc.player
      this.generation = setOrDoc.generation
      this.score = setOrDoc.score.map(score => new Score(this.buildScoreFunction(), score))
      this._totalScore = setOrDoc.totalScore
    }
    this.scoreIsMoney = gameData() !== undefined
  }

  /**
   * Create a new character in which player identifier is decreased to reflect a lower index deleted player.
   *
   * @returns The created character.
   */
  public shiftForDeletedPlayer(): Character {
    const result = new Character(
      this.gameData,
      this.valueCost,
      this.roundSet,
      this.player - 1,
      this.generation
    )
    this.score.forEach(score => result.score.push(score))
    result._totalScore = this._totalScore
    result.scoreIsMoney = this.scoreIsMoney
    return result
  }

  public get currentScore(): Score {
    this.requireModifiable()
    const round = this.gameData()!.currentRound
    let currentScore = this.score.find(score => score.round === round)
    if (!currentScore) {
      currentScore = new Score(this.buildScoreFunction(), round)
      this.score.push(currentScore)
    }
    return currentScore
  }

  public get totalScore(): number {
    if (this.scoreIsMoney && this.gameData() === undefined) {
      this.scoreIsMoney = false
      this._totalScore /= this.valueCost
    }
    return this._totalScore
  }

  public set totalScore(value: number) {
    this.requireModifiable()
    this._totalScore = value
  }

  /**
   * Create a clone of the object in a simple document.
   *
   * @param safe - If safe, some values will not be shown.
   * @returns The character document.
   */
  public cloneToDoc(safe = false): CharacterDoc {
    const data = this.gameData()
    const hideScore: boolean = safe && data !== undefined
    const result: CharacterDoc = {
      roundSet: this.roundSet,
      player: this.player,
      score: hideScore
        ? []
        : this.score
            .filter(score => !score.zero)
            .sort((a, b) => a.round - b.round)
            .map(score => score.cloneToDoc()),
      totalScore: hideScore ? 0 : this.totalScore,
    }
    if (
      this.generation !== undefined &&
      (!safe || data === undefined || this.generation < data.currentRound)
    ) {
      ;(result as any).generation = this.generation
    }
    return result
  }

  /**
   * Build the function used to update the total score.
   *
   * @returns The function to update score.
   */
  private buildScoreFunction(): (value: number, type: ScoreType, round: number) => void {
    return (value: number, type: ScoreType, round: number) => {
      const data = this.gameData()
      if (!data || data.currentRound !== round) {
        throw new Error('Old scores cannot be modified')
      }
      switch (type) {
        case ScoreType.Values:
          this._totalScore += value * this.valueCost * Math.pow(2, data.techBreakCount)
          break
        case ScoreType.Money:
          this._totalScore += value
          break
        default:
          // eslint-disable-next-line no-case-declarations
          const scoreType: never = type
          throw new Error(`Unexpected score type: ${scoreType}`)
      }
    }
  }

  /**
   * Ensure the character is modifiable. Throw an exception if it is not.
   */
  private requireModifiable(): void {
    if (this.gameData() === undefined) {
      throw new Error('Inactive character cannot be modified')
    }
  }
}
