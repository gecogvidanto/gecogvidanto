/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Gecobject as GecobjectDoc } from '@gecogvidanto/shared'

/**
 * Base class of ĞecoĞvidanto objects.
 */
export default abstract class Gecobject<T extends GecobjectDoc> implements GecobjectDoc {
  private _id$?: string

  /**
   * Create an object from a document, or from nothing.
   *
   * @param gecobjectDoc - The document.
   */
  protected constructor(gecobjectDoc?: T) {
    gecobjectDoc && (this._id$ = gecobjectDoc._id)
  }

  /**
   * @returns The identifier of the object. This must not be called on non-persistent object.
   */
  public get _id(): string {
    if (this._id$) {
      return this._id$
    } else {
      throw new Error('Non persistent object')
    }
  }

  /**
   * Set the identifier of the object. This must not be called on persistent object.
   *
   * @param _id - The identifier.
   */
  public set _id(_id: string) {
    if (this._id$) {
      throw new Error('Cannot update object identifier')
    } else {
      this._id$ = _id
    }
  }

  /**
   * @returns True if the object is persistent, meaning already has an identifier.
   */
  public get persistent(): boolean {
    return !!this._id$
  }

  /**
   * Compare this object with the other one. Only identifiers are compared, so objects must be persistent.
   *
   * @param other - The object to compare with.
   * @returns True if objects are the same.
   */
  public isSameAs(other: Gecobject<any>): boolean {
    return !!this._id$ && this._id$ === other._id$
  }

  /**
   * Clone this object to a document (litteral object).
   *
   * @returns The pure object.
   */
  public cloneToDoc(): GecobjectDoc {
    const result: GecobjectDoc = {}
    this._id$ && ((result as any)._id = this._id$)
    return result
  }
}
