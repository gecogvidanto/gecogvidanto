/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import { Character } from '.'
import { GameData } from './Character'

describe('Character', function () {
  let character: Character
  const filledGameData: GameData = {
    currentRound: 1,
    techBreakCount: 0,
  }
  let gameData: GameData | undefined
  const onRequestGameData = (): GameData | undefined => gameData

  beforeEach('Create default player', function () {
    filledGameData.currentRound = 1
    filledGameData.techBreakCount = 0
    gameData = filledGameData
    character = new Character(onRequestGameData, 2, 1, 2, 3)
  })

  describe('#constructor', function () {
    it('must be created with appropriate values', function () {
      expect(character.roundSet).to.equal(1)
      expect(character.player).to.equal(2)
      expect(character.generation).to.equal(3)
      expect(character.score).to.be.empty
    })

    it('must be created with no generation if none given', function () {
      character = new Character(onRequestGameData, 2, 12, 24)
      expect(character.roundSet).to.equal(12)
      expect(character.player).to.equal(24)
      expect(character.generation).to.be.undefined
      expect(character.score).to.be.empty
    })

    it('must be created with no score', function () {
      expect(character.score).to.be.empty
    })

    it('must be created correctly from doc', function () {
      character = new Character(onRequestGameData, 2, {
        roundSet: 123,
        player: 321,
        score: [
          {
            round: 3,
            values: {
              low: 0,
              medium: 2,
              high: 3,
            },
            money: {
              low: 4,
              medium: 5,
              high: 6,
            },
          },
          {
            round: 10,
            values: {
              low: 7,
              medium: 8,
              high: 9,
            },
            money: {
              low: 10,
              medium: 11,
              high: 0,
            },
          },
        ],
        totalScore: 12,
      })
      const characterDoc = character.cloneToDoc()
      expect(characterDoc).to.have.all.keys('roundSet', 'player', 'score', 'totalScore')
      expect(characterDoc.roundSet).to.equal(123)
      expect(characterDoc.player).to.equal(321)
      expect(characterDoc.score).to.have.lengthOf(2)
      expect(characterDoc.score[0].round).to.equal(3)
      expect(characterDoc.score[1].round).to.equal(10)
      expect(characterDoc.score[0].values.medium).to.equal(2)
      expect(characterDoc.score[0].money.high).to.equal(6)
      expect(characterDoc.score[1].values.low).to.equal(7)
      expect(characterDoc.score[1].money.medium).to.equal(11)
      expect(characterDoc.totalScore).to.equal(12)
      filledGameData.currentRound = 10
      expect(() => (character.score.find(score => score.round === 3)!.values.low = 1)).to.throw(
        /cannot be modified/i
      )
      expect(() => (character.score.find(score => score.round === 10)!.money.high = 12)).to.not.throw()
      expect(character.totalScore).to.equal(60)
    })
  })

  describe('#currentScore', function () {
    it('must create score card for current round', function () {
      filledGameData.currentRound = 2
      character.currentScore.values.low = 1
      character.currentScore.values.medium = 2
      character.currentScore.values.high = 3
      character.currentScore.money.low = 4
      character.currentScore.money.medium = 5
      character.currentScore.money.high = 6
      filledGameData.currentRound = 5
      character.currentScore.values.low = 7
      character.currentScore.values.medium = 8
      character.currentScore.values.high = 9
      filledGameData.currentRound = 8
      filledGameData.techBreakCount = 1
      character.currentScore.money.low = 10
      character.currentScore.money.medium = 11
      character.currentScore.money.high = 12
      character.currentScore.values.medium = 3
      expect(character.score).to.have.lengthOf(3)
      expect(character.score[0].round).to.equal(2)
      expect(character.score[1].round).to.equal(5)
      expect(character.score[2].round).to.equal(8)
      expect(character.score[0].values.low).to.equal(1)
      expect(character.score[0].money.medium).to.equal(5)
      expect(character.score[1].values.high).to.equal(9)
      expect(character.score[2].money.low).to.equal(10)
      expect(character.totalScore).to.equal(294)
    })

    it('must fail if score is sending bad values', function () {
      filledGameData.currentRound = 1
      const updateScore = (character as any).buildScoreFunction()
      expect(() => updateScore(1, 3, 1)).to.throw(/score type/i)
    })
  })

  describe('#score', function () {
    it('must refuse score modification for non current round', function () {
      expect(() => (character.currentScore.values.low = 1)).to.not.throw()
      expect(() => (character.score.find(score => score.round === 1)!.money.medium = 2)).to.not.throw()
      filledGameData.currentRound = 3
      expect(character.score.find(score => score.round === 1)!.values.low).to.equal(1)
      expect(character.score.find(score => score.round === 1)!.money.medium).to.equal(2)
      expect(character.totalScore).to.equal(6)
      expect(() => (character.score.find(score => score.round === 1)!.values.high = 3)).to.throw(
        /cannot be modified/i
      )
    })

    it('must refuse score modification for undefined round', function () {
      expect(() => (character.currentScore.values.low = 1)).to.not.throw()
      gameData = undefined
      expect(character.score.find(score => score.round === 1)!.values.low).to.equal(1)
      expect(character.totalScore).to.equal(1)
      expect(() => (character.score.find(score => score.round === 1)!.values.high = 3)).to.throw(
        /cannot be modified/i
      )
    })
  })

  describe('#totalScore', function () {
    it('must modify total score unless there is no current round', function () {
      expect(() => (character.totalScore = 24)).to.not.throw()
      filledGameData.currentRound = 3
      expect(() => (character.totalScore += 12)).to.not.throw()
      gameData = undefined
      expect(character.totalScore).to.equal(18)
      expect(() => character.totalScore++).to.throw(/cannot be modified/i)
    })
  })

  describe('#cloneToDoc', function () {
    beforeEach('Prepare character', function () {
      character = new Character(onRequestGameData, 2, 2, 12, 8)
      filledGameData.currentRound = 6
      character.currentScore.values.low = 0
      character.currentScore.values.medium = 2
      character.currentScore.values.high = 4
      character.currentScore.money.low = 1
      character.currentScore.money.medium = 3
      character.currentScore.money.high = 5
      character.totalScore += 36
    })

    it('must provide doc', function () {
      const characterDoc = character.cloneToDoc()
      expect(characterDoc).to.have.all.keys('roundSet', 'player', 'generation', 'score', 'totalScore')
      expect(characterDoc.roundSet).to.equal(2)
      expect(characterDoc.player).to.equal(12)
      expect(characterDoc.generation).to.equal(8)
      expect(character.score).to.have.lengthOf(1)
      expect(character.score[0].round).to.equal(6)
      expect(characterDoc.score[0].values.high).to.equal(4)
      expect(characterDoc.score[0].money.low).to.equal(1)
      expect(characterDoc.totalScore).to.equal(103)
    })

    it('must not give generation if no generation provided', function () {
      ;(character as any).generation = undefined
      const characterDoc = character.cloneToDoc()
      expect(characterDoc).to.have.all.keys('roundSet', 'player', 'score', 'totalScore')
    })

    describe('(safe mode)', function () {
      it('must give all data if set finished', function () {
        gameData = undefined
        const characterDoc = character.cloneToDoc(true)
        expect(characterDoc).to.have.all.keys('roundSet', 'player', 'generation', 'score', 'totalScore')
        expect(characterDoc.roundSet).to.equal(2)
        expect(characterDoc.player).to.equal(12)
        expect(characterDoc.generation).to.equal(8)
        expect(characterDoc.score).to.have.lengthOf(1)
        expect(character.score[0].round).to.equal(6)
        expect(characterDoc.score[0].values.high).to.equal(4)
        expect(characterDoc.score[0].money.low).to.equal(1)
        expect(characterDoc.totalScore).to.equal(51.5)
      })

      it('must not give generation until death round', function () {
        for (
          filledGameData.currentRound = 0;
          filledGameData.currentRound < 9;
          filledGameData.currentRound++
        ) {
          const characterDoc = character.cloneToDoc(true)
          expect(characterDoc).to.not.have.any.keys('generation')
        }
      })

      it('must give generation if death occurred', function () {
        for (
          filledGameData.currentRound = 9;
          filledGameData.currentRound <= 12;
          filledGameData.currentRound++
        ) {
          const characterDoc = character.cloneToDoc(true)
          expect(characterDoc).to.have.any.keys('generation')
          expect(characterDoc.generation).to.equal(8)
        }
      })

      it('must not give score if set in progress', function () {
        for (
          filledGameData.currentRound = 0;
          filledGameData.currentRound <= 12;
          filledGameData.currentRound++
        ) {
          const characterDoc = character.cloneToDoc(true)
          expect(characterDoc.score).to.have.lengthOf(0)
          expect(characterDoc.totalScore).to.equal(0)
        }
      })
    })
  })
})
