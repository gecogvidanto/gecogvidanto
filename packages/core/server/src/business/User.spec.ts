/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import * as BCrypt from 'bcrypt'
import { expect } from 'chai'
import * as sinon from 'sinon'
import { SinonStub } from 'sinon'

import { CertLevel } from '@gecogvidanto/shared'

import { User } from '.'
import { addConfig, addModel } from '../AppData.spec'
import { UserModel } from '../database'

type Identifier = import('@gecogvidanto/plugin').Identifier
type ModelUser = import('../database/UserModel').ModelUser
const model = new UserModel(undefined as any)
addModel('user', model)

const MAX_FAILURES = 3
addConfig(MAX_FAILURES, 'security', 'maxFailures')
const SALT_ROUNDS = 10

const ADMIN = {
  email: 'linus@example.com',
  name: 'Linus T.',
  level: CertLevel.Administrator,
  account: 'IBAN',
}
const MASTER = {
  email: 'd.targaryen@example.com',
  name: 'Daenerys',
  level: CertLevel.GameMaster,
}

describe('User', function () {
  let user: User

  beforeEach('Create default user', function () {
    user = new User(MASTER.email, MASTER.name)
  })

  describe('#constructor', function () {
    it('must provide default level if none set', function () {
      expect(user.level).to.equal(CertLevel.GameMaster)
    })

    it('must provide correct level if set', function () {
      user.level = CertLevel.Administrator
      expect(user.level).to.equal(CertLevel.Administrator)
    })

    it('must be created correctly from UserDoc with minimal info', function () {
      user = new User(ADMIN)
      const userDoc = user.cloneToDoc()
      expect(userDoc).to.deep.equal(ADMIN)
    })

    it('must be created correctly from UserDoc with all info', function () {
      user = new User({ _id: 'khaleesi', ...MASTER })
      const userDoc = user.cloneToDoc()
      expect(userDoc).to.deep.equal({ _id: 'khaleesi', ...MASTER })
    })
  })

  describe('#search', function () {
    let readStub: SinonStub<[string?, string?], Promise<(ModelUser & Identifier)[]>>

    beforeEach('Create doubles', function () {
      readStub = sinon.stub(model, 'search')
    })

    afterEach('Clean up', function () {
      readStub.restore()
    })

    it('must return found user', async function () {
      readStub.resolves([ADMIN, MASTER] as (ModelUser & Identifier)[])
      const users = await User.search()
      expect(users).to.have.lengthOf(2)
      expect(users[0].cloneToDoc()).to.deep.equal(ADMIN)
      expect(users[1].cloneToDoc()).to.deep.equal(MASTER)
      expect(readStub).to.have.been.calledOnce
    })

    it('must send query data', async function () {
      readStub.resolves([])
      await User.search(MASTER.email, MASTER.name)
      expect(readStub).to.have.been.calledOnceWith(MASTER.email, MASTER.name)
    })
  })

  describe('#find', function () {
    let findStub: SinonStub<[string], Promise<(ModelUser & Identifier) | undefined>>

    beforeEach('Create doubles', function () {
      findStub = sinon.stub(model, 'find')
    })

    afterEach('Clean up', function () {
      findStub.restore()
    })

    it('must return found user', async function () {
      findStub.resolves(MASTER as ModelUser & Identifier)
      const searchedUser = await User.find('khaleesi')
      expect(searchedUser).to.exist
      expect(searchedUser!.cloneToDoc()).to.deep.equal(MASTER)
      expect(findStub).to.have.been.calledOnceWith('khaleesi')
    })

    it('must return empty e-mail if none found', async function () {
      const { email, ...result } = MASTER
      findStub.resolves(result as ModelUser & Identifier)
      const searchedUser = await User.find('khaleesi')
      expect(searchedUser).to.exist
      expect(searchedUser!.cloneToDoc()).to.deep.equal({
        email: '',
        ...result,
      })
      expect(findStub).to.have.been.calledOnceWith('khaleesi')
    })

    it('must return undefined if no matching user', async function () {
      findStub.resolves(undefined)
      const searchedUser = await User.find('casper')
      expect(searchedUser).to.not.exist
      expect(findStub).to.have.been.calledOnceWith('casper')
    })
  })

  describe('#findConnection', function () {
    let emailStub: SinonStub<[string?, string?], Promise<(ModelUser & Identifier)[]>>
    let failStub: SinonStub<[string, boolean], Promise<void>>

    beforeEach('Create doubles', function () {
      emailStub = sinon.stub(model, 'search')
      failStub = sinon.stub(model, 'updateFailures')
    })

    afterEach('Clean up', function () {
      failStub.restore()
      emailStub.restore()
    })

    it('must return found user if any', async function () {
      const password = await BCrypt.hash('secret', SALT_ROUNDS)
      emailStub.resolves([{ _id: 'id-linus', ...ADMIN, password, failures: 0 }])
      const searchedUser = await User.findConnection(ADMIN.email, 'secret')
      expect(searchedUser).to.exist
      expect(searchedUser!.cloneToDoc()).to.deep.equal({
        _id: 'id-linus',
        ...ADMIN,
      })
      expect(emailStub).to.have.been.calledOnceWith(ADMIN.email)
      expect(failStub).to.not.have.been.called
    })

    it('must clear failed tries if needed', async function () {
      const password = await BCrypt.hash('secret', SALT_ROUNDS)
      emailStub.resolves([
        {
          _id: 'id-linus',
          ...ADMIN,
          password,
          failures: MAX_FAILURES - 1,
        },
      ])
      failStub.resolves()
      const searchedUser = await User.findConnection(ADMIN.email, 'secret')
      expect(searchedUser).to.exist
      expect(searchedUser!.cloneToDoc()).to.deep.equal({
        _id: 'id-linus',
        ...ADMIN,
      })
      expect(emailStub).to.have.been.calledOnceWith(ADMIN.email)
      expect(failStub).to.have.been.calledOnceWith('id-linus', true)
    })

    it('must forbid access if no matching user', async function () {
      emailStub.resolves([])
      const searchedUser = await User.findConnection(ADMIN.email, 'secret')
      expect(searchedUser).to.not.exist
      expect(emailStub).to.have.been.calledOnceWith(ADMIN.email)
      expect(failStub).to.not.have.been.called
    })

    it('must forbid access if too many failures', async function () {
      const password = await BCrypt.hash('secret', SALT_ROUNDS)
      emailStub.resolves([
        {
          _id: 'id-linus',
          ...ADMIN,
          password,
          failures: MAX_FAILURES,
        },
      ])
      const searchedUser = await User.findConnection(ADMIN.email, 'secret')
      expect(searchedUser).to.not.exist
      expect(emailStub).to.have.been.calledOnceWith(ADMIN.email)
      expect(failStub).to.not.have.been.called
    })

    it('must forbid access and count error if wrong password', async function () {
      const password = await BCrypt.hash('bad-password', SALT_ROUNDS)
      emailStub.resolves([{ _id: 'id-linus', ...ADMIN, password, failures: 0 }])
      failStub.resolves()
      const searchedUser = await User.findConnection(ADMIN.email, 'secret')
      expect(searchedUser).to.not.exist
      expect(emailStub).to.have.been.calledOnceWith(ADMIN.email)
      expect(failStub).to.have.been.calledOnceWith('id-linus', false)
    })
  })

  describe('#removeFailedCreations', function () {
    let removeStub: SinonStub<[string[]], Promise<void>>

    beforeEach('Create doubles', function () {
      removeStub = sinon.stub(model, 'removeFailedCreations')
    })

    afterEach('Clean up', function () {
      removeStub.restore()
    })

    it('must send data to model', async function () {
      removeStub.resolves()
      await expect(User.removeFailedCreations(['-id-'])).to.be.fulfilled
      expect(removeStub).to.have.been.calledOnceWith(['-id-'])
    })
  })

  describe('#save', function () {
    let createStub: SinonStub<[Readonly<ModelUser>], Promise<string>>
    let updateStub: SinonStub<[Readonly<ModelUser>], Promise<void>>

    beforeEach('Create doubles', function () {
      createStub = sinon.stub(model, 'create')
      updateStub = sinon.stub(model, 'update')
    })

    afterEach('Clean up', function () {
      updateStub.restore()
      createStub.restore()
    })

    it('must save existing user', async function () {
      user._id = 'bad-id'
      await expect(user.save()).to.be.fulfilled
      expect(createStub).to.not.have.been.called
      expect(updateStub).to.have.been.calledOnce
      const args = updateStub.firstCall.args
      expect(args).to.have.lengthOf(1)
      expect(args[0]).to.deep.equal({ _id: 'bad-id', ...MASTER })
    })

    it('must not save empty e-mail', async function () {
      user._id = 'id'
      user.email = ''
      await expect(user.save()).to.be.fulfilled
      expect(createStub).to.not.have.been.called
      expect(updateStub).to.have.been.calledOnce
      const args = updateStub.firstCall.args
      expect(args).to.have.lengthOf(1)
      const { email, ...result } = { _id: 'id', ...MASTER }
      expect(args[0]).to.deep.equal(result)
    })

    it('must save new user and give its identifier', async function () {
      createStub.resolves('khaleesi')
      await expect(user.save('dragon')).to.be.fulfilled
      expect(user._id).to.equal('khaleesi')
      expect(createStub).to.have.been.calledOnce
      expect(updateStub).to.not.have.been.called
      const args = createStub.firstCall.args
      expect(args).to.have.lengthOf(1)
      expect(args[0]).to.have.all.keys('email', 'name', 'level', 'failures', 'password')
      expect(args[0].email).to.equal(MASTER.email)
      expect(args[0].failures).to.equal(0)
      expect(args[0].password).to.not.equal('dragon')
    })

    it('must fail if creating user without password', async function () {
      await expect(user.save()).to.be.rejectedWith(/password is required/i)
      expect(createStub).to.not.have.been.called
      expect(updateStub).to.not.have.been.called
    })
  })

  describe('#checkUnicity', function () {
    let checkStub: SinonStub<[Readonly<ModelUser>], Promise<boolean>>

    beforeEach('Create doubles', function () {
      checkStub = sinon.stub(model, 'checkUnicity')
    })

    afterEach('Clean up', function () {
      checkStub.restore()
    })

    it('must send data to model', async function () {
      checkStub.resolves(true)
      await expect(user.checkUnicity()).to.eventually.be.true
      expect(checkStub).to.have.been.calledOnce
      const args = checkStub.firstCall.args
      expect(args).to.have.lengthOf(1)
      expect(args[0]).to.deep.equal(MASTER)
    })
  })

  describe('#delete', function () {
    let deleteStub: SinonStub<[string], Promise<void>>

    beforeEach('Create doubles', function () {
      deleteStub = sinon.stub(model, 'delete')
    })

    afterEach('Clean up', function () {
      deleteStub.restore()
    })

    it('must remove user from database', async function () {
      deleteStub.resolves()
      user._id = 'khaleesi'
      await expect(user.delete()).to.be.fulfilled
      expect(deleteStub).to.have.been.calledOnceWith('khaleesi')
    })

    it('must do nothing for non persistent user', async function () {
      await expect(user.delete()).to.be.fulfilled
      expect(deleteStub).to.not.have.been.called
    })
  })

  describe('#cloneToDoc', function () {
    it('must provide minimal doc if minimal info provided', function () {
      const userDoc = user.cloneToDoc()
      expect(userDoc).to.deep.equal(MASTER)
    })

    it('must provide full doc if all info provided', function () {
      user = new User(ADMIN.email, ADMIN.name, ADMIN.account)
      user.level = ADMIN.level
      user._id = 'id-linus'
      const userDoc = user.cloneToDoc()
      expect(userDoc).to.deep.equal({ _id: 'id-linus', ...ADMIN })
    })
  })
})
