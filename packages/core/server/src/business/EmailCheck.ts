/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { EmailCheck as EmailCheckDoc } from '@gecogvidanto/shared'

import { app } from '../AppData'
import { EmailCheckModel, ModelEmailCheck } from '../database'
import Gecobject from './Gecobject'

function model(): EmailCheckModel {
  return app.data.database.models.emailCheck
}
function validity(): number {
  return 1000 * app.data.config.security.validity.email
}

/**
 * An e-mail checking object, containing e-mail, user id and token.
 */
export default class EmailCheck extends Gecobject<EmailCheckDoc> implements EmailCheckDoc {
  public readonly email: string
  public readonly token: string

  /**
   * Create an EmailCheck from a document.
   *
   * @param emailCheck - The document.
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public constructor(emailCheck: EmailCheckDoc)

  /**
   * Create an EmailCheck from data.
   *
   * @param email - The e-mail address.
   * @param token - The control token.
   */
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public constructor(email: string, token: string)

  /*
   * Constructor implementation.
   */
  public constructor(emailOrDoc: string | EmailCheckDoc, token?: string) {
    super(typeof emailOrDoc === 'string' ? undefined : emailOrDoc)
    if (typeof emailOrDoc === 'string') {
      this.email = emailOrDoc
      this.token = token!
    } else {
      this.email = emailOrDoc.email
      this.token = emailOrDoc.token
    }
  }

  /**
   * Read identifiers of all users waiting for email confirmation.
   *
   * @returns User identifiers.
   */
  public static readAllUserIds(): Promise<string[]> {
    return model().readAllUserIds()
  }

  /**
   * Save the email check to the database.
   *
   * @param userId - The identifier of the concerned user.
   */
  public async save(userId: string): Promise<void> {
    if (!this.persistent) {
      await model().delete(this.email, userId)
      const emailCheck: ModelEmailCheck = {
        ...this.cloneToDoc(),
        userId,
        expires: Date.now() + validity(),
      }
      this._id = await model().create(emailCheck)
    } else {
      throw new Error('Email token cannot be updated')
    }
  }

  /**
   * Validate the current email check.
   *
   * @returns The identifier of matching user if validated.
   */
  public async validate(): Promise<string | undefined> {
    const result = await model().validate(this.email, this.token)
    await model().delete(this.email)
    return result
  }

  /**
   * Create a clone of the object in a simple document.
   *
   * @returns The pure document.
   */
  public cloneToDoc(): EmailCheckDoc {
    const result: EmailCheckDoc = {
      ...super.cloneToDoc(),
      email: this.email,
      token: this.token,
    }
    return result
  }
}
