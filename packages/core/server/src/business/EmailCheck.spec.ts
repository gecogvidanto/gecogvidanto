/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import * as sinon from 'sinon'
import { SinonStub } from 'sinon'

import { EmailCheck } from '.'
import { addConfig, addModel } from '../AppData.spec'
import { EmailCheckModel } from '../database'

type ModelEmailCheck = import('../database/EmailCheckModel').ModelEmailCheck
const model = new EmailCheckModel(undefined as any)
addModel('emailCheck', model)
addConfig(3600, 'security', 'validity', 'email')

const EMAIL_CHECK = {
  email: 'me@example.com',
  token: 'random',
}

describe('EmailCheck', function () {
  let emailCheck: EmailCheck

  beforeEach('Create default email check', function () {
    emailCheck = new EmailCheck(EMAIL_CHECK.email, EMAIL_CHECK.token)
  })

  describe('#readAllUserIds', function () {
    let readStub: SinonStub<[], Promise<string[]>>

    beforeEach('Prepare doubles', function () {
      readStub = sinon.stub(model, 'readAllUserIds')
    })

    afterEach('Clean up', function () {
      readStub.restore()
    })

    it('must give back database result', async function () {
      readStub.resolves(['id1', 'id2'])
      await expect(EmailCheck.readAllUserIds()).to.eventually.have.members(['id1', 'id2'])
      expect(readStub).to.have.been.calledOnce
    })
  })

  describe('#save', function () {
    let createStub: SinonStub<[Readonly<ModelEmailCheck>], Promise<string>>
    let deleteStub: SinonStub<[string, string?], Promise<void>>

    beforeEach('Prepare doubles', function () {
      createStub = sinon.stub(model, 'create')
      deleteStub = sinon.stub(model, 'delete')
    })

    afterEach('Clean up', function () {
      deleteStub.restore()
      createStub.restore()
    })

    it('must prepare and send data to database', async function () {
      deleteStub.resolves()
      createStub.resolves('new-id')
      await expect(emailCheck.save('user-id')).to.be.fulfilled
      expect(emailCheck._id).to.equal('new-id')
      expect(deleteStub).to.have.been.calledOnceWith(emailCheck.email, 'user-id')
      expect(createStub).to.have.been.calledOnce
    })

    it('must be rejected if already persistent', async function () {
      emailCheck._id = 'id'
      await expect(emailCheck.save('user-id')).to.be.rejected
      expect(deleteStub).to.not.have.been.called
      expect(createStub).to.not.have.been.called
    })
  })

  describe('#validate', function () {
    let deleteStub: SinonStub<[string, string?], Promise<void>>
    let validStub: SinonStub<[string, string], Promise<string | undefined>>

    beforeEach('Prepare doubles', function () {
      deleteStub = sinon.stub(model, 'delete')
      validStub = sinon.stub(model, 'validate')
    })

    afterEach('Clean up', function () {
      validStub.restore()
      deleteStub.restore()
    })

    it('must send request to database', async function () {
      deleteStub.resolves()
      validStub.resolves('user-id')
      await expect(emailCheck.validate()).to.eventually.equal('user-id')
      expect(deleteStub).to.have.been.calledOnceWith(emailCheck.email)
      expect(validStub).to.have.been.calledOnce
    })
  })

  describe('#cloneToDoc', function () {
    it('must provide doc', function () {
      const emailCheckDoc = emailCheck.cloneToDoc()
      expect(emailCheckDoc).to.deep.equal(EMAIL_CHECK)
    })

    it('must be created correctly from doc', function () {
      emailCheck = new EmailCheck(EMAIL_CHECK)
      const emailCheckDoc = emailCheck.cloneToDoc()
      expect(emailCheckDoc).to.deep.equal(EMAIL_CHECK)
    })
  })
})
