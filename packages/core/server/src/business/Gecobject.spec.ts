/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import { Gecobject as GecobjectDoc } from '@gecogvidanto/shared'

import Gecobject from './Gecobject'

interface FakeDoc extends GecobjectDoc {
  data: string
  count: number
}

class Fake extends Gecobject<FakeDoc> implements FakeDoc {
  public data: string
  public count: number

  public constructor(fake: FakeDoc)
  public constructor(data: string, count: number)
  public constructor(dataOrDoc: string | FakeDoc, count?: number) {
    super(typeof dataOrDoc === 'string' ? undefined : dataOrDoc)
    if (typeof dataOrDoc === 'string') {
      this.data = dataOrDoc
      this.count = count!
    } else {
      this.data = dataOrDoc.data
      this.count = dataOrDoc.count
    }
  }

  public cloneToDoc(): FakeDoc {
    const result = {
      ...super.cloneToDoc(),
      data: this.data,
      count: this.count,
    }
    return result
  }
}

describe('Gecobject', function () {
  let fake: Fake

  beforeEach('Create default object', function () {
    fake = new Fake('Malgranda pezo, sed granda prezo', 12)
  })

  it('must not accept multiple _id setting', function () {
    expect(() => {
      fake._id = 'my-id'
    }).not.to.throw()
    expect(() => {
      fake._id = 'my-id'
    }).to.throw(/cannot update object identifier/i)
  })

  it('must throw error if reading _id of non persistent object', function () {
    expect(fake.persistent).to.be.false
    expect(() => fake._id).to.throw(/non persistent object/i)
  })

  it('must return _id for persistent object', function () {
    fake._id = 'id#12'
    expect(fake.persistent).to.be.true
    expect(fake._id).to.equal('id#12')
  })

  it('must be equal to other object with same _id', function () {
    const other = new Fake('Mi ne estas la sama', 144)
    other._id = 'same-id'
    fake._id = 'same-id'
    expect(fake.isSameAs(other)).to.be.true
  })

  it('must not be equal to other object with different _id', function () {
    const other = new Fake(fake.cloneToDoc())
    other._id = 'id-02'
    fake._id = 'id-01'
    expect(fake.isSameAs(other)).to.be.false
  })

  it('must not be equal when no _id provided', function () {
    const other = new Fake(fake.cloneToDoc())
    expect(fake.isSameAs(other)).to.be.false
  })

  it('must not fill doc with id if no id provided', function () {
    expect(fake.cloneToDoc()).to.have.all.keys('data', 'count')
  })

  it('must fill doc with id if id provided', function () {
    fake._id = 'id-in-doc'
    const fakeDoc = fake.cloneToDoc()
    expect(fakeDoc).to.have.all.keys('_id', 'data', 'count')
    expect(fakeDoc._id).to.equal('id-in-doc')
  })

  it('must be created without id from doc with no id', function () {
    const other = new Fake({
      data: '#data#',
      count: -1,
    })
    expect(other.cloneToDoc()).to.have.all.keys('data', 'count')
  })

  it('must be created with id from doc with id', function () {
    const other = new Fake({
      _id: 'I have got an idea',
      data: '#data#',
      count: -1,
    })
    const fakeDoc = other.cloneToDoc()
    expect(fakeDoc).to.have.all.keys('_id', 'data', 'count')
    expect(fakeDoc._id).to.equal('I have got an idea')
  })
})
