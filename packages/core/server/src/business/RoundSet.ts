/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { ServerSet } from '@gecogvidanto/plugin'
import { RoundSet as SetDoc } from '@gecogvidanto/shared'

/**
 * A set.
 */
export default class RoundSet implements ServerSet {
  public readonly absoluteInitialRound: number
  public readonly ecoSysId: string

  private _currentRound: number
  private readonly _techBreakRounds: number[]

  /**
   * Create a set from data.
   *
   * @param roundsPerSet - The count of rounds for each set.
   * @param index - The index of the set in the game.
   * @param ecoSysIdOrDoc - The (internal) name of the economic system, or document to take data from.
   */
  public constructor(private readonly roundsPerSet: number, index: number, ecoSysIdOrDoc: string | SetDoc) {
    this.absoluteInitialRound = index * roundsPerSet + 1
    if (typeof ecoSysIdOrDoc === 'string') {
      this.ecoSysId = ecoSysIdOrDoc
      this._currentRound = 0
      this._techBreakRounds = []
    } else {
      this.ecoSysId = ecoSysIdOrDoc.ecoSysId
      this._currentRound = ecoSysIdOrDoc.currentRound
      this._techBreakRounds = ecoSysIdOrDoc.techBreakRounds.slice()
    }
  }

  public get currentRound(): number {
    return this._currentRound
  }

  public set currentRound(value: number) {
    this.requireModifiable()
    if (value !== this._currentRound && value !== this._currentRound + 1) {
      throw new Error('Rounds can only be increased 1 by 1')
    }
    this._currentRound = value
  }

  public get techBreakRounds(): ReadonlyArray<number> {
    return this._techBreakRounds
  }

  public addTechBreak(): void {
    this.requireModifiable()
    if (this._currentRound <= 0) {
      throw new Error('Cannot create a technological break if set has not started')
    }
    this._techBreakRounds.push(this._currentRound)
  }

  /**
   * @returns True if the set is finished.
   */
  public get finished(): boolean {
    return this._currentRound > this.roundsPerSet
  }

  /**
   * Create a clone of the object in a simple document.
   *
   * @returns The pure document.
   */
  public cloneToDoc(): SetDoc {
    const result: SetDoc = {
      ecoSysId: this.ecoSysId,
      currentRound: this._currentRound,
      techBreakRounds: this._techBreakRounds,
    }
    return result
  }

  /**
   * Ensure the set is modifiable. Throw an exception if it is not.
   */
  private requireModifiable(): void {
    if (this.finished) {
      throw new Error('Finished set cannot be modified')
    }
  }
}
