/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import { Player } from '.'

describe('Player', function () {
  const USER_NAME = 'user-name'
  let player: Player
  let locked: boolean
  const onRequestLock = (): boolean => locked

  beforeEach('Create default player', function () {
    locked = false
    player = new Player(onRequestLock, USER_NAME, 0)
  })

  describe('#constructor', function () {
    it('must create player with appropriate values', function () {
      expect(player.name).to.equal(USER_NAME)
      expect(player.roundArrived).to.equal(0)
      expect(player.roundLeft).to.equal(0)
    })

    it('must be created correctly from doc', function () {
      locked = true
      const newPlayer = new Player(onRequestLock, {
        name: 'Luke Skywalker',
        roundArrived: 2,
        roundLeft: 8,
      })
      const playerDoc = newPlayer.cloneToDoc()
      expect(playerDoc).to.have.all.keys('name', 'roundArrived', 'roundLeft')
      expect(playerDoc.name).to.equal('Luke Skywalker')
      expect(playerDoc.roundArrived).to.equal(2)
      expect(playerDoc.roundLeft).to.equal(8)
    })
  })

  describe('#name', function () {
    it('must be modifiable unless object is locked', function () {
      expect(() => (player.name = 'Obi-Wan Kenobi')).to.not.throw()
      locked = true
      expect(() => (player.name = 'Dark Vador')).to.throw(/cannot be modified/i)
    })
  })

  describe('#roundLeft', function () {
    it('must be modifiable unless object is locked', function () {
      locked = true
      expect(() => (player.roundLeft = 2)).to.throw(/cannot be modified/i)
    })

    it('must throw exception when trying to make a left player leave', function () {
      expect(() => (player.roundLeft = 2)).to.not.throw()
      expect(player.roundLeft).to.equal(2)
      expect(() => (player.roundLeft = 3)).to.throw(/already left/i)
    })

    it('must not throw exception for non modification', function () {
      expect(() => (player.roundLeft = 2)).to.not.throw()
      expect(player.roundLeft).to.equal(2)
      expect(() => (player.roundLeft = 2)).to.not.throw()
    })
  })

  describe('#cloneToDoc', function () {
    it('must provide doc', function () {
      player.roundLeft = 4
      const playerDoc = player.cloneToDoc()
      expect(playerDoc).to.have.all.keys('name', 'roundArrived', 'roundLeft')
      expect(playerDoc.name).to.equal(USER_NAME)
      expect(playerDoc.roundArrived).to.equal(0)
      expect(playerDoc.roundLeft).to.equal(4)
    })
  })
})
