/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { createMessages } from 'intl-ts'

import { messages as sharedMessages } from '@gecogvidanto/shared'

export const messages = createMessages({
  ...sharedMessages,
  configurationParameters:
    'Only one argument is expected, for specifying configuration file — others ignored',
  databaseNoPlugin: 'No database plugin found, please add one',
  economicSystemNotEnough: 'Too few economic systems loaded, please add appropriate plugins',
  initPhaseConfiguration: 'Loading configuration',
  initPhaseDatabase: 'Initializing database',
  initPhaseEconomicSystems: 'Initializing economic systems',
  initPhaseLogger: 'Initializing logger',
  initPhaseMailer: 'Initializing mailer',
  initPhaseModels: 'Initializing database models',
  initPhasePlugins: 'Initializing plugins',
  initPhaseStart: 'Waiting for all services are started',
  mailerCheckHtmlText: (name: string, url: string) => `<html><p>Hi ${name}!</p>
<p>In order to validate your e-mail on the ĞecoĞvidanto server, please click
<a href="${url}">on this link</a>.
<p>If you cannot directly click on the link, copy and paste the URL below in your web browser:</p>
${url}</html>`,
  mailerCheckPlainText: (name: string, url: string) => `Hi ${name}!
In order to validate your e-mail on the ĞecoĞvidanto server, please click on the link below:
${url}
If you cannot directly click on the link, copy and paste it in your web browser.`,
  mailerCheckSubject: 'e-mail check',
  mailerError: (subject: string, to: string) =>
    `Error occurred while sending message “${subject}” to ${to}`,
  mailerNotConfigured: 'Mailer is not configured — New users will be created without e-mail checking',
  mailerSent: (subject: string, to: string) => `e-mail “${subject}” sent to ${to}`,
  pluginMultipleDatabase: 'Multiple database plugin found, please remove extra',
  reCaptchaConfigError: 'Error in captcha configuration: a secret key must be provided with site key',
  reCaptchaPreventNewUsers: 'No captcha secret key given: new users will not be able to register',
  serverNewConnection: (ip: string) => `New connection from ${ip}`,
  serverNewWSConnection: 'Web socket connection',
  serverPortInUse: (port: number) => `The port ${port} is already taken by another application`,
  serverPortPermissions: (port: number) => `You do not have enough permissions to bind to the port ${port}`,
  serverRunning: (hostname: string, port: number) =>
    `Server running at https://${hostname || 'localhost'}:${port}/` +
    (hostname ? '' : ' (listening to all interfaces)') +
    `\nHit Ctrl-C to terminate`,
  serverStopped: 'Server stopped listening',
  serverWSRegister: (id: string, name: string) => `WS registered to ${name} event for game ${id}`,
  serverWSUnknown: (name: string) => `Will not manage unknown WS event ${name}`,
  terminated: 'ĞecoĞvidanto server terminated',
})

export type langType = typeof messages
