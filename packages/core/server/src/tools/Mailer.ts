/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as nodemailer from 'nodemailer'
import { Transporter } from 'nodemailer'
import * as winston from 'winston'

import { app } from '../AppData'

/**
 * Class managing mail sending.
 */
export default class Mailer {
  /**
   * This promise either resolves to true if the mailer is usable or false if it is not configured. It is rejected if
   * configured but verification failed.
   */
  public readonly usable: Promise<boolean>

  private readonly transporter: Transporter
  private readonly prefix: string

  public constructor() {
    const mailerConfig = app.data.config.email
    const _transporter = nodemailer.createTransport(mailerConfig.config, {
      from: mailerConfig.from,
    })
    this.transporter = _transporter
    this.prefix = Mailer.normalizePrefix(mailerConfig.prefix)
    this.usable = (async () => {
      try {
        await _transporter.verify()
        return true
      } catch (error) {
        if (Object.keys(mailerConfig.config).length === 0) {
          return false
        } else {
          throw error
        }
      }
    })()
  }

  private static normalizePrefix(prefix: string): string {
    if (prefix.length !== 0 && !prefix.endsWith(' ')) {
      return prefix + ' '
    } else {
      return prefix
    }
  }

  public async send(to: string, subject: string, plainText: string, htmlText: string): Promise<boolean> {
    if (!(await this.usable)) {
      return false
    }
    const lang = app.data.serverLang
    try {
      await this.transporter.sendMail({
        to,
        subject: this.prefix + subject,
        text: plainText,
        html: htmlText,
      })
      winston.debug(lang.mailerSent(subject, to))
      return true
    } catch (error) {
      winston.debug(error.message || error.toString())
      winston.error(lang.mailerError(subject, to))
      return false
    }
  }
}
