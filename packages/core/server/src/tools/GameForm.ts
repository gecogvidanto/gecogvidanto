/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { langType } from '@gecogvidanto/client-web'
import { FormBuilder, FormData } from '@gecogvidanto/plugin'
import { HelpSheet, PlaceContent, ServerField } from '@gecogvidanto/shared'

import { Game, Score } from '../business'

/**
 * This class is used to manage game forms.
 */
export default class GameForm {
  /**
   * Create a game form.
   *
   * @param game - The game for which to manage forms.
   */
  public constructor(private readonly game: Game) {}

  /**
   * Build the form for the current game round.
   *
   * @param moneyHelpSheet - The money help sheet.
   * @returns The form builder.
   */
  public build(moneyHelpSheet: HelpSheet): FormBuilder<langType, any> {
    const builder = new FormBuilder<langType, any>()
    const valuesHelpSheet = this.game.buildValuesHelpSheet()
    this.game.terminatingPlayers.forEach(player => {
      builder
        .toPart(player)
        .newGroup(true, collector =>
          collector.addTextField(ServerField.Dying, {
            template: 'formDying',
            parameters: [this.game.players[player].name],
          })
        )
        .addDyingValues(valuesHelpSheet)
      if (
        moneyHelpSheet.low !== PlaceContent.Empty ||
        moneyHelpSheet.medium !== PlaceContent.Empty ||
        moneyHelpSheet.high !== PlaceContent.Empty
      ) {
        builder.addDyingMoney(moneyHelpSheet)
      }
    })
    return builder
  }

  /**
   * Analyze the result of a form.
   *
   * @param data - The form data.
   */
  public analyze(data: FormData): void {
    this.game.terminatingPlayers.forEach(player => {
      const fields: Map<string, string> = data.getValuesFor(player)
      const score: Score = this.game.getCharacterFor(player).currentScore
      score.values.low = this.convertNumber(fields.get(ServerField.LowValues))
      score.values.medium = this.convertNumber(fields.get(ServerField.MediumValues))
      score.values.high = this.convertNumber(fields.get(ServerField.HighValues))
      score.money.low = this.convertNumber(fields.get(ServerField.LowMoney))
      score.money.medium = this.convertNumber(fields.get(ServerField.MediumMoney))
      score.money.high = this.convertNumber(fields.get(ServerField.HighMoney))
    })
  }

  /**
   * Convert a value coming from the form data into a number.
   *
   * @param value - The value to convert.
   * @returns The converted value.
   */
  private convertNumber(value: string | undefined): number {
    const numericValue = Number(value || 0)
    return !isNaN(numericValue) ? numericValue : 0
  }
}
