/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * The data found in a module.
 */
export interface ModuleData {
  /**
   * Indicate if the module data could be read. False if not.
   */
  readonly found: boolean

  /**
   * The registered name of the module, if any.
   */
  readonly name?: string

  /**
   * The registred version of the module, if any.
   */
  readonly version?: string

  /**
   * The dependencies of the module.
   */
  readonly dependencies: ReadonlyArray<string>
}

/**
 * Read data about a module.
 */
export default class ModuleReader {
  public data: Promise<ModuleData>

  /**
   * Create the module data.
   *
   * @param moduleName - The name of the module to search, or a full path to the module directory.
   */
  public constructor(moduleName: string) {
    this.data = (async (): Promise<ModuleData> => {
      try {
        const moduleDescription = await import(moduleName + '/package.json')
        return {
          name: moduleDescription.name,
          version: moduleDescription.version,
          dependencies: Object.keys(moduleDescription.dependencies || {}),
          found: true,
        }
      } catch {
        return {
          dependencies: [],
          found: false,
        }
      }
    })()
  }
}
