/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback */
import { expect } from 'chai'

import { createRandom } from './helpers'

describe('Server helper functions', function () {
  describe('#createRandom', function () {
    it('must return a different number at each call', function () {
      const len = 10
      const tries: string[] = []
      for (let i = 0; i < 500; i++) {
        const rand = createRandom(len)
        expect(rand, `Random creation #${i} should not be same of previous one`).to.not.be.oneOf(tries)
        tries.push(rand)
      }
    })

    it('must have the required length', function () {
      for (let i = 0; i < 50; i++) {
        expect(createRandom(i), `Unexpected length for creation #${i}`).to.have.lengthOf(i)
      }
    })
  })
})
