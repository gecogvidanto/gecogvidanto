/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { request as httpsRequest } from 'https'
import { ParsedUrlQueryInput, stringify } from 'querystring'

import { CaptchaAction } from '@gecogvidanto/shared'

export type ReCaptchaErrorCode =
  | 'missing-input-secret'
  | 'invalid-input-secret'
  | 'missing-input-response'
  | 'invalid-input-response'
  | 'bad-request'

export interface ReCaptchaResponse {
  success: boolean
  score: number
  action: CaptchaAction
  challenge_ts: string
  hostname: string
  'error-codes'?: ReCaptchaErrorCode[]
}

interface ReCaptchaRequest {
  secret: string
  response: string
  remoteip?: string
}

/**
 * Class managing the Google ReCaptcha control.
 */
export default class ReCaptcha {
  public constructor(private readonly secret: string) {}

  /**
   * Check the captcha response.
   *
   * @param token - The token to check.
   * @param remoteIp - The remote IP address.
   * @returns The response.
   */
  public check(token: string, remoteIp?: string): Promise<ReCaptchaResponse> {
    const data: ReCaptchaRequest & ParsedUrlQueryInput = {
      secret: this.secret,
      response: token,
    }
    remoteIp && (data.remoteip = remoteIp)
    const stringized: string = stringify(data)
    return new Promise((resolve, reject) => {
      const request = httpsRequest(
        {
          host: 'www.google.com',
          path: '/recaptcha/api/siteverify',
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(stringized),
          },
        },
        res => {
          res.on('data', chunk => {
            resolve(JSON.parse(chunk.toString()))
          })
        }
      )
      request.on('error', error => reject(error))
      request.write(stringized)
      request.end()
    })
  }
}
