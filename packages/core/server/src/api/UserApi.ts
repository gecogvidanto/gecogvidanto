/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { ApiDefinition, CertLevel, validators } from '@gecogvidanto/shared'

import { app } from '../AppData'
import { EmailCheck, User } from '../business'
import { ReCaptcha, createRandom } from '../tools'
import ApiEntry, { ApiEntryExecutor } from './ApiEntry'

/**
 * The API managing user data.
 */
export default class UserApi {
  /**
   * User sign out.
   */
  public onSignOut: ApiEntryExecutor<ApiDefinition['userSignOut']> = new ApiEntry<
    ApiDefinition['userSignOut']
  >().execute(async (_variables, _logged, _params, extra) => {
    extra.req.logout()
    extra.req.session &&
      (await new Promise<void>((res, rej) => extra.req.session!.destroy(err => (err ? rej(err) : res()))))
  })

  /**
   * User creation.
   */
  public onCreate: ApiEntryExecutor<ApiDefinition['userCreate']> = new ApiEntry<
    ApiDefinition['userCreate'],
    { user: User }
  >(async params => {
    const { email, name, account } = params.data
    return { user: new User(email, name, account) }
  })
    .activateDataValidation(
      { key: 'email', validators: validators.email },
      { key: 'name', validators: validators.name },
      { key: 'account', validators: validators.account, optional: true },
      { key: 'password', validators: validators.password }
    )
    .execute(async ({ user }, _logged, params) => {
      const { email, password, captchaToken } = params.data
      const { secretKey, threshold } = app.data.config.reCaptcha

      if (secretKey) {
        const captcha = new ReCaptcha(secretKey)
        const result = await captcha.check(captchaToken)
        if (result.action !== 'userCreate') {
          throw new Error('Unexpected action')
        } else if (result.score < threshold / 100) {
          throw new Error('Robot suspected')
        }
      }

      if (!(await user.checkUnicity())) {
        throw new Error('User name or e-mail is not unique')
      }

      user.email = ''
      await user.save(password)
      await this.validateEmail(user, email)
      return user.cloneToDoc()
    })

  /**
   * Search users.
   */
  public onSearch: ApiEntryExecutor<ApiDefinition['userSearch']> = new ApiEntry<
    ApiDefinition['userSearch']
  >()
    .activateDataValidation(
      { key: 'email', validators: validators.email, optional: true },
      { key: 'name', validators: validators.name, optional: true }
    )
    .execute(async (_variables, _logged, params) => {
      const users = await User.search(params.data.email, params.data.name)
      return users.map(user => user.cloneToDoc())
    })

  /**
   * Read given user.
   */
  public onRead: ApiEntryExecutor<ApiDefinition['userRead']> = new ApiEntry<
    ApiDefinition['userRead'],
    { user: User }
  >(async params => ({
    user: await User.find(params.url.id),
  }))
    .activateUrlValidation({ key: 'id', validators: [] })
    .execute(async variables => variables.user.cloneToDoc())

  /**
   * Update given user.
   */
  public onUpdate: ApiEntryExecutor<ApiDefinition['userUpdate']> = new ApiEntry<
    ApiDefinition['userUpdate'],
    { user: User }
  >(async params => ({
    user: await User.find(params.url.id),
  }))
    .activateUrlValidation({ key: 'id', validators: [] })
    .activateDataValidation(
      { key: 'email', validators: validators.email },
      { key: 'name', validators: validators.name },
      { key: 'level', validators: validators.certLevel },
      { key: 'account', validators: validators.account, optional: true },
      { key: 'password', validators: validators.password, optional: true }
    )
    .activateAuthorization(
      async (variables, logged, params) =>
        !!(
          logged &&
          (logged.level === CertLevel.Administrator || logged.isSameAs(variables.user)) &&
          (!logged.isSameAs(variables.user) || variables.user.level === params.data.level)
        )
    )
    .execute(async ({ user }, _logged, params) => {
      const { email, name, level, account, password } = params.data
      user.name = name
      user.level = level as CertLevel
      user.account = account
      const saved = user.email
      user.email = email
      if (!(await user.checkUnicity())) {
        throw new Error('User name or e-mail is not unique')
      }
      user.email = saved
      await user.save(password)

      // Check e-mail validity
      if (user.email !== email) {
        await this.validateEmail(user, email)
      }
      return user.cloneToDoc()
    })

  /**
   * User deletion.
   */
  public onDelete: ApiEntryExecutor<ApiDefinition['userDelete']> = new ApiEntry<
    ApiDefinition['userDelete'],
    { user: User }
  >(async params => ({
    user: await User.find(params.url.id),
  }))
    .activateUrlValidation({ key: 'id', validators: [] })
    .activateAuthorization(
      async (variables, logged) =>
        !!(logged && logged.level === CertLevel.Administrator && !logged.isSameAs(variables.user))
    )
    .execute(async variables => variables.user.delete())

  private async validateEmail(user: User, email: string): Promise<void> {
    if (await app.data.mailer.usable) {
      const token = createRandom(24)
      const emailCheck = new EmailCheck(email, token)
      await emailCheck.save(user._id)
      const clientLang = app.data.clientLang
      const url = this.buildEmailCheckUrl(email, token)
      if (
        !(await app.data.mailer.send(
          email,
          clientLang.mailerCheckSubject(),
          clientLang.mailerCheckPlainText(user.name, url),
          clientLang.mailerCheckHtmlText(user.name, url)
        ))
      ) {
        await emailCheck.validate() // Remove useless check
      }
    } else {
      user.email = email
      await user.save()
    }
  }

  private buildEmailCheckUrl(email: string, token: string): string {
    const { host, port, remoteHost, remotePort } = app.data.config.server
    const encodedEmail = encodeURIComponent(email)
    const encodedToken = encodeURIComponent(token)
    return `https://${remoteHost || host || 'localhost'}:${
      remotePort || port
    }/email-check/${encodedEmail}/${encodedToken}`
  }
}
