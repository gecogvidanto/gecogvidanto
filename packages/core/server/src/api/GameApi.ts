/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { EconomicSystem, FormBuilder, FormData } from '@gecogvidanto/plugin'
import {
  ApiDefinition,
  AsyncEmitter,
  CertLevel,
  Form,
  HelpSheet,
  IdentityValidator,
  Location,
  NumberValueValidator,
  PlaceContent,
  SerializableForm,
  Unlocked,
  validators,
} from '@gecogvidanto/shared'

import { app } from '../AppData'
import { Game, User } from '../business'
import { langType } from '../tools'
import ApiEntry, { ApiEntryExecutor } from './ApiEntry'

const numberValidator = new NumberValueValidator(0)
const negativeNumberValidator = new NumberValueValidator(undefined, 0)
const closedValidator = new IdentityValidator('true')

export const EMPTY_HELP_SHEET: HelpSheet = {
  low: PlaceContent.Empty,
  medium: PlaceContent.Empty,
  high: PlaceContent.Empty,
  waiting: PlaceContent.Empty,
}

/**
 * Events which can be emitted.
 */
export const enum GameApiEventType {
  /**
   * A new round has started.
   */
  NewRound,

  /**
   * A technological break happend.
   */
  TechBreak,
}

/**
 * Definition of the API events.
 */
export interface GameApiEvent {
  /**
   * Name of the event.
   */
  type: GameApiEventType

  /**
   * The modified game.
   */
  game: Game
}

/**
 * The API managing game.
 */
export default class GameApi extends AsyncEmitter<GameApiEvent> {
  /**
   * Create a new game.
   */
  public onCreate: ApiEntryExecutor<ApiDefinition['gameCreate']> = new ApiEntry<
    ApiDefinition['gameCreate']
  >()
    .activateDataValidation(
      { key: 'locationName', validators: validators.location, optional: true },
      { key: 'roundsPerSet', validators: validators.roundsPerSet },
      { key: 'roundLength', validators: validators.roundLength }
    )
    .activateAuthorization(
      async (_variables, logged) =>
        !!(logged && (logged.level === CertLevel.Administrator || logged.level === CertLevel.GameMaster))
    )
    .execute(async (_variables, logged, params) => {
      const { locationName, value, roundsPerSet, roundLength } = params.data
      const location: Location = { name: locationName }
      if (value && 'city' in params.data) {
        const { city, countryCode, latitude, longitude } = params.data
        location.address = {
          value,
          city,
          countryCode,
          latitude: Number(latitude),
          longitude: Number(longitude),
        }
      } else {
        location.address = value
      }
      const game = new Game(logged!._id, location, Number(roundsPerSet), Number(roundLength))
      await game.save()
      return game.cloneToDoc(true)
    })

  /**
   * Update game.
   */
  public onUpdate: ApiEntryExecutor<ApiDefinition['gameUpdate']> = new ApiEntry<
    ApiDefinition['gameUpdate'],
    { game: Game }
  >(async params => ({
    game: await Game.find(params.url.id),
  }))
    .activateDataValidation(
      { key: 'locationName', validators: validators.location, optional: true },
      { key: 'summary', validators: [], optional: true },
      { key: 'closed', validators: [closedValidator], optional: true }
    )
    .activateAuthorization(async (variables, logged) => this.canUpdateGame(logged, variables.game))
    .execute(async ({ game }, _logged, params) => {
      const { locationName, value, summary, closed } = params.data
      const location: Location = { name: locationName }
      if (value && 'city' in params.data) {
        const { city, countryCode, latitude, longitude } = params.data
        location.address = {
          value,
          city,
          countryCode,
          latitude: Number(latitude),
          longitude: Number(longitude),
        }
      } else {
        location.address = value
      }
      game.location = location
      game.summary = summary
      game.closed = !!closed
      await game.save()
      return game.cloneToDoc(true)
    })

  /**
   * Search games.
   */
  public onSearch: ApiEntryExecutor<ApiDefinition['gameSearch']> = new ApiEntry<
    ApiDefinition['gameSearch']
  >().execute(async () => {
    const games = await Game.search()
    return games.map(game => game.cloneToDoc(true))
  })

  /**
   * Read the given game.
   */
  public onRead: ApiEntryExecutor<ApiDefinition['gameRead']> = new ApiEntry<
    ApiDefinition['gameRead'],
    { game: Game }
  >(async params => ({
    game: await Game.find(params.url.id),
  }))
    .activateUrlValidation({ key: 'id', validators: [] })
    .execute(async variables => variables.game.cloneToDoc(true))

  /**
   * Add a player to the game.
   */
  public onPlayerAdd: ApiEntryExecutor<ApiDefinition['gamePlayerAdd']> = new ApiEntry<
    ApiDefinition['gamePlayerAdd'],
    { game: Game; playerName: string }
  >(async params => ({
    game: await Game.find(params.url.id),
    playerName: params.data.name,
  }))
    .activateUrlValidation({ key: 'id', validators: [] })
    .activateDataValidation({ key: 'name', validators: validators.playerName })
    .activateAuthorization(async (variables, logged) => this.canUpdateGame(logged, variables.game))
    .execute(async ({ game, playerName }) => {
      game.addPlayer(playerName)
      await game.save()
      return game.cloneToDoc(true)
    })

  /**
   * Update a player's name.
   */
  public onPlayerUpdate: ApiEntryExecutor<ApiDefinition['gamePlayerUpdate']> = new ApiEntry<
    ApiDefinition['gamePlayerUpdate'],
    { game: Game; player: number }
  >(async params => {
    const game = await Game.find(params.url.id)
    const playerId = Number(params.url.player)
    const player = game && game.players.length > playerId ? playerId : undefined
    return { game, player }
  })
    .activateUrlValidation({ key: 'id', validators: [] }, { key: 'player', validators: [numberValidator] })
    .activateDataValidation({ key: 'name', validators: validators.playerName })
    .activateAuthorization(async (variables, logged) => this.canUpdateGame(logged, variables.game))
    .execute(async ({ game, player }, _logged, { data }) => {
      game.updatePlayer(player, data.name)
      await game.save()
      return game.cloneToDoc(true)
    })

  /**
   * Delete a player from the game.
   */
  public onPlayerDelete: ApiEntryExecutor<ApiDefinition['gamePlayerDelete']> = new ApiEntry<
    ApiDefinition['gamePlayerDelete'],
    { game: Game; player: number }
  >(async params => {
    const game = await Game.find(params.url.id)
    const player = game && game.onGamePlayers.find(value => value === Number(params.url.player))
    return { game, player }
  })
    .activateUrlValidation({ key: 'id', validators: [] }, { key: 'player', validators: [numberValidator] })
    .activateAuthorization(async (variables, logged) => this.canUpdateGame(logged, variables.game))
    .execute(async ({ game, player }) => {
      game.deletePlayer(player)
      await game.save()
      return game.cloneToDoc(true)
    })

  /**
   * Add a set to the game.
   */
  public onSetAdd: ApiEntryExecutor<ApiDefinition['gameSetAdd']> = new ApiEntry<
    ApiDefinition['gameSetAdd'],
    { game: Game; economicSystem: EconomicSystem<langType> }
  >(async params => {
    const game = await Game.find(params.url.id)
    const economicSystem = app.data.economicSystems.find(value => value.id === params.data.ecoSysId)
    return { game, economicSystem }
  })
    .activateUrlValidation({ key: 'id', validators: [] })
    .activateDataValidation({ key: 'ecoSysId', validators: [] })
    .activateAuthorization(async (variables, logged) => this.canUpdateGame(logged, variables.game))
    .execute(async ({ game, economicSystem }) => {
      game.addSet(economicSystem.id)
      await game.save()
      return game.cloneToDoc(true)
    })

  /**
   * Return the name of the non-player character with given (negative) identifier.
   */
  public onNpcName: ApiEntryExecutor<ApiDefinition['gameNpcName']> = new ApiEntry<
    ApiDefinition['gameNpcName'],
    { economicSystem: EconomicSystem<langType> }
  >(async ({ url }) => {
    const game = await Game.find(url.id)
    const roundSet = game && game.roundSets[Number(url.roundSet)]
    const economicSystem =
      roundSet && app.data.economicSystems.find(value => value.id === roundSet.ecoSysId)
    return { economicSystem }
  })
    .activateUrlValidation(
      { key: 'id', validators: [] },
      { key: 'roundSet', validators: [numberValidator] },
      { key: 'character', validators: [negativeNumberValidator] }
    )
    .execute(async ({ economicSystem }, _logged, { url }) =>
      economicSystem.getNonPlayerCharacterName(Number(url.character), url.id, Number(url.roundSet))
    )

  /**
   * Return the help sheet for values.
   */
  public onHelpSheetValues: ApiEntryExecutor<ApiDefinition['gameHelpSheetValues']> = new ApiEntry<
    ApiDefinition['gameHelpSheetValues'],
    { game: Game; check: any }
  >(async params => {
    const game = await Game.find(params.url.id)
    const check = game && (game.setInProgress || undefined)
    return { game, check }
  })
    .activateUrlValidation({ key: 'id', validators: [] })
    .execute(async ({ game }) =>
      game.currentSet.currentRound === 0 ? EMPTY_HELP_SHEET : game.buildValuesHelpSheet()
    )

  /**
   * Return the help sheet for money.
   */
  public onHelpSheetMoney: ApiEntryExecutor<ApiDefinition['gameHelpSheetMoney']> = new ApiEntry<
    ApiDefinition['gameHelpSheetMoney'],
    { game: Game; economicSystem: EconomicSystem<langType> }
  >(async params => {
    const game = await Game.find(params.url.id)
    const economicSystem =
      game &&
      (game.setInProgress || undefined) &&
      app.data.economicSystems.find(value => value.id === game.currentSet.ecoSysId)
    return { game, economicSystem }
  })
    .activateUrlValidation({ key: 'id', validators: [] })
    .execute(async ({ economicSystem, game }) =>
      game.currentSet.currentRound === 0 ? EMPTY_HELP_SHEET : economicSystem.getMoneyHelpSheet(game)
    )

  /**
   * Return the options for the round.
   */
  public onOptionsCreate: ApiEntryExecutor<ApiDefinition['gameOptionsCreate']> = new ApiEntry<
    ApiDefinition['gameOptionsCreate'],
    { game: Game; economicSystem: EconomicSystem<langType> }
  >(async params => {
    const game = await Game.find(params.url.id)
    const economicSystem =
      game &&
      (game.setInProgress || undefined) &&
      app.data.economicSystems.find(value => value.id === game.currentSet.ecoSysId)
    return { game, economicSystem }
  })
    .activateUrlValidation({ key: 'id', validators: [] })
    .execute(async variables => (await variables.economicSystem.getOptions(variables.game)).slice())

  /**
   * Return the form for current option or round.
   */
  public onFormCreate: ApiEntryExecutor<ApiDefinition['gameFormCreate']> = new ApiEntry<
    ApiDefinition['gameFormCreate'],
    { game: Game; economicSystem: EconomicSystem<langType> }
  >(async params => {
    const game = await Game.find(params.url.id)
    const economicSystem =
      game &&
      (game.setInProgress || undefined) &&
      app.data.economicSystems.find(value => value.id === game.currentSet.ecoSysId)
    return { game, economicSystem }
  })
    .activateUrlValidation({ key: 'id', validators: [] })
    .activateDataValidation({ key: 'optionId', validators: [], optional: true })
    .activateAuthorization(async (variables, logged) => this.canUpdateGame(logged, variables.game))
    .execute(async ({ game, economicSystem }, _logged, { data }) => {
      const builder: FormBuilder<any> = await economicSystem.getForm(
        data.optionId || game.currentSet.currentRound === 0
          ? new FormBuilder()
          : game.buildForm(await economicSystem.getMoneyHelpSheet(game)),
        game,
        data.optionId
      )
      const form: Form<any> = builder.build()
      const sentForm: Unlocked<SerializableForm> = {
        parts: form.parts.map(part => ({
          player: isFinite(part.player) ? part.player : undefined,
          groups: part.groups,
        })),
        params: form.params,
        dynFunction: null,
      }
      if (form.dynFunction) {
        sentForm.dynFunction = `"use strict"; return ${form.dynFunction.toString()}`
      }
      return sentForm
    })

  /**
   * Execute the form.
   */
  public onFormSend: ApiEntryExecutor<ApiDefinition['gameFormSend']> = new ApiEntry<
    ApiDefinition['gameFormSend'],
    { game: Game; economicSystem: EconomicSystem<langType> }
  >(async params => {
    const game = await Game.find(params.url.id)
    const economicSystem =
      game &&
      (game.setInProgress || undefined) &&
      app.data.economicSystems.find(value => value.id === game.currentSet.ecoSysId)
    return { game, economicSystem }
  })
    .activateUrlValidation({ key: 'id', validators: [] })
    .activateDataValidation({ key: 'optionId', validators: [], optional: true })
    .activateAuthorization(async (variables, logged) => this.canUpdateGame(logged, variables.game))
    .execute(async ({ game, economicSystem }, _logged, params) => {
      const { optionId, ...data } = params.data
      const formData: FormData = await economicSystem.execForm(new FormData(data), game, optionId)
      if (!optionId) {
        game.analyzeFormResult(formData)
        await economicSystem.terminateRound(game)
        game.currentSet.currentRound++
        this.emit({ type: GameApiEventType.NewRound, game })
      }
      await game.save()
      return game.cloneToDoc(true)
    })

  /**
   * Add a technological break.
   */
  public onTechnologicalBreak: ApiEntryExecutor<ApiDefinition['gameTechnologicalBreak']> = new ApiEntry<
    ApiDefinition['gameTechnologicalBreak'],
    { game: Game }
  >(async params => ({
    game: await Game.find(params.url.id),
  }))
    .activateUrlValidation({ key: 'id', validators: [] })
    .activateAuthorization(async (variables, logged) => this.canUpdateGame(logged, variables.game))
    .execute(async ({ game }) => {
      game.currentSet.addTechBreak()
      this.emit({ type: GameApiEventType.TechBreak, game })
      await game.save()
      return game.cloneToDoc(true)
    })

  private canUpdateGame(logged: User | undefined, game: Game): boolean {
    return !!(logged && (logged.level === CertLevel.Administrator || logged._id === game.masterId))
  }
}
