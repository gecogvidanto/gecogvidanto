/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import * as express from 'express'
import { ErrorRequestHandler, Express, RequestHandler } from 'express'

import { ApiDefinition, apiRoot, apiRoutes } from '@gecogvidanto/shared'

import Passport from '../Passport'
import EconomicSystemApi from './EconomicSystemApi'
import EmailCheckApi from './EmailCheckApi'
import GameApi from './GameApi'
import LangApi from './LangApi'
import UserApi from './UserApi'

type HttpServerRoutes = {
  [P in keyof ApiDefinition]:
    | RequestHandler
    | ErrorRequestHandler
    | Array<RequestHandler | ErrorRequestHandler>
}

/**
 * The path to the API.
 */
export const apiPath = '/' + apiRoot

/**
 * The server side API.
 */
export default class ServerApi {
  public readonly api: Express = express()
  public readonly lang = new LangApi()
  public readonly user = new UserApi()
  public readonly emailCheck = new EmailCheckApi()
  public readonly economicSystem = new EconomicSystemApi()
  public readonly game = new GameApi()
  private readonly httpServerRoutes: HttpServerRoutes

  public constructor(passport: Passport) {
    this.httpServerRoutes = {
      lang: this.lang.onSetup.route,
      userSignIn: passport.authenticate(),
      userSignOut: this.user.onSignOut.route,
      userCreate: this.user.onCreate.route,
      userSearch: this.user.onSearch.route,
      userRead: this.user.onRead.route,
      userUpdate: this.user.onUpdate.route,
      userDelete: this.user.onDelete.route,
      emailValidate: this.emailCheck.onValidate.route,
      economicSystemSearch: this.economicSystem.onSearch.route,
      economicSystemRead: this.economicSystem.onRead.route,
      gameCreate: this.game.onCreate.route,
      gameUpdate: this.game.onUpdate.route,
      gameSearch: this.game.onSearch.route,
      gameRead: this.game.onRead.route,
      gamePlayerAdd: this.game.onPlayerAdd.route,
      gamePlayerUpdate: this.game.onPlayerUpdate.route,
      gamePlayerDelete: this.game.onPlayerDelete.route,
      gameSetAdd: this.game.onSetAdd.route,
      gameNpcName: this.game.onNpcName.route,
      gameHelpSheetValues: this.game.onHelpSheetValues.route,
      gameHelpSheetMoney: this.game.onHelpSheetMoney.route,
      gameOptionsCreate: this.game.onOptionsCreate.route,
      gameFormCreate: this.game.onFormCreate.route,
      gameFormSend: this.game.onFormSend.route,
      gameTechnologicalBreak: this.game.onTechnologicalBreak.route,
    }
    Object.keys(apiRoutes).forEach(path => {
      const routePath = path as keyof ApiDefinition
      this.api[apiRoutes[routePath].method](apiRoutes[routePath].subUrl, this.httpServerRoutes[routePath])
    })
  }
}
