/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Request, Response } from 'express'
import * as winston from 'winston'

import {
  ApiDefinition,
  HttpError,
  Stringized,
  Undefinedable,
  Validator,
  validate,
} from '@gecogvidanto/shared'

import { User } from '../business'
import ApiError from './ApiError'

interface ParamValidation<K> {
  key: K
  validators: Validator[]
  optional?: true
}

interface Params<D extends ApiDefinition[keyof ApiDefinition]> {
  url: Stringized<D['url']>
  data: Stringized<D['data']>
}

interface Extra {
  [key: string]: any
  req: Request
  res: Response
}

export class ApiEntryExecutor<D extends ApiDefinition[keyof ApiDefinition]> {
  public readonly route: (req: Request, res: Response) => Promise<void>
  public constructor(
    public readonly exec: (req: Request, res: Response, params: Params<D>) => Promise<D['return']>
  ) {
    this.route = async (req: Request, res: Response): Promise<void> => {
      const params: Params<D> = {
        url: req.params as Stringized<D['url']>,
        data: this.getDataParameters(req),
      }
      try {
        const result = await this.exec(req, res, params)
        res.send(result)
      } catch (error) {
        winston.verbose(`Error in ${req.originalUrl}: ${error.message || error}`)
        if (error instanceof ApiError) {
          res.status(error.code)
        } else {
          res.status(HttpError.Server)
        }
        res.send()
      }
    }
  }

  /**
   * Get the parameters containing data from the request.
   *
   * @param req - The request.
   * @returns The data parameters.
   */
  private getDataParameters(req: Request): any {
    switch (req.method.toLowerCase()) {
      case 'post':
      case 'put':
      case 'patch':
        return req.body
      default:
        return req.query
    }
  }
}

/**
 * An API entry.
 */
export default class ApiEntry<D extends ApiDefinition[keyof ApiDefinition], V = Record<string, unknown>> {
  private urlParamsValidation: Array<ParamValidation<any>>
  private dataParamsValidation: Array<ParamValidation<any>>

  private loadVariables: (params: Params<D>, extra: Extra) => Promise<Undefinedable<V>>
  private authenticate: (variables: V, params: Params<D>, extra: Extra) => Promise<boolean>
  private authorize: (
    variables: V,
    logged: User | undefined,
    params: Params<D>,
    extra: Extra
  ) => Promise<boolean>

  /**
   * Create an API entry.
   *
   * @param loadVariables - The method to load variables.
   */
  public constructor(
    // eslint-disable-next-line @typescript-eslint/ban-types
    ...loadVariables: {} extends V ? [] : [(params: Params<D>) => Promise<Undefinedable<V>>]
  ) {
    this.urlParamsValidation = []
    this.dataParamsValidation = []

    this.loadVariables = loadVariables.length ? loadVariables[0] : ((async () => ({})) as any)
    this.authenticate = () => Promise.resolve(true)
    this.authorize = () => Promise.resolve(true)
  }

  /**
   * Activate validation of the URL parameters.
   *
   * @param values - The values expected for URL parameters.
   * @returns `this`.
   */
  public activateUrlValidation<K extends keyof D['url']>(...values: Array<ParamValidation<K>>): this {
    this.urlParamsValidation = values
    return this
  }

  /**
   * Activate validation of the data parameters.
   *
   * @param values - The values expected for data parameters.
   * @returns `this`.
   */
  public activateDataValidation<K extends keyof D['data']>(...values: Array<ParamValidation<K>>): this {
    this.dataParamsValidation = values
    return this
  }

  /**
   * Activate user authentication.
   *
   * @param authenticate - The authentication method.
   * @returns `this`.
   */
  public activateAuthentication(
    authenticate: (variables: V, params: Params<D>, extra: Extra) => Promise<boolean>
  ): this {
    this.authenticate = authenticate
    return this
  }

  /**
   * Activate entry authorisation.
   *
   * @param authorize - The authorization method.
   * @returns `this`.
   */
  public activateAuthorization(
    authorize: (variables: V, logged: User | undefined, params: Params<D>, extra: Extra) => Promise<boolean>
  ): this {
    this.authorize = authorize
    return this
  }

  /**
   * Execute the entry.
   *
   * @param action - The action.
   * @returns The handler which can be used for queries.
   */
  public execute(
    action: (
      variables: V,
      logged: User | undefined,
      params: Params<D>,
      extra: Extra
    ) => Promise<D['return']>
  ): ApiEntryExecutor<D> {
    return new ApiEntryExecutor(
      async (req: Request, res: Response, params: Params<D>): Promise<D['return']> => {
        const extra: Extra = { req, res }

        // Check data integrity
        this.checkBasicSanity(params.url, this.urlParamsValidation)
        this.checkBasicSanity(params.data, this.dataParamsValidation)

        // Load data
        let found = true
        const loadedVariables = await this.loadVariables(params, extra)
        for (const key in loadedVariables) {
          if (loadedVariables[key] === undefined) {
            found = false
          }
        }
        if (!found) {
          throw new ApiError(HttpError.NotFound, 'Could not load all needed variables')
        }
        const variables = loadedVariables as V

        // Authenticate
        if (!(await this.authenticate(variables, params, extra))) {
          throw new ApiError(HttpError.Unauthorized, 'User must be authenticated to execute this query')
        }

        // Check authorization
        const logged: User | undefined = req.user
        if (!(await this.authorize(variables, logged, params, extra))) {
          throw new ApiError(HttpError.Forbidden, 'User is not authorized to execute this query')
        }

        // Action
        let result: D['return']
        try {
          result = await action(variables, logged, params, extra)
        } catch (error) {
          throw new ApiError(HttpError.Conflict, error.message || error.toString())
        }

        return result
      }
    )
  }

  /**
   * Check basic sanity of the incoming data.
   *
   * @param data - The object containing the data.
   * @param values - The values to check.
   */
  private checkBasicSanity(data: any, values: Array<ParamValidation<any>>): void {
    for (const key in data) {
      data[key] = String(data[key])
    }
    for (const value of values) {
      if (!(value.key in data)) {
        if (!value.optional) {
          throw new ApiError(HttpError.BadRequest, `Missing mandatory value ${value.key}`)
        }
      } else {
        const testedValue = data[value.key] as string
        for (const validator of value.validators) {
          if (!validate(testedValue, validator)) {
            throw new ApiError(
              HttpError.BadRequest,
              `Value ${testedValue} is not appropriate for ${value.key}`
            )
          }
        }
      }
    }
  }
}
