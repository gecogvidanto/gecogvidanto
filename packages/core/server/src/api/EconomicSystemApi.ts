/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { EconomicSystem } from '@gecogvidanto/plugin'
import { ApiDefinition } from '@gecogvidanto/shared'

import { app } from '../AppData'
import { EconomicSystemHelper } from '../business'
import { langType } from '../tools'
import ApiEntry, { ApiEntryExecutor } from './ApiEntry'

/**
 * The API managing economic systems.
 */
export default class EconomicSystemApi {
  /**
   * Search available economic systems.
   */
  public onSearch: ApiEntryExecutor<ApiDefinition['economicSystemSearch']> = new ApiEntry<
    ApiDefinition['economicSystemSearch']
  >().execute(() =>
    Promise.resolve(
      app.data.economicSystems.map(economicSystem => EconomicSystemHelper.extractDoc(economicSystem))
    )
  )

  /**
   * Read economic system name.
   */
  public onRead: ApiEntryExecutor<ApiDefinition['economicSystemRead']> = new ApiEntry<
    ApiDefinition['economicSystemRead'],
    { economicSystem: EconomicSystem<langType> }
  >(params =>
    Promise.resolve({
      economicSystem: app.data.economicSystems.find(economicSystem => economicSystem.id === params.url.id),
    })
  ).execute(({ economicSystem }) => Promise.resolve(EconomicSystemHelper.extractDoc(economicSystem)))
}
