/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { ApiDefinition, validators } from '@gecogvidanto/shared'

import { EmailCheck, User } from '../business'
import ApiEntry, { ApiEntryExecutor } from './ApiEntry'

/**
 * The API managing user checking.
 */
export default class EmailCheckApi {
  /**
   * Validate the email.
   */
  public onValidate: ApiEntryExecutor<ApiDefinition['emailValidate']> = new ApiEntry<
    ApiDefinition['emailValidate'],
    { emailCheck: EmailCheck }
  >(async params => ({
    emailCheck: new EmailCheck(params.url),
  }))
    .activateUrlValidation({ key: 'email', validators: validators.email }, { key: 'token', validators: [] })
    .activateAuthentication(async (variables, _params, extra) => {
      const checkedId: string | undefined = await variables.emailCheck.validate()
      if (!checkedId) {
        await this.clearFailed()
        return false
      }

      const foundUser: User | undefined = await User.find(checkedId)
      if (!foundUser) {
        await this.clearFailed()
        return false
      }

      extra.user = foundUser
      return true
    })
    .execute(async (variables, logged, _params, extra) => {
      if (logged) {
        extra.req.logout()
      }
      extra.user.email = variables.emailCheck.email
      await extra.user.save()
    })

  /**
   * Clear the failed user creations.
   */
  private async clearFailed(): Promise<void> {
    const userIds = await EmailCheck.readAllUserIds()
    await User.removeFailedCreations(userIds)
  }
}
