/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Collection, Identifier, Model } from '@gecogvidanto/plugin'
import { Game } from '@gecogvidanto/shared'

/**
 * Model managing game data.
 */
export default class GameModel implements Model {
  public readonly indices = [
    {
      fieldName: 'start',
      unique: false,
      sparse: false,
    },
    {
      fieldName: 'masterId',
      unique: false,
      sparse: false,
    },
    {
      fieldName: 'closed',
      unique: false,
      sparse: false,
    },
  ]

  public constructor(private db: Collection<Game>) {}

  public async init(): Promise<void> {
    return Promise.resolve()
  }

  /**
   * Record a new game in database.
   *
   * @param game - The game to record.
   * @returns The identifier of the game.
   */
  public async create(game: Readonly<Game>): Promise<string> {
    return (await this.db.insert(game))._id
  }

  /**
   * Update game data.
   *
   * @param game - The game to update.
   */
  public async update(game: Readonly<Game>): Promise<void> {
    await this.db.update({ _id: game._id }, game)
  }

  /**
   * Search games.
   *
   * @returns The found games.
   */
  public search(): Promise<Array<Game & Identifier>> {
    return this.db.find({})
  }

  /**
   * Read a single game.
   *
   * @param _id - The game identifier.
   * @returns The matching game, if any.
   */
  public find(_id: string): Promise<(Game & Identifier) | undefined> {
    return this.db.findOne({ _id })
  }

  /**
   * Delete the game from database.
   *
   * @param _id - The identifier of the game to remove.
   */
  public async delete(_id: string): Promise<void> {
    await this.db.remove({ _id })
  }
}
