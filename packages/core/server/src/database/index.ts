/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { ModelList } from '@gecogvidanto/plugin'

import EmailCheckModel from './EmailCheckModel'
import GameModel from './GameModel'
import SessionModel from './SessionModel'
import UserModel from './UserModel'

/**
 * The server model, useful for typing database.
 */
export interface ServerModels extends ModelList {
  emailCheck: EmailCheckModel
  game: GameModel
  session: SessionModel
  user: UserModel
}

/**
 * The real models.
 */
export const serverModels = {
  emailCheck: EmailCheckModel,
  game: GameModel,
  session: SessionModel,
  user: UserModel,
}

export { default as EmailCheckModel, ModelEmailCheck } from './EmailCheckModel'
export { default as GameModel } from './GameModel'
export { default as SessionModel } from './SessionModel'
export { default as UserModel, ModelUser } from './UserModel'
