/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Collection, Identifier, Model, Query } from '@gecogvidanto/plugin'
import { CertLevel, User } from '@gecogvidanto/shared'

/**
 * User is internally saved with password and failure count.
 */
export interface ModelUser extends Partial<User> {
  name: string
  level: CertLevel
  password?: string
  failures?: number
}

const DEFAULT_ADMIN: ModelUser = {
  email: 'admin@gecogvidanto.org',
  name: 'Administrator',
  level: CertLevel.Administrator,
  password: '$2b$10$cZk0eqvkjePZXkSZr0f3Le6rrTg8C4hz8T/LvcqMWB7NKG3STKb0q', // admin*
  failures: 0,
}

/**
 * Model for managing user data.
 */
export default class UserModel implements Model {
  public readonly indices = [
    {
      fieldName: 'email',
      unique: true,
      sparse: true,
    },
    {
      fieldName: 'name',
      unique: true,
      sparse: false,
    },
  ]

  public constructor(private db: Collection<ModelUser>) {}

  /**
   * Create admin user if database is empty.
   */
  public async init(): Promise<void> {
    const elements = await this.db.count({})
    if (elements === 0) {
      await this.db.insert(DEFAULT_ADMIN)
    }
  }

  /**
   * Create a new user.
   *
   * @param user - The user to save.
   * @returns The user identifier.
   */
  public async create(user: Readonly<ModelUser>): Promise<string> {
    return (await this.db.insert(user))._id
  }

  /**
   * Update an existing user.
   *
   * @param user - The user to update.
   */
  public async update(user: Readonly<ModelUser>): Promise<void> {
    await this.db.update({ _id: user._id }, { $set: user })
  }

  /**
   * Clear or increase the failure count for a user.
   *
   * @param _id - The identifier of the user.
   * @param clear - True to clear failure count, false to increase.
   */
  public async updateFailures(_id: string, clear: boolean): Promise<void> {
    const updateQuery = clear ? { $set: { failures: 0 } } : { $inc: { failures: 1 } }
    await this.db.update({ _id }, updateQuery)
  }

  /**
   * Search users.
   *
   * @param email - Search only users matching e-mail if provided.
   * @param name - Search only users matching name if provided.
   * @returns Found users.
   */
  public search(email?: string, name?: string): Promise<Array<ModelUser & Identifier>> {
    const query: Partial<Identifier & ModelUser> = {}
    email && (query.email = email)
    name && (query.name = name)
    return this.db.find(query)
  }

  /**
   * Find the user with given identifier.
   *
   * @param _id - The identifier to search for.
   * @returns Found user, if any.
   */
  public find(_id: string): Promise<(ModelUser & Identifier) | undefined> {
    return this.db.findOne({ _id })
  }

  /**
   * Check that given name and email are unique across users.
   *
   * @param user - The user to chek unicity for.
   * @returns True if unique.
   */
  public async checkUnicity(user: Readonly<ModelUser>): Promise<boolean> {
    let query: Query<Identifier & ModelUser> = {
      $or: [{ name: user.name }, { email: user.email }],
    }
    if ('_id' in user) {
      query = { $and: [query, { $not: { _id: user._id } }] }
    }
    const result = await this.db.count(query)
    return result === 0
  }

  /**
   * Delete the user from database.
   *
   * @param _id - Identifier of the user.
   */
  public async delete(_id: string): Promise<void> {
    await this.db.remove({ _id })
  }

  /**
   * Remove users with no email (not fully created).
   *
   * @param saveIds - The identifiers of users not to be removed (waiting for confirmation).
   */
  public async removeFailedCreations(saveIds: string[]): Promise<void> {
    await this.db.remove({
      $and: [{ email: { $exists: false } }, { _id: { $nin: saveIds } }],
    })
  }
}
