/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Store } from 'express-session'

import { Collection, Model } from '@gecogvidanto/plugin'

/*
 * Model for storing sessions.
 */
export default class SessionModel extends Store implements Model {
  public readonly indices = []

  public constructor(private db: Collection<Express.SessionData>) {
    super()
  }

  public init(): Promise<void> {
    return Promise.resolve()
  }

  public destroy = (sid: string, callback?: (err?: any) => void): void => {
    this.db
      .remove({ _id: sid })
      .then(() => callback && callback())
      .catch(err => callback && callback(err))
  }

  public get = (sid: string, callback: (err: any, session?: Express.SessionData | null) => void): void => {
    this.getSession(sid)
      .then(session => callback(null, session))
      .catch(err => callback(err))
  }

  public set = (sid: string, session: Express.SessionData, callback?: (err?: any) => void): void => {
    const document = { _id: sid, ...session }
    this.db
      .update({ _id: sid }, document, true)
      .then(() => callback && callback())
      .catch(err => callback && callback(err))
  }

  public touch = (sid: string, session: Express.SessionData, callback?: (err?: any) => void): void => {
    this.getSession(sid)
      .then(currentSession => {
        if (currentSession) {
          currentSession.cookie = session.cookie
          return this.db.update({ _id: sid }, currentSession)
        } else {
          return Promise.resolve(0)
        }
      })
      .then(() => callback && callback())
      .catch(err => callback && callback(err))
  }

  private async getSession(sid: string): Promise<Express.SessionData | null> {
    await this.db.remove({ 'cookie.expires': { $lte: new Date() } })
    const result = await this.db.findOne({ _id: sid })
    if (result) {
      const { _id, ...session } = result
      return session
    } else {
      return null
    }
  }
}
