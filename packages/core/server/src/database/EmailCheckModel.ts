/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Collection, Identifier, Model, Query } from '@gecogvidanto/plugin'
import { EmailCheck } from '@gecogvidanto/shared'

/**
 * The model also internaly stores user id and expiration time.
 */
export interface ModelEmailCheck extends EmailCheck {
  readonly userId: string
  expires: number
}

/**
 * Model managing the email check data.
 */
export default class EmailCheckModel implements Model {
  public readonly indices = [
    {
      fieldName: 'email',
      unique: true,
      sparse: false,
    },
  ]

  public constructor(private db: Collection<ModelEmailCheck>) {}

  public async init(): Promise<void> {
    await this.clearExpired()
  }

  /**
   * Create an email check.
   *
   * @param emailCheck - The email check data.
   * @returns The created email check identifier.
   */
  public async create(emailCheck: Readonly<ModelEmailCheck>): Promise<string> {
    await this.clearExpired()
    return (await this.db.insert(emailCheck))._id
  }

  /**
   * Validate the email.
   *
   * @param email - The email to validate.
   * @param token - The token to control.
   * @returns The corresponding user identifier.
   */
  public async validate(email: string, token: string): Promise<string | undefined> {
    await this.clearExpired()
    const modelEmailCheck = await this.db.findOne({ email, token })
    return modelEmailCheck ? modelEmailCheck.userId : undefined
  }

  /**
   * Read all available user identifiers.
   *
   * @returns User identifiers.
   */
  public async readAllUserIds(): Promise<string[]> {
    await this.clearExpired()
    const modelEmailChecks = await this.db.find({})
    return modelEmailChecks.map(item => item.userId)
  }

  /**
   * Delete entries with either given email or user identifier (if given).
   *
   * @param email - The email to be removed.
   * @param userId - The user id to be removed.
   */
  public async delete(email: string, userId?: string): Promise<void> {
    let query: Query<Identifier & ModelEmailCheck> = { email }
    if (userId) {
      query = { $or: [{ userId }, query] }
    }
    await this.db.remove(query)
  }

  /**
   * Clear the expired entries.
   *
   * @returns Count of removed entries.
   */
  private clearExpired(): Promise<number> {
    return this.db.remove({ expires: { $lte: Date.now() } })
  }
}
