/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {
  ConfigDescription,
  Confinode,
  FileDescription,
  Level,
  Message,
  anyItem,
  defaultValue,
  literal,
  numberItem,
  stringItem,
} from 'confinode'
// eslint-disable-next-line import/no-internal-modules
import yamlLoaderDescription from 'confinode/lib/Loader/loaders/yaml'
import * as winston from 'winston'

import { app } from '../AppData'
import { DeepReadonly, createRandom } from '../tools'
import { LoggingDefinition, loggingDescription } from './configureLogger'

interface RealConfig {
  server: {
    host: string
    port: number
    keyFile: string
    certFile: string
    remoteHost: string
    remotePort: number
  }
  security: {
    sessionKey: string
    maxFailures: number
    validity: {
      session: number
      email: number
    }
  }
  reCaptcha: {
    siteKey: string
    secretKey: string
    threshold: number
  }
  email: {
    from: string
    prefix: string
    config: any
  }
  logging: LoggingDefinition
  game: {
    maxPlayers: number
  }
  plugins: {
    [name: string]: any
  }
}

/**
 * The full application configuration.
 */
type Config = DeepReadonly<RealConfig>
export default Config

/**
 * Filter to add `etc` configuration file.
 *
 * @param descriptions - The file descriptions to filter.
 * @returns The filtered description (with `etc` file added).
 */
const yamlLoader = new yamlLoaderDescription.Loader(undefined)
function fileFilter(descriptions: FileDescription[]): FileDescription[] {
  return [
    ...descriptions,
    { name: '/etc/gecogvidanto/gecogvidanto.conf', loader: yamlLoader },
    { name: '/etc/gecogvidanto.conf', loader: yamlLoader },
    '/etc/gecogvidanto/gecogvidanto',
    '/etc/gecogvidanto',
  ]
}

/**
 * The logger for confinode.
 *
 * @param msg - The message to display.
 */
function logger(msg: Message<any>): void {
  const displayFn =
    msg.level === Level.Error
      ? winston.error
      : msg.level === Level.Warning
      ? winston.warn
      : msg.level === Level.Information
      ? winston.info
      : winston.debug
  displayFn(msg.toString())
}

/**
 * The configuration builder.
 */
export class ConfigBuilder {
  private readonly pluginDescriptions: { [name: string]: ConfigDescription<any> } = {}

  public addPluginDescription(name: string, description: ConfigDescription<any>): this {
    this.pluginDescriptions[name] = description
    return this
  }

  public async build(): Promise<Config | undefined> {
    if (process.argv.length > 3) {
      winston.warn(app.data.serverLang.configurationParameters())
    }
    const confinode = new Confinode('gecogvidanto', this.buildDescription(), {
      files: [fileFilter],
      logger,
    })
    const result = await (process.argv.length > 2 ? confinode.load(process.argv[2]) : confinode.search())
    return result?.configuration
  }

  private buildDescription(): ConfigDescription<RealConfig> {
    return literal<RealConfig>({
      server: literal({
        host: stringItem(''),
        port: numberItem(443),
        keyFile: stringItem(),
        certFile: stringItem(),
        remoteHost: stringItem(''),
        remotePort: numberItem(0),
      }),
      security: literal({
        sessionKey: stringItem(createRandom(48)),
        maxFailures: numberItem(3),
        validity: literal({
          session: numberItem(4 * 3600),
          email: numberItem(24 * 3600),
        }),
      }),
      reCaptcha: literal({
        siteKey: stringItem(''),
        secretKey: stringItem(''),
        threshold: numberItem(50),
      }),
      email: literal({
        from: stringItem('"ĞecoĞvidanto server" <no-reply@gecogvidanto.org>'),
        prefix: stringItem(''),
        config: defaultValue(anyItem(), {}),
      }),
      logging: loggingDescription,
      game: literal({
        maxPlayers: numberItem(20),
      }),
      plugins: literal(this.pluginDescriptions),
    })
  }
}
