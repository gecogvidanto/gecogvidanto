/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import {
  anyItem,
  array,
  booleanItem,
  choiceItem,
  defaultValue,
  literal,
  optional,
  stringItem,
} from 'confinode'
import * as winston from 'winston'

import { DeepReadonly } from '../tools'

type Format = import('logform').Format
type TransformableInfo = import('logform').TransformableInfo
type Transport = import('winston-transport')

export interface LoggerDefinition {
  type: 'console' | 'file'
  level: string
  strict: boolean
  formats: {
    // eslint-disable-next-line @typescript-eslint/ban-types
    align?: {}
    colorize?: {
      all: boolean
      level: boolean
      message: boolean
    }
    label?: {
      label: string
      message: boolean
    }
    padLevels?: {
      filler: string
    }
    timestamp?: {
      format: string
    }
  }
  output: {
    type: 'json' | 'printf'
    format: string | number
  }
  options: any
}

const loggerDescription = literal<LoggerDefinition>({
  type: choiceItem(['console', 'file']),
  level: stringItem('info'),
  strict: booleanItem(),
  formats: literal({
    align: optional(anyItem()),
    colorize: optional(
      literal({
        all: booleanItem(),
        level: booleanItem(true),
        message: booleanItem(),
      })
    ),
    label: optional(
      literal({
        label: stringItem(),
        message: booleanItem(true),
      })
    ),
    padLevels: optional(
      literal({
        filler: stringItem(' '),
      })
    ),
    timestamp: optional(
      literal({
        format: stringItem(''),
      })
    ),
  }),
  output: literal({
    type: choiceItem(['json', 'printf'], 'printf'),
    // eslint-disable-next-line no-template-curly-in-string
    format: stringItem('${message}'),
  }),
  options: defaultValue(anyItem(), {}),
})

export interface LoggingDefinition {
  level: string
  color: boolean
  loggers: LoggerDefinition[]
}

export const loggingDescription = literal<LoggingDefinition>({
  level: stringItem(process.env.NODE_ENV === 'production' ? 'info' : 'silly'),
  color: booleanItem(true),
  loggers: defaultValue(array(loggerDescription), []),
})

function buildDefaultConfig(level?: string, color?: boolean): winston.transports.ConsoleTransportOptions {
  const options: winston.transports.ConsoleTransportOptions = {}
  options.level = level || (process.env.NODE_ENV === 'production' ? 'info' : 'silly')
  if (options.level === 'silent') {
    options.level = 'info'
    options.silent = true
  }
  options.format = winston.format.printf((info: any) => info.message)
  if (color === undefined || color === true) {
    options.format = winston.format.combine(
      winston.format.colorize({
        message: true,
        colors: { info: 'white' },
      }),
      options.format
    )
  }
  return options
}

function levelFilter(info: TransformableInfo, opts?: any): TransformableInfo | boolean {
  return info.level === opts.level ? info : false
}

function outputFormatter(format: string): (info: TransformableInfo) => string {
  const parts: string[][] = ('}' + format)
    .split(/\$\{/)
    .map(part => part.split(/\}/))
    .map(portions => [portions[0], portions.slice(1).join('}')])
  return info => {
    return parts.reduce((previous, current) => {
      let portion = previous
      if (current[0].length > 0 && current[0] in info) {
        portion += info[current[0]]
      }
      portion += current[1]
      return portion
    }, '')
  }
}

function buildOutput(type: 'json' | 'printf', format: string | number): Format {
  switch (type) {
    case 'json':
      return winston.format.json({ space: Number(format) })
    case 'printf':
      return winston.format.printf(outputFormatter(format.toString()))
    default:
      throw new Error('Unknown logger output type')
  }
}

function buildLogger(definition: LoggerDefinition): Transport {
  const formats: Format[] = []
  if (definition.strict) {
    formats.push(winston.format(levelFilter)({ level: definition.level }))
  }
  const formatters: Array<'align' | 'colorize' | 'label' | 'padLevels' | 'timestamp'> = [
    'align',
    'colorize',
    'label',
    'padLevels',
    'timestamp',
  ]
  formats.push(
    ...formatters
      .filter(key => definition.formats[key] !== undefined)
      .map(key => {
        const reworked: any = { ...definition.formats[key] }
        if (key === 'padLevels') {
          reworked.levels = winston.config.npm.levels
        } else if (key === 'timestamp' && reworked.format === '') {
          delete reworked.format
        }
        return { key, reworked }
      })
      .map(({ key, reworked }) => (winston.format[key] as (f: any) => Format)(reworked))
  )
  formats.push(buildOutput(definition.output.type, definition.output.format))

  const level = definition.level
  const format: Format = formats.length === 1 ? formats[0] : winston.format.combine(...formats)
  switch (definition.type) {
    case 'console':
      return new winston.transports.Console({
        level,
        format,
        ...definition.options,
      })
    case 'file':
      return new winston.transports.File({
        level,
        format,
        ...definition.options,
      })
    default:
      throw new Error('Unknown logger type')
  }
}

export default function (config?: DeepReadonly<LoggingDefinition>): void {
  if (!config) {
    winston.configure({
      transports: [new winston.transports.Console(buildDefaultConfig())],
    })
  } else {
    const transports: Transport[] = []
    transports.push(new winston.transports.Console(buildDefaultConfig(config.level, config.color)))
    transports.push(...config.loggers.map(logger => buildLogger(logger)))
    winston.configure({ transports })
  }
}
