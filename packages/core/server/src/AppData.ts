/*
 * This file is part of @gecogvidanto/server.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { LanguageMap } from 'intl-ts'

import { langType as uiLangType } from '@gecogvidanto/client-web'
import { Database, EconomicSystem, GecoPlugin, ServerApp } from '@gecogvidanto/plugin'

import type { Config } from './config'
import type { ServerModels } from './database'
import type { Mailer, langType } from './tools'

/**
 * Information about this server.
 */
export interface ServerInfo {
  name?: string
  version?: string
  plugins: ReadonlyArray<
    Readonly<{
      module: string
      name: string
      description: string
      version?: string
      database: boolean
      ecoSysIds: ReadonlyArray<string>
    }>
  >
}

/**
 * A loaded plugin.
 */
export interface LoadedPlugin {
  readonly name: string
  readonly description: string
  readonly plugin: GecoPlugin
}

/**
 * Application data.
 */
export default interface AppData extends ServerApp<langType> {
  readonly serverInfo: Readonly<ServerInfo>
  readonly plugins: ReadonlyArray<LoadedPlugin>
  readonly uiLangMap: LanguageMap<uiLangType>
  readonly config: Config
  readonly mailer: Mailer
  readonly database: Database<ServerModels>
  readonly economicSystems: ReadonlyArray<EconomicSystem<langType>>
}

export const app = {
  data: (undefined as any) as AppData,
}
