/*
 * This file is part of @gecogvidanto/plugin-locale-eo.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { sharedMessages } from './ui.shared.eo'

export const messages: import('@gecogvidanto/server/dist/tools/locale.en').langType &
  import('@gecogvidanto/plugin-nedb/dist/locale.en').langType = {
  ...sharedMessages,

  configurationParameters:
    'Nur unu parametro estas atendita, por specifi la agordan dosieron — la aliaj estas ignoritaj',
  databaseNoPlugin: 'Neniu datumbaza etendo trovita, bonvolu aldoni unu',
  economicSystemNotEnough: 'Tro malmultaj ekonomiaj sistemoj, bonvolu aldoni etendojn kun iuj',
  initPhaseConfiguration: 'Ŝarĝante la agordon',
  initPhaseDatabase: 'Agordante datumbazon',
  initPhaseEconomicSystems: 'Agordante ekonomiaj sistemoj',
  initPhaseLogger: 'Agordante ĵurnalo',
  initPhaseMailer: 'Agordante mesaĝado',
  initPhaseModels: 'Agordante datumbazaj modeloj',
  initPhasePlugins: 'Agordante etendoj',
  initPhaseStart: 'Atendante ke ĉiuj servoj estas komencitaj',
  mailerCheckHtmlText: (name: string, url: string) => `<html><p>Saluton ${name}!</p>
  <p>Por validigi vian retadreson sur la servilo ĞecoĞvidanto, bonvolu klaki
  <a href="${url}">ĉi-tiun ligilon</a>.
  <p>Se vi ne povas klaki rekte la ligilon, kopiu la suba URL
  kaj almetu ĝin en vian retumilon:</p>
  ${url}</html>`,
  mailerCheckPlainText: (name: string, url: string) => `Saluton ${name}!
  Por validigi vian retadreson sur la servilo ĞecoĞvidanto, bonvolu klaki la suba ligilon:
  ${url}
  Se vi ne povas klaki rekte la ligilon, kopiu ĝin kaj almetu ĝin en vian retumilon.`,
  mailerCheckSubject: 'Retadreson validigo',
  mailerError: (subject: string, to: string) => `Eraro sendante la mesaĝon «${subject}» al ${to}`,
  mailerNotConfigured: 'Mesaĝumilo ne estas agordita — Novaj uzantoj estos kreitaj sen retadresa kontrolo',
  mailerSent: (subject: string, to: string) => `Mesaĝo «${subject}» sendita al ${to}`,
  pluginMultipleDatabase: 'Multaj datumbaza etendo trovita, bonvolu forigi la supernombraj',
  reCaptchaConfigError:
    'La agordo de captcha estas malĝusta: sekreta ŝlosilo devas esti provizita kun la reteja ŝlosilo',
  reCaptchaPreventNewUsers: 'La sekreta ŝlosilo de captcha ne agordas: novaj uzantoj ne povos registriĝi',
  serverNewConnection: (ip: string) => `Nova rilato de ${ip}`,
  serverNewWSConnection: 'Web socket rilato',
  serverPortInUse: (port: number) => `La haveno ${port} estas jam uzita per alia apliko`,
  serverPortPermissions: (port: number) => `Vi ne havas sufiĉe da rajtoj por uzi la havenon ${port}`,
  serverRunning: (hostname: string, port: number) =>
    `Servilo kuranta sur https://${hostname || 'localhost'}:${port}/` +
    (hostname ? '' : ' (aŭskultante ĉiuj interfacoj)') +
    `\nPremu Ctrl-C por fini`,
  serverStopped: 'La servilo ĉesis aŭskulti',
  serverWSRegister: (id: string, name: string) =>
    `La WS registriĝis por la evento ${name} sur la ludo ${id}`,
  serverWSUnknown: (name: string) => `Nekonata ${name} evento nekonsiderita`,
  terminated: 'La servilo ĞecoĞvidanto ĉesis korekte',

  nedb$databaseLoaded: 'Datubazo ŝarĝita en memoro',
  nedb$defaultDirectory: (directory: string) =>
    `Datubaza dosierujo ne provizita, uzu defaŭlta:\n${directory}`,
}
