/*
 * This file is part of @gecogvidanto/plugin-locale-eo.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { sharedMessages } from './ui.shared.eo'

export const messages: import('@gecogvidanto/client-web').langType &
  import('@gecogvidanto/plugin-basesys/dist/ui.locale.en').langType &
  import('@gecogvidanto/plugin-barter/dist/ui.locale.en').langType &
  import('@gecogvidanto/plugin-mutcredit/dist/ui.locale.en').langType = {
  ...sharedMessages,

  $client$connection: 'Malsukceso komuniki kun ĞecoĞvidanto servilo',
  $client$credentials: 'Malsukcesa konekto: nevalida identiĝo',
  $client$economicSystem: 'Ekonomia systemo netrovebla en la servilo',
  $client$emailCheck: 'La retmesaĝa ĵetono ne kongruas — Bonvolu rekomenci la proceduro',
  $client$gameClose: 'Neebla fermi la ludon: ne sufiĉaj rondaroj aŭ ne finita rondaro',
  $client$gameNextRound: 'La nuna rondaro jam estas finita',
  $client$gamePlayers: 'Maksimuma kvanto da ludantoj atingis aŭ multoblajn ludantojn kun la sama nomo',
  $client$gameSets: 'Jam estas nefinita rondaro en la nuna ludo',
  $client$gameTechBreak: 'Ne estas nuna rondaro por registri teknologian progreson',
  $client$userExists: 'Alia ludanto estas jam registrata kun sama nomo aŭ sama retadreso',

  backToSiteHome: 'Reen al la ĉefpaĝon',
  cancel: 'Niligi',
  close: 'Fermi',
  closeMenu: 'Fermi la menuon',
  delete: 'Forigi',
  economicSystem: 'Ekenemia sistemo',
  email: 'Retadreso',
  formDying: (name: string) => `${name} mortigas je ĉi-tiu rondo`,
  gameClosed: 'Fermita ludo',
  gameScoreSet: (name: string) => `Poentaro registrita por la ludanto ${name}`,
  gameTechBreakRecorded: 'Nova teknologia progreso registrita',
  loading: 'Ŝarĝante…',
  logged: (name: string) => `Vi nun ensalutis kiel ${name}`,
  loggedOut: 'Vi nun elsalutis',
  menuAction: 'Ago',
  menuActionTechBreak: 'Teknologia progreso',
  menuActionTechBreakConfirm: 'Bonvolu konfirmi la teknologian progreson',
  menuPlayerAdd: 'Aldoni ludanton',
  minutes: (value: number) => `${value} minutoj`,
  notConnected: '(ne ensalutante)',
  ok: 'OK',
  openMenu: 'Malfermi la menuon',
  pageAbout: 'Pri',
  pageAboutPlugins: 'Instalitaj etendoj',
  pageAboutServer: (app: string, name: string | undefined, version: string | undefined) =>
    `Servilo ${app}: ${name || '(nekonata)'}, versio ${version || 'nekonata'}`,
  pageAboutVersion: (version: string | undefined) => `Versio: ${version || 'nekonata'}`,
  pageAddPlayers: 'Aldoni ludantojn',
  pageAddPlayersId: (id: number) => `Ludanto ${id}`,
  pageAddPlayersSameName: 'Alia ludanto jam havas tiun nomon',
  pageEmailCheck: 'Retadresa kontrolo',
  pageEmailCheckFailed: 'Malsukcesis validigi la retadreson',
  pageEmailCheckPleaseWait: 'Kurante validigi vian retadreson, bonvolu atendi…',
  pageEmailCheckSucceeded: 'Valida Retadreso — Vi nun povas ensaluti per la nova adreso',
  pageGameAddPlayers: 'Aldoni ludantojn',
  pageGameChartIncludeNpc: 'Inkluzivi ne-ludantajn karakterojn',
  pageGameChartMean: 'Mezumo',
  pageGameChartRelDeviation: 'Norma devio/mezumo',
  pageGameChartStdDeviation: 'Norma devio',
  pageGameChartTotal: 'Totalo',
  pageGameClose: 'Fermu la ludon',
  pageGameMaster: (name: string) => `Ludo gvidanta de ${name}`,
  pageGameMasterThanks: (key: string) =>
    `Danku ŝin/lin kun donaco en Ğ1 danke al ŝia/lia publika ŝlosilo ${key}`,
  pageGameNoPlayers: 'Nuntempe ne estas ludantoj en la ludo',
  pageGamePlay: 'Ludu la ludon',
  pageGamePlayers: 'Ludantoj',
  pageGamePlayersLimit: (start: boolean, roundSet: number, round: number) =>
    `${start ? 'de' : 'ĝis'} rondo ${round + 1} de rondaro ${roundSet + 1}`,
  pageGameSet: (index: number, xchgSysName: string) => `Rondaro ${index + 1}: ${xchgSysName}`,
  pageGameSetInProgress: 'Kuranta rondaro',
  pageGameSummary: 'Resumo',
  pageGameViewMoney: 'Vidu la monan helpan paĝon',
  pageGameViewValues: 'Vidu la valoran helpan paĝon',
  pageHome: 'Hejme',
  pageIndex: 'Ludoj',
  pageIndexDate: 'Dato',
  pageIndexLocation: 'Loko',
  pageIndexNoGame: 'Neniu ludo en datumbazo',
  pageIndexPlayers: 'Nb da ludantoj',
  pageIndexSets: 'Nb da rondaroj',
  pageNewGame: 'Nova ludo',
  pageNewGameLocationAddress: 'Adreso',
  pageNewGameLocationName: 'Nomo de loko',
  pageNewGameRoundLength: 'Daŭro de rondo',
  pageNewGameRoundsPerSet: 'Rondoj per rondaro',
  pageNotFound: 'Netrovata paĝo',
  pageNotFoundLost: 'Ŝajnas, ke vi perdis vin mem en la meandroj de mona kreado.',
  pagePlayStatsCardGame: 'Kartoj',
  pagePlayStatsFour: 'Kvarajoj',
  pagePlayStatsPlayers: 'Ludantoj',
  pageProfile: 'Profilo',
  pageProfileAccount: 'Banka publika ŝlosilo',
  pageProfileDataSent: 'Datumoj senditaj al la servilo',
  pageProfileEmailDuplicate: 'Tiu retadreso jam estas prenita de alia uzanto',
  pageProfileLevel: 'Akreditada nivelo',
  pageProfileName: 'Nomo aŭ kromnomo',
  pageProfileNameDuplicate: 'Tiu nomo jam estas prenita de alia uzanto',
  pageProfilePasswordCheck: 'Tajpu la pasvorton denove',
  pageProfilePasswordReset: 'Restarigu pasvorton',
  pageProfileSignUp: (app: string) => `Registri kiel ludgvidanto de ${app}`,
  pageRound: (round: number, total: number) =>
    round ? `Rondo ${round} de ${total} (${(round * 80) / total} jarojn)` : 'Rondo de komenco',
  pageRoundPreview: 'Preparu la helpfoliojn',
  password: 'Pasvorto',
  pause: 'Paŭzo',
  playerExit: 'Foriro de ludanto',
  playerExitConfirm: (name: string) => `Bonvolu konfirmi, ke ${name} forlasas la ludon.`,
  playerExitSelect: 'Elektu la ludanton forlasante la ludon',
  refresh: 'Renovigi',
  removeAll: 'Forigi ĉion',
  setCreate: 'Krei rondaro',
  setNew: 'Nova rondaro',
  signIn: 'Ensaluti',
  signInTitle: 'Ensaluto',
  signOut: 'Elsaluti',
  signUp: 'Registri',
  start: 'Eki',
  unchanged: '(nemodifita)',
  unknownError: 'Nekonata eraro',
  userCheckEmail: 'Bonvolu kontroli vian mesaĝumilon por konfirmi vian retadreson',
  userCreated: (name: string) => `Uzanto ${name} sukcese kreita`,
  userUpdateSuccess: (name: string) => `Uzanto ${name} sukcese ĝisdatigita`,
  _displayCertLevel: (level: keyof typeof import('@gecogvidanto/shared').CertLevel): string => {
    switch (level) {
      case 'Administrator':
        return 'Administranto'
      case 'GameMaster':
        return 'Ludgvidanto'
      default:
        // eslint-disable-next-line no-case-declarations
        const l: never = level
        throw new Error(`Nekonata nivelo ${l}`)
    }
  },
  _displayDate: (date: Date) => {
    const fmt = (n: number): string => ('0' + n).slice(-2)
    return `${fmt(date.getDate())}/${fmt(date.getMonth() + 1)}/${date.getFullYear()} ${fmt(
      date.getHours()
    )}:${fmt(date.getMinutes())}`
  },

  basesys$: '',
  basesys$bank: 'Banko',
  basesys$bankResult: 'Banko bilanco',
  basesys$debtMoney: 'Ŝuldo mono',
  basesys$extend: 'Pruntoj etenditaj',
  basesys$grantedCheck: (count: number) =>
    `La posedaĵo de ludanto devas jam valori almenaŭ ${count} unuecoj`,
  basesys$grantedCheckStatic: 'La posedaĵo de ludanto devas jam valori almenaŭ tiel unuecoj',
  basesys$grantedField: 'Novaj pruntoj',
  basesys$grantedHelp: (count: number) => `Donaci ${count} unuecoj al la ludanto`,
  basesys$grantedHelpStatic: 'Donaci 3 fojoj la kvanto de pruntoj en unuecoj al la ludanto',
  basesys$libreMoney: 'Libera mono',
  basesys$mMassPP: (mmass: number) => `Mona provizo: ${mmass} unuecoj per ludanto`,
  basesys$nonRefundable: 'Nerepagebla pruntoj (preno)',
  basesys$outstanding: (player: string, loanCount: number) =>
    `${player} havas ${loanCount} prunto(j) en progreso — ${loanCount * 4} unuecoj je repagi`,
  basesys$refund: 'Repagitaj pruntoj',
  basesys$refundHelp: (count: number) => `Preni ${count} unuecoj al ludanto`,
  basesys$refundHelpStatic: 'Preni al ludanto 4 unuecoj per repagita prunto kay 1 per etendita prunto',
  basesys$requestLoan: 'Petu prunton',
  basesys$seized: 'Prenita kvanto',
  basesys$sell: 'Vendi prenaĵojn',
  basesys$soldField: 'Kvanto',
  basesys$soldLabel: 'Indiki la kvanton de mono gajnita de la banko',
  basesys$toSeize: (count: number) => `Prenu almenaŭ la ekvivalento de ${count} unuecoj`,
  basesys$toSeizeStatic: 'Prenu almenaŭ la ekvivalento de 1,5 fojoj la nerepagita kvanto',

  barter$name: 'Interŝanĝo',

  mutcredit$name: 'Mutuala kredito',
}
