/*
 * This file is part of @gecogvidanto/plugin-locale-eo.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { createGecoPlugin } from '@gecogvidanto/plugin'

import EoPlugin from './EoPlugin'
import { messages as eo } from './locale.eo'
import { messages as uiEo } from './ui.locale.eo'

module.exports = createGecoPlugin(EoPlugin, {
  plugin: 'gecogvidanto-plugin-v1',
  description: 'Esperantaj tradukoj (Esperanto translations)',
  messages: { eo },
  uiMessages: { eo: uiEo },
  models: {},
})
