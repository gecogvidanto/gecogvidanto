/*
 * This file is part of @gecogvidanto/plugin-locale-eo.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
export const sharedMessages: import('@gecogvidanto/shared').langType = {
  $: 'Esperanto',
  $shared$empty: 'La kampo ne povas esti malplena',
  $shared$formMoney: 'Skribi la kvanto de monbiletoj',
  $shared$formMoneyHigh: 'Altaj biletoj',
  $shared$formMoneyLow: 'Malaltaj biletoj',
  $shared$formMoneyMedium: 'Mezaj biletoj',
  $shared$formValues: 'Skribi la kvanto de valoroj',
  $shared$formValuesHigh: 'Altaj valoroj',
  $shared$formValuesLow: 'Malaltaj valoroj',
  $shared$formValuesMedium: 'Mezaj valoroj',
  $shared$invalidEmail: 'La valoro estu valida retadreso',
  $shared$maxLength: (length: number) => `La valoro estu malpli longa ol ${length} signoj`,
  $shared$minLength: (length: number) => `La valoro estu pli longa ol ${length} signoj`,
  $shared$nonMatching: 'La valoro ne kongruas',
  $shared$notInList: (valid: string) => `La valoro estu unu de ${valid}`,
}
