/*
 * This file is part of @gecogvidanto/plugin-locale-fr.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
export const sharedMessages: import('@gecogvidanto/shared').langType = {
  $: 'Français',
  $shared$empty: 'Le champ ne peut pas être vide',
  $shared$formMoney: 'Indiquer le nombre de billets',
  $shared$formMoneyHigh: 'Billets hauts',
  $shared$formMoneyLow: 'Billets bas',
  $shared$formMoneyMedium: 'Billets moyens',
  $shared$formValues: 'Indiquer le nombre de valeurs',
  $shared$formValuesHigh: 'Valeurs hautes',
  $shared$formValuesLow: 'Valeurs basses',
  $shared$formValuesMedium: 'Valeurs moyennes',
  $shared$invalidEmail: 'La valeur doit être une adresse électronique valide',
  $shared$maxLength: (length: number) => `La valeur doit faire au plus ${length} caractères de long`,
  $shared$minLength: (length: number) => `La valeur doit faire au moins ${length} caractères de long`,
  $shared$nonMatching: 'La valeur ne correspond pas',
  $shared$notInList: (valid: string) => `La valeur doit être choisie parmi ${valid}`,
}
