/*
 * This file is part of @gecogvidanto/plugin-locale-fr.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { sharedMessages } from './ui.shared.fr'

/* eslint-disable no-irregular-whitespace */
export const messages: import('@gecogvidanto/server/dist/tools/locale.en').langType &
  import('@gecogvidanto/plugin-nedb/dist/locale.en').langType = {
  ...sharedMessages,

  configurationParameters:
    'Un seul paramètre est attendu, afin de spécifier le fichier de configuration — les autres sont ignorés',
  databaseNoPlugin: 'Aucune extension de base de données trouvée, veuillez en ajouter une',
  economicSystemNotEnough:
    'Il y a trop peu de systèmes économiques, veuillez ajouter des extensions en contenant',
  initPhaseConfiguration: 'Chargement de la configuration',
  initPhaseDatabase: 'Initialisation de la base de données',
  initPhaseEconomicSystems: 'Initialisation des systèmes économiques',
  initPhaseLogger: 'Initialisation de la journalisation',
  initPhaseMailer: 'Initialisation de la messagerie',
  initPhaseModels: 'Initialisation des modèles de base de données',
  initPhasePlugins: 'Initialisation des extensions',
  initPhaseStart: 'Attente du démarrage de tous les services',
  mailerCheckHtmlText: (name: string, url: string) => `<html><p>Bonjour ${name}&nbsp;!</p>
  <p>Afin de valider votre adresse électronique sur le serveur ĞecoĞvidanto, veuillez cliquer
  <a href="${url}">sur ce lien</a>.
  <p>Si vous ne pouvez pas cliquer directement sur le lien, copiez l’URL ci-dessous
  et collez-la dans votre navigateur&nbsp;:</p>
  ${url}</html>`,
  mailerCheckPlainText: (name: string, url: string) => `Bonjour ${name} !
  Afin de valider votre adresse électronique sur le serveur ĞecoĞvidanto, veuillez cliquer sur le lien ci-dessous :
  ${url}
  Si vous ne pouvez pas cliquer directement sur le lien, copiez-le et collez-le dans votre navigateur.`,
  mailerCheckSubject: 'Validation d’adresse',
  mailerError: (subject: string, to: string) => `Erreur lors de l’envoi du message « ${subject} » à ${to}`,
  mailerNotConfigured:
    'La messagerie n’est pas configurée — Les nouveaux utilisateurs seront créés sans contrôle d’adresse électronique',
  mailerSent: (subject: string, to: string) => `Message « ${subject} » envoyé à ${to}`,
  pluginMultipleDatabase:
    'Plusieurs extensions de base de données trouvées, veuillez supprimer les surnuméraires',
  reCaptchaConfigError:
    'La configuration du captcha est erronée : une clé secrète doit être fournie avec la clé du site',
  reCaptchaPreventNewUsers:
    'La clé secrète de captcha n’a pas été configurée : les nouveaux utilisateurs ne pourront pas s’enregistrer',
  serverNewConnection: (ip: string) => `Nouvelle connexion depuis ${ip}`,
  serverNewWSConnection: 'Connexion Web socket',
  serverPortInUse: (port: number) => `Le port ${port} est déjà utilisé par une autre application`,
  serverPortPermissions: (port: number) =>
    `Vous n’avez pas suffisamment de droits pour utiliser le port ${port}`,
  serverRunning: (hostname: string, port: number) =>
    `Serveur en fonctionnement sur https://${hostname || 'localhost'}:${port}/` +
    (hostname ? '' : ' (en écoute sur toutes les interfaces)') +
    `\nAppuyez sur Ctrl-C pour terminer`,
  serverStopped: 'Le serveur n’est plus en écoute',
  serverWSRegister: (id: string, name: string) =>
    `La WS s’est enregistré pour l’évènement ${name} sur la partie ${id}`,
  serverWSUnknown: (name: string) => `Évènement WS inconnu ${name} ignoré`,
  terminated: 'Le serveur ĞecoĞvidanto s’est correctement arrêté',

  nedb$databaseLoaded: 'Base de donnée chargée en mémoire',
  nedb$defaultDirectory: (directory: string) =>
    `Répertoire de base de données non fourni, utilisation du défaut :\n${directory}`,
}
