/*
 * This file is part of @gecogvidanto/plugin-locale-fr.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import { GecoPlugin, ServerApp, isGecoPluginType } from '@gecogvidanto/plugin'

import * as plugin from '.'

const serverApp: ServerApp = {
  serverLang: undefined as any,
  clientLang: undefined as any,
  config: {},
  database: undefined as any,
}

describe('FrPluginType', function () {
  it('must have the plugin signature', function () {
    expect(plugin).to.exist.and.to.include.all.keys('plugin')
    expect(isGecoPluginType(plugin)).to.be.true
  })
})

describe('FrPlugin', function () {
  let pluginInstance: GecoPlugin

  beforeEach('Create the plugin', function () {
    if (isGecoPluginType(plugin)) {
      pluginInstance = new plugin(serverApp)
      pluginInstance.ready() // Should not modify anything
    }
    expect(pluginInstance).to.exist
  })

  it('must be created correctly', function () {
    expect(pluginInstance).to.be.ok
  })
})
