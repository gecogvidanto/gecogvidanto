/*
 * This file is part of @gecogvidanto/plugin-locale-fr.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import { messages } from './locale.fr'
import { messages as uiMessages } from './ui.locale.fr'

const DUMMY: any[] = []
for (let i = 0; i < 10; i++) {
  DUMMY.push('!DUMMY!')
}

function show(key: string, msg: string): void {
  // eslint-disable-next-line no-console
  console.log(`  \x1b[0;32m${key}\x1b[0;30m =>\n    "\x1b[1;36m${msg}\x1b[0;30m"\x1b[0m`)
}

const TESTED_MESSAGES = [
  { id: 'locale.fr', messages },
  { id: 'ui.locale.fr', messages: uiMessages },
]

TESTED_MESSAGES.forEach(tested => {
  describe(tested.id, function () {
    it('must be ordered', function () {
      const prefixes: string[] = []
      let previous: string | undefined

      for (const key in tested.messages) {
        const idx = key.lastIndexOf('$')
        const prefix = key.substring(0, idx + 1)
        const msgKey = key.substring(idx + 1)

        if (prefixes.length === 0 || prefix !== prefixes[prefixes.length - 1]) {
          expect(prefixes.includes(prefix), `${key} is not ordered`).to.be.false
          previous = undefined
          prefixes.push(prefix)
        }

        if (previous) {
          if (msgKey.startsWith('_') !== previous.startsWith('_')) {
            expect(msgKey.startsWith('_'), `${key} is not ordered`).to.be.true
          } else {
            expect(msgKey > previous, `${key} is not ordered`).to.be.true
          }
        }
        previous = msgKey
      }
    })

    it('must display messages in french', function () {
      const testedMessages: { [key: string]: any } = tested.messages
      for (const key in testedMessages) {
        if (key === '_displayCertLevel') {
          show(key, testedMessages[key]('Administrator'))
          show(key, testedMessages[key]('GameMaster'))
          expect(() => show(key, testedMessages[key]('Nenio'))).to.throw()
        } else if (key === '_displayDate') {
          const date = new Date()
          show(key, testedMessages[key](date))
          show(key, testedMessages[key](new Date(date.valueOf() - 12 * 60 * 60 * 1000)))
        } else if (typeof testedMessages[key] === 'function') {
          show(key, testedMessages[key](...DUMMY))
        } else {
          show(key, testedMessages[key])
        }
      }
    })
  })
})
