/*
 * This file is part of @gecogvidanto/plugin-locale-fr.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { createGecoPlugin } from '@gecogvidanto/plugin'

import FrPlugin from './FrPlugin'
import { messages as fr } from './locale.fr'
import { messages as uiFr } from './ui.locale.fr'

module.exports = createGecoPlugin(FrPlugin, {
  plugin: 'gecogvidanto-plugin-v1',
  description: 'Traductions françaises (French translations)',
  messages: { fr },
  uiMessages: { fr: uiFr },
  models: {},
})
