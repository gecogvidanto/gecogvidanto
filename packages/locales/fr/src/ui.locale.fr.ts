/*
 * This file is part of @gecogvidanto/plugin-locale-fr.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { sharedMessages } from './ui.shared.fr'

/* eslint-disable no-irregular-whitespace */
export const messages: import('@gecogvidanto/client-web').langType &
  import('@gecogvidanto/plugin-basesys/dist/ui.locale.en').langType &
  import('@gecogvidanto/plugin-barter/dist/ui.locale.en').langType &
  import('@gecogvidanto/plugin-mutcredit/dist/ui.locale.en').langType = {
  ...sharedMessages,

  $client$connection: 'Échec de la communication avec le serveur ĞecoĞvidanto',
  $client$credentials: 'Connexion échouée: informations d’identification invalides',
  $client$economicSystem: 'Système économique non trouvé sur le serveur',
  $client$emailCheck: 'Le jeton de courriel ne correspond pas — Veuillez recommencer la procédure',
  $client$gameClose:
    'Impossible de fermer la partie: il n’y a pas assez de manches ou une manche n’est pas terminée',
  $client$gameNextRound: 'La manche en cours est déjà terminée',
  $client$gamePlayers: 'Nombre maximum de joueurs atteint ou plusieurs joueurs avec le même nom',
  $client$gameSets: 'Il y a déjà une manche non terminée dans la partie en cours',
  $client$gameTechBreak: 'Il n’y a pas de manche en cours pour enregistrer une rupture technologique',
  $client$userExists: 'Un autre utilisateur est déjà enregistré avec le même nom ou le même courriel',

  backToSiteHome: 'Retour à l’accueil du site',
  cancel: 'Annuler',
  close: 'Fermer',
  closeMenu: 'Fermer le menu',
  delete: 'Supprimer',
  economicSystem: 'Système économique',
  email: 'Courriel',
  formDying: (name: string) => `${name} meurt à ce tour`,
  gameClosed: 'Partie fermée',
  gameScoreSet: (name: string) => `Score enregistré pour le joueur ${name}`,
  gameTechBreakRecorded: 'Nouvelle rupture technologique enregistrée',
  loading: 'Chargement…',
  logged: (name: string) => `Vous êtes maintenant connecté en tant que ${name}`,
  loggedOut: 'Vous êtes maintenant déconnecté',
  menuAction: 'Action',
  menuActionTechBreak: 'Rupture technologique',
  menuActionTechBreakConfirm: 'Veuillez confirmer la rupture technologique',
  menuPlayerAdd: 'Ajouter un joueur',
  minutes: (value: number) => `${value} minutes`,
  notConnected: '(non connecté)',
  ok: 'OK',
  openMenu: 'Ouvrir le menu',
  pageAbout: 'À propos',
  pageAboutPlugins: 'Extentions installées',
  pageAboutServer: (app: string, name: string | undefined, version: string | undefined) =>
    `Serveur ${app} : ${name || '(inconnu)'}, version ${version || 'inconnue'}`,
  pageAboutVersion: (version: string | undefined) => `Version : ${version || 'inconnue'}`,
  pageAddPlayers: 'Ajouter des joueurs',
  pageAddPlayersId: (id: number) => `Joueur ${id}`,
  pageAddPlayersSameName: 'Un autre joueur a déjà ce nom',
  pageEmailCheck: 'Contrôle de l’adresse électronique',
  pageEmailCheckFailed: 'Échec de la validation de l’adresse électronique',
  pageEmailCheckPleaseWait: 'Validation de votre adresse électronique en cours, veuillez patienter…',
  pageEmailCheckSucceeded:
    'Adresse électronique vérifiée — Vous pouvez maintenant vous connecter avec la nouvelle adresse',
  pageGameAddPlayers: 'Ajouter des joueurs',
  pageGameChartIncludeNpc: 'Inclure les personnages non-joueurs',
  pageGameChartMean: 'Moyenne',
  pageGameChartRelDeviation: 'Écart type/moyenne',
  pageGameChartStdDeviation: 'Écart type',
  pageGameChartTotal: 'Total',
  pageGameClose: 'Fermer la partie',
  pageGameMaster: (name: string) => `Partie animée par ${name}`,
  pageGameMasterThanks: (key: string) => `Remerciez-la/le avec un don en Ğ1 grâce à sa clé publique ${key}`,
  pageGameNoPlayers: 'Il n’y a actuellement aucun joueur dans la partie',
  pageGamePlay: 'Jouer la partie',
  pageGamePlayers: 'Joueurs',
  pageGamePlayersLimit: (start: boolean, roundSet: number, round: number) =>
    `${start ? 'à partir du' : 'jusqu’au'} tour ${round + 1} de la manche ${roundSet + 1}`,
  pageGameSet: (index: number, xchgSysName: string) => `Manche ${index + 1} : ${xchgSysName}`,
  pageGameSetInProgress: 'Manche en cours',
  pageGameSummary: 'Résumé',
  pageGameViewMoney: 'Voir la page d’aide monnaie',
  pageGameViewValues: 'Voir la page d’aide valeurs',
  pageHome: 'Accueil',
  pageIndex: 'Parties',
  pageIndexDate: 'Date',
  pageIndexLocation: 'Lieu',
  pageIndexNoGame: 'Aucune partie en base de données',
  pageIndexPlayers: 'Nb joueurs',
  pageIndexSets: 'Nb manches',
  pageNewGame: 'Nouvelle partie',
  pageNewGameLocationAddress: 'Adresse',
  pageNewGameLocationName: 'Nom du lieu',
  pageNewGameRoundLength: 'Durée d’un tour',
  pageNewGameRoundsPerSet: 'Tours par manche',
  pageNotFound: 'Page non trouvée',
  pageNotFoundLost: 'Il semblerait que vous vous soyez perdu dans les méandres de la création monétaire.',
  pagePlayStatsCardGame: 'Cartes',
  pagePlayStatsFour: 'Carrés',
  pagePlayStatsPlayers: 'Joueurs',
  pageProfile: 'Profil',
  pageProfileAccount: 'Clé publique du compte',
  pageProfileDataSent: 'Données envoyées au serveur',
  pageProfileEmailDuplicate: 'Cette adresse est déjà prise par un autre utilisateur',
  pageProfileLevel: 'Niveau d’accréditation',
  pageProfileName: 'Nom ou pseudonyme',
  pageProfileNameDuplicate: 'Ce nom est déjà pris par un autre utilisateur',
  pageProfilePasswordCheck: 'Tapez le mot de passe à nouveau',
  pageProfilePasswordReset: 'Réinitialiser le mot de passe',
  pageProfileSignUp: (app: string) => `S’inscrire comme maitre du jeu ${app}`,
  pageRound: (round: number, total: number) =>
    round ? `Tour ${round} sur ${total} (${(round * 80) / total} ans)` : 'Tour d’initialisation',
  pageRoundPreview: 'Preparez les feuilles d’aide',
  password: 'Mot de passe',
  pause: 'Pause',
  playerExit: 'Départ d’un joueur',
  playerExitConfirm: (name: string) => `Veuillez confirmer que ${name} quitte la partie.`,
  playerExitSelect: 'Sélectionnez le joueur quittant la partie',
  refresh: 'Rafraichir',
  removeAll: 'Tout supprimer',
  setCreate: 'Créer une manche',
  setNew: 'Nouvelle manche',
  signIn: 'Se connecter',
  signInTitle: 'Connexion',
  signOut: 'Se déconnecter',
  signUp: 'S’inscrire',
  start: 'Démarrer',
  unchanged: '(non modifié)',
  unknownError: 'Erreur inconnue',
  userCheckEmail: 'Veuillez vérifier votre boite électronique pour la confirmation de votre courriel',
  userCreated: (name: string) => `Utilisateur ${name} créé avec succès`,
  userUpdateSuccess: (name: string) => `Utilisateur ${name} mis à jour avec succès`,
  _displayCertLevel: (level: keyof typeof import('@gecogvidanto/shared').CertLevel): string => {
    switch (level) {
      case 'Administrator':
        return 'Administrateur'
      case 'GameMaster':
        return 'Maitre du jeu'
      default:
        // eslint-disable-next-line no-case-declarations
        const l: never = level
        throw new Error(`Unknown level ${l}`)
    }
  },
  _displayDate: (date: Date) => {
    const fmt = (n: number): string => ('0' + n).slice(-2)
    return `${fmt(date.getDate())}/${fmt(date.getMonth() + 1)}/${date.getFullYear()} ${fmt(
      date.getHours()
    )}:${fmt(date.getMinutes())}`
  },

  basesys$: '',
  basesys$bank: 'Banque',
  basesys$bankResult: 'Bilan de la banque',
  basesys$debtMoney: 'Monnaie dette',
  basesys$extend: 'Prêts prolongés',
  basesys$grantedCheck: (count: number) =>
    `Les biens du joueurs doivent déjà valoir au moins ${count} unités`,
  basesys$grantedCheckStatic: 'Les biens du joueurs doivent déjà valoir au moins autant d’unités',
  basesys$grantedField: 'Nouveaux prêts',
  basesys$grantedHelp: (count: number) => `Donner ${count} unités au joueur`,
  basesys$grantedHelpStatic: 'Donner 3 fois le nombre de prêts en unités au joueur',
  basesys$libreMoney: 'Monnaie libre',
  basesys$mMassPP: (mmass: number) => `Masse monétaire: ${mmass} unités par joueur`,
  basesys$nonRefundable: 'Prêts non remboursables (saisie)',
  basesys$outstanding: (player: string, loanCount: number) =>
    `${player} a ${loanCount} prêt(s) en cours — ${loanCount * 4} unités à rembourser`,
  basesys$refund: 'Prêts remboursés',
  basesys$refundHelp: (count: number) => `Prendre ${count} unités au joueur`,
  basesys$refundHelpStatic: 'Prendre au joueur 4 unités par prêt remboursé et 1 par prêt étendu',
  basesys$requestLoan: 'Demander un prêt',
  basesys$seized: 'Montant saisi',
  basesys$sell: 'Vendre des saisies',
  basesys$soldField: 'Montant',
  basesys$soldLabel: 'Indiquer la somme d’argent gagnée par la banque',
  basesys$toSeize: (count: number) => `Saisir environ l’équivalent de ${count} unités`,
  basesys$toSeizeStatic: 'Saisir environ l’équivalent de 1,5 fois le montant dû',

  barter$name: 'Troc',

  mutcredit$name: 'Crédit mutuel',
}
