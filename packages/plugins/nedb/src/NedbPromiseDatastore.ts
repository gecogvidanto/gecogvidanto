/*
 * This file is part of @gecogvidanto/plugin-nedb.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import * as Nedb from 'nedb'

import { Identifier, Projection } from '@gecogvidanto/plugin'

/**
 * Convert Nedb datastore calls into promises.
 */
export default class NedbPromiseDatastore {
  public constructor(public readonly db: Nedb) {}

  public loadDatabase(): Promise<void> {
    return new Promise((resolve, reject) => this.db.loadDatabase(this.voidCallback(resolve, reject)))
  }

  public ensureIndex(options: Nedb.EnsureIndexOptions): Promise<void> {
    return new Promise((resolve, reject) => {
      this.db.ensureIndex(options, this.voidCallback(resolve, reject))
    })
  }

  public insert<T>(newDoc: T): Promise<Identifier & T> {
    return new Promise((resolve, reject) => {
      this.db.insert(newDoc as Identifier & T, this.callback(resolve, reject))
    })
  }

  public count(query: unknown): Promise<number> {
    return new Promise<number>((resolve, reject) => {
      this.db.count(query, this.callback(resolve, reject))
    })
  }

  public find<T>(query: unknown, projection?: Projection<Identifier & T>): Promise<Array<Identifier & T>> {
    return new Promise((resolve, reject) => {
      this.db.find(query, projection as Identifier & T, this.callback(resolve, reject))
    })
  }

  public findOne<T>(
    query: unknown,
    projection?: Projection<Identifier & T>
  ): Promise<(Identifier & T) | undefined> {
    return new Promise((resolve, reject) => {
      this.db.findOne(query, projection as Identifier & T, this.callback(resolve, reject))
    })
  }

  public update(query: unknown, updateQuery: unknown, options: Nedb.UpdateOptions = {}): Promise<number> {
    return new Promise((resolve, reject) => {
      this.db.update(query, updateQuery, options, this.callback(resolve, reject))
    })
  }

  public remove(query: unknown, options: Nedb.RemoveOptions = {}): Promise<number> {
    return new Promise((resolve, reject) => {
      this.db.remove(query, options, this.callback(resolve, reject))
    })
  }

  private voidCallback(
    resolve: () => void,
    reject: (err: Error | null) => void
  ): (err: Error | null) => void {
    return err => (err ? reject(err) : resolve())
  }

  private callback<T>(
    resolve: (value: T) => void,
    reject: (err: Error | null) => void
  ): (err: Error | null, value: T) => void {
    return (err, value) => (err ? reject(err) : resolve(value))
  }
}
