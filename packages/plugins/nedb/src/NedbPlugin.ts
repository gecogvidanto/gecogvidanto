/*
 * This file is part of @gecogvidanto/plugin-nedb.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import Intl from 'intl-ts'

import { Database, DatabasePlugin, ModelList, ModelTypeList, ServerApp } from '@gecogvidanto/plugin'

import { langType } from './locale.en'
import { FullConfig, isFullConfig } from './NedbConfig'
import NedbDatabase from './NedbDatabase'

/**
 * Nedb plugin manage an nedb database for ĞecoĞvidanto.
 */
export default class NedbPlugin implements DatabasePlugin {
  private readonly nedbConfig: FullConfig['plugins']['nedb']
  private readonly lang: Intl<langType>

  public constructor(serverApp: ServerApp) {
    if (isFullConfig(serverApp.config)) {
      this.nedbConfig = serverApp.config.plugins.nedb
    } else {
      throw new Error('Configuration does not have expected values')
    }
    this.lang = serverApp.serverLang as any
  }

  public ready(): Promise<void> {
    return Promise.resolve()
  }

  public openDatabase<T extends ModelList>(types: ModelTypeList<T>): Database<T> {
    return new NedbDatabase(types, this.nedbConfig, this.lang)
  }
}
