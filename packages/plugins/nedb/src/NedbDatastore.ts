/*
 * This file is part of @gecogvidanto/plugin-nedb.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import * as Nedb from 'nedb'

import { Collection, Identifier, Projection, Query, Update } from '@gecogvidanto/plugin'

import { BinaryDataManager, BinaryFilter, BinaryStore } from './bindata'
import NedbPromiseDatastore from './NedbPromiseDatastore'

/**
 * A nedb collection (datastore in nedb vocabulary).
 */
export default class NedbDatastore<T> implements Collection<T> {
  private nextCompact: number

  public constructor(
    private readonly db: NedbPromiseDatastore,
    private readonly name: string,
    private readonly compactInterval: number,
    private readonly binaryStore: BinaryStore
  ) {
    this.nextCompact = compactInterval
  }

  private static checkCompact<I extends any[], O>(
    _target: any,
    _propertyKey: string,
    descriptor: TypedPropertyDescriptor<(...args: I) => Promise<O>>
  ): TypedPropertyDescriptor<(...args: I) => Promise<O>> {
    const originalMethod = descriptor.value!
    descriptor.value = async function (this: NedbDatastore<unknown>, ...args: I): Promise<O> {
      const result: O = await originalMethod.apply(this, args)
      if (--this.nextCompact <= 0) {
        setImmediate(() => {
          this.db.db.persistence.compactDatafile()
        })
        this.nextCompact = this.compactInterval
      }
      return result
    }
    return descriptor
  }

  @NedbDatastore.checkCompact
  public async insert(newDoc: T): Promise<Identifier & T> {
    const binManager = new BinaryDataManager(this.binaryStore, this.name)
    const filtered = await binManager.inputFilter(newDoc)
    const result = (await this.db.insert(filtered)) as BinaryFilter<Identifier & T>
    if (binManager.transformed) {
      await binManager.save(result._id as string)
    }
    return binManager.outputFilter(result)
  }

  public async count(query: Query<Identifier & T>): Promise<number> {
    const binManager = new BinaryDataManager(this.binaryStore, this.name)
    const filtered = await binManager.inputFilter(query)
    return this.db.count(filtered)
  }

  public async find(
    query: Query<Identifier & T>,
    projection?: Projection<Identifier & T>
  ): Promise<Array<Identifier & T>> {
    const binManager = new BinaryDataManager(this.binaryStore, this.name)
    const filtered = await binManager.inputFilter(query)
    const result = await this.db.find<BinaryFilter<Identifier & T>>(filtered, projection)
    return binManager.outputFilterAll(result as Array<BinaryFilter<Identifier & T>>)
  }

  public async findOne(
    query: Query<Identifier & T>,
    projection?: Projection<Identifier & T>
  ): Promise<(Identifier & T) | undefined> {
    const binManager = new BinaryDataManager(this.binaryStore, this.name)
    const filtered = await binManager.inputFilter(query)
    const result = await this.db.findOne<BinaryFilter<Identifier & T>>(filtered, projection)
    return result ? binManager.outputFilter(result as BinaryFilter<Identifier & T>) : undefined
  }

  @NedbDatastore.checkCompact
  public async update(
    query: Query<Identifier & T>,
    updateQuery: Update<T>,
    upsert = false
  ): Promise<number> {
    const options: Nedb.UpdateOptions = { upsert, multi: !upsert }
    const binManager = new BinaryDataManager(this.binaryStore, this.name)
    const filteredUpdate = await binManager.inputFilter(updateQuery)
    if (binManager.transformed) {
      const modifs = await this.find(query, { _id: 1 } as any)
      await Promise.all(modifs.map(async modif => binManager.save(modif._id)))
    }
    const filteredQuery = await binManager.inputFilter(query)
    return this.db.update(filteredQuery, filteredUpdate, options)
  }

  @NedbDatastore.checkCompact
  public async remove(query: Query<Identifier & T>): Promise<number> {
    const binManager = new BinaryDataManager(this.binaryStore, this.name)
    const filtered = await binManager.inputFilter(query)
    return this.db.remove(filtered, { multi: true })
  }
}
