/*
 * This file is part of @gecogvidanto/plugin-nedb.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { mkdir } from 'fs'
import Intl from 'intl-ts'
import * as Nedb from 'nedb'
import { homedir } from 'os'
import { resolve as resolvePath } from 'path'
import { promisify } from 'util'
import * as winston from 'winston'

import { Database, Model, ModelList, ModelTypeList } from '@gecogvidanto/plugin'

import { BinaryStore } from './bindata'
import { langType } from './locale.en'
import { FullConfig } from './NedbConfig'
import NedbDatastore from './NedbDatastore'
import NedbPromiseDatastore from './NedbPromiseDatastore'
import { exists } from './tools'

const DEFAULT_DIRECTORY = resolvePath(homedir(), '.config/gecogvidanto/')
const INTERNAL_SUBDIRECTORY = '_internal'
const BINARY_STORE = 'binary'

type DataStores<T> = { [K in keyof T]: NedbPromiseDatastore }

/**
 * The nedb database.
 */
export default class NedbDatabase<T extends ModelList> implements Database<T> {
  public readonly models: T
  public readonly dbs: DataStores<T>
  private readonly binaryStore: BinaryStore
  private readonly started: Promise<void>

  public constructor(
    desc: ModelTypeList<T>,
    nedbConfig: FullConfig['plugins']['nedb'],
    lang: Intl<langType>
  ) {
    if (nedbConfig.directory.trim().length === 0) {
      winston.info(lang.nedb$defaultDirectory(DEFAULT_DIRECTORY))
    }
    const directory = nedbConfig.directory.trim().length === 0 ? DEFAULT_DIRECTORY : nedbConfig.directory

    // Prepare binary store
    const internalPath = resolvePath(directory, INTERNAL_SUBDIRECTORY)
    const binaryDb = new NedbPromiseDatastore(new Nedb(resolvePath(internalPath, BINARY_STORE + '.nedb')))
    this.binaryStore = new BinaryStore(this, binaryDb, internalPath, nedbConfig.compactInterval)

    // Prepare models
    const initModels: Partial<T> = {}
    const initDb: Partial<{ [key: string]: NedbPromiseDatastore }> = {}
    for (const name in desc) {
      const db = new NedbPromiseDatastore(new Nedb(resolvePath(directory, name + '.nedb')))
      initModels[name] = new desc[name](
        new NedbDatastore(db, name, nedbConfig.compactInterval, this.binaryStore)
      ) as any
      initDb[name] = db
    }
    this.models = initModels as T
    this.dbs = initDb as DataStores<T>

    // Initialize database
    this.started = new Promise((resolve, reject) => {
      exists(directory)
        .then(dirExists => {
          return dirExists ? Promise.resolve() : promisify(mkdir)(directory)
        })
        .then(() => exists(internalPath))
        .then(dirExists => {
          return dirExists ? Promise.resolve() : promisify(mkdir)(internalPath)
        })
        .then(() => binaryDb.loadDatabase())
        .then(() =>
          Promise.all(Object.keys(desc).map(name => this.startDatastore(this.dbs[name], this.models[name])))
        )
        .then(() => this.binaryStore.start())
        .then(() => {
          winston.verbose(lang.nedb$databaseLoaded())
          resolve()
        })
        .catch(err => {
          reject(err)
        })
    })
  }

  public waitForStarted(): Promise<void> {
    return this.started
  }

  private async startDatastore(db: NedbPromiseDatastore, model: Model): Promise<void> {
    await db.loadDatabase()
    await Promise.all(model.indices.map(index => db.ensureIndex(index)))
    await model.init()
  }
}
