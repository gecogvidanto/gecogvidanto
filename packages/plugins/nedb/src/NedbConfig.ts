/*
 * This file is part of @gecogvidanto/plugin-nedb.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { literal, numberItem, stringItem } from 'confinode'

/**
 * The configuration, with data directory and interval for datastore compaction.
 */
export default interface NedbConfig {
  directory: string
  compactInterval: number
}

/**
 * Description of the configuration.
 */
export const configDescription = literal<NedbConfig>({
  directory: stringItem(''),
  compactInterval: numberItem(1000),
})

/**
 * The nedb plugin configuration in the full server configuration.
 */
export interface FullConfig {
  plugins: {
    nedb: NedbConfig
  }
}

/**
 * Test (type guard) if a given object contains nedb configuration.
 *
 * @param config - The supposed configuration.
 * @returns True if object contains configuration.
 */
export function isFullConfig(config: unknown): config is FullConfig {
  return typeof config === 'object' && 'plugins' in (config as any) && 'nedb' in (config as any).plugins
}
