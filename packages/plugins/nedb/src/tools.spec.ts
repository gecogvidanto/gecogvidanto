/*
 * This file is part of @gecogvidanto/plugin-nedb.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import { resolve } from 'path'

import { exists } from './tools'

describe('Tools', () => {
  describe('#exists', () => {
    it('must return true if path exists', async () => {
      const res = await exists(resolve('.'))
      expect(res).to.be.true
    })

    it('must return true if path exists', async () => {
      const res = await exists(resolve('/i suppose this path does not/exist/'))
      expect(res).to.be.false
    })

    it('must reject promise if error', async () => {
      await expect(exists('/dev/null/un:creatable')).to.be.rejected
    })
  })
})
