/*
 * This file is part of @gecogvidanto/plugin-nedb.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import * as Nedb from 'nedb'

// eslint-disable-next-line import/no-internal-modules
import { BUFFER1 } from './bindata/BinaryStore.spec'
import NedbDatastore from './NedbDatastore'
import NedbPromiseDatastore from './NedbPromiseDatastore'

interface DocumentType {
  [key: string]: any
}

interface PartialResult {
  docIndices: number[]
  query: any
}

class FakeBinaryStore {
  public saved: { [hash: string]: Buffer } = {}

  public save(_storeName: string, _recordId: string, hash: string, data: Buffer): Promise<void> {
    this.saved[hash] = data
    return new Promise(resolve => {
      setImmediate(() => resolve())
    })
  }

  public read(hash: string): Promise<Buffer> {
    return new Promise(resolve => {
      setImmediate(() => resolve(this.saved[hash]))
    })
  }
}

const nedb = new NedbPromiseDatastore(new Nedb())
const db = new NedbDatastore<DocumentType>(nedb, 'fake', 1000, new FakeBinaryStore() as any)

// All documents
const insertedDocuments: DocumentType[] = [
  { name: 'first value', today: new Date(), value: 2 },
  { name: 'other value', today: new Date(2018, 1, 1), value: 12 },
  { name: 'one more value', value: 2, amo: 'mi amas vin' },
  { name: 'last value', value: 123, buffer: BUFFER1 },
]

// Queries
const noSelectQuery: any = { value: 9876 }
const partials: PartialResult[] = [
  {
    docIndices: [1],
    query: {
      value: insertedDocuments[1].value,
    },
  },
  {
    docIndices: [0, 2],
    query: {
      value: insertedDocuments[0].value,
    },
  },
  {
    docIndices: [],
    query: noSelectQuery,
  },
]

// Other useful constants
const projection: any = { name: 1, _id: 0 }
const updatedValue: any = { name: 'Updated!' }

// Insertion results
let insertedValues: any[]

describe('NedbDatastore', () => {
  before('Create and load database', async () => {
    expect(db).to.exist
    await expect(nedb.loadDatabase()).to.be.fulfilled
  })

  beforeEach('Insert values in database', async () => {
    insertedValues = await Promise.all(insertedDocuments.map(document => db.insert(document)))
  })

  afterEach('Clean up database', async () => {
    db.remove({})
  })

  describe('#loadDatabase (failing)', () => {
    let loadDatabaseMethod: typeof nedb.db.loadDatabase

    beforeEach('Replace method', () => {
      loadDatabaseMethod = nedb.db.loadDatabase
      nedb.db.loadDatabase = (cb?: (err: Error) => void): void => {
        cb && cb(new Error())
      }
    })

    it('must reject promise if failing', async () => {
      await expect(nedb.loadDatabase()).to.be.rejected
    })

    afterEach('Reset method', () => {
      nedb.db.loadDatabase = loadDatabaseMethod
    })
  })

  describe('#insert', () => {
    it('must insert the right count of documents', () => {
      expect(insertedValues).to.exist.and.to.have.lengthOf(insertedDocuments.length)
    })

    it('must give new identifier to inserted documents', () => {
      insertedValues.forEach(value => {
        expect(value).to.exist.and.to.have.property('_id')
        expect(value._id).to.exist
      })
    })

    it('must return inserted documents', () => {
      insertedValues.forEach((value, index) => {
        expect(value, 'Value does not have document keys').to.exist.and.to.include.all.keys(
          Object.keys(insertedDocuments[index])
        )
        for (const keyName in insertedDocuments[index]) {
          expect(value[keyName], `Value[${keyName}] does not equal document[${keyName}]`).to.equal(
            insertedDocuments[index][keyName]
          )
        }
      })
    })
  })

  describe('#count', () => {
    it('must give the count of all documents', async () => {
      const count = await db.count({})
      expect(count).to.equal(insertedDocuments.length)
    })

    it('must give the count of matching documents', async () => {
      const counts = await Promise.all(partials.map(partial => db.count(partial.query)))
      counts.forEach((count, index) => {
        expect(count, `Unexpected count in document[${index}]`).to.equal(partials[index].docIndices.length)
      })
    })
  })

  describe('#count (failing)', () => {
    let countMethod: typeof nedb.db.count

    beforeEach('Replace method', () => {
      countMethod = nedb.db.count
      nedb.db.count = ((_query: any, cb: (err: Error, n?: number) => void): void => {
        cb(new Error())
      }) as typeof nedb.db.count
    })

    it('must reject promise if failing', async () => {
      await expect(db.count({})).to.be.rejected
    })

    afterEach('Reset method', () => {
      nedb.db.count = countMethod
    })
  })

  describe('#find', () => {
    it('must return all documents', async () => {
      const documents = await db.find({})
      expect(documents).to.exist.and.to.have.lengthOf(insertedDocuments.length)
    })

    it('must return only matching documents', async () => {
      const documentsResult = await Promise.all(partials.map(partial => db.find(partial.query)))
      documentsResult.forEach((documents, index) => {
        expect(documents, `Result[${index}] does not have expected length`).to.exist.and.to.have.lengthOf(
          partials[index].docIndices.length
        )
        if (documents.length === 1) {
          expect(documents[0], `Result[${index}] does not have expected keys`).to.include.all.keys(
            Object.keys(insertedDocuments[partials[index].docIndices[0]])
          )
          for (const key in insertedDocuments[partials[index].docIndices[0]]) {
            expect(documents[0][key], `Key ${key} for result[${index}] does not match`).to.equal(
              insertedDocuments[partials[index].docIndices[0]][key]
            )
          }
        }
      })
    })

    it('must return only projection fields', async () => {
      const documents = await db.find({}, projection)
      expect(documents).to.exist
      documents.forEach((document, index) => {
        expect(document, `Result[${index}] does not have expected keys`).to.have.all.keys(
          Object.keys(projection).filter(key => projection[key] !== 0)
        )
      })
    })
  })

  describe('#findOne', () => {
    it('must return only first found document', async () => {
      const document = await db.findOne({})
      expect(document).to.exist.but.to.not.be.an('array')
    })

    it('must return only first matching document', async () => {
      const documentResult = await Promise.all(
        partials
          .filter(partial => partial.query !== noSelectQuery)
          .map(partial => db.findOne(partial.query))
      )
      documentResult.forEach((document, index) => {
        expect(document, `Result[${index}] is not has expected`).to.exist.but.to.not.be.an('array')
        if (partials[index].docIndices.length === 1) {
          expect(document, `Result[${index}] does not have expected keys`).to.include.all.keys(
            Object.keys(insertedDocuments[partials[index].docIndices[0]])
          )
          for (const key in insertedDocuments[partials[index].docIndices[0]]) {
            expect(document![key], `Result[${index}][${key}] does not equal document`).to.equal(
              insertedDocuments[partials[index].docIndices[0]][key]
            )
          }
        }
      })
    })

    it('must return undefined if no matching document', async () => {
      const document = await db.findOne(noSelectQuery)
      expect(document).to.be.undefined
    })

    it('must return only projection fields', async () => {
      const document = await db.findOne({}, projection)
      expect(document).to.exist.and.to.have.all.keys(
        Object.keys(projection).filter(key => projection[key] !== 0)
      )
    })
  })

  describe('#update', () => {
    it('must update all documents', async () => {
      const count = await db.update({}, { $set: updatedValue })
      expect(count, 'Unexpected updated count').to.equal(insertedDocuments.length)
      const documents = await db.find({})
      expect(documents).to.exist
      documents.forEach((document, index) => {
        for (const key in updatedValue) {
          expect(document[key], `Result[${index}][${key}] is not equal to updated value`).to.equal(
            updatedValue[key]
          )
        }
      })
    })

    it('must update all documents with binary value', async () => {
      const count = await db.update({}, { $set: { data: BUFFER1 } })
      expect(count, 'Unexpected updated count').to.equal(insertedDocuments.length)
      const documents = await db.find({})
      expect(documents).to.exist
      documents.forEach((document, index) => {
        expect(document.data, `Result[${index}].data is not equal to BUFFER`).to.deep.equal(BUFFER1)
      })
    })

    partials.forEach((partial, index) => {
      it(`must update only selected documents — #${index}`, async () => {
        const count = await db.update(partial.query, { $set: updatedValue })
        expect(count, 'Unexpected updated count').to.equal(partials[index].docIndices.length)
        const documents = await db.find({})
        expect(documents).to.exist
        documents.forEach((document, ind) => {
          let matching = true
          for (const key in partial.query) {
            matching = matching && key in document && partial.query[key] === document[key]
          }
          for (const key in updatedValue) {
            if (matching) {
              expect(document[key], `Result[${ind}][${key}] is not equal to expected value`).to.equal(
                updatedValue[key]
              )
            } else {
              expect(document[key], `Result[${ind}][${key}] is equal to unexpected value`).to.not.equal(
                updatedValue[key]
              )
            }
          }
        })
      })
    })

    partials.forEach((partial, index) => {
      it(`must replace document values — #${index}`, async () => {
        const count = await db.update(partial.query, updatedValue)
        expect(count, 'Unexpected updated count').to.equal(partials[index].docIndices.length)
        const documents = await db.find({}, { _id: 0 })
        expect(documents).to.exist
        documents.forEach((document, ind) => {
          let matching = true
          for (const key in partial.query) {
            matching = matching && !(key in document && partial.query[key] !== document[key])
          }
          for (const key in updatedValue) {
            if (matching) {
              expect(document[key], `Result[${ind}][${key}] is not equal to expected value`).to.equal(
                updatedValue[key]
              )
              expect(document, `Result[${ind}] does not have expected keys`).to.have.all.keys(
                Object.keys(updatedValue)
              )
            } else {
              expect(document[key], `Result[${ind}][${key}] is equal to unexpected value`).to.not.equal(
                updatedValue[key]
              )
            }
          }
        })
      })
    })

    partials
      .filter(partial => partial.query !== noSelectQuery)
      .forEach((partial, index) => {
        it(`must update a single document for upsert — #${index}`, async () => {
          let count = await db.update(partial.query, { $set: updatedValue }, true)
          expect(count, 'Unexpected updated count').to.equal(1)
          count = await db.count({})
          expect(count).to.equal(insertedDocuments.length)
        })
      })

    it('must insert a new document', async () => {
      let count = await db.update(noSelectQuery, { $set: updatedValue }, true)
      expect(count, 'Unexpected updated count').to.equal(1)
      count = await db.count({})
      expect(count).to.equal(insertedDocuments.length + 1)
    })
  })

  describe('#remove', () => {
    it('must remove all documents', async () => {
      let count = await db.remove({})
      expect(count, 'Unexpected removed count').to.equal(insertedDocuments.length)
      count = await db.count({})
      expect(count).to.equal(0)
    })

    partials.forEach((partial, index) => {
      it(`must remove only selected documents — #${index}`, async () => {
        let count = await db.remove(partial.query)
        expect(count, 'Unexpected removed count').to.equal(partials[index].docIndices.length)
        count = await db.count({})
        expect(count).to.equal(insertedDocuments.length - partials[index].docIndices.length)
      })
    })
  })
})
