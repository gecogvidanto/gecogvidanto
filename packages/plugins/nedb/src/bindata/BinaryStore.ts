/*
 * This file is part of @gecogvidanto/plugin-nedb.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { readFile, readdir, unlink, writeFile } from 'fs'
import { resolve as resolvePath } from 'path'
import { promisify } from 'util'

import { Database, ModelList } from '@gecogvidanto/plugin'

import NedbPromiseDatastore from '../NedbPromiseDatastore'
import { exists } from '../tools'

interface NedbDatabase<T extends ModelList> extends Database<T> {
  readonly models: T
  readonly dbs: { [K in keyof T]: NedbPromiseDatastore }
}

export interface BinaryData {
  type: '$$BinaryData$$'
  hash: string
}

export function isBinaryData(data: unknown): data is BinaryData {
  return (
    !!data &&
    typeof data === 'object' &&
    'type' in (data as any) &&
    'hash' in (data as any) &&
    (data as any).type === '$$BinaryData$$'
  )
}

export interface BinaryStoreDoc {
  _id?: string
  hash: string
  usages: Array<{ storeName: string; recordId: string }>
}

/**
 * A datastore managing binary data.
 */
export default class BinaryStore {
  private nextCompact: number

  /**
   * Create a new binary store.
   *
   * @param allDb - The full database system.
   * @param db - The database (actually, the binary datastore).
   * @param binaryDirectory - The directory where binary items will be saved.
   * @param compactInterval - Interval between datastore cleaning.
   */
  public constructor(
    private readonly allDb: NedbDatabase<ModelList>,
    private readonly db: NedbPromiseDatastore,
    private readonly binaryDirectory: string,
    private readonly compactInterval: number
  ) {
    // eslint-disable-next-line id-blacklist
    this.nextCompact = Number.MAX_SAFE_INTEGER
  }

  /**
   * Ensure store is ready.
   */
  public async start(): Promise<void> {
    await this.db.ensureIndex({
      fieldName: 'hash',
      unique: true,
    })
    await this.compact()
  }

  /**
   * Save a binary item.
   *
   * @param storeName - The name of the store having binary data to store.
   * @param recordId - The identifier of the record containing binary data.
   * @param hash - The hash of the data to store.
   * @param data - The data to store.
   */
  public async save(storeName: string, recordId: string, hash: string, data: Buffer): Promise<void> {
    if (!(await exists(this.pathFor(hash)))) {
      await promisify(writeFile)(this.pathFor(hash), data)
    }
    await this.db.update({ hash }, { $addToSet: { usages: { storeName, recordId } } }, { upsert: true })
    if (--this.nextCompact <= 0) {
      await this.compact()
    }
  }

  /**
   * Read the binary data for a hash.
   *
   * @param hash - The hash for which to read data.
   * @returns The read binary data.
   */
  public async read(hash: string): Promise<Buffer> {
    const data = await this.db.findOne({ hash })
    if (data !== null && (await exists(this.pathFor(hash)))) {
      return promisify(readFile)(this.pathFor(hash))
    } else {
      throw new Error('Corrupted database — Binary file not found')
    }
  }

  /**
   * Clean the database.
   */
  private async compact(): Promise<void> {
    await this.checkUsage()
    await this.checkConsistency()
    this.db.db.persistence.compactDatafile()
    this.nextCompact = this.compactInterval
  }

  /**
   * Remove unused binaries.
   */
  private async checkUsage(): Promise<void> {
    const allBinaries: BinaryStoreDoc[] = await this.db.find<BinaryStoreDoc>({})
    await Promise.all(
      allBinaries.map(async binary => {
        const toRemove: BinaryStoreDoc['usages'] = []
        await Promise.all(
          binary.usages.map(async usage => {
            let found = false
            if (usage.storeName in this.allDb.dbs) {
              const store = this.allDb.dbs[usage.storeName]
              const record = await store.findOne<{ [key: string]: any }>({
                _id: usage.recordId,
              })
              if (record) {
                for (const key in record) {
                  if (isBinaryData(record[key])) {
                    found = found || record[key].hash === binary.hash
                  }
                }
              }
            }
            if (!found) {
              toRemove.push(usage)
            }
          })
        )
        if (toRemove.length > 0) {
          binary.usages = binary.usages.filter(usage => !toRemove.includes(usage))
          if (binary.usages.length > 0) {
            await this.db.update({ _id: binary._id }, { $set: { usages: binary.usages } })
          } else {
            await this.db.remove({ _id: binary._id })
            if (await exists(this.pathFor(binary.hash))) {
              await promisify(unlink)(this.pathFor(binary.hash))
            }
          }
        }
      })
    )
  }

  /**
   * Check consistency between existing files and stored hashes.
   */
  private async checkConsistency(): Promise<void> {
    const files = await promisify(readdir)(this.binaryDirectory)
    await Promise.all(
      files
        .filter(file => !file.endsWith('.nedb'))
        .map(async file => {
          const count = await this.db.count({ hash: file })
          if (count === 0) {
            await promisify(unlink)(resolvePath(this.binaryDirectory, file))
          }
        })
    )
  }

  /**
   * Get the path of the file containing data for the binary hash.
   *
   * @param hash - The binary hash.
   * @returns The path of the file.
   */
  private pathFor(hash: string): string {
    return resolvePath(this.binaryDirectory, hash)
  }
}
