/*
 * This file is part of @gecogvidanto/plugin-nedb.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import BinaryDataManager from './BinaryDataManager'
import { isBinaryData } from './BinaryStore'
import { BUFFER1, BUFFER2 } from './BinaryStore.spec'

const QUERY = {
  $and: [{ $set: { value1: BUFFER1, simpleData: 'value' } }, { value2: BUFFER2 }],
}
const INPUT = {
  value1: BUFFER1,
  simpleData: 'value',
  arrayData: [1, 2, 3],
}

class FakeBinaryStore {
  public saved: { [hash: string]: Buffer } = {}
  public readCalled = 0

  public save(_storeName: string, _recordId: string, hash: string, data: Buffer): Promise<void> {
    this.saved[hash] = data
    return new Promise(resolve => {
      setImmediate(() => resolve())
    })
  }

  public read(hash: string): Promise<Buffer> {
    this.readCalled++
    return new Promise(resolve => {
      setImmediate(() => resolve(this.saved[hash]))
    })
  }
}

describe('BinaryDataManager', () => {
  let binaryStore: FakeBinaryStore
  let binMan: BinaryDataManager

  beforeEach('Create binary data manager', () => {
    binaryStore = new FakeBinaryStore()
    binMan = new BinaryDataManager(binaryStore as any, 'store')
  })

  describe('#inputFilter', () => {
    it('must extract buffers from query', async () => {
      const result = await binMan.inputFilter(QUERY)
      expect(isBinaryData(result.$and[0].$set!.value1)).to.be.true
      expect(result.$and[0].$set!.simpleData).to.equal('value')
      expect(isBinaryData(result.$and[1].value2)).to.be.true
    })
  })

  describe('#save', () => {
    it('must save the seen data', async () => {
      expect(binMan.transformed, 'There should be no value to save').to.be.false
      const result = await binMan.inputFilter(QUERY)
      expect(binMan.transformed, 'There should be value to save').to.be.true
      await binMan.save('id')
      const compare: any = {}
      compare[result.$and[0].$set!.value1.hash] = QUERY.$and[0].$set!.value1
      compare[result.$and[1].value2!.hash] = QUERY.$and[1].value2
      expect(binaryStore.saved).to.be.deep.equal(compare)
    })
  })

  describe('#outputFilter', () => {
    it('must restore buffer from binary data without reading database if in cache', async () => {
      const filtered = await binMan.inputFilter(INPUT)
      await binMan.save('id')
      expect(binaryStore.readCalled, 'Read has been called in binary store before starting').to.equal(0)
      const result = await binMan.outputFilter(filtered)
      expect(binaryStore.readCalled, 'Database should not have been read').to.equal(0)
      expect(result).to.deep.equal(INPUT)
    })

    it('must restore buffer from binary data, reading database if needed', async () => {
      const filtered = await binMan.inputFilter(INPUT)
      await binMan.save('id')
      expect(binaryStore.readCalled, 'Read has been called in binary store before starting').to.equal(0)
      binMan = new BinaryDataManager(binaryStore as any, 'store')
      const result = await binMan.outputFilter(filtered)
      expect(binaryStore.readCalled, 'Database should have been read').to.equal(1)
      expect(result).to.deep.equal(INPUT)
    })
  })

  describe('#outputFilterAll', () => {
    it('must restore buffer from binary data', async () => {
      const filtered = [await binMan.inputFilter(INPUT), await binMan.inputFilter(INPUT)]
      await binMan.save('id')
      const result = await binMan.outputFilterAll(filtered)
      expect(result).to.deep.equal([INPUT, INPUT])
    })
  })
})
