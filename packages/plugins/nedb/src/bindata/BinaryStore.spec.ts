/*
 * This file is part of @gecogvidanto/plugin-nedb.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import { access, readFile, readdir, unlink } from 'fs'
import * as Nedb from 'nedb'
import { resolve } from 'path'
import * as tmp from 'tmp'
import { promisify } from 'util'

import { Model } from '@gecogvidanto/plugin'

import NedbDatabase from '../NedbDatabase'
import NedbPromiseDatastore from '../NedbPromiseDatastore'
import BinaryStore, { BinaryData, BinaryStoreDoc } from './BinaryStore'

const COMPACT_INTERVAL = 4
const BUFFER_DATA1 = [0x52, 0x6f, 0x78, 0x61, 0x6e, 0x65, 0x20, 0x42, 0x72, 0x65, 0x74]
export const BUFFER1 = Buffer.from(BUFFER_DATA1)
const HASH1 = '42'
const BUFFER_DATA2 = [0x4a, 0x65, 0x73, 0x73, 0x69, 0x63, 0x61, 0x20, 0x41, 0x6c, 0x62, 0x61]
export const BUFFER2 = Buffer.from(BUFFER_DATA2)
const HASH2 = 'deadbeef'
const VALUE1: BinaryData = {
  type: '$$BinaryData$$',
  hash: HASH1,
}
const VALUE2: BinaryData = {
  type: '$$BinaryData$$',
  hash: HASH2,
}
let foundValues: any

const fakeList = {
  fake: (undefined as any) as Model,
}

class FakeNedb {
  public findOne(
    _query: any,
    _projection: any,
    callback: (err: Error | undefined, document: any) => void
  ): void {
    setImmediate(() => {
      callback(undefined, foundValues)
    })
  }
}
class FakeNedbDatabase {
  public models: any
  public dbs: { [x: string]: NedbPromiseDatastore } = {
    fake: new NedbPromiseDatastore(new FakeNedb() as Nedb),
  }
}

describe('BinaryStore', () => {
  let nedbDatabase: NedbDatabase<typeof fakeList>
  let realDb: NedbPromiseDatastore
  let binaryDir: string
  let binaryStore: BinaryStore

  beforeEach('Create binary store', async () => {
    nedbDatabase = new FakeNedbDatabase() as NedbDatabase<typeof fakeList>
    realDb = new NedbPromiseDatastore(new Nedb())
    binaryDir = await promisify<tmp.DirOptions, string>(tmp.dir)({
      unsafeCleanup: true,
    })
    await expect(promisify(access)(binaryDir), 'Binary file not created').to.be.fulfilled
    binaryStore = new BinaryStore(nedbDatabase, realDb, binaryDir, COMPACT_INTERVAL)
    foundValues = { value1: VALUE1, value2: VALUE2, simpleData: 'hello' }
  })

  describe('#start', () => {
    it('must reset next compact interval to constructor value', async () => {
      expect((binaryStore as any).nextCompact, 'Unexpected initial nextCompact value').to.be.gt(255)
      await binaryStore.start()
      expect((binaryStore as any).nextCompact, 'Unexpected final nextCompact value').to.equal(
        COMPACT_INTERVAL
      )
    })
  })

  describe('#save', () => {
    it('must create matching file', async () => {
      await binaryStore.save('fake', 'myPrivateId', HASH1, BUFFER1)
      const fileName = resolve(binaryDir, HASH1)
      await expect(promisify(access)(fileName), `Cannot read ${fileName}`).to.be.fulfilled
      return expect(promisify(readFile)(fileName), 'Unexpected file content').to.become(BUFFER1)
    })

    it('must keep track of records using binaries', async () => {
      const usages: Array<{ storeName: string; recordId: string }> = [
        { storeName: 'store1', recordId: 'myFirstId' },
        { storeName: 'store2', recordId: 'myOtherId' },
      ]
      await Promise.all(
        usages.map(usage => binaryStore.save(usage.storeName, usage.recordId, HASH1, BUFFER1))
      )
      await expect(realDb.count({}), 'Unexpected record count').to.eventually.equal(1)
      const saved = await realDb.findOne<BinaryStoreDoc>({})
      expect(saved).to.exist
      expect(saved!.usages, 'Unexpected usages')
        .to.have.lengthOf(usages.length)
        .and.to.have.deep.members(usages)
    })

    it('must not create new usage if same values', async () => {
      await binaryStore.save('fake', 'myPrivateId', HASH1, BUFFER1)
      await binaryStore.save('fake', 'myPrivateId', HASH1, BUFFER1)
      await expect(realDb.count({}), 'Unexpected record count').to.eventually.equal(1)
      const saved = await realDb.findOne<BinaryStoreDoc>({})
      expect(saved).to.exist
      expect(saved!.usages, 'Unexpected usage count').to.have.lengthOf(1)
    })
  })

  describe('#read', () => {
    it('must retrieve saved data', async () => {
      await binaryStore.save('fake', 'myPrivateId', HASH1, BUFFER1)
      await expect(binaryStore.read(HASH1)).to.become(BUFFER1)
    })

    it('must throw exception if file is not found', async () => {
      await binaryStore.save('fake', 'myPrivateId', HASH1, BUFFER1)
      await promisify(unlink)(resolve(binaryDir, HASH1))
      await expect(binaryStore.read(HASH1)).to.be.rejectedWith(/corrupted database/i)
    })

    it('must throw exception if no entry in database', async () => {
      await expect(binaryStore.read(HASH1)).to.be.rejectedWith(/corrupted database/i)
    })
  })

  describe('#compact', function () {
    this.slow(150)

    beforeEach('Start the binary store', async () => {
      realDb = new NedbPromiseDatastore(new Nedb(resolve(binaryDir, 'file.nedb')))
      await realDb.loadDatabase()
      binaryStore = new BinaryStore(nedbDatabase, realDb, binaryDir, COMPACT_INTERVAL)
      await binaryStore.start()
    })

    it('must compact (only) after given interval', async () => {
      for (let i = 0; i < COMPACT_INTERVAL; i++) {
        expect((binaryStore as any).nextCompact, 'Unexpected initial nextCompact value').to.equal(
          COMPACT_INTERVAL - i
        )
        await binaryStore.save('fake', 'myPrivateId', HASH1, BUFFER1)
      }
      expect((binaryStore as any).nextCompact, 'Unexpected final nextCompact value').to.equal(
        COMPACT_INTERVAL
      )
    })

    it('must remove entry if store is not found', async () => {
      // Record data
      const fileName = resolve(binaryDir, HASH2)
      await binaryStore.save('notexist', 'myPrivateId2', HASH2, BUFFER2)
      await binaryStore.save('fake', 'myPrivateId', HASH1, BUFFER1)
      await expect(
        promisify(readdir)(binaryDir),
        'Unexpected initial file count'
      ).to.eventually.have.lengthOf(3)
      await expect(promisify(access)(fileName), `Cannot read ${fileName}`).to.be.fulfilled
      await expect(realDb.count({}), 'Unexpected initial record count').to.eventually.equal(2)

      // Compact database
      for (let i = 0; i < COMPACT_INTERVAL - 2; i++) {
        await binaryStore.save('fake', 'myPrivateId', HASH1, BUFFER1)
      }

      // See result
      await expect(realDb.count({}), 'Unexpected final record count').to.eventually.equal(1)
      await expect(
        promisify(readdir)(binaryDir),
        'Unexpected final file count'
      ).to.eventually.have.lengthOf(2)
      await expect(promisify(access)(fileName), `${fileName} should be deleted`).to.be.rejected
    })

    it('must not fail if file has already been deleted', async () => {
      // Record data
      const fileName = resolve(binaryDir, HASH2)
      await binaryStore.save('notexist', 'myPrivateId2', HASH2, BUFFER2)
      await binaryStore.save('fake', 'myPrivateId', HASH1, BUFFER1)

      // Remove file
      await promisify(unlink)(fileName)

      // Compact database
      for (let i = 0; i < COMPACT_INTERVAL - 2; i++) {
        await expect(binaryStore.save('fake', 'myPrivateId', HASH1, BUFFER1)).to.be.fulfilled
      }
    })

    it('must remove entry if unused in store', async () => {
      // Only HASH1 is used by store
      foundValues = { value1: VALUE1 }

      // Record data
      const fileName = resolve(binaryDir, HASH2)
      await binaryStore.save('fake', 'myPrivateId', HASH2, BUFFER2)
      await binaryStore.save('fake', 'myPrivateId', HASH1, BUFFER1)
      await expect(
        promisify(readdir)(binaryDir),
        'Unexpected initial file count'
      ).to.eventually.have.lengthOf(3)
      await expect(promisify(access)(fileName), `Cannot read ${fileName}`).to.be.fulfilled
      await expect(realDb.count({}), 'Unexpected initial record count').to.eventually.equal(2)

      // Compact database
      for (let i = 0; i < COMPACT_INTERVAL - 2; i++) {
        await binaryStore.save('fake', 'myPrivateId', HASH1, BUFFER1)
      }

      // See result
      await expect(realDb.count({}), 'Unexpected final record count').to.eventually.equal(1)
      await expect(
        promisify(readdir)(binaryDir),
        'Unexpected final file count'
      ).to.eventually.have.lengthOf(2)
      await expect(promisify(access)(fileName), `${fileName} should be deleted`).to.be.rejected
    })

    it('must remove entry if totally unused in store', async () => {
      // No values
      foundValues = undefined

      // Record data
      const fileName = resolve(binaryDir, HASH2)
      await binaryStore.save('fake', 'myPrivateId', HASH2, BUFFER2)
      await binaryStore.save('fake', 'myPrivateId', HASH1, BUFFER1)
      await expect(
        promisify(readdir)(binaryDir),
        'Unexpected initial file count'
      ).to.eventually.have.lengthOf(3)
      await expect(promisify(access)(fileName), `Cannot read ${fileName}`).to.be.fulfilled
      await expect(realDb.count({}), 'Unexpected initial record count').to.eventually.equal(2)

      // Compact database
      for (let i = 0; i < COMPACT_INTERVAL - 2; i++) {
        await binaryStore.save('fake', 'myPrivateId', HASH1, BUFFER1)
      }

      // See result
      await expect(realDb.count({}), 'Unexpected final record count').to.eventually.equal(0)
      await expect(
        promisify(readdir)(binaryDir),
        'Unexpected final file count'
      ).to.eventually.have.lengthOf(1)
      await expect(promisify(access)(fileName), `${fileName} should be deleted`).to.be.rejected
    })

    it('must only remove usage if still in use by other entries', async () => {
      let record: BinaryStoreDoc | undefined

      // Record data
      await binaryStore.save('notexist', 'myPrivateId2', HASH1, BUFFER1)
      await binaryStore.save('fake', 'myPrivateId', HASH1, BUFFER1)
      await expect(
        promisify(readdir)(binaryDir),
        'Unexpected initial file count'
      ).to.eventually.have.lengthOf(2)
      await expect(realDb.count({}), 'Unexpected initial record count').to.eventually.equal(1)
      record = await realDb.findOne<BinaryStoreDoc>({})
      expect(record).to.exist
      expect(record!.usages, 'Unexpected initial usage count').to.have.lengthOf(2)

      // Compact database
      for (let i = 0; i < COMPACT_INTERVAL - 2; i++) {
        await binaryStore.save('fake', 'myPrivateId', HASH1, BUFFER1)
      }

      // See result
      await expect(realDb.count({}), 'Unexpected final record count').to.eventually.equal(1)
      await expect(
        promisify(readdir)(binaryDir),
        'Unexpected final file count'
      ).to.eventually.have.lengthOf(2)
      record = await realDb.findOne<BinaryStoreDoc>({})
      expect(record).to.exist
      expect(record!.usages, 'Unexpected final usage count').to.have.lengthOf(1)
    })

    it('must remove (only) orphan files', async () => {
      // Record data
      const fileName = resolve(binaryDir, HASH2)
      await binaryStore.save('fake', 'myPrivateId', HASH2, BUFFER2)
      await binaryStore.save('fake', 'myPrivateId', HASH1, BUFFER1)
      await expect(
        promisify(readdir)(binaryDir),
        'Unexpected initial file count'
      ).to.eventually.have.lengthOf(3)
      await expect(promisify(access)(fileName), `Cannot read ${fileName}`).to.be.fulfilled

      // Delete database entry
      await realDb.remove({ hash: HASH2 })

      // Compact database
      for (let i = 0; i < COMPACT_INTERVAL - 2; i++) {
        await binaryStore.save('fake', 'myPrivateId', HASH1, BUFFER1)
      }

      // See result
      await expect(
        promisify(readdir)(binaryDir),
        'Unexpected final file count'
      ).to.eventually.have.lengthOf(2)
      await expect(promisify(access)(fileName), `${fileName} should be deleted`).to.be.rejected
    })
  })
})
