/*
 * This file is part of @gecogvidanto/plugin-nedb.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import Intl, { LanguageMap } from 'intl-ts'

import { GecoPlugin, ServerApp, isDatabasePlugin, isGecoPluginType } from '@gecogvidanto/plugin'

import * as plugin from '.'
import { langType, messages } from './locale.en'
import NedbDatabase from './NedbDatabase'

const config = {
  plugins: {
    nedb: {
      directory: '',
      compactInterval: 1000,
    },
  },
}
const lang = new Intl(new LanguageMap({ $: '', ...messages }))
const serverApp: ServerApp<langType> = {
  serverLang: lang,
  clientLang: lang,
  config,
  database: undefined as any,
}

describe('NedbPluginType', () => {
  it('must have the plugin signature', () => {
    expect(plugin).to.exist.and.to.include.all.keys('plugin')
    expect(isGecoPluginType(plugin)).to.be.true
  })

  it('must fail to create plugin type if inappropriate server data', () => {
    const badApp: ServerApp<langType> = { ...serverApp, config: {} }
    const newPlugin = (): void => {
      if (isGecoPluginType(plugin)) {
        new plugin(badApp)
      }
    }
    expect(newPlugin).to.throw(/configuration/i)
  })
})

describe('NedbPlugin', () => {
  let pluginInstance: GecoPlugin

  beforeEach('Create the plugin', () => {
    if (isGecoPluginType(plugin)) {
      pluginInstance = new plugin(serverApp)
      pluginInstance.ready() // Should not modify anything
    }
    expect(pluginInstance).to.exist
  })

  it('must be identified as database plugin', () => {
    expect(isDatabasePlugin(pluginInstance)).to.be.true
  })

  describe('#openDatabase', () => {
    it('must create the database', () => {
      if (isDatabasePlugin(pluginInstance)) {
        const database = pluginInstance.openDatabase({})
        expect(database).to.be.instanceOf(NedbDatabase)
      }
    })
  })
})
