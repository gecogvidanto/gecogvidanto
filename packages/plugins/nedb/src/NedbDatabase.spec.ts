/*
 * This file is part of @gecogvidanto/plugin-nedb.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import { access, rmdir, unlink } from 'fs'
import Intl, { LanguageMap } from 'intl-ts'
import { homedir } from 'os'
import { resolve } from 'path'
import * as tmp from 'tmp'
import { promisify } from 'util'
import * as winston from 'winston'

import { Collection, Model, ModelList } from '@gecogvidanto/plugin'

import { langType, messages } from './locale.en'
import { FullConfig } from './NedbConfig'
import NedbDatabase from './NedbDatabase'

winston.configure({ silent: true })

const lang: Intl<langType> = new Intl(new LanguageMap({ $: '', ...messages }))

const DEFAULT_DIRECTORY = resolve(homedir(), '.config/gecogvidanto/')
const INTERNAL_DIRECTORY = '_internal'
const BINSTORE_FILE_NAME = 'binary.nedb'
const FAKE_FILE_NAME = 'fake.nedb'

interface FakeDoc {
  name: string
  firstName: string
}

const fakeDoc: FakeDoc = {
  name: 'Zamenhof',
  firstName: 'Ludwik Lejzer',
}

class FakeModel implements Model {
  public indices = [{ fieldName: 'name', unique: false, sparse: false }]

  public constructor(private db: Collection<FakeDoc>) {}

  public async init(): Promise<void> {
    const elements = await this.db.count({})
    if (elements === 0) {
      await this.db.insert(fakeDoc)
    }
  }

  public countElements(): Promise<number> {
    return this.db.count({})
  }

  public async addElement(): Promise<void> {
    await this.db.insert({
      name: 'Curie',
      firstName: 'Marie',
    })
  }
}

interface FakeModels extends ModelList {
  fake: FakeModel
}

const fakeModels = {
  fake: FakeModel,
}

function pluginConfig(directory?: string, compactInterval?: number): FullConfig['plugins']['nedb'] {
  return {
    directory: directory || '',
    compactInterval: compactInterval || 1000,
  }
}

describe('NedbDatabase', function () {
  this.slow(250)

  it('must use default directory when none provided', async () => {
    const db = new NedbDatabase<FakeModels>(fakeModels, pluginConfig(), lang)
    const fileName = resolve(DEFAULT_DIRECTORY, FAKE_FILE_NAME)
    await db.waitForStarted()
    const count = await db.models.fake.countElements()
    expect(count).to.equal(1)
    await promisify(access)(fileName)
    await promisify(unlink)(fileName)
  })

  it('must create directory if needed', async () => {
    const path = await promisify<string>(tmp.tmpName)()
    const db = new NedbDatabase<FakeModels>(fakeModels, pluginConfig(path), lang)
    const fileName = resolve(path, FAKE_FILE_NAME)
    const internalPathName = resolve(path, INTERNAL_DIRECTORY)
    const binstoreFileName = resolve(internalPathName, BINSTORE_FILE_NAME)
    await db.waitForStarted()
    const count = await db.models.fake.countElements()
    expect(count).to.equal(1)
    await expect(promisify(access)(fileName), `Cannot find ${fileName}`).to.be.fulfilled
    await expect(promisify(access)(binstoreFileName), `Cannot find ${binstoreFileName}`).to.be.fulfilled
    await promisify(unlink)(fileName)
    await promisify(unlink)(binstoreFileName)
    await promisify(rmdir)(internalPathName)
    await promisify(rmdir)(path)
  })

  it('must use different data if different directories', async () => {
    let dbs: Array<NedbDatabase<FakeModels>> | null = null
    const paths = await Promise.all([
      promisify<tmp.DirOptions, string>(tmp.dir)({ unsafeCleanup: true }),
      promisify<tmp.DirOptions, string>(tmp.dir)({ unsafeCleanup: true }),
    ])
    dbs = paths.map(path => new NedbDatabase<FakeModels>(fakeModels, pluginConfig(path, 1), lang))
    await Promise.all(dbs.map(db => db.waitForStarted()))
    const counts = await Promise.all(dbs!.map(db => db.models.fake.countElements()))
    counts.forEach((count, index) =>
      expect(count, `Unexpected initial element count in db #${index}`).to.equal(1)
    )
    await dbs![0].models.fake.addElement()
    const [count0, count1] = await Promise.all(dbs!.map(db => db.models.fake.countElements()))
    expect(count0, 'Unexpected final element count in db #0').to.equal(2)
    expect(count1, 'Unexpected final element count in db #1').to.equal(1)
  })

  it('must fail if directory or file cannot be created', async () => {
    const db = new NedbDatabase<FakeModels>(fakeModels, pluginConfig('/dev/null/un:creatable'), lang)
    await expect(db.waitForStarted()).to.be.rejected
  })
})
