# Nedb local database for ĞecoĞvidanto server

This plugin gives the ability to ĞecoĞvidanto server (@gecogvidanto/server) to store its data in a Nedb local database. Note that there should be one and only one database plugin added to ĞecoĞvidanto server, so if you want to add this one, first remove any other database plugin.

:fr: Une version française de ce document se trouve [ici](doc/fr/README.md).

# Language/langue

Documents, messages, code (including variable names and comments), are in English.

Anyway, because French is my native language, all documents and important messages must also be provided in French. Other translations are welcome.

:fr: Une version française de ce document se trouve [ici](doc/fr/README.md).

# Installation

Installation is done using `npm install` command:

```bash
$ npm install --save @gecogvidanto/plugin-nedb
```

# License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

# Configuration

As with other plugins, configuration is made through the ĞecoĞvidanto server configuration file. The following parameters may be provided:

- `plugins.nedb.directory`: directory where data files will be created. Defaults to `$HOME/.config/gecogvidanto/`,
- `plugins.nedb.compactInterval`: interval of datastore compaction in write count. Defaults to 1000.

Example:

```json
{
  …
  "plugins": {
    "nedb": {
      "directory": "/var/lib/gecogvidanto",
      "compactInterval": 10368
    }
  }
}
```

# Contributing

Even though I cannot guarantee a response time, please feel free to file an issue if you have any question or problem using the package.

_Pull Requests_ are welcome. You can, of course, submit corrections or improvements for code, but do not hesitate to also improve documentation, even for small spell or grammar errors.
