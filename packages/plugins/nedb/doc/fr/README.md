# Base de données locale Nedb pour le serveur ĞecoĞvidanto

Cette extension ajoute la possibilité au server ĞecoĞvidanto (@gecogvidanto/server) d'enregistrer ses données dans une base de donnée locale Nedb. Notez qu'il devrait y avoir une et une seule extension de base de données, donc si vous souhaitez ajouter celle-ci, veuillez d'abord supprimer les autres extensions de base de données.

# Langue

Les documents et messages, le code (y compris les noms de variable et commentaires), sont en anglais.

Cependant, le français étant ma langue maternelle, tous les documents et messages importants doivent également être fournis en français. Les autres traductions sont bienvenues.

# Installation

L’installation se fait avec la commande `npm install` :

```bash
$ npm install --save @gecogvidanto/plugin-nedb
```

# Licence

Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes de la GNU General Public License telle que publiée par la Free Software Foundation ; soit la version 3 de la licence, soit (à votre gré) toute version ultérieure.

Ce programme est distribué dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans même la garantie tacite de QUALITÉ MARCHANDE ou d'ADÉQUATION à UN BUT PARTICULIER. Consultez la GNU General Public License pour plus de détails.

Vous devez avoir reçu une copie de la GNU General Public License en même temps que ce programme ; si ce n'est pas le cas, consultez <http://www.gnu.org/licenses>.

# Configuration

Comme pour les autres extensions, la configuration se fait par le fichier de configuration du serveur ĞecoĞvidanto. Les paramètres suivants peuvent être fournis :

- `plugins.nedb.directory` : répertoire ou les fichiers de données seront créés. Par défaut : `$HOME/.config/gecogvidanto/` ;
- `plugins.nedb.compactInterval` : interval de compactage des données en nombre d'écritures. Par défaut : 1000.

Exemple:

```json
{
  …
  "plugins": {
    "nedb": {
      "directory": "/var/lib/gecogvidanto",
      "compactInterval": 10368
    }
  }
}
```

# Contribuer

Bien que je ne puisse pas garantir un temps de réponse, n’hésitez pas à ouvrir un incident si vous avez une question ou un problème pour utiliser ce paquet.

Les _Pull Requests_ sont bienvenues. Vous pouvez bien sûr soumettre des corrections ou améliorations de code, mais n’hésitez pas également à améliorer la documentation, même pour de petites fautes d’orthographe ou de grammaire.
