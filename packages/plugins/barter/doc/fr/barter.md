# Règles du troc

Ces règles doivent se combiner aux [règles du jeu génériques du Ğeconomicus](http://geconomicus.glibre.org/).

# Troc

Le troc est sans doute le plus ancien système marchand que l'homme ait créé. Son fonctionnement est très simple puisqu'il s'agit d'échanger avec un partenaire des biens qui peuvent l'intéresser contre des biens qui nous intéressent, sans qu'il ne soit fait usage de monnaie.

# Jeu

Aucun billet n'est nécessaire pour une manche en troc. L'application indique que la valeur économique la plus basse vaut 1, mais cela permet surtout d'apprécier les valeurs économiques entre elles.

Les échanges entre joueurs sont obligatoirement mutuellement intéressé. Cela veut dire qu'un joueur ne devrait accepter un échange proposé par un partenaire que s'il y trouve lui aussi un intérêt. Il est par contre possible de faire des échanges circulaires à plus de deux partenaires.
