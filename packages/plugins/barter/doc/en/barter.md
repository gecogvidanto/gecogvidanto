# Barter rules

These rules must be combined to the [global rules of the Ğeconomicus game](http://geconomicus.glibre.org/).

# Barter

Barter is probably the oldest trading system that man has created. Its operation is very simple since it is a matter of exchanging with a partner goods that may interest him for goods that interest us, without the use of money.

# Game

No banknote is needed for a set in barter. The application indicates that the lowest economic value is 1, but it is only used to appreciate the economic values ​​between them.

The exchanges between players must be of mutually interest. This means that a player should only accept an exchange offered by a partner if he also finds an interest in it. It is however possible to make circular exchanges with more than two partners.
