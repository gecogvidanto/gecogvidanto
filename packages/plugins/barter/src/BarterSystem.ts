/*
 * This file is part of @gecogvidanto/plugin-barter.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { EconomicSystem, FormBuilder, FormData } from '@gecogvidanto/plugin'
import { HelpSheet, LocalizedOption, PlaceContent, UnassembledMessages } from '@gecogvidanto/shared'

import { langType } from './ui.locale.en'

const BARTER_HELP_SHEET: HelpSheet = {
  low: PlaceContent.Empty,
  medium: PlaceContent.Empty,
  high: PlaceContent.Empty,
  waiting: PlaceContent.Empty,
}

/**
 * A barter economic system.
 */
export default class BarterSystem extends EconomicSystem<langType> {
  public constructor() {
    super('barter', 'barter$name')
  }

  public getNonPlayerCharacterName(): Promise<UnassembledMessages<langType>[keyof langType]> {
    return Promise.reject(new Error('No non player character in barter system'))
  }

  public getMoneyHelpSheet(): Promise<HelpSheet> {
    return Promise.resolve(BARTER_HELP_SHEET)
  }

  public getOptions(): Promise<ReadonlyArray<LocalizedOption<langType, keyof langType>>> {
    return Promise.resolve([])
  }

  public getForm(builder: FormBuilder<any, any>): Promise<FormBuilder<any, any>> {
    return Promise.resolve(builder)
  }

  public execForm(data: FormData): Promise<FormData> {
    return Promise.resolve(data)
  }

  public terminateRound(): Promise<void> {
    return Promise.resolve()
  }
}
