/*
 * This file is part of @gecogvidanto/plugin-barter.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import Intl, { LanguageMap } from 'intl-ts'

import { FormBuilder, FormData } from '@gecogvidanto/plugin'
import { PlaceContent } from '@gecogvidanto/shared'

import BarterSystem from './BarterSystem'
import { langType, messages } from './ui.locale.en'

const klingon: langType = {
  $: 'Klingon',
  barter$name: 'tlhong',
}
export const lang = new Intl(new LanguageMap({ $: 'English', ...messages }).merge({ klingon }))

describe('BarterSystem', () => {
  const barterSystem = new BarterSystem()

  it('must have sensible static values', () => {
    expect(barterSystem.id, 'Bad barter identifier').to.equal('barter')
    expect(barterSystem.valueCost, 'Bad value cost').to.equal(1)
  })

  it('must give localized name', () => {
    lang.$changePreferences(['en'])
    expect(lang[barterSystem.name](), 'Bad default (english) name').to.equal('Barter')
    lang.$changePreferences(['klingon'])
    expect(lang[barterSystem.name](), 'Bad klingon name').to.equal('tlhong')
  })

  describe('#getNonPlayerCharacterName', () => {
    it('must return failing promise', async () => {
      await expect(barterSystem.getNonPlayerCharacterName()).to.be.rejected
    })
  })

  describe('#getMoneyHelpSheet', () => {
    it('must provide empty help sheet', async () => {
      await expect(barterSystem.getMoneyHelpSheet()).to.eventually.deep.equal({
        low: PlaceContent.Empty,
        medium: PlaceContent.Empty,
        high: PlaceContent.Empty,
        waiting: PlaceContent.Empty,
      })
    })
  })

  describe('#getOptions', () => {
    it('must return empty options', async () => {
      await expect(barterSystem.getOptions()).to.eventually.be.empty
    })
  })

  describe('#getForm', () => {
    it('must return empty form', async () => {
      const formBuilder = await barterSystem.getForm(new FormBuilder())
      const form = formBuilder.build()
      expect(form.parts).to.be.empty
    })
  })

  describe('#execForm', () => {
    it('must return fulfilled promise', async () => {
      await expect(barterSystem.execForm(new FormData({}))).to.be.fulfilled
    })
  })

  describe('#terminateRound', () => {
    it('must do nothing', async () => {
      await expect(barterSystem.terminateRound()).to.be.fulfilled
    })
  })
})
