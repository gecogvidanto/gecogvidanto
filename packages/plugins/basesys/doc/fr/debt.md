# Règles de la monnaie dette

Ces règles doivent se combiner aux [règles du jeu génériques du Ğeconomicus](http://geconomicus.glibre.org/).

# Monnaie dette

Le système de réserve fractionnaire, plus simplement appelé « monnaie dette », est le système économique adopté dans tous les pays du monde. Dans ce système, l'essentiel de la monnaie est créée par les banques privées à chaque fois que l'un de leur client contracte une dette.

Il est conseillé au maitre du jeu de se renseigner plus profondément sur ce type de monnaie afin de pouvoir répondre aux éventuelles questions des joueurs.

# Jeu

Une manche en monnaie dette utilise trois types de billets différents. Comme indiqué par l'application sur les feuilles d'aide, la valeur économique la plus basse vaut 1, tout comme le billet le plus bas.

Les échanges entre joueurs sont obligatoirement monétisés (le troc est donc interdit) et doivent respecter les équivalences entre valeurs et monnaie.

Dans cette manche, un personnage non joueur nommé « Banque » se joint à la partie. L'animateur pourra servir d'interface entre la banque et les joueurs, guidé par l'application. Il doit garder la réserve de billets à portée de main.

Lors du tour d'initialisation, alors que les joueurs n'ont pas encore de monnaie, la banque les incite à faire un emprunt. L'argent emprunté sera le seul à circuler. Chaque joueur n'ayant que 4 cartes de niveau bas à ce moment-là, il ne peut faire qu'un seul emprunt (voir les règles des prêts ci-dessous).

D'autres crédits pourront être contractés à n'importe quel moment du jeu, à la discrétion de la banque, mais les intérêts, comme les remboursements, sont perçus à la fin de chaque tour. Pour faire un crédit durant le tour de jeu, il faut utiliser l'action « Demander un prêt » dans la barre de menu.

Lorsque des joueurs ne pourront plus rembourser leur prêt, la banque devra procéder à une saisie (voir les règles des prêts ci-dessous). Toutes les valeurs économiques saisies par la banque sont immédiatement disponibles à la vente. Si un joueur souhaite acheter un bien saisi, il faut utiliser l'action « Vendre des saisies » dans la barre de menu (voir le chapitre « Vente » ci-dessous).

# Prêts

## Masse monétaire

Les formulaires permettant de faire un crédit affichent systématiquement la valeur de la masse monétaire par joueurs. Le maitre du jeu doit veiller à ce que cette valeur soit le plus proche possible de 2 unités. Il incitera donc les joueurs à faire des crédits si cette valeur est inférieur, ou au contraire, refusera les demandes de prêts si cette valeur augmente trop.

## Accord de prêt

Lorsque la banque accorde un crédit, elle donne 3 unités au joueur. Celui-ci devra rembourser le capital et les intérêts dès la fin du tour, soit 4 unités. Il est éventuellement possible de prolonger le crédit en ne payant que les intérêts (1 seule unité). L'animateur alertera le joueur qui souhaite faire un emprunt à quelque secondes de la fin du tour, puisque celui-ci risque de devoir payer des intérêts avant d'avoir pu profiter de l'argent perçu.

Si un joueur ne peut plus rembourser et qu'une saisie en nature est effectuée, la banque récupérera l'équivalent de 2 fois le nombre d'unités qu'elle a accordé au joueur. Il faut donc s'assurer que celui-ci possède au moins 2 fois le capital total de ses crédits (en incluant l'argent des nouveaux prêts).

Ces règles sont rappelées par le formulaire, soit de façon dynamique si les paramètres de sécurité de votre terminal le permettent, soit directement en indiquant les formules.

Par exemple, pour un joueur ayant déjà 1 crédit et souhaitant en contracter 1 deuxième :

- l'entête de la rubrique rappelle que le joueur a déjà 1 prêt en cours et qu'il devra rembourser 4 unités ;
- l'animateur saisit « 1 » dans le champ « Nouveaux prêts » ;
- un message indique au maitre du jeu qu'il va devoir donner 3 unités au joueur ;
- un message indique que le joueur doit déjà avoir l'équivalent 9 unités en main pour qu'on lui accorde le prêt (soit 12 unités en incluant le nouveau prêt, ce qui est bien le double du capital total emprunté).

## Remboursement ou prolongation

À chaque fin de tour, les joueurs ayant des crédits doivent aller voir l'animateur. Ils peuvent décider de rembourser leur prêt, ou de le prolonger. Dans ce dernier cas, la banque doit donner son accord et vérifier que les possessions du joueur seront suffisantes après paiement des intérêts.

Pour rembourser totalement un prêt, le joueur doit donner 4 unités à la banque (soit 3 pour le capital et 1 pour les intérêts). Pour prolonger le prêt, seuls les intérêts doivent être payés (soit 1 unité).

Ces règles sont rappelées par le formulaire, soit de façon dynamique si les paramètres de sécurité de votre terminal le permettent, soit directement en indiquant les formules.

Par exemple, pour un joueur ayant 2 crédits, souhaitant en rembourser un et prolonger l'autre :

- l'entête de la rubrique rappelle que le joueur a déjà 2 prêts en cours et qu'il devra rembourser 8 unités ;
- l'animateur saisit « 1 » dans le champ « Prêts remboursés » ;
- l'animateur saisit « 1 » dans le champ « Prêts prolongés » ;
- un message indique à l'animateur qu'il va devoir prendre 5 unités au joueur ;

Si le joueur ne peut pas rembourser ses prêts, la banque doit procéder à une saisie.

## Saisie

Lorsque la somme des prêts remboursés et prolongés ne couvre pas le nombre total de crédits contractés par le joueur, la banque procède à une saisie. Dans un premier temps, elle s'empare de tout l'argent que possède le joueur, montant qu'elle déduit de ce que le joueur lui doit. Puis elle saisie en nature (valeur) approximativement l'équivalent de 1,5 fois le montant restant dû (pour les frais de recouvrement).

Ces règles sont rappelées par le formulaire, soit de façon dynamique si les paramètres de sécurité de votre terminal le permettent, soit directement en indiquant les formules.

Par exemple, pour un joueur ayant 2 crédits à rembourser et seulement 3 unités de monnaie :

- le maitre du jeu saisit « 3 » dans le champ « Montant saisi » ;
- un message indique au maitre du jeu qu'il va devoir saisir environ l'équivalent de 7,5 unités au joueur, libre à lui d'arrondir la valeur comme bon lui semble ;

Si le joueur n'a plus assez de valeur pour honorer la saisie, c'est que la banque n'a pas été suffisamment attentive. Mais comme dans la vraie vie, ce n'est pas elle qui paiera son erreur, mais le joueur, qui ira en prison.

Un joueur ayant moins de l'équivalent de 3 unités suite à une saisie va se retrouver coincé :

- il ne pourra plus faire de crédits ;
- il ne pourra plus faire de carré et donc créer de valeurs.

Par conséquent, ce joueur est considéré en banqueroute ; il remet les cartes qu'il lui reste dans la pioche, et va passer un tour en prison.

## Prison

Lorsqu'un joueur n'a plus de cartes suite à une saisie (ou une banqueroute), il passe un tour en prison. Cela signifie que le joueur ne participe pas à ce tour et reste isolé des autres joueurs. Avant de débuter le tour suivant (le cas échéant), le joueur sortira de prison et piochera 4 nouvelles cartes de valeur basses.

# Vente

Lorsque la banque effectue des ventes de bien saisie, le formulaire permet d'indiquer le nombre d'unités reçues par la banque. Par exemple, pour une vente d'une valeur moyenne et d'une valeur basse, sans rupture technologique, l'animateur saisira « 3 » dans le champ « Montant ».

La vente modifiant la masse monétaire en circulation, la banque pourra être amenée a inciter les joueurs à contracter des crédits. C'est pour cette raison que la masse monétaire est rappelée dans ce formulaire.

# Rupture technologique

En cas de rupture technologique, la banque se débarrasse de ses éventuelles valeurs basses saisies en les échangeant contre des valeurs moyennes (les nouvelles valeurs basses) à raison de deux basses pour une moyenne.
