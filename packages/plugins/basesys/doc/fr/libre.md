# Règles de la monnaie libre

Ces règles doivent se combiner aux [règles du jeu génériques du Ğeconomicus](http://geconomicus.glibre.org/).

# Monnaie libre

Une monnaie libre respecte l'algorithme démontrés dans la [théorie relative de la monnaie](https://trm.creationmonetaire.info/). Dans une monnaie libre, tous les utilisateurs sont co-créateur de la monnaie à parts égales. Ils perçoivent donc un dividende universel (DU) qui provoque un accroissement de la masse monétaire d'environ 10% par an. Les prix peuvent être comptés en unités de base de la monnaie ou en relatif par rapport au DU.

Il est conseillé à l'animateur de se renseigner plus profondément sur ce type de monnaie afin de pouvoir répondre aux éventuelles questions des joueurs.

# Jeu

Une manche en monnaie libre utilise les quatre types de billets. Comme indiqué par l'application sur les feuilles d'aide, la valeur économique la plus basse vaut 3, il faut donc 3 billets bas pour en acheter une. Les billets représentent un montant relatif (en DU) et non absolu. On procédera à une rotation des billets pour simuler l'accroissement de la masse monétaire et l'augmentation relative du montant du DU.

Les échanges entre joueurs sont obligatoirement monétisés (le troc est donc interdit) et doivent respecter les équivalences entre valeurs et monnaie.

Avant le début de chaque tour, les joueurs qui ne sont pas des nouveaux arrivants (joueur arrivant en cours de partie, ou re-naissance suite à la mort du joueur) vont perçevoir leur DU. Pour chacun d'eux, le maitre du jeu va devoir :

- distribuer un billet de niveau « en attente » (qui vaut virtuellement 8 unités) et récupèrer un billet de niveau bas (qui vaut 1 unité) ;
- échanger chaque paire de billets de niveau bas restante par un billet de niveau moyen ;
- récupèrer, le cas échéant, le billet de niveau bas restant pour l'éliminer du jeu ;
- faire « tourner » les billets comme indiqué sur la feuille d'aide, passant le billet de niveau bas en attente et divisant par 2 la valeur des autres.

Si un joueur n'a plus de billet de niveau bas lors de la distribution, l'animateur lui fera de la monnaie. Si le joueur n'a plus du tout d'argent, il percevra simplement le nouveau billet.

À ce moment là, pour les joueurs qui n'ont rien perçu (soit lors du tour d'initialisation, soit pour les joueurs nouveaux arrivants), les joueurs perçoivent un billet de chaque type en jeu (excluant donc le type en attente), ce qui fait un équivalent de 7 unités.

On a donc en permanence approximativement 7 unités par joueur. À chaque tour, 7 unités supplémentaires sont distribués, ce qui fait 14 unités par joueur, mais comme dans le même temps, la valeur des billets est divisée par 2, on retrouve bien nos 7 unités.
