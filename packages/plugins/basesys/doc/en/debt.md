# Debt money rules

These rules must be combined to the [global rules of the Ğeconomicus game](http://geconomicus.glibre.org/).

# Debt money

The fractional-reserve banking, also simply called “debt money”, is the economic system adopted in all countries of the world. In this system, most of the money is created by private banks whenever one of their clients incurs a debt.

The game master is advised to inquire more deeply about this type of money in order to answer any questions from the players.

# Game

A debt money set uses three different types of notes. As indicated by the application on the help sheets, the lowest economic value is 1, as is the lowest banknote.

The exchanges between players are necessarily monetized (bartering is prohibited) and must respect the equivalence between values ​​and money.

In this set, a non-player character named “Bank” joins the game. The game master can serve as interface between the bank and the players, guided by the application. He must keep the banknotes close to him.

During the initialization round, while the players have no money yet, the bank encourages them to make loans. The money borrowed will be the only one to circulate. Each player having only 4 low level cards at that time, he can only make one loan (see the loan rules below).

Other credits may be taken at any time during the game, at the discretion of the bank, but interest and/or repayments are collected at the end of each round. To make a credit during the round, use the action “Request loan” in the menu bar.

When players can no longer repay their loan, the bank will have to make a seizure (see the loan rules below). All economic values ​​entered by the bank are immediately available for sale. If a player wants to buy a seized good, you must use the “Sell seizures” action in the menu bar (see the “Sale” chapter below).

# Loans

## Money mass

Forms for credit always display the value of the money mass per player. The game master must ensure that this value is as close as possible to 2 units. He will encourage players to make credits if this value is lower, or on the contrary, will refuse loan applications if this value increases too much.

## Loan agreement

When the bank grants a credit, it gives 3 units to the player. This one will have to repay the capital and the interests which is 4 units, as soon as the end of the turn. It may be possible to extend the loan by paying only interest (1 single unit). The game master will alert the player who wishes to make a loan a few seconds to the end of the round, since he may have to pay interest before he can use the money.

If a player can no longer repay and a seizure is made, the bank will recover the equivalent of 2 times the number of units it has granted to the player. One must therefore ensure that the player has at least 2 times the total capital of his credits (including the money from new loans).

These rules are recalled by the form, either dynamically if the security settings of your terminal allow it, or directly by indicating the formulas.

For example, for a player who already has 1 credit and wishing to contract 1 other one:

- the header recalls that the player already has 1 loan in progress and that he will have to repay 4 units;
- the game master enters “1” in the “New loans” field;
- a message tells the game master that he will have to give 3 units to the player;
- a message indicates that the player must already have the equivalent of 9 units in hand to be granted the loan (i.e. 12 units including the new loan, which is the double of the total capital borrowed).

## Refund or extension

At the end of each round, players with loans must go to the game master. They may decide to repay their loan, or extend it. In the latter case, the bank must agree and check that the possessions of the player will be sufficient after payment of interest.

To repay a loan, the player must give 4 units to the bank (3 for the capital and 1 for the interest). To extend the loan, only interest must be paid (i.e. 1 unit).

These rules are recalled by the form, either dynamically if the security settings of your terminal allow it, or directly by indicating the formulas.

For example, for a player with 2 credits, wishing to repay one and extend the other:

- the header recalls that the player already has 2 loans in progress and that he will have to repay 8 units;
- the game master enters “1” in the field “Refunded loans”;
- the game master enters “1” in the field “Extended loans”;
- a message indicates to the game master that he will have to take 5 units from the player;

If the player can not repay his loans, the bank must make a seizure.

## Seizure

When the sum of the refunded and extended loans does not cover the total amount of loans taken by the player, the bank proceeds to a seizure. At first, it takes all the money the player has, which it deducts from what the player owes it. Then it takes in kind (value) approximately the equivalent of 1.5 times the remaining due amount (for collection costs).

These rules are recalled by the form, either dynamically if the security settings of your terminal allow it, or directly by indicating the formulas.

For example, for a player with 2 credits to pay back and only 3 money units:

- the game master enters “3” in the “Amount seized” field;
- a message tells the game master that he will have to seize about the equivalent of 7.5 units to the player, free to round the value as he whiches;

If the player does not have enough value to honor the seizure, it is because the bank was not attentive enough. But as in real life, it is not it who will pay for its mistake, but the player, who will go to jail.

A player with less than the equivalent of 3 units after a seizure will be stuck:

- he will not be able to make any more loans;
- he will not be able to make any more four of some, and thus to create values.

Therefore, this player is considered bankrupt; he puts the cards he has left in the deck, and goes to jail.

## Jail

When a player has no more cards after a seizure (or bankruptcy), he goes to jail. This means that the player does not participate in this round and remains isolated from other players. Before starting the next round (if applicable), the player will come out of jail and draw 4 new low value cards.

# Sale

When the bank makes sales of goods, the form can be used to indicate the amount of units received by the bank. For example, for a sale of a medium value and a low value, without technological break, the game master will enter “3” in the “Amount” field.

The sale modifying the mass of circulating money, the bank may be led to encourage players to take out loans. It is for this reason that the money mass is recalled in this form.

# Technological break

In the event of a technological break, the bank gets rid of its possible seized low values by exchanging them for medium values ​​(the new low values) at the rate of two low for one medium.
