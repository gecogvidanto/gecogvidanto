# Libre money rules

These rules must be combined to the [global rules of the Ğeconomicus game](http://geconomicus.glibre.org/).

# Libre money

A libre money follows the algorithm demonstrated in the [relative theory of money](https://en.trm.creationmonetaire.info/). In a libre money, all users are co-creators of the money in equal shares. They therefore receive a universal dividend (UD), which increases the money mass by about 10% per year. Prices can be counted in base units of the money or in relative to the UD.

The game master is advised to inquire more deeply about this type of money in order to answer any questions from the players.

# Game

A set in libre money uses the four types of banknotes. As indicated by the application on the help sheets, the lowest economic value is 3, so you need 3 low banknotes to buy one. The banknotes represent a relative amount (in UD) and not an absolute one. The banknotes will be rotated to simulate the increase of the money mass and the relative increase of the value of the UD.

The exchanges between players are necessarily monetized (bartering is prohibited) and must respect the equivalence between values ​​and money.

Before the start of each round, players who are not newcomers (player arriving during the game, or re-birth following the death of the player) will receive their UD. For each of them, the game master will have to:

- distribute a “waiting” banknote (which worth virtually 8 units) and retrieve a low banknote (which worth 1 unit);
- exchange each remaining low level banknote pair with a medium level banknote;
- recover, if available, the remaining low level single banknote to eliminate it from the game;
- rotate the banknotes as indicated on the help sheet, passing the low level banknote on “waiting” and dividing by 2 the value of the others.

If a player no longer has a low level “banknote” during distribution, the game master will make change. If the player does not have any money at all, he will simply receive the new banknote.

At this time, for players who have not received anything (either during the initialization round or for newcomer players), players receive a banknote of each type in play (thus excluding the waiting type), which is equivalent to 7 units.

We therefore have approximately 7 units per player at a time. Each turn, 7 additional units are distributed, which makes 14 units per player, but as at the same time, the value of the banknotes is divided by 2, we find our 7 units.
