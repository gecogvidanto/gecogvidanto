/*
 * This file is part of @gecogvidanto/plugin-basesys.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import Intl, { LanguageMap } from 'intl-ts'

import { ServerApp, ServerGame, ServerSet } from '@gecogvidanto/plugin'
import { PlaceContent, messages as sharedMessages } from '@gecogvidanto/shared'

import { BankModel } from './database'
import { langType, messages } from './ui.locale.en'

const newspeak: Partial<langType> = {
  $: 'New speak',
  basesys$debtMoney: 'Real money',
  basesys$libreMoney: 'Toy money',
}

export const appData = {
  lang: new Intl(new LanguageMap({ ...sharedMessages, ...messages }).merge({ newspeak })),
  model: new BankModel(undefined as any),
}

export const fakeServerApp: ServerApp<langType> = {
  serverLang: appData.lang,
  clientLang: appData.lang,
  config: {},
  database: { models: { baseSys$Bank: appData.model } } as any,
}

interface FakeSet extends ServerSet {
  currentRound: number
}
interface FakeGame extends ServerGame {
  currentSet: FakeSet
  absoluteCurrentRound: number
  terminatingPlayers: number[]
}
export function buildFakeGame(): FakeGame {
  return {
    _id: 'fake-game-id',
    roundsPerSet: 10,
    currentSet: {
      currentRound: 0,
    },
    absoluteCurrentRound: 12,
    onGamePlayers: [0, 2, 3],
    terminatingPlayers: [],
    players: [
      { name: 'Anne Frank', roundLeft: 13 },
      { name: 'Lucy', roundLeft: 11 },
      { name: 'Annie Besant', roundLeft: 0 },
      { name: 'Simone de Beauvoir', roundLeft: 0 },
    ],
    getCharacterFor() {
      return undefined as any
    },
    addNonPlayerCharacter() {
      return undefined as any
    },
    buildValuesHelpSheet() {
      return {
        low: PlaceContent.Red,
        medium: PlaceContent.Yellow,
        high: PlaceContent.Green,
        waiting: PlaceContent.Blue,
      }
    },
  } as any
}

export function assembleMessage(value: Promise<any[]>, extractor?: (keyof any)[]): Promise<string[]>
export function assembleMessage(value: Promise<any>, extractor?: (keyof any)[]): Promise<string>
export function assembleMessage(value: any[], extractor?: (keyof any)[]): string[]
export function assembleMessage(value: any, extractor?: (keyof any)[]): string
export function assembleMessage(
  value: Promise<any> | any,
  extractor: (keyof any)[] = []
): Promise<string | string[]> | string | string[] {
  if (value instanceof Promise) {
    return new Promise((resolve, reject) =>
      value.then(key => resolve(assembleMessage(key, extractor))).catch(error => reject(error))
    )
  } else if (Array.isArray(value)) {
    return value.map(key => assembleMessage(key, extractor))
  } else if (extractor.length) {
    extractor.forEach(key => (value[key] = assembleMessage(value[key])))
    return value
  } else {
    const lang: any = appData.lang
    let message: any
    let parameters: any[]
    if (typeof value === 'string') {
      message = value
      parameters = []
    } else {
      message = value.template
      parameters = value.parameters
    }
    try {
      return lang[message].bind(lang)(...parameters)
    } catch {
      throw new Error(`Cannot build message ${message}(${parameters.join(', ')})`)
    }
  }
}
