/*
 * This file is part of @gecogvidanto/plugin-basesys.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import * as sinon from 'sinon'
import { SinonSpy, SinonStub } from 'sinon'

import { Collection } from '@gecogvidanto/plugin'

import BankModel, { Bank } from './BankModel'

type Identifier = import('@gecogvidanto/plugin').Identifier
type Projection<T> = import('@gecogvidanto/plugin').Projection<T>
type Query<T> = import('@gecogvidanto/plugin').Query<T>
type Update<T> = import('@gecogvidanto/plugin').Update<T>

const db: Collection<Bank> = {
  async update() {
    return 0
  },
  async findOne() {
    return null
  },
  async remove() {
    return 0
  },
} as any
const GAME_ID = 'game-id'

describe('BankModel', () => {
  let updateSpy: SinonSpy<[Query<Identifier & Bank>, Update<Bank>, boolean?], Promise<number>>
  let findStub: SinonStub<
    [Query<Identifier & Bank>, Projection<Identifier & Bank>?],
    Promise<(Identifier & Bank) | undefined>
  >
  let removeSpy: SinonSpy<[Query<Identifier & Bank>], Promise<number>>
  let bankModel: BankModel
  let bank: Bank

  beforeEach('Create model and doubles', () => {
    updateSpy = sinon.spy(db, 'update')
    findStub = sinon.stub(db, 'findOne')
    removeSpy = sinon.spy(db, 'remove')
    bankModel = new BankModel(db)
    bank = {
      gameId: GAME_ID,
      monetaryMass: 12,
      loans: [
        { playerId: 1, loanCount: 2 },
        { playerId: 3, loanCount: 1 },
      ],
    }
  })

  afterEach('Clean up doubles', () => {
    removeSpy.restore()
    findStub.restore()
    updateSpy.restore()
  })

  describe('#init', () => {
    it('must do nothing', async () => {
      await expect(bankModel.init()).to.be.fulfilled

      expect(updateSpy).to.not.have.been.called
      expect(findStub).to.not.have.been.called
      expect(removeSpy).to.not.have.been.called
    })
  })

  describe('#save', () => {
    it('must send given data to database', async () => {
      await bankModel.save(bank)

      expect(updateSpy).to.have.been.calledOnce
      expect(findStub).to.not.have.been.called
      expect(removeSpy).to.not.have.been.called

      const args = updateSpy.firstCall.args
      expect(args[0]).to.deep.equal({ gameId: GAME_ID })
      expect(args[1]).to.deep.equal({ $set: bank })
      expect(args[2]).to.be.true
    })
  })

  describe('#find', () => {
    it('must return existing value', async () => {
      findStub.resolves(bank as Identifier & Bank)
      const result = await bankModel.find(GAME_ID)

      expect(updateSpy).to.not.have.been.called
      expect(findStub).to.have.been.calledOnce
      expect(removeSpy).to.not.have.been.called

      expect(result).to.deep.equal(bank)
    })

    it('must return empty value for non existing game', async () => {
      findStub.resolves(undefined)
      const result = await bankModel.find(GAME_ID)

      expect(updateSpy).to.not.have.been.called
      expect(findStub).to.have.been.calledOnce
      expect(removeSpy).to.not.have.been.called

      expect(result.gameId).to.equal(GAME_ID)
      expect(result.monetaryMass).to.equal(0)
      expect(result.loans).to.have.lengthOf(0)
    })
  })

  describe('#delete', () => {
    it('must request database deletion for given game', async () => {
      await bankModel.delete(GAME_ID)

      expect(updateSpy).to.not.have.been.called
      expect(findStub).to.not.have.been.called
      expect(removeSpy).to.have.been.calledOnce

      expect(removeSpy.firstCall.args[0]).to.deep.equal({ gameId: GAME_ID })
    })
  })
})
