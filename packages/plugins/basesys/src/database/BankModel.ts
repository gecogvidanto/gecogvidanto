/*
 * This file is part of @gecogvidanto/plugin-basesys.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Collection, Model } from '@gecogvidanto/plugin'

/**
 * Loans for a given player.
 */
interface PlayerLoan {
  /**
   * Identifier of the user in a given game.
   */
  readonly playerId: number

  /**
   * Count of loans the user must refund.
   */
  loanCount: number
}

/**
 * Bank data for a given game.
 */
export interface Bank {
  /**
   * Identifier of the game.
   */
  readonly gameId: string

  /**
   * The known monetary mass.
   */
  monetaryMass: number

  /**
   * Loans for each player of the game.
   */
  readonly loans: PlayerLoan[]
}

/**
 * Model for storing bank data.
 */
export default class BankModel implements Model {
  public readonly indices = [
    {
      fieldName: 'gameId',
      unique: true,
      sparse: false,
    },
  ]

  public constructor(private readonly db: Collection<Bank>) {}

  public init(): Promise<void> {
    return Promise.resolve()
  }

  /**
   * Save the bank data, wheather it is persistent or not.
   *
   * @param bank - The bank data to save.
   */
  public async save(bank: Bank): Promise<void> {
    const { gameId } = bank
    await this.db.update({ gameId }, { $set: bank }, true)
  }

  /**
   * Find the bank data for the given game identifier.
   *
   * @param gameId - The game identifier.
   * @returns The bank data.
   */
  public async find(gameId: string): Promise<Bank> {
    const result = await this.db.findOne({ gameId })
    if (!result) {
      return { gameId, monetaryMass: 0, loans: [] }
    } else {
      return result
    }
  }

  /**
   * Delete the bank data for the given game.
   *
   * @param gameId - The game identifier.
   */
  public async delete(gameId: string): Promise<void> {
    await this.db.remove({ gameId })
  }
}
