/*
 * This file is part of @gecogvidanto/plugin-basesys.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { EconomicSystemPlugin, ServerApp, databaseHasModels } from '@gecogvidanto/plugin'

import { BankModel, PluginModels } from './database'
import { DebtSystem } from './debt'
import { LibreSystem } from './libre'

/**
 * Base system plugin manage debt money and libre money economic systems for ĞecoĞvidanto.
 */
export default class BaseSysPlugin implements EconomicSystemPlugin {
  private readonly debtSystem: DebtSystem
  private readonly libreSystem: LibreSystem
  private _model?: BankModel

  public constructor(private readonly serverApp: ServerApp) {
    this.debtSystem = new DebtSystem(this)
    this.libreSystem = new LibreSystem()
  }

  public ready(): Promise<void> {
    return Promise.resolve()
  }

  public openEconomicSystems(): [DebtSystem, LibreSystem] {
    return [this.debtSystem, this.libreSystem]
  }

  /**
   * @returns The model for the bank.
   */
  public get model(): BankModel {
    if (this._model) {
      return this._model
    } else if (databaseHasModels<PluginModels>(this.serverApp.database, 'baseSys$Bank')) {
      this._model = this.serverApp.database.models.baseSys$Bank
      return this._model
    } else {
      throw new Error('Needed model not found')
    }
  }
}
