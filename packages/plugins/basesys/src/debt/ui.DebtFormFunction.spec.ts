/*
 * This file is part of @gecogvidanto/plugin-basesys.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import {
  Aspect,
  DynField,
  DynFunction,
  InitFunction,
  ServerField,
  Toolbox,
  UpdateFunction,
} from '@gecogvidanto/shared'

import { assembleMessage } from '../common.spec'
import DebtField from './DebtField'
import { dynamicManagement } from './ui.DebtFormFunction'

type Bank = import('../database/BankModel').Bank
type UpdateParams = import('./DebtFormBuilder').UpdateParams

const TestedPlayers: Array<{ id: number; name: string }> = [
  { id: 0, name: 'Isaac Asimov' },
  { id: 2, name: 'Mary Higgins Clark' },
  { id: 3, name: 'Enid Blyton' },
]
const TestedLoans: Array<Bank['loans'][number]> = [
  { playerId: 0, loanCount: 12 },
  { playerId: 3, loanCount: 9 },
]
const TestedParams: UpdateParams = {
  players: TestedPlayers,
  loans: TestedLoans,
}

const dynFunction: DynFunction<UpdateParams> =
  process.env.TEST_UI === undefined
    ? dynamicManagement
    : Function(`"use strict"; return ${dynamicManagement.toString()}`)()

describe('dynamicManagement', () => {
  let toolbox: Toolbox
  let innerFunction: InitFunction & UpdateFunction

  beforeEach('Prepare function', () => {
    const dynFields: DynField[] = [
      new DynField(-Infinity, {
        name: DebtField.MonetaryMass,
        content: 12,
        options: {},
        hasData: false,
      }),
      new DynField(-Infinity, {
        name: DebtField.MonetaryMassPP,
        content: '',
        options: {},
        hasData: false,
      }),
      new DynField(-Infinity, {
        name: DebtField.ValuesSold,
        content: 4,
        options: {},
        hasData: true,
      }),
      ...TestedPlayers.map(
        ({ id }) =>
          new DynField(id, {
            name: DebtField.GrantedLoans,
            content: 2,
            options: {},
            hasData: true,
          })
      ),
      ...TestedPlayers.map(
        ({ id }) =>
          new DynField(id, {
            name: DebtField.GrantedHelp,
            content: '',
            options: {},
            hasData: false,
          })
      ),
      ...TestedPlayers.map(
        ({ id }) =>
          new DynField(id, {
            name: DebtField.GrantedCheck,
            content: '',
            options: {},
            hasData: false,
          })
      ),
      ...TestedPlayers.map(
        ({ id }) =>
          new DynField(id, {
            name: DebtField.RefundedLoans,
            content: 4,
            options: {},
            hasData: true,
          })
      ),
      ...TestedPlayers.map(
        ({ id }) =>
          new DynField(id, {
            name: DebtField.ExtendedLoans,
            content: 2,
            options: {},
            hasData: true,
          })
      ),
      ...TestedPlayers.map(
        ({ id }) =>
          new DynField(id, {
            name: DebtField.SeizedMoney,
            content: 1,
            options: {},
            hasData: true,
          })
      ),
      ...TestedPlayers.map(
        ({ id }) =>
          new DynField(id, {
            name: DebtField.RefundedHelp,
            content: '',
            options: {},
            hasData: false,
          })
      ),
      ...TestedPlayers.map(
        ({ id }) =>
          new DynField(id, {
            name: DebtField.ValuesToSeize,
            content: '',
            options: {},
            hasData: false,
          })
      ),
      ...TestedPlayers.map(
        ({ id }) =>
          new DynField(id, {
            name: ServerField.LowMoney,
            content: 1,
            options: {},
            hasData: true,
          })
      ),
      ...TestedPlayers.map(
        ({ id }) =>
          new DynField(id, {
            name: ServerField.MediumMoney,
            content: 1,
            options: {},
            hasData: true,
          })
      ),
      ...TestedPlayers.map(
        ({ id }) =>
          new DynField(id, {
            name: ServerField.HighMoney,
            content: 1,
            options: {},
            hasData: true,
          })
      ),
    ]
    toolbox = {
      forField: (player: number, name: string) => (callback: (field: DynField) => void) => {
        const field = dynFields.find(dynField => dynField.player === player && dynField.name === name)
        if (field) {
          callback(field)
        }
      },
    }
    innerFunction = dynFunction(dynFields, TestedParams, toolbox)
  })

  it('must be a function', () => {
    expect(innerFunction).to.be.a('function')
  })

  it('must initialize correctly', () => {
    innerFunction()
    toolbox.forField(
      -Infinity,
      DebtField.MonetaryMassPP
    )(field => {
      expect(assembleMessage(field.content)).to.match(/4 units per player/i)
    })
    const seizures: number[] = [34.5, 0, -37.5, 16.5]
    TestedPlayers.map(({ id }) => {
      toolbox.forField(
        id,
        DebtField.GrantedHelp
      )(field => {
        expect(assembleMessage(field.content)).to.match(/6 units to player/i)
      })
      toolbox.forField(
        id,
        DebtField.GrantedCheck
      )(field => {
        expect(assembleMessage(field.content)).to.match(/must at least already be worth 18 units/i)
      })
      toolbox.forField(
        id,
        DebtField.RefundedHelp
      )(field => {
        expect(assembleMessage(field.content)).to.match(/18 units from player/i)
      })
      toolbox.forField(
        id,
        DebtField.ValuesToSeize
      )(field => {
        expect(assembleMessage(field.content)).to.match(
          new RegExp(`equivalent of ${seizures[id]} units`, 'i')
        )
      })
    })
  })

  it('must manage monetary mass aspect', () => {
    toolbox.forField(
      -Infinity,
      DebtField.MonetaryMassPP
    )(mmassField => {
      toolbox.forField(
        -Infinity,
        DebtField.ValuesSold
      )(field => {
        innerFunction(field, 4)
        expect(mmassField.options.aspect).to.equal(Aspect.Error)
        innerFunction(field, -1)
        expect(mmassField.options.aspect).to.equal(Aspect.Warning)
        innerFunction(field, 3)
        expect(mmassField.options.aspect).to.equal(Aspect.Info)
      })
    })
  })

  it('must manage sold items', () => {
    toolbox.forField(
      -Infinity,
      DebtField.ValuesSold
    )(field => {
      innerFunction(field, 1)
    })
    toolbox.forField(
      -Infinity,
      DebtField.MonetaryMass
    )(field => {
      expect(field.content).to.equal(9)
    })
    toolbox.forField(
      -Infinity,
      DebtField.MonetaryMassPP
    )(field => {
      expect(assembleMessage(field.content)).to.match(/3 units per player/i)
    })
  })

  it('must manage granted loans', () => {
    toolbox.forField(
      2,
      DebtField.GrantedLoans
    )(field => {
      innerFunction(field, 0)
    })
    toolbox.forField(
      -Infinity,
      DebtField.MonetaryMass
    )(field => {
      expect(field.content).to.equal(18)
    })
    toolbox.forField(
      -Infinity,
      DebtField.MonetaryMassPP
    )(field => {
      expect(assembleMessage(field.content)).to.match(/6 units per player/i)
    })
    toolbox.forField(
      2,
      DebtField.GrantedHelp
    )(field => {
      expect(assembleMessage(field.content)).to.match(/6 units to player/i)
    })
    toolbox.forField(
      2,
      DebtField.GrantedCheck
    )(field => {
      expect(assembleMessage(field.content)).to.match(/must at least already be worth 18 units/i)
    })
  })

  it('must manage refunded loans (simple case)', () => {
    toolbox.forField(
      0,
      DebtField.SeizedMoney
    )(field => {
      field.options = { disabled: true }
    })
    toolbox.forField(
      0,
      DebtField.RefundedLoans
    )(field => {
      innerFunction(field, 1)
    })
    toolbox.forField(
      -Infinity,
      DebtField.MonetaryMass
    )(field => {
      expect(field.content).to.equal(0)
    })
    toolbox.forField(
      0,
      DebtField.ExtendedLoans
    )(field => {
      expect(field.content).to.equal(2)
    })
    toolbox.forField(
      0,
      DebtField.SeizedMoney
    )(field => {
      expect(field.content).to.equal(1)
      expect(field.options.disabled).to.not.be.ok
    })
    toolbox.forField(
      0,
      DebtField.RefundedHelp
    )(field => {
      expect(assembleMessage(field.content)).to.match(/18 units from player/i)
    })
    toolbox.forField(
      0,
      DebtField.ValuesToSeize
    )(field => {
      expect(assembleMessage(field.content)).to.match(/the equivalent of 34.5 units/i)
      expect(field.options.aspect).to.equal(Aspect.Error)
    })
  })

  it('must manage refunded loans (lower extended case)', () => {
    toolbox.forField(
      0,
      DebtField.RefundedLoans
    )(field => {
      field.content = 11
      innerFunction(field, 10)
    })
    toolbox.forField(
      -Infinity,
      DebtField.MonetaryMass
    )(field => {
      expect(field.content).to.equal(10)
    })
    toolbox.forField(
      0,
      DebtField.ExtendedLoans
    )(field => {
      expect(field.content).to.equal(1)
    })
    toolbox.forField(
      0,
      DebtField.SeizedMoney
    )(field => {
      expect(field.content).to.equal(0)
      expect(field.options.disabled).to.be.true
    })
    toolbox.forField(
      0,
      DebtField.RefundedHelp
    )(field => {
      expect(assembleMessage(field.content)).to.match(/45 units from player/i)
    })
    toolbox.forField(
      0,
      DebtField.ValuesToSeize
    )(field => {
      expect(assembleMessage(field.content)).to.match(/the equivalent of 0 units/i)
      expect(field.options.aspect).to.be.undefined
    })
  })

  it('must manage refunded loans (oversize case)', () => {
    toolbox.forField(
      0,
      DebtField.RefundedLoans
    )(field => {
      field.content = 14
      innerFunction(field, 10)
      expect(field.content).to.equal(12)
    })
    toolbox.forField(
      -Infinity,
      DebtField.MonetaryMass
    )(field => {
      expect(field.content).to.equal(7)
    })
    toolbox.forField(
      0,
      DebtField.ExtendedLoans
    )(field => {
      expect(field.content).to.equal(0)
    })
  })

  it('must manage extended loans (simple case)', () => {
    toolbox.forField(
      0,
      DebtField.ExtendedLoans
    )(field => {
      innerFunction(field, 1)
    })
    toolbox.forField(
      -Infinity,
      DebtField.MonetaryMass
    )(field => {
      expect(field.content).to.equal(11)
    })
    toolbox.forField(
      0,
      DebtField.RefundedLoans
    )(field => {
      expect(field.content).to.equal(4)
    })
    toolbox.forField(
      0,
      DebtField.SeizedMoney
    )(field => {
      expect(field.content).to.equal(1)
      expect(field.options.disabled).to.not.be.ok
    })
    toolbox.forField(
      0,
      DebtField.RefundedHelp
    )(field => {
      expect(assembleMessage(field.content)).to.match(/18 units from player/i)
    })
    toolbox.forField(
      0,
      DebtField.ValuesToSeize
    )(field => {
      expect(assembleMessage(field.content)).to.match(/the equivalent of 34.5 units/i)
      expect(field.options.aspect).to.equal(Aspect.Error)
    })
  })

  it('must manage extended loans (lower refunded case)', () => {
    toolbox.forField(
      0,
      DebtField.ExtendedLoans
    )(field => {
      field.content = 9
      innerFunction(field, 6)
    })
    toolbox.forField(
      -Infinity,
      DebtField.MonetaryMass
    )(field => {
      expect(field.content).to.equal(14)
    })
    toolbox.forField(
      0,
      DebtField.RefundedLoans
    )(field => {
      expect(field.content).to.equal(3)
    })
    toolbox.forField(
      0,
      DebtField.SeizedMoney
    )(field => {
      expect(field.content).to.equal(0)
      expect(field.options.disabled).to.be.true
    })
    toolbox.forField(
      0,
      DebtField.RefundedHelp
    )(field => {
      expect(assembleMessage(field.content)).to.match(/21 units from player/i)
    })
    toolbox.forField(
      0,
      DebtField.ValuesToSeize
    )(field => {
      expect(assembleMessage(field.content)).to.match(/the equivalent of 0 units/i)
      expect(field.options.aspect).to.be.undefined
    })
  })

  it('must manage extended loans (oversize case)', () => {
    toolbox.forField(
      0,
      DebtField.SeizedMoney
    )(field => {
      field.content = 0
      field.options = { disabled: true }
    })
    toolbox.forField(
      0,
      DebtField.ExtendedLoans
    )(field => {
      field.content = 14
      innerFunction(field, 0)
      expect(field.content).to.equal(12)
    })
    toolbox.forField(
      -Infinity,
      DebtField.MonetaryMass
    )(field => {
      expect(field.content).to.equal(16)
    })
    toolbox.forField(
      0,
      DebtField.RefundedLoans
    )(field => {
      expect(field.content).to.equal(0)
    })
  })

  it('must manage seizure', () => {
    toolbox.forField(
      3,
      DebtField.SeizedMoney
    )(field => {
      innerFunction(field, 2)
    })
    toolbox.forField(
      -Infinity,
      DebtField.MonetaryMass
    )(field => {
      expect(field.content).to.equal(13)
    })
    toolbox.forField(
      3,
      DebtField.ValuesToSeize
    )(field => {
      expect(assembleMessage(field.content)).to.match(/the equivalent of 16.5 units/i)
      expect(field.options.aspect).to.equal(Aspect.Error)
    })
  })

  it('must manage low money (player death)', () => {
    toolbox.forField(
      2,
      ServerField.LowMoney
    )(field => {
      innerFunction(field, 3)
    })
    toolbox.forField(
      -Infinity,
      DebtField.MonetaryMass
    )(field => {
      expect(field.content).to.equal(14)
    })
  })

  it('must manage medium money (player death)', () => {
    toolbox.forField(
      2,
      ServerField.MediumMoney
    )(field => {
      innerFunction(field, 3)
    })
    toolbox.forField(
      -Infinity,
      DebtField.MonetaryMass
    )(field => {
      expect(field.content).to.equal(16)
    })
  })

  it('must manage high money (player death)', () => {
    toolbox.forField(
      2,
      ServerField.HighMoney
    )(field => {
      innerFunction(field, 3)
    })
    toolbox.forField(
      -Infinity,
      DebtField.MonetaryMass
    )(field => {
      expect(field.content).to.equal(20)
    })
  })
})
