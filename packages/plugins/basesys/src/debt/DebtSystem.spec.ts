/*
 * This file is part of @gecogvidanto/plugin-basesys.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import * as sinon from 'sinon'
import { SinonStub } from 'sinon'

import { FormBuilder, FormData, ServerCharacter } from '@gecogvidanto/plugin'
import { Aspect, Field, Form, PlaceContent, Score } from '@gecogvidanto/shared'

import BaseSysPlugin from '../BaseSysPlugin'
import { appData, assembleMessage, buildFakeGame, fakeServerApp } from '../common.spec'
import { Bank } from '../database'
import { langType } from '../ui.locale.en'
import DebtSystem from './DebtSystem'

describe('DebtSystem', () => {
  const debtSystem = new DebtSystem(new BaseSysPlugin(fakeServerApp))

  it('must have sensible static values', () => {
    expect(debtSystem.id, 'Bad debt money identifier').to.equal('debt-money')
    expect(debtSystem.valueCost, 'Bad value cost').to.equal(1)
  })

  it('must give localized name', () => {
    appData.lang.$changePreferences(['en'])
    expect(assembleMessage(debtSystem.name), 'Bad default (english) name').to.equal('Debt money')
    appData.lang.$changePreferences(['newspeak'])
    expect(assembleMessage(debtSystem.name), 'Bad newspeak name').to.equal('Real money')
  })

  describe('#getNonPlayerCharacterName', () => {
    it('must return bank name for 1st NPC', async () => {
      await expect(assembleMessage(debtSystem.getNonPlayerCharacterName(-1))).to.eventually.equal('Bank')
    })

    it('must return failing promise for other NPCs', async () => {
      for (let i = -2; i > -24; i--) {
        await expect(assembleMessage(debtSystem.getNonPlayerCharacterName(i))).to.be.rejected
      }
    })
  })

  describe('#getMoneyHelpSheet', () => {
    it('must return the good help sheet, whatever the game', async () => {
      await expect(debtSystem.getMoneyHelpSheet()).to.eventually.deep.equal({
        low: PlaceContent.Red,
        medium: PlaceContent.Yellow,
        high: PlaceContent.Green,
        waiting: PlaceContent.Empty,
      })
    })
  })

  describe('#getOptions', () => {
    it('must return the debt options', async () => {
      await expect(assembleMessage(debtSystem.getOptions(), ['description'])).to.eventually.deep.equal([
        { id: 'sell', description: 'Sell seizures' },
        { id: 'loan', description: 'Request loan' },
      ])
    })
  })

  describe('#getForm', () => {
    const fakeGame = buildFakeGame()
    let bank: Bank
    let findStub: SinonStub<[string], Promise<Bank>>
    let gameCharStub: SinonStub<[number], ServerCharacter>

    beforeEach('Prepare doubles', () => {
      bank = {
        gameId: fakeGame._id!,
        monetaryMass: 6,
        loans: [
          { playerId: 0, loanCount: 1 },
          { playerId: 3, loanCount: 2 },
        ],
      }
      findStub = sinon.stub(appData.model, 'find')
      findStub.resolves(bank)
      gameCharStub = sinon.stub(fakeGame, 'getCharacterFor')
      gameCharStub.withArgs(0).returns({ generation: 6 } as ServerCharacter)
      gameCharStub.returns({} as ServerCharacter)
    })

    afterEach('Clean doubles', () => {
      gameCharStub.restore()
      findStub.restore()
    })

    function checkField(
      fieldOrArray: Field<langType> | ReadonlyArray<Field<langType>>,
      playerId: number | undefined,
      type: string,
      name: string,
      content: any,
      aspect?: Aspect,
      help?: RegExp
    ): void {
      expect(fieldOrArray).to.not.be.an('array')
      const field: Field<langType> = fieldOrArray as Field<langType>
      const id = playerId === undefined ? 'global' : String(playerId)
      expect(field.name, 'Unexpected identifier').to.equal(name)
      expect(field.type, `Unexpected ${name}-${id} type`).to.equal(type)
      if (help) {
        expect(field.content, `Unexpected ${name}-${id} content`).to.equal(content)
        expect(assembleMessage((field as any).help), 'Unexpected help').to.match(help)
      } else {
        expect(assembleMessage(field.content), `Unexpected content`).to.match(content)
      }
      if (aspect) {
        expect(field.options.aspect, `Unexpected ${name}-${id} aspect`).to.equal(aspect)
      }
    }

    function checkSoldLabel(field: Field<langType> | ReadonlyArray<Field<langType>>): void {
      checkField(field, undefined, 'text', 'soldlabel', /indicate .* money earned/i)
    }

    function checkSold(field: Field<langType> | ReadonlyArray<Field<langType>>): void {
      checkField(field, undefined, 'number-input', 'sold', 0, undefined, /amount/i)
    }

    function checkMonetaryMass(form: Form<any>, withSoldFields: boolean, aspect: Aspect): void {
      const part = form.parts[0]
      expect(part.player, 'Player should not be global').to.equal(-Infinity)
      expect(part.groups, 'Unexpected group count').to.have.lengthOf(withSoldFields ? 2 : 1)
      const fieldCount = [2, 2]
      part.groups.forEach((group, index) => {
        expect(group.fields, `Unexpected field count in group ${index}`).to.have.lengthOf(fieldCount[index])
      })
      const mmassGroup = part.groups[0]
      checkField(
        mmassGroup.fields[0],
        undefined,
        'number-input',
        'mmass',
        bank.monetaryMass,
        undefined,
        /^$/
      )
      checkField(
        mmassGroup.fields[1],
        undefined,
        'text',
        'mmasspp',
        /monetary mass: .* per player/i,
        aspect
      )
    }

    function checkOutstanding(
      field: Field<langType> | ReadonlyArray<Field<langType>>,
      playerId: number
    ): void {
      checkField(field, playerId, 'text', 'outstanding', /has \d+ outstanding loan/i)
    }

    function checkGrantedLoans(
      field: Field<langType> | ReadonlyArray<Field<langType>>,
      playerId: number
    ): void {
      checkField(field, playerId, 'number-input', 'grantedloans', 0, undefined, /new loans/i)
    }

    function checkGrantedHelp(
      field: Field<langType> | ReadonlyArray<Field<langType>>,
      playerId: number
    ): void {
      checkField(field, playerId, 'text', 'grantedhelp', /give .* units to player/i)
    }

    function checkGrantedCheck(
      field: Field<langType> | ReadonlyArray<Field<langType>>,
      playerId: number
    ): void {
      checkField(field, playerId, 'text', 'grantedcheck', /must at least already be worth/i)
    }

    function checkRefund(
      field: Field<langType> | ReadonlyArray<Field<langType>>,
      extend: boolean,
      playerId: number
    ): void {
      expect(field).to.be.an('array').and.be.lengthOf(2)
      const fields = field as ReadonlyArray<Field<langType>>
      checkField(fields[0], playerId, 'number-input', 'refund', 0, undefined, /refunded loans/i)
      checkField(fields[1], playerId, 'number-input', 'extend', 0, undefined, /extended loans/i)
      expect(fields[1].options.invisible).to.equal(extend)
    }

    function checkRefundHelp(
      field: Field<langType> | ReadonlyArray<Field<langType>>,
      playerId: number
    ): void {
      checkField(field, playerId, 'text', 'refundhelp', /ake .* units/i)
    }

    function checkNonRefundable(
      field: Field<langType> | ReadonlyArray<Field<langType>>,
      playerId: number
    ): void {
      checkField(field, playerId, 'text', 'nonrefundable', /non refundable loans/i)
    }

    function checkSeized(field: Field<langType> | ReadonlyArray<Field<langType>>, playerId: number): void {
      checkField(field, playerId, 'number-input', 'seized', 0, undefined, /amount seized/i)
    }

    function checkToSeize(field: Field<langType> | ReadonlyArray<Field<langType>>, playerId: number): void {
      checkField(field, playerId, 'text', 'toseize', /seize roughly .* due amount/i)
    }

    it('must get the form for item selling', async () => {
      bank.monetaryMass = 0
      const formBuilder = await debtSystem.getForm(new FormBuilder(), fakeGame, 'sell')
      const form = formBuilder.build()
      expect(findStub).to.have.been.calledOnce
      expect(form.dynFunction).to.exist
      expect(form.parts, 'Unexpected parts count').to.have.lengthOf(1)
      checkMonetaryMass(form, true, Aspect.Error)
      expect(form.parts[0].groups, 'Unexpected group count').to.have.lengthOf(2)
      const group = form.parts[0].groups[1]
      checkSoldLabel(group.fields[0])
      checkSold(group.fields[1])
    })

    it('must get the form for loan request', async () => {
      bank.monetaryMass = 5
      const formBuilder = await debtSystem.getForm(new FormBuilder(), fakeGame, 'loan')
      const form = formBuilder.build()
      expect(findStub).to.have.been.calledOnce
      expect(form.dynFunction).to.exist
      expect(form.parts, 'Unexpected parts count').to.have.lengthOf(4)
      checkMonetaryMass(form, false, Aspect.Warning)
      const players = [0, 2, 3]
      for (let i = 1; i <= 3; i++) {
        const part = form.parts[i]
        expect(part.player, `Unexpected player #${i} part`).to.equal(players[i - 1])
        expect(part.groups, `Unexpected group count for player #${i}`).to.have.lengthOf(1)
        expect(part.groups[0].fields, `Unexpected field count for player #${i}`).to.have.lengthOf(3)
        expect(part.groups[0].title).to.exist
        checkOutstanding(part.groups[0].title!, players[i - 1])
        checkGrantedLoans(part.groups[0].fields[0], players[i - 1])
        checkGrantedHelp(part.groups[0].fields[1], players[i - 1])
        checkGrantedCheck(part.groups[0].fields[2], players[i - 1])
      }
    })

    it('must get the form for game start', async () => {
      fakeGame.currentSet.currentRound = 0
      fakeGame.absoluteCurrentRound = 12
      const formBuilder = await debtSystem.getForm(new FormBuilder(), fakeGame, undefined)
      const form = formBuilder.build()
      expect(findStub).to.have.been.calledOnce
      expect(form.dynFunction).to.exist
      expect(form.parts, 'Unexpected parts count').to.have.lengthOf(4)
      checkMonetaryMass(form, false, Aspect.Info)
      const players = [0, 2, 3]
      for (let i = 1; i <= 3; i++) {
        const part = form.parts[i]
        expect(part.player, `Unexpected player #${i} part`).to.equal(players[i - 1])
        expect(part.groups, `Unexpected group count for player #${i}`).to.have.lengthOf(1)
        expect(part.groups[0].fields, `Unexpected field count for player #${i}`).to.have.lengthOf(3)
        checkGrantedLoans(part.groups[0].fields[0], players[i - 1])
        checkGrantedHelp(part.groups[0].fields[1], players[i - 1])
        checkGrantedCheck(part.groups[0].fields[2], players[i - 1])
      }
    })

    it('must get the form for game end', async () => {
      fakeGame.currentSet.currentRound = 10
      fakeGame.terminatingPlayers = [0, 2, 3]
      const formBuilder = await debtSystem.getForm(new FormBuilder(), fakeGame, undefined)
      const form = formBuilder.build()
      expect(findStub).to.have.been.calledOnce
      expect(form.dynFunction).to.exist
      expect(form.parts, 'Unexpected parts count').to.have.lengthOf(5)
      checkMonetaryMass(form, false, Aspect.Info)
      expect(form.parts[1].groups, 'Unexpected bank group count').to.have.lengthOf(1)
      expect(form.parts[1].groups[0].fields, 'Unexpected bank field count').to.have.lengthOf(2)
      const players = [0, 2, 3]
      const loans = [true, false, true]
      for (let i = 0; i < 3; i++) {
        const part = form.parts[i + 2]
        expect(part.player, `Unexpected player #${i} part`).to.equal(players[i])
        expect(part.groups, `Unexpected group count for player #${i}`).to.have.lengthOf(1)
        expect(part.groups[0].title, `Title should exist for player #${i}`).to.exist
        expect(part.groups[0].fields, `Unexpected field count for player #${i}`).to.have.lengthOf(
          loans[i] ? 5 : 0
        )
        checkOutstanding(part.groups[0].title!, players[i])
        if (loans[i]) {
          checkRefund(part.groups[0].fields[0], true, players[i])
          checkRefundHelp(part.groups[0].fields[1], players[i])
          checkNonRefundable(part.groups[0].fields[2], players[i])
          checkSeized(part.groups[0].fields[3], players[i])
          checkToSeize(part.groups[0].fields[4], players[i])
        }
      }
    })

    it('must get the form for other rounds', async () => {
      fakeGame.currentSet.currentRound = 6
      fakeGame.terminatingPlayers = [0]
      const formBuilder = await debtSystem.getForm(new FormBuilder(), fakeGame, undefined)
      const form = formBuilder.build()
      expect(findStub).to.have.been.calledOnce
      expect(form.dynFunction).to.exist
      expect(form.parts, 'Unexpected parts count').to.have.lengthOf(4)
      checkMonetaryMass(form, false, Aspect.Info)
      const players = [0, 2, 3]
      const loans = [true, false, true]
      const leaving = [true, false, false]
      const fieldCount = [8, 3, 8]
      for (let i = 1; i <= 3; i++) {
        const part = form.parts[i]
        expect(part.player, `Unexpected player #${i} part`).to.equal(players[i - 1])
        expect(part.groups, `Unexpected group count for player #${i}`).to.have.lengthOf(1)
        expect(part.groups[0].title, `Title should exist for player #${i}`).to.exist
        expect(part.groups[0].fields, `Unexpected field count for player #${i}`).to.have.lengthOf(
          fieldCount[i - 1]
        )
        let index = 0
        checkOutstanding(part.groups[0].title!, players[i - 1])
        if (loans[i - 1]) {
          checkRefund(part.groups[0].fields[index++], leaving[i - 1], players[i - 1])
          checkRefundHelp(part.groups[0].fields[index++], players[i - 1])
          checkNonRefundable(part.groups[0].fields[index++], players[i - 1])
          checkSeized(part.groups[0].fields[index++], players[i - 1])
          checkToSeize(part.groups[0].fields[index++], players[i - 1])
        }
        checkGrantedLoans(part.groups[0].fields[index++], players[i - 1])
        checkGrantedHelp(part.groups[0].fields[index++], players[i - 1])
        checkGrantedCheck(part.groups[0].fields[index++], players[i - 1])
      }
    })

    it('must fail if bad id given', async () => {
      await expect(debtSystem.getForm(new FormBuilder(), fakeGame, 'bad-id')).to.be.rejected
    })
  })

  describe('#execForm', () => {
    const fakeGame = buildFakeGame()
    let bank: Bank
    let findStub: SinonStub<[string], Promise<Bank>>
    let saveStub: SinonStub<[Bank], Promise<void>>
    let gameCharStub: SinonStub<[number], ServerCharacter>
    let currentScore: Score

    beforeEach('Prepare doubles', () => {
      fakeGame.currentSet.currentRound = 1
      bank = {
        gameId: fakeGame._id!,
        monetaryMass: 12,
        loans: [],
      }
      findStub = sinon.stub(appData.model, 'find')
      findStub.resolves(bank)
      saveStub = sinon.stub(appData.model, 'save')
      saveStub.resolves()
      currentScore = {
        round: 1,
        values: {
          low: 0,
          medium: 0,
          high: 0,
        },
        money: {
          low: 0,
          medium: 0,
          high: 0,
        },
      }
      const character = { currentScore } as ServerCharacter
      gameCharStub = sinon.stub(fakeGame, 'getCharacterFor')
      gameCharStub.withArgs(-1).returns(character)
      gameCharStub.throws()
    })

    afterEach('Clean doubles', () => {
      gameCharStub.restore()
      saveStub.restore()
      findStub.restore()
    })

    function checkResult(
      mmass: number,
      loans: Array<{ playerId: number; loanCount: number }>,
      money: number
    ): void {
      expect(findStub).to.have.been.calledOnce
      expect(findStub).to.have.been.calledWith(fakeGame._id)
      expect(saveStub).to.have.been.calledOnce
      const saved = saveStub.firstCall
      expect(saved.args[0].gameId).to.equal(fakeGame._id)
      expect(saved.args[0].monetaryMass, 'Unexpected monetary mass').to.equal(mmass)
      expect(saved.args[0].loans, 'Unexpected loan count').to.have.lengthOf(loans.length)
      loans.forEach((loan, index) =>
        expect(saved.args[0].loans[index], `Bad loan #${index}`).to.deep.equal(loan)
      )
      expect(gameCharStub).to.have.been.calledOnce
      expect(currentScore.money.low, 'Unexpected money count').to.equal(money)
      expect(currentScore.money.medium).to.equal(0)
      expect(currentScore.money.high).to.equal(0)
    }

    it('must record sold values', async () => {
      ;(bank as any).loans = [{ playerId: 8, loanCount: 1 }]
      await debtSystem.execForm(new FormData({ sold$$: '3' }), fakeGame, 'sell')
      checkResult(9, [{ playerId: 8, loanCount: 1 }], 3)
    })

    it('must record granted loans', async () => {
      await debtSystem.execForm(new FormData({ grantedloans$3$: '1' }), fakeGame, undefined)
      checkResult(15, [{ playerId: 3, loanCount: 1 }], 0)
    })

    it('must record refunded loans', async () => {
      bank.loans.push({ playerId: 0, loanCount: 1 })
      await debtSystem.execForm(new FormData({ refund$0$: '1' }), fakeGame, undefined)
      checkResult(8, [], 1)
    })

    it('must record extended loans', async () => {
      bank.loans.push({ playerId: 3, loanCount: 2 })
      await debtSystem.execForm(new FormData({ extend$3$: '2' }), fakeGame, undefined)
      checkResult(10, [{ playerId: 3, loanCount: 2 }], 2)
    })

    it('must record seized money', async () => {
      bank.loans.push({ playerId: 2, loanCount: 1 })
      await debtSystem.execForm(new FormData({ seized$2$: '2' }), fakeGame, undefined)
      checkResult(10, [], -1)
    })

    it('must update monetary mass when death', async () => {
      await debtSystem.execForm(
        new FormData({
          '!lowMoney$0$': '1',
          '!mediumMoney$0$': '1',
          '!highMoney$0$': '1',
        }),
        fakeGame,
        undefined
      )
      checkResult(5, [], 0)
    })

    it('must record bank values', async () => {
      fakeGame.currentSet.currentRound = fakeGame.roundsPerSet
      await debtSystem.execForm(
        new FormData({
          '!lowValues$-1$': '1',
          '!mediumValues$-1$': '1',
          '!highValues$-1$': '1',
        }),
        fakeGame,
        undefined
      )
      expect(currentScore.values).to.deep.equal({
        low: 1,
        medium: 1,
        high: 1,
      })
    })

    it('must save new values for complex and invalid data', async () => {
      bank.loans.push({ playerId: 0, loanCount: 2 }, { playerId: 3, loanCount: 1 })
      await debtSystem.execForm(
        new FormData({
          '!lowMoney$2$': '3',
          '!highMoney$2$': '1',
          grantedloans$2$: '1',
          refund$0$: '1',
          extend$0$: '1',
          invalid: '5',
          sold$1$: '1',
          grantedloans$3$: 'invalid',
          grantedloans$$: '2',
        }),
        fakeGame,
        undefined
      )
      checkResult(
        3,
        [
          { playerId: 0, loanCount: 1 },
          { playerId: 2, loanCount: 1 },
        ],
        -1
      )
    })

    it('must accept loans at initialization round', async () => {
      fakeGame.currentSet.currentRound = 0
      await expect(debtSystem.execForm(new FormData({ grantedloans$3$: '1' }), fakeGame, undefined)).to.be
        .fulfilled
      expect(findStub).to.have.been.calledOnce
      expect(saveStub).to.have.been.calledOnce
      expect(gameCharStub).to.have.not.been.called
    })

    it('must refuse to receive money at initialization round', async () => {
      fakeGame.currentSet.currentRound = 0
      await expect(
        debtSystem.execForm(new FormData({ sold$$: '3' }), fakeGame, undefined)
      ).to.be.rejectedWith(/cannot earn money at initialization round/i)
      expect(findStub).to.have.been.calledOnce
      expect(saveStub).to.have.been.calledOnce
      expect(gameCharStub).to.have.not.been.called
    })

    it('must refuse to receive values at non-final round', async () => {
      fakeGame.currentSet.currentRound = 1
      await expect(
        debtSystem.execForm(new FormData({ '!lowValues$-1$': '1' }), fakeGame, undefined)
      ).to.be.rejectedWith(/result should only arrive at final round/i)
      expect(findStub).to.have.been.calledOnce
      expect(saveStub).to.have.been.calledOnce
      expect(gameCharStub).to.have.been.calledOnce
    })
  })

  describe('#terminateRound', () => {
    const fakeGame = buildFakeGame()
    let deleteStub: SinonStub<[string], Promise<void>>
    let gameAddNPCStub: SinonStub<[], number>

    beforeEach('Prepare doubles', () => {
      deleteStub = sinon.stub(appData.model, 'delete')
      deleteStub.resolves()
      gameAddNPCStub = sinon.stub(fakeGame, 'addNonPlayerCharacter')
      gameAddNPCStub.returns(-1)
    })

    afterEach('Clean doubles', () => {
      gameAddNPCStub.restore()
      deleteStub.restore()
    })

    it('must create NPC before 1st round', async () => {
      await expect(debtSystem.terminateRound(fakeGame)).to.be.fulfilled
      expect(gameAddNPCStub).to.have.been.calledOnce
      expect(deleteStub).to.not.have.been.called
    })

    it('must return failing promise if NPC does not have good id', async () => {
      gameAddNPCStub.returns(-12)
      await expect(debtSystem.terminateRound(fakeGame)).to.be.rejected
      expect(gameAddNPCStub).to.have.been.calledOnce
      expect(deleteStub).to.not.have.been.called
    })

    it('must delete data after last round', async () => {
      fakeGame.currentSet.currentRound = 10
      await expect(debtSystem.terminateRound(fakeGame)).to.be.fulfilled
      expect(gameAddNPCStub).to.have.not.been.called
      expect(deleteStub).to.have.been.calledOnceWith(fakeGame._id)
    })

    it('must do nothing for other rounds', async () => {
      fakeGame.currentSet.currentRound = 8
      await expect(debtSystem.terminateRound(fakeGame)).to.be.fulfilled
      expect(gameAddNPCStub).to.have.not.been.called
      expect(deleteStub).to.have.not.been.called
    })
  })
})
