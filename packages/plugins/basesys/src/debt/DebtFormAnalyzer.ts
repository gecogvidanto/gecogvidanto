/*
 * This file is part of @gecogvidanto/plugin-basesys.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { FormData } from '@gecogvidanto/plugin'
import { ServerField } from '@gecogvidanto/shared'

import { Bank } from '../database'
import DebtField from './DebtField'

/**
 * The loans of a player.
 */
type PlayerLoan = Bank['loans'][number] & { nextLoans: number }

/**
 * Actions associated to a field.
 */
interface FieldAction<T> {
  field: DebtField | ServerField
  valid: (playerId: number) => boolean
  action: (value: T, loan: PlayerLoan | undefined) => any
}

/**
 * Analyze the result of the form.
 */
export default class DebtFormAnalyzer {
  private _values: { low: number; medium: number; high: number } = {
    low: 0,
    medium: 0,
    high: 0,
  }
  private _money = 0
  private readonly gameId: string
  private monetaryMass: number
  private loans: PlayerLoan[]

  /**
   * The actions associated to each field.
   */
  private readonly numberActions: ReadonlyArray<FieldAction<number>> = [
    {
      field: DebtField.ValuesSold,
      valid: playerId => playerId === -Infinity,
      action: value => {
        this.monetaryMass -= value
        this._money += value
      },
    },
    {
      field: DebtField.GrantedLoans,
      valid: playerId => playerId >= 0,
      action: (value, playerLoan) => {
        playerLoan!.nextLoans += value
        this.monetaryMass += 3 * value
      },
    },
    {
      field: DebtField.RefundedLoans,
      valid: playerId => playerId >= 0,
      action: (value, playerLoan) => {
        value = Math.min(value, playerLoan!.loanCount)
        playerLoan!.loanCount -= value
        this.monetaryMass -= 4 * value
        this._money += value
      },
    },
    {
      field: DebtField.ExtendedLoans,
      valid: playerId => playerId >= 0,
      action: (value, playerLoan) => {
        value = Math.min(value, playerLoan!.loanCount)
        playerLoan!.loanCount -= value
        playerLoan!.nextLoans += value
        this.monetaryMass -= value
        this._money += value
      },
    },
    {
      field: DebtField.SeizedMoney,
      valid: playerId => playerId >= 0,
      action: value => {
        this.monetaryMass -= value
        this._money += value
      },
    },
    {
      field: ServerField.LowMoney,
      valid: playerId => playerId >= 0,
      action: value => {
        this.monetaryMass -= value
      },
    },
    {
      field: ServerField.MediumMoney,
      valid: playerId => playerId >= 0,
      action: value => {
        this.monetaryMass -= 2 * value
      },
    },
    {
      field: ServerField.HighMoney,
      valid: playerId => playerId >= 0,
      action: value => {
        this.monetaryMass -= 4 * value
      },
    },
    {
      field: ServerField.LowValues,
      valid: playerId => playerId === -1,
      action: value => {
        this._values.low = value
      },
    },
    {
      field: ServerField.MediumValues,
      valid: playerId => playerId === -1,
      action: value => {
        this._values.medium = value
      },
    },
    {
      field: ServerField.HighValues,
      valid: playerId => playerId === -1,
      action: value => {
        this._values.high = value
      },
    },
  ]

  /**
   * Create a new analyzer.
   *
   * @param data - The data coming from server.
   * @param bank - The bank data.
   * @param option - True if this is the result of an option form (so not the round end).
   */
  public constructor(data: FormData, bank: Bank, option: boolean) {
    // Prepare private data
    this.gameId = bank.gameId
    this.monetaryMass = bank.monetaryMass
    this.loans = bank.loans.map(loan => ({ ...loan, nextLoans: 0 }))

    // Loop through all input
    for (const playerId of data.playerIds) {
      const playerData = data.getValuesFor(playerId)
      playerData.forEach((stringValue, key) => {
        const action = this.numberActions.find(numberAction => numberAction.field === key)
        if (action && action.valid(playerId)) {
          const value = Number(stringValue)
          if (!isNaN(value)) {
            let playerLoan: PlayerLoan | undefined
            if (playerId >= 0) {
              playerLoan = this.loans.find(loan => loan.playerId === playerId)
              if (!playerLoan) {
                playerLoan = { playerId, loanCount: 0, nextLoans: 0 }
                this.loans.push(playerLoan)
              }
            }
            action.action(value, playerLoan)
          }
        }
      })
    }

    // At round end, all non treated loans are seizures, for other cases, report loans
    if (option) {
      for (const playerLoan of this.loans) {
        playerLoan.nextLoans += playerLoan.loanCount
      }
    } else {
      for (const playerLoan of this.loans) {
        this._money -= 3 * playerLoan.loanCount
      }
    }
  }

  /**
   * @returns The (bank) money count for this form.
   */
  public get money(): number {
    return this._money
  }

  /**
   * @returns The bank low values for this form.
   */
  public get lowValues(): number {
    return this._values.low
  }

  /**
   * @returns The bank medium values for this form.
   */
  public get mediumValues(): number {
    return this._values.medium
  }

  /**
   * @returns The bank high values for this form.
   */
  public get highValues(): number {
    return this._values.high
  }

  /**
   * @returns The result of the analysis.
   */
  public get result(): Bank {
    return {
      gameId: this.gameId,
      monetaryMass: this.monetaryMass,
      loans: this.loans
        .filter(playerLoan => playerLoan.nextLoans > 0)
        .map(playerLoan => ({
          playerId: playerLoan.playerId,
          loanCount: playerLoan.nextLoans,
        }))
        .sort((a, b) => a.playerId - b.playerId),
    }
  }
}
