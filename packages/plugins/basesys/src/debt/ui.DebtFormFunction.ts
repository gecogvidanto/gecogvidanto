/*
 * This file is part of @gecogvidanto/plugin-basesys.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Aspect, ServerField } from '@gecogvidanto/shared'

import DebtField from './DebtField'

/**
 * Create the dynamic management of the form. Note that to prevent accidental external “real” import, only
 * dynamic imports are used in this file, as if we don't use promises, these kind of import can only be used
 * for types.
 *
 * @param fields - The dynamic fields.
 * @param params - The system parameters.
 * @param toolbox - The provided toolbox.
 * @returns The object containing initialisation and update functions.
 */
export function dynamicManagement(
  fields: ReadonlyArray<import('@gecogvidanto/shared').DynField>,
  params: Readonly<import('./DebtFormBuilder').UpdateParams>,
  toolbox: import('@gecogvidanto/shared').Toolbox
): import('@gecogvidanto/shared').InitFunction & import('@gecogvidanto/shared').UpdateFunction {
  /**
   * Get the count of loans for the player.
   *
   * @param player - The identifier of the player.
   * @returns The count of loans.
   */
  function getLoanCount(player: number): number {
    return (
      params.loans.find(loan => loan.playerId === player) || {
        loanCount: 0,
      }
    ).loanCount
  }

  /**
   * Update the monetary mass fields.
   *
   * @param variation - The monetary mass variation.
   */
  function updateMonetaryMass(variation: number): void {
    toolbox.forField(
      -Infinity,
      DebtField.MonetaryMass
    )(mmassField => {
      toolbox.forField(
        -Infinity,
        DebtField.MonetaryMassPP
      )(mmassppField => {
        const mMass = (mmassField.content as number) + variation
        const mMassPerPlayer = mMass / params.players.length
        const mMassAspect =
          mMassPerPlayer < 1.5 || mMassPerPlayer > 2.5
            ? Aspect.Error
            : mMassPerPlayer < 1.8 || mMassPerPlayer > 2.2
            ? Aspect.Warning
            : Aspect.Info
        mmassField.content = mMass
        mmassppField.content = { template: 'basesys$mMassPP', parameters: [mMassPerPlayer] }
        mmassppField.options = { aspect: mMassAspect }
      })
    })
  }

  /**
   * Update the granted help field.
   *
   * @param field - The granted help field.
   * @param count - The count of granted loans.
   */
  function updateGrantedHelp(field: import('@gecogvidanto/shared').DynField, count: number): void {
    field.content = { template: 'basesys$grantedHelp', parameters: [3 * count] }
  }

  /**
   * Update the granted check field.
   *
   * @param field - The granted check field.
   * @param count - The count of granted loans.
   */
  function updateGrantedCheck(field: import('@gecogvidanto/shared').DynField, count: number): void {
    let goods = 6 * getLoanCount(field.player)
    toolbox.forField(
      field.player,
      DebtField.ExtendedLoans
    )(extendedField => (goods = 6 * Number(extendedField.content)))
    goods += 3 * count
    field.content = { template: 'basesys$grantedCheck', parameters: [goods] }
  }

  /**
   * Update the refunded help field.
   *
   * @param field - The refunded help field.
   * @param refunded - The count of refunded loans.
   * @param extended - The count of extended loans.
   */
  function updateRefundedHelp(
    field: import('@gecogvidanto/shared').DynField,
    refunded: number,
    extended: number
  ): void {
    const take: number = 4 * refunded + extended
    field.content = { template: 'basesys$refundHelp', parameters: [take] }
  }

  /**
   * Update the seizure help field.
   *
   * @param field - The seizure help field.
   * @param refunded - The count of refunded loans.
   * @param extended - The count of extended loans.
   * @param seizure - The seized amount.
   */
  function updateSeizureHelp(
    field: import('@gecogvidanto/shared').DynField,
    refunded: number,
    extended: number,
    seizure: number
  ): void {
    const seize: number = 1.5 * (4 * (getLoanCount(field.player) - refunded - extended) - seizure)
    field.content = { template: 'basesys$toSeize', parameters: [seize] }
    field.options = seize === 0 ? {} : { aspect: Aspect.Error }
  }

  /**
   * Initialization function, prepare every help text depending on field values.
   */
  function initFunction(): void {
    fields.forEach(field => {
      switch (field.name) {
        case DebtField.GrantedHelp:
          toolbox.forField(
            field.player,
            DebtField.GrantedLoans
          )(grantedField => updateGrantedHelp(field, Number(grantedField.content)))
          break
        case DebtField.GrantedCheck:
          toolbox.forField(
            field.player,
            DebtField.GrantedLoans
          )(grantedField => updateGrantedCheck(field, Number(grantedField.content)))
          break
        case DebtField.RefundedHelp:
          toolbox.forField(
            field.player,
            DebtField.RefundedLoans
          )(refundedField => {
            toolbox.forField(
              field.player,
              DebtField.ExtendedLoans
            )(extendedField =>
              updateRefundedHelp(field, Number(refundedField.content), Number(extendedField.content))
            )
          })
          break
        case DebtField.ValuesToSeize:
          toolbox.forField(
            field.player,
            DebtField.RefundedLoans
          )(refundedField => {
            toolbox.forField(
              field.player,
              DebtField.ExtendedLoans
            )(extendedField => {
              toolbox.forField(
                field.player,
                DebtField.SeizedMoney
              )(seizureField =>
                updateSeizureHelp(
                  field,
                  Number(refundedField.content),
                  Number(extendedField.content),
                  Number(seizureField.content)
                )
              )
            })
          })
      }
    })
    updateMonetaryMass(0)
  }

  /**
   * Manage an update of the sold field.
   *
   * @param field - The sold field.
   * @param previous - The previous content.
   */
  function sold(field: import('@gecogvidanto/shared').DynField, previous: unknown): void {
    updateMonetaryMass(Number(previous) - Number(field.content))
  }

  /**
   * Manage an update of a granted loans field.
   *
   * @param field - The loans field.
   * @param previous - The previous content.
   */
  function grantedLoans(field: import('@gecogvidanto/shared').DynField, previous: unknown): void {
    const newLoans = Number(field.content)
    updateMonetaryMass(3 * (newLoans - Number(previous)))
    toolbox.forField(
      field.player,
      DebtField.GrantedHelp
    )(helpField => updateGrantedHelp(helpField, newLoans))
    toolbox.forField(
      field.player,
      DebtField.GrantedCheck
    )(checkField => updateGrantedCheck(checkField, newLoans))
  }

  /**
   * Terminate adjustments for refunded and extended loans.
   *
   * @param player - The player identifier.
   * @param mmassVariation - The monetary mass variation including refunded and extended loans.
   * @param noLoans - Indicate if there are no more loans to refund.
   * @param refunded - The count of refunded loans.
   * @param extended - The count of extended loans.
   */
  function refundedOrExtendedLoans(
    player: number,
    mmassVariation: number,
    noLoans: boolean,
    refunded: number,
    extended: number
  ): void {
    // Adjust refunded help
    toolbox.forField(
      player,
      DebtField.RefundedHelp
    )(helpField => updateRefundedHelp(helpField, refunded, extended))

    // Adjust seizure
    toolbox.forField(
      player,
      DebtField.SeizedMoney
    )(seizureField => {
      const seizureInit = Number(seizureField.content)
      let seizure: number = seizureInit
      if (noLoans) {
        if (seizureField.content !== 0) {
          seizure = 0
          seizureField.content = seizure
        }
        if (!seizureField.options.disabled) {
          seizureField.options = { disabled: true }
        }
      } else if (seizureField.options.disabled) {
        seizureField.options = {}
      }
      mmassVariation -= seizure - seizureInit
      toolbox.forField(
        player,
        DebtField.ValuesToSeize
      )(seizureHelp => updateSeizureHelp(seizureHelp, refunded, extended, seizure))
    })

    // Adjust new loan check
    toolbox.forField(
      player,
      DebtField.GrantedLoans
    )(grantedField => {
      const loanCount = Number(grantedField.content)
      toolbox.forField(
        player,
        DebtField.GrantedCheck
      )(checkField => updateGrantedCheck(checkField, loanCount))
    })

    // Update monetary mass
    updateMonetaryMass(mmassVariation)
  }

  /**
   * Manage an update of refunded field.
   *
   * @param field - The refunded field.
   * @param previous - The previous content.
   */
  function refundedLoans(field: import('@gecogvidanto/shared').DynField, previous: unknown): void {
    let loans: number = getLoanCount(field.player)
    let mmassVariation = 0

    // Check refunded amount
    let refunded = Number(field.content)
    if (refunded > loans) {
      refunded = loans
      field.content = refunded
    }
    loans -= refunded
    mmassVariation -= 4 * (refunded - Number(previous))

    // Check extended amount
    let extended: number
    toolbox.forField(
      field.player,
      DebtField.ExtendedLoans
    )(extendedField => {
      const extendedInit = Number(extendedField.content)
      extended = extendedInit
      if (extended > loans) {
        extended = loans
        extendedField.content = extended
      }
      loans -= extended
      mmassVariation -= extended - extendedInit
    })

    // Terminate adjustments
    refundedOrExtendedLoans(field.player, mmassVariation, loans === 0, refunded, extended!)
  }

  /**
   * Manage an update of extended loan field.
   *
   * @param field - The extended fields.
   * @param previous - The previous content.
   */
  function extendedLoans(field: import('@gecogvidanto/shared').DynField, previous: unknown): void {
    let loans: number = getLoanCount(field.player)
    let mmassVariation = 0

    // Check extended amount
    let extended = Number(field.content)
    if (extended > loans) {
      extended = loans
      field.content = extended
    }
    loans -= extended
    mmassVariation -= extended - Number(previous)

    // Check refunded amount
    let refunded: number
    toolbox.forField(
      field.player,
      DebtField.RefundedLoans
    )(refundedField => {
      const refundedInit = Number(refundedField.content)
      refunded = refundedInit
      if (refunded > loans) {
        refunded = loans
        refundedField.content = refunded
      }
      loans -= refunded
      mmassVariation -= 4 * (refunded - refundedInit)
    })

    // Terminate adjustments
    refundedOrExtendedLoans(field.player, mmassVariation, loans === 0, refunded!, extended)
  }

  /**
   * Manage an update of seizure field.
   *
   * @param field - The seizure fields.
   * @param previous - The previous content.
   */
  function seized(field: import('@gecogvidanto/shared').DynField, previous: unknown): void {
    const seizedValue = Number(field.content)

    // Adjust moneraty mass
    updateMonetaryMass(Number(previous) - seizedValue)

    // Adjust seizure help message
    toolbox.forField(
      field.player,
      DebtField.RefundedLoans
    )(refundedField => {
      toolbox.forField(
        field.player,
        DebtField.ExtendedLoans
      )(extendedField => {
        toolbox.forField(
          field.player,
          DebtField.ValuesToSeize
        )(seizureHelp =>
          updateSeizureHelp(
            seizureHelp,
            Number(refundedField.content),
            Number(extendedField.content),
            seizedValue
          )
        )
      })
    })
  }

  /**
   * Update all dependent fields after a field modification.
   *
   * @param field - The updated field.
   * @param previous - The previous value of the field.
   */
  function updateFunction(field: import('@gecogvidanto/shared').DynField, previous: unknown): void {
    switch (field.name) {
      case DebtField.ValuesSold:
        sold(field, previous)
        break
      case DebtField.GrantedLoans:
        grantedLoans(field, previous)
        break
      case DebtField.RefundedLoans:
        refundedLoans(field, previous)
        break
      case DebtField.ExtendedLoans:
        extendedLoans(field, previous)
        break
      case DebtField.SeizedMoney:
        seized(field, previous)
        break
      case ServerField.LowMoney:
        updateMonetaryMass(Number(previous) - Number(field.content))
        break
      case ServerField.MediumMoney:
        updateMonetaryMass(2 * (Number(previous) - Number(field.content)))
        break
      case ServerField.HighMoney:
        updateMonetaryMass(4 * (Number(previous) - Number(field.content)))
        break
    }
  }

  return (field?: import('@gecogvidanto/shared').DynField, previous?: unknown): void => {
    if (field && previous !== undefined) {
      updateFunction(field, previous)
    } else {
      initFunction()
    }
  }
}
