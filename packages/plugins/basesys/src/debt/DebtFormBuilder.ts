/*
 * This file is part of @gecogvidanto/plugin-basesys.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { FormBuilder } from '@gecogvidanto/plugin'
import { Aspect, HelpSheet, ServerField, langType as sharedLang } from '@gecogvidanto/shared'

import { Bank } from '../database'
import { langType } from '../ui.locale.en'
import DebtField from './DebtField'
import { dynamicManagement } from './ui.DebtFormFunction'

interface Player {
  id: number
  name: string
}
interface PlayerLoans extends Player {
  loans: number
}

type Loans = ReadonlyArray<Bank['loans'][number]>

export interface UpdateParams {
  players: ReadonlyArray<Readonly<Player>>
  loans: Loans
}

export default class DebtFormBuilder {
  /**
   * Create the builder.
   *
   * @param builder - The form builder.
   * @param players - The identifier and name of players still on game.
   * @param loans - The outstanding loans.
   * @param mMass - The full monetary mass.
   */
  public constructor(
    private readonly builder: FormBuilder<langType & sharedLang, UpdateParams>,
    private readonly players: ReadonlyArray<Readonly<Player>>,
    private readonly loans: Loans,
    mMass: number
  ) {
    const mMassPerPlayer = mMass / players.length
    const mMassAspect: Aspect =
      mMassPerPlayer < 1.5 || mMassPerPlayer > 2.5
        ? Aspect.Error
        : mMassPerPlayer < 1.8 || mMassPerPlayer > 2.2
        ? Aspect.Warning
        : Aspect.Info
    builder
      .toPart(-Infinity)
      .newGroup(true)
      .addNumberInputField(DebtField.MonetaryMass, 'basesys$', mMass, {
        options: { hidden: true },
        hasData: false,
      })
      .addTextField(
        DebtField.MonetaryMassPP,
        { template: 'basesys$mMassPP', parameters: [mMassPerPlayer] },
        {
          options: { aspect: mMassAspect },
        }
      )
    this.addUpdateFunction()
  }

  /**
   * Add the fields for item selling.
   */
  public addSellItem(): void {
    this.builder
      .toPart(-Infinity)
      .newGroup(true)
      .addTextField(DebtField.ValuesSoldLabel, 'basesys$soldLabel')
      .addNumberInputField(DebtField.ValuesSold, 'basesys$soldField', 0, { minValue: 0 })
  }

  /**
   * Add the fields to display outstanding loans for each player.
   */
  public addOutstandingLoans(): void {
    this.buildPlayerLoans().forEach(playerLoan => {
      const { id, name, loans: loanCount } = playerLoan
      this.builder.toPart(id).newGroup(loanCount > 0, collector =>
        collector.addTextField(DebtField.OutstandingLoans, {
          template: 'basesys$outstanding',
          parameters: [name, loanCount],
        })
      )
    })
  }

  /**
   * Add the fields to ask new loans.
   *
   * @param excluded - Identifier of non eligible players.
   * @param inGroup - Indicate if this should be added in a new group.
   */
  public addNewLoan(excluded: ReadonlyArray<number>, inGroup: boolean): void {
    this.buildPlayerLoans()
      .filter(playerLoan => !excluded.includes(playerLoan.id))
      .forEach(playerLoan => {
        this.builder.toPart(playerLoan.id)
        if (inGroup) {
          this.builder.newGroup(false)
        }
        this.builder
          .addNumberInputField(DebtField.GrantedLoans, 'basesys$grantedField', 0, {
            minValue: 0,
          })
          .addTextField(DebtField.GrantedHelp, 'basesys$grantedHelpStatic')
          .addTextField(DebtField.GrantedCheck, 'basesys$grantedCheckStatic')
      })
  }

  /**
   * Add the refund fields for each player.
   *
   * @param mustRefund - Players which must refund (dying or leaving).
   */
  public addRefund(mustRefund: ReadonlyArray<number>): void {
    this.buildPlayerLoans()
      .filter(playerLoan => playerLoan.loans !== 0)
      .forEach(playerLoan => {
        this.builder
          .toPart(playerLoan.id)
          .addLinkedFields(collector =>
            collector
              .addNumberInputField(DebtField.RefundedLoans, 'basesys$refund', 0, {
                minValue: 0,
                maxValue: playerLoan.loans,
              })
              .addNumberInputField(DebtField.ExtendedLoans, 'basesys$extend', 0, {
                options: { invisible: mustRefund.includes(playerLoan.id) },
                minValue: 0,
                maxValue: playerLoan.loans,
              })
          )
          .addTextField(DebtField.RefundedHelp, 'basesys$refundHelpStatic')
          .addTextField(DebtField.NonRefundable, 'basesys$nonRefundable')
          .addNumberInputField(
            DebtField.SeizedMoney,
            'basesys$seized',
            0,
            { minValue: 0, maxValue: 3 } // If more than 3, then a refund is possible
          )
          .addTextField(DebtField.ValuesToSeize, 'basesys$toSeizeStatic')
      })
  }

  /**
   * Add the bank result fields.
   *
   * @param helpSheet - The values help sheet.
   */
  public addBankResult(helpSheet: HelpSheet): void {
    this.builder
      .toPart(-1)
      .newGroup(true, collector => collector.addTextField(ServerField.Dying, 'basesys$bankResult'))
      .addDyingValues(helpSheet)
  }

  /**
   * Add the function and appropriate parameters.
   */
  private addUpdateFunction(): void {
    this.builder.dynFunction = dynamicManagement
    this.builder.params = {
      players: this.players,
      loans: this.loans,
    }
  }

  /**
   * Build an array with players and loans data.
   *
   * @returns The player loans.
   */
  private buildPlayerLoans(): ReadonlyArray<Readonly<PlayerLoans>> {
    return this.players.map(player => ({
      ...player,
      loans: (this.loans.find(loan => loan.playerId === player.id) || { loanCount: 0 }).loanCount,
    }))
  }
}
