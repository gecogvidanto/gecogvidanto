/*
 * This file is part of @gecogvidanto/plugin-basesys.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { EconomicSystem, FormBuilder, FormData, ServerGame } from '@gecogvidanto/plugin'
import {
  EntityCount,
  HelpSheet,
  LocalizedOption,
  PlaceContent,
  UnassembledMessages,
  Unlocked,
} from '@gecogvidanto/shared'

import { BankModel, Bank } from '../database'
import { langType } from '../ui.locale.en'
import DebtFormAnalyzer from './DebtFormAnalyzer'
import DebtFormBuilder from './DebtFormBuilder'

const DEBT_HELP_SHEET: HelpSheet = {
  low: PlaceContent.Red,
  medium: PlaceContent.Yellow,
  high: PlaceContent.Green,
  waiting: PlaceContent.Empty,
}

const DEBT_SELL_ITEM = 'sell'
const DEBT_REQUEST_LOAN = 'loan'

/**
 * A debt money economic system.
 */
export default class DebtSystem extends EconomicSystem<langType> {
  public constructor(private readonly appData: { model: BankModel }) {
    super('debt-money', 'basesys$debtMoney')
  }

  public getNonPlayerCharacterName(id: number): Promise<UnassembledMessages<langType>[keyof langType]> {
    if (id === -1) {
      return Promise.resolve(this.unassembledMessage('basesys$bank'))
    } else {
      return Promise.reject(new Error('This NPC does not exist'))
    }
  }

  public getMoneyHelpSheet(): Promise<HelpSheet> {
    return Promise.resolve(DEBT_HELP_SHEET)
  }

  public getOptions(): Promise<ReadonlyArray<LocalizedOption<langType, keyof langType>>> {
    return Promise.resolve([
      this.localizedOption({ id: DEBT_SELL_ITEM, description: 'basesys$sell' }),
      this.localizedOption({ id: DEBT_REQUEST_LOAN, description: 'basesys$requestLoan' }),
    ])
  }

  public async getForm(
    builder: FormBuilder<any, any>,
    game: Readonly<ServerGame>,
    optionId: string | undefined
  ): Promise<FormBuilder<any>> {
    const { terminatingPlayers } = game
    const bank: Bank = await this.appData.model.find(game._id!)
    const onGamePlayers = game.onGamePlayers.map(id => ({
      id,
      name: game.players[id].name,
    }))
    const debtBuilder = new DebtFormBuilder(builder, onGamePlayers, bank.loans, bank.monetaryMass)

    if (optionId === DEBT_SELL_ITEM) {
      debtBuilder.addSellItem()
    } else if (optionId === DEBT_REQUEST_LOAN) {
      debtBuilder.addOutstandingLoans()
      debtBuilder.addNewLoan([], false)
    } else if (optionId) {
      throw new Error(`Internal error: option ${optionId} is not managed by this system`)
    } else {
      if (game.currentSet.currentRound !== 0) {
        debtBuilder.addOutstandingLoans()
        debtBuilder.addRefund(terminatingPlayers)
      }
      if (game.currentSet.currentRound === game.roundsPerSet) {
        debtBuilder.addBankResult(game.buildValuesHelpSheet())
      } else {
        debtBuilder.addNewLoan(
          game.onGamePlayers.filter(
            player =>
              game.players[player].roundLeft !== 0 &&
              game.players[player].roundLeft === game.absoluteCurrentRound
          ),
          game.currentSet.currentRound === 0
        )
      }
    }
    return builder
  }

  public async execForm(data: FormData, game: ServerGame, optionId: string | undefined): Promise<FormData> {
    const bank: Bank = await this.appData.model.find(game._id!)
    const analyzer = new DebtFormAnalyzer(data, bank, !!optionId)
    await this.appData.model.save(analyzer.result)
    if (game.currentSet.currentRound !== 0) {
      game.getCharacterFor(-1).currentScore.money.low += analyzer.money
    } else if (analyzer.money !== 0) {
      throw new Error('Bank cannot earn money at initialization round')
    }
    if (game.currentSet.currentRound === game.roundsPerSet) {
      const valueScore: Unlocked<EntityCount> = game.getCharacterFor(-1).currentScore.values
      valueScore.low = analyzer.lowValues
      valueScore.medium = analyzer.mediumValues
      valueScore.high = analyzer.highValues
    } else if (analyzer.lowValues !== 0 || analyzer.mediumValues !== 0 || analyzer.highValues !== 0) {
      throw new Error('Bank result should only arrive at final round')
    }
    return data
  }

  public async terminateRound(game: ServerGame): Promise<void> {
    if (game.currentSet.currentRound === 0) {
      const id = game.addNonPlayerCharacter()
      if (id !== -1) {
        throw new Error('NCP should be created at slot -1')
      }
    } else if (game.currentSet.currentRound === game.roundsPerSet) {
      await this.appData.model.delete(game._id!)
    }
  }
}
