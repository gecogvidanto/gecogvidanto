/*
 * This file is part of @gecogvidanto/plugin-basesys.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
export const messages = {
  basesys$: '',
  basesys$bank: 'Bank',
  basesys$bankResult: 'Result of the bank',
  basesys$debtMoney: 'Debt money',
  basesys$extend: 'Extended loans',
  basesys$grantedCheck: (count: number) => `Player goods must at least already be worth ${count} units`,
  basesys$grantedCheckStatic: 'Player goods must at least already be worth as much units',
  basesys$grantedField: 'New loans',
  basesys$grantedHelp: (count: number) => `Give ${count} units to player`,
  basesys$grantedHelpStatic: 'Give 3 times the count of loans in units to player',
  basesys$libreMoney: 'Libre money',
  basesys$mMassPP: (mmass: number) => `Monetary mass: ${mmass} units per player`,
  basesys$nonRefundable: 'Non refundable loans (seizure)',
  basesys$outstanding: (name: string, loanCount: number) =>
    `${name} has ${loanCount} outstanding loan(s) — ${loanCount * 4} units to refund`,
  basesys$refund: 'Refunded loans',
  basesys$refundHelp: (count: number) => `Take ${count} units from player`,
  basesys$refundHelpStatic: 'Take from player 4 units per refunded loan and 1 per extended loan',
  basesys$requestLoan: 'Request loan',
  basesys$seized: 'Amount seized',
  basesys$sell: 'Sell seizures',
  basesys$soldField: 'Amount',
  basesys$soldLabel: 'Indicate the amount of money earned by the bank',
  basesys$toSeize: (count: number) => `Seize roughly the equivalent of ${count} units`,
  basesys$toSeizeStatic: 'Seize roughly the equivalent of 1.5 times the due amount',
}
/* eslint-enable @typescript-eslint/explicit-module-boundary-types */

export type langType = typeof messages & { $: string }
