/*
 * This file is part of @gecogvidanto/plugin-basesys.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { EconomicSystem, FormBuilder, FormData, ServerGame } from '@gecogvidanto/plugin'
import { HelpSheet, LocalizedOption, PlaceContent, UnassembledMessages } from '@gecogvidanto/shared'

import { langType } from '../ui.locale.en'

/**
 * A libre money economic system.
 */
export default class LibreSystem extends EconomicSystem<langType> {
  public constructor() {
    super('libre-money', 'basesys$libreMoney', 3)
  }

  public getNonPlayerCharacterName(): Promise<UnassembledMessages<langType>[keyof langType]> {
    return Promise.reject(new Error('There are no NPC for libre money'))
  }

  public getMoneyHelpSheet(game: Readonly<ServerGame>): Promise<HelpSheet> {
    const notes = [PlaceContent.Red, PlaceContent.Yellow, PlaceContent.Green, PlaceContent.Blue]
    let index = game.currentSet.currentRound - 1
    const low: PlaceContent = notes[index++ % notes.length]
    const medium: PlaceContent = notes[index++ % notes.length]
    const high: PlaceContent = notes[index++ % notes.length]
    const waiting: PlaceContent = notes[index++ % notes.length]
    return Promise.resolve({ low, medium, high, waiting })
  }

  public getOptions(): Promise<ReadonlyArray<LocalizedOption<langType, keyof langType>>> {
    return Promise.resolve([])
  }

  public getForm(builder: FormBuilder<any>): Promise<FormBuilder<any>> {
    return Promise.resolve(builder)
  }

  public execForm(data: FormData): Promise<FormData> {
    return Promise.resolve(data)
  }

  public terminateRound(game: ServerGame): Promise<void> {
    // Remove the bias due to initial money gift
    game.terminatingPlayers.forEach(player => (game.getCharacterFor(player).totalScore -= 7))
    return Promise.resolve()
  }
}
