/*
 * This file is part of @gecogvidanto/plugin-basesys.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import * as sinon from 'sinon'
import { SinonStub } from 'sinon'

import { FormBuilder, FormData } from '@gecogvidanto/plugin'
import { PlaceContent } from '@gecogvidanto/shared'

import { appData, assembleMessage, buildFakeGame } from '../common.spec'
import LibreSystem from './LibreSystem'

describe('LibreSystem', () => {
  const libreSystem = new LibreSystem()

  it('must have sensible static values', () => {
    expect(libreSystem.id, 'Bad libre money identifier').to.equal('libre-money')
    expect(libreSystem.valueCost, 'Bad value cost').to.equal(3)
  })

  it('must give localized name', () => {
    appData.lang.$changePreferences(['en'])
    expect(assembleMessage(libreSystem.name), 'Bad default (english) name').to.equal('Libre money')
    appData.lang.$changePreferences(['newspeak'])
    expect(assembleMessage(libreSystem.name), 'Bad newspeak name').to.equal('Toy money')
  })

  describe('#getNonPlayerCharacterName', () => {
    it('must return failing promise', async () => {
      await expect(assembleMessage(libreSystem.getNonPlayerCharacterName())).to.be.rejected
    })
  })

  describe('#getMoneyHelpSheet', () => {
    it('must return cyclic value', async () => {
      const fakeGame = buildFakeGame()
      fakeGame.currentSet.currentRound = 1
      await expect(libreSystem.getMoneyHelpSheet(fakeGame)).to.eventually.deep.equal({
        low: PlaceContent.Red,
        medium: PlaceContent.Yellow,
        high: PlaceContent.Green,
        waiting: PlaceContent.Blue,
      })
      fakeGame.currentSet.currentRound++
      await expect(libreSystem.getMoneyHelpSheet(fakeGame)).to.eventually.deep.equal({
        low: PlaceContent.Yellow,
        medium: PlaceContent.Green,
        high: PlaceContent.Blue,
        waiting: PlaceContent.Red,
      })
    })
  })

  describe('#getOptions', () => {
    it('must return empty options', async () => {
      await expect(libreSystem.getOptions()).to.eventually.be.empty
    })
  })

  describe('#getForm', () => {
    it('must return empty form', async () => {
      const formBuilder = await libreSystem.getForm(new FormBuilder())
      const form = formBuilder.build()
      expect(form.parts).to.be.empty
    })
  })

  describe('#execForm', () => {
    it('must be fulfilled', async () => {
      await expect(libreSystem.execForm(new FormData({}))).to.be.fulfilled
    })
  })

  describe('#terminateRound', () => {
    const fakeGame = buildFakeGame()
    let anneFrank: { generation?: number; totalScore: number }
    let annieBesant: { generation?: number; totalScore: number }
    let gameCharStub: SinonStub<[number], unknown>

    beforeEach('Prepare doubles', () => {
      fakeGame.currentSet.currentRound = 1
      fakeGame.absoluteCurrentRound = 12
      anneFrank = { totalScore: 12 }
      annieBesant = { generation: 6, totalScore: 12 }
      gameCharStub = sinon.stub(fakeGame, 'getCharacterFor')
      gameCharStub.withArgs(0).returns(anneFrank)
      gameCharStub.withArgs(2).returns(annieBesant)
      gameCharStub.returns({})
    })

    afterEach('Clean doubles', () => {
      gameCharStub.restore()
    })

    it('must do nothing if no impact', async () => {
      await expect(libreSystem.terminateRound(fakeGame)).to.be.fulfilled
      expect(anneFrank.totalScore).to.equal(12)
      expect(annieBesant.totalScore).to.equal(12)
    })

    it('must adjust bias if player terminating', async () => {
      fakeGame.terminatingPlayers = [2]
      await expect(libreSystem.terminateRound(fakeGame)).to.be.fulfilled
      expect(anneFrank.totalScore).to.equal(12)
      expect(annieBesant.totalScore).to.equal(5)
    })
  })
})
