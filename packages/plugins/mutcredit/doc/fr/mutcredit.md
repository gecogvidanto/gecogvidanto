# Règles du crédit mutuel

Ces règles doivent se combiner aux [règles du jeu génériques du Ğeconomicus](http://geconomicus.glibre.org/).

# Crédit mutuel

Le crédit mutuel est un système dans lequel les participants se font mutuellement crédit, jusqu'à une certaine limite. Ainsi, si un acteur souhaite acheter un bien à un partenaire, le premier retire la valeur du bien de son solde, et le second l'ajoute au sien, tout comme dans une transaction normale. La différence se trouve dans le fait que chacun démarre avec un solde nul, et que ce solde peut aller dans le négatif, jusqu'à une certaine limite. Cette même limite est en général également appliquée au solde positif.

# Jeu

Dans la manche en crédit mutuel, on autorisera chaque joueur à avoir un solde compris entre -2 et +2 unités. Afin de simplifier les échanges, plutôt que d'avoir des valeurs négatives, on « translate » ces valeurs à respectivement 0 et +4, ce qui revient au même au final.

Une manche en crédit mutuel n'utilise qu'un seul type de billets. Comme indiqué par l'application sur les feuilles d'aide, la valeur économique la plus basse vaut 1, il faut donc 1 billet pour en acheter une. Deux billets sont distribués à chaque joueur avant le début de la manche afin de décaler le solde de 0 en +2.

Les échanges entre joueurs sont obligatoirement monétisés (le troc est donc interdit) et doivent respecter les équivalences entre valeurs et monnaie. Les joueurs ne peuvent accepter une transaction si cela leur fait dépasser le solde maximum de 4 billets.

# Rupture technologique

En cas de rupture technologique, afin que les joueurs puissent continuer à échanger toutes les valeurs, il faut doubler la quantité de monnaie échangeable par personne. Ainsi, à la première rupture technologique, le maitre du jeu distribuera 2 nouveaux billets à chaque joueur, et le solde maximum des joueurs passera à 8 billets. À la rupture suivante, il en distribuera 4, pour un solde maximum de 16. Et ainsi de suite, en doublant ces valeurs à chaque fois.
