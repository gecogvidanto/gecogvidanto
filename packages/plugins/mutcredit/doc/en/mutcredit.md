# Mutual credit rules

These rules must be combined to the [global rules of the Ğeconomicus game](http://geconomicus.glibre.org/).

# Mutual credit

Mutual Credit is a system in which participants credit one another, up to a certain limit. Thus, if an actor wishes to buy good from one partner, the first take out the value of the good from his balance, and the second adds it to his own, just as in a normal transaction. The difference lies in the fact that everyone starts with a zero balance, and that balance can go into the negative, up to a certain limit. This same limit is generally also applied to the positive balance.

# Game

In the mutual credit set, each player will be allowed to have a balance between -2 and +2 units. In order to simplify the exchanges, rather than having negative values, we “translate” these values ​​to respectively 0 and +4, which makes the same in the end.

A mutual credit set uses only one type of banknote. As indicated by the application on the help sheets, the lowest economic value is 1, so you need 1 banknote to buy one. Two bamknotes are distributed to each player before the start of the round to shift the balance from 0 to +2.

The exchanges between players are necessarily monetized (bartering is forbidden) and must respect equivalence between values ​​and currency. Players can not accept a transaction if it makes his balance exceeds the maximum of 4 banknotes.

# Technological break

In case of technological break, so that players can continue to exchange all the values, it is necessary to double the amount of exchangeable banknotes per person. Thus, at the first technological break, the game master will distribute 2 new banknotes to each player, and the maximum balance of players will increase to 8 banknotes. At the next break, he will distribute 4, for a maximum balance of 16. And so on, doubling those values at each time.
