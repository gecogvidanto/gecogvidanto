/*
 * This file is part of @gecogvidanto/plugin-mutcredit.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'

import { GecoPlugin, ServerApp, isEconomicSystemPlugin, isGecoPluginType } from '@gecogvidanto/plugin'

import * as plugin from '.'
import MutCreditSystem from './MutCreditSystem'
import { lang } from './MutCreditSystem.spec'
import { langType } from './ui.locale.en'

export const app: ServerApp<langType> = {
  serverLang: lang,
  clientLang: lang,
  config: {},
  database: undefined as any,
}

describe('MutCreditPluginType', () => {
  it('must have the plugin signature', () => {
    expect(plugin).to.exist.and.to.include.all.keys('plugin')
    expect(isGecoPluginType(plugin)).to.be.true
  })
})

describe('MutCreditPlugin', () => {
  let pluginInstance: GecoPlugin

  beforeEach('Create the plugin', () => {
    if (isGecoPluginType(plugin)) {
      pluginInstance = new plugin(app)
      pluginInstance.ready() // Should not modify anything
    }
    expect(pluginInstance).to.exist
  })

  it('must be identified as economic system plugin', () => {
    expect(isEconomicSystemPlugin(pluginInstance)).to.be.true
  })

  describe('#openEconomicSystems', () => {
    it('must create the economic system', () => {
      if (isEconomicSystemPlugin(pluginInstance)) {
        const ecoSystems = pluginInstance.openEconomicSystems()
        expect(ecoSystems).to.have.lengthOf(1)
        expect(ecoSystems[0]).to.be.instanceOf(MutCreditSystem)
      }
    })
  })
})
