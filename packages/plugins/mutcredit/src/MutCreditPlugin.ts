/*
 * This file is part of @gecogvidanto/plugin-mutcredit.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { EconomicSystemPlugin } from '@gecogvidanto/plugin'

import MutCreditSystem from './MutCreditSystem'

/**
 * MutCredit plugin manage a mutual credit economic system for ĞecoĞvidanto.
 */
export default class MutCreditPlugin implements EconomicSystemPlugin {
  private readonly mutCreditSystem: MutCreditSystem

  public constructor() {
    this.mutCreditSystem = new MutCreditSystem()
  }

  public ready(): Promise<void> {
    return Promise.resolve()
  }

  public openEconomicSystems(): [MutCreditSystem] {
    return [this.mutCreditSystem]
  }
}
