/*
 * This file is part of @gecogvidanto/plugin-mutcredit.
 * Copyright (C) 2020  Stéphane Veyret
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-disable prefer-arrow-callback, no-unused-expressions */
import { expect } from 'chai'
import Intl, { LanguageMap } from 'intl-ts'

import { FormBuilder, FormData, ServerGame } from '@gecogvidanto/plugin'
import { PlaceContent } from '@gecogvidanto/shared'

import MutCreditSystem from './MutCreditSystem'
import { langType, messages } from './ui.locale.en'

const louchebem: langType = {
  $: 'Louchébèm',
  mutcredit$name: 'lrédiquès lutuelèm',
}
export const lang = new Intl(new LanguageMap({ $: 'English', ...messages }).merge({ louchebem }))

describe('MutCreditSystem', () => {
  const mutCreditSystem = new MutCreditSystem()

  it('must have sensible static values', () => {
    expect(mutCreditSystem.id, 'Bad mutual credit identifier').to.equal('mutual-credit')
    expect(mutCreditSystem.valueCost, 'Bad value cost').to.equal(1)
  })

  it('must give localized name', () => {
    lang.$changePreferences(['en'])
    expect(lang[mutCreditSystem.name](), 'Bad default (english) name').to.equal('Mutual credit')
    lang.$changePreferences(['louchebem'])
    expect(lang[mutCreditSystem.name](), 'Bad louchebèm name').to.equal('lrédiquès lutuelèm')
  })

  describe('#getNonPlayerCharacterName', () => {
    it('must return failing promise', async () => {
      await expect(mutCreditSystem.getNonPlayerCharacterName()).to.be.rejected
    })
  })

  describe('#getMoneyHelpSheet', () => {
    it('must provide simple help sheet', async () => {
      await expect(mutCreditSystem.getMoneyHelpSheet()).to.eventually.deep.equal({
        low: PlaceContent.Red,
        medium: PlaceContent.Empty,
        high: PlaceContent.Empty,
        waiting: PlaceContent.Empty,
      })
    })
  })

  describe('#getOptions', () => {
    it('must return empty options', async () => {
      await expect(mutCreditSystem.getOptions()).to.eventually.be.empty
    })
  })

  describe('#getForm', () => {
    it('must return empty form', async () => {
      const formBuilder = await mutCreditSystem.getForm(new FormBuilder())
      const form = formBuilder.build()
      expect(form.parts).to.be.empty
    })
  })

  describe('#execForm', () => {
    it('must return fulfilled promise', async () => {
      await expect(mutCreditSystem.execForm(new FormData({}))).to.be.fulfilled
    })
  })

  describe('#terminateRound', () => {
    const player = {
      generation: 0,
      totalScore: 0,
      roundLeft: 0,
    }
    const currentSet = {
      techBreakRounds: [] as number[],
      currentRound: 0,
    }
    const game: ServerGame = {
      currentSet,
      roundsPerSet: 10,
      absoluteCurrentRound: 12,
      players: [undefined, player],
      onGamePlayers: [1],
      getCharacterFor: () => player,
    } as any

    beforeEach('Prepare fake game', () => {
      player.generation = 1
      player.totalScore = 10
      player.roundLeft = 0
      currentSet.techBreakRounds = []
      currentSet.currentRound = 2
    })

    it('must do nothing for usual cases', async () => {
      await expect(mutCreditSystem.terminateRound(game)).to.be.fulfilled
      expect(player.totalScore).to.equal(10)
    })

    it('must remove bias for last round', async () => {
      currentSet.currentRound = 10
      await expect(mutCreditSystem.terminateRound(game)).to.be.fulfilled
      expect(player.totalScore).to.equal(8)
    })

    it('must remove bias for dying player', async () => {
      player.generation = 2
      currentSet.techBreakRounds = [1]
      await expect(mutCreditSystem.terminateRound(game)).to.be.fulfilled
      expect(player.totalScore).to.equal(6)
    })

    it('must remove bias for leaving player', async () => {
      player.roundLeft = 12
      currentSet.techBreakRounds = [1, 2]
      await expect(mutCreditSystem.terminateRound(game)).to.be.fulfilled
      expect(player.totalScore).to.equal(2)
    })
  })
})
