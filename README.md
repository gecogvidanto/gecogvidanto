[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lernajs.io/)
[![pipeline status](https://git.duniter.org/gecogvidanto/gecogvidanto/badges/master/pipeline.svg)](https://git.duniter.org/gecogvidanto/gecogvidanto/commits/master)
[![coverage report](https://git.duniter.org/gecogvidanto/gecogvidanto/badges/master/coverage.svg)](https://git.duniter.org/gecogvidanto/gecogvidanto/commits/master)

# ĞecoĞvidanto

_ĞecoĞvidanto_ is a tool which purpose is to help game masters of the Ğeconomicus game. The tool is composed of:

- a [server](packages/core/server), which both gives API for running clients and serves as a game registry;
- [clients](packages/clients) connecting to the server to manage games, and especially give step by step help to the game master, allowing uninteresting parts of the game to last shorter;
- [plugins](packages/plugins) to add features to the application;
- [locales](packages/locales) (which are actually also plugins) to add languages to the application.

Refer to documentation of each specific package to have more information on its purpose and usage.

# Language/langue

Documents, messages, code (including variable names and comments), are in English.

Anyway, because French is my native language, all documents and important messages must also be provided in French. Other translations are welcome.

:fr: Une version française de ce document se trouve [ici](doc/fr/README.md).

# Installation

Installation is done using `npm install` command:

```bash
$ npm install --save gecogvidanto
```

# License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

# Contributing

Even though I cannot guarantee a response time, please feel free to file an issue if you have any question or problem using the package.

_Pull Requests_ are welcome. You can, of course, submit corrections or improvements for code, but do not hesitate to also improve documentation, even for small spell or grammar errors.
